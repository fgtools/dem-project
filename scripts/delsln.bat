@setlocal
@REM Version 20110302 - Add MSVC10 stuff...
@REM Version 20101001
@echo Delete solution files *.sln and *.vcproj plus more...
@set TEMP1=
@if EXIST *.sln set TEMP1=%TEMP1% *.sln
@if EXIST *.vcproj set TEMP1=%TEMP1% *.vcproj
@if EXIST *.ncb set TEMP1=%TEMP1% *.ncb
@if EXIST *.suo set TEMP1=%TEMP1% *.suo
@if EXIST *.user set TEMP1=%TEMP1% *.user
@if EXIST *.res set TEMP1=%TEMP1% *.res
@if EXIST *.ilk set TEMP1=%TEMP1% *.ilk
@if EXIST *.manifest set TEMP1=%TEMP1% *.manifest
@if EXIST *.pdb set TEMP1=%TEMP1% *.pdb
@if EXIST *.idb set TEMP1=%TEMP1% *.idb
@if EXIST *.vcxproj set TEMP1=%TEMP1% *.vcxproj
@if EXIST *.vcxproj.user set TEMP1=%TEMP1% *.vcxproj.user
@if EXIST *.vcxproj.filters set TEMP1=%TEMP1% *.vcxproj.filters
@if EXIST *.sdf set TEMP1=%TEMP1% *.sdf
@if "%TEMP1%x" == "x" goto NOTHING

@echo Will delete [%TEMP1%] files ...
@if "%1x" == "x" goto DOASK
@if /I "%1x" == "NOPAUSEx" goto DNCHECK
@echo.
@echo ERROR: Only input allowed is NOPAUSE! Not [%1]?
@echo.
@goto END

:DOASK
@echo Continue? Ctrl+C to abort ...
@pause
@echo.
@echo Will delete [%TEMP1%] files ...
@echo ARE YOU SURE? Ctrl+C to abort ...
@echo.
@pause

@echo.
@echo As stated, will delete [%TEMP1%] files ...
@echo ARE YOU REALLY SURE? *** LAST, LAST CHANCE!!!
@if NOT EXIST C:\MDOS\ask.exe goto DOPAUSE
@ask "Only 'Y' to continue ..."
@if ERRORLEVEL 9009 goto NOASK
@if ERRORLEVEL 1 goto DNCHECK
@echo Not 'Y' so aborting ...
@goto END

:NOASK
@echo ERROR: Can NOT locate 'ask.exe' application!
@echo Aborting...
@goto END


:DOPAUSE
@echo WARNING: Can NOT locate 'ask.exe' application!
@echo Download [http://geoffair.org/ms/ask.htm#downloads], and put EXE in your PATH
@echo Using pause *** Only Ctrl+C to abort ... all other keys continue...
@pause

:DNCHECK
@if EXIST *.sln del *.sln
@if EXIST *.vcproj del *.vcproj
@if EXIST *.ncb del *.ncb
@if EXIST *.suo (
attrib -s -h *.suo
del *.suo
)
@if EXIST *.suo (
@echo.
@echo ERROR: Unable to DELETE *.suo file!
@echo Appears MSVC is still running! Exit MSVC and try again...
@echo.
@pause
)

@if EXIST *.user del *.user
@if EXIST *.res del *.res
@if EXIST *.ilk del *.ilk
@if EXIST *.manifest del *.manifest
@if EXIST *.pdb del *.pdb
@if EXIST *.idb del *.idb
@if EXIST *.vcxproj del *.vcxproj
@if EXIST *.vcxproj.user del *.vcxproj.user
@if EXIST *.vcxproj.filters del *.vcxproj.filters
@if EXIST *.sdf del *.sdf

@REM echo.
@REM echo.
@echo ALL Done! [%TEMP1%] delete ...
@REM echo.
@REM echo.
@goto END

:NOTHING
@echo Appears ***NO*** solution files to DELETE! [%TEMP1%]
@goto END

:END
@endlocal
