@setlocal

@call clobnow
@call delsln NOPAUSE
@call deldsw NOPAUSE

@if EXIST *.suo (
@echo ERROR: Unable to delete *.suo! IS MSVC running? Close it first!
@goto ISERR
)

@REM Add extra directories
@set TMPFIL=cmake-clean.txt
@set TMPEDIRS=
@set TMPEFILS=

@if NOT EXIST %TMPFIL% goto DNEXTRA
@for /F %%i in (%TMPFIL%) do @(call :ADDIT %%i)
@goto DNEXTRA

:ADDIT
@if "%1x" == "x" goto :EOF
@if EXIST %1\nul (
@set TMPEDIRS=%TMPEDIRS% %1
) else (
@if EXIST %1 (
@set TMPEFILS=%TMPEFILS% %1
)
)
@goto :EOF

:DNEXTRA

@xdelete -? >nul
@if ERRORLEVEL 1 goto NOXDEL


@REM leave this file 'install_manifest.txt'
@set TMPDIRS=%TMPEDIRS% Release Debug RelWithDebInfo MinSizeRel Win32 x64 ipch CMakeFiles install bin lib src fgx.dir ALL_BUILD.dir
@set TMPFILE=%TMPEFILS% CMakeCache.txt Makefile cmake_install.cmake moc_*
@set TMPFILE=%TMPFILE% qrc_artwork.cxx qrc_default.cxx qrc_fonts.cxx qrc_images.cxx qrc_openlayers.cxx qrc_ycons.cxx

@set TMPDCNT=0
@set TMPFCNT=0

@for %%i in (%TMPDIRS%) do @(call :DELDIR %%i)
@for %%i in (%TMPFILE%) do @(call :DELFIL %%i)
@REM Remove ALL *.dir directories also
@for /D %%i in (*.dir)  do @(call :DELDIR %%i)

@goto DONE

:DELDIR
@REM echo Doing %1...
@if "%1x" == "x" goto :EOF
@set TMPDIR=%1
@if EXIST %TMPDIR%\nul (
@echo Deleting the %TMPDIR% directory...
xdelete -NO-confirmation-please=0 -dfrm %TMPDIR% >nul 2>&1
@set /A TMPDCNT+=1
)
@goto :EOF

:DELFIL
@REM echo Doing %1...
@if "%1x" == "x" goto :EOF
@set TMPFIL=%1
@if EXIST %TMPFIL% (
@echo Deleting file %TMPFIL%...
@del %TMPFIL% >nul
@set /A TMPFCNT+=1
)
@goto :EOF

:NOXDEL
@echo.
@echo ERROR: This batch file uses an xdelete utility!
@echo Such a utility was NOT found in your syste!
@echo A copy can be obtained from [http://geoffair.org/ms/xdelete.htm]
@echo It also uses some BATCH files - delsln.bat and deldsw.bat
@echo that remove ALL the MSVC files completely.
@echo Without all these tools in place, this cleanup.bat file is of little use...
@echo.
@goto END

:DONE

@echo Deleted %TMPFCNT% files, and %TMPDCNT% directories... Results...
@call dirmin *.*

@if EXIST *.sdf (
@echo.
@call dirmin *.sdf
@echo NOTE: MSVC is running and holding onto the '*.sdf' file...
@echo.
)


:END
