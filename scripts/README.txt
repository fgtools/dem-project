README.txt - 20140519

Just some hopefully useful 'scripts' I use all the time...

They are setup, and intended to be run from the 'build' directory.

=================================================================
bm - unix sh script
bm.bat - windows batch file

Always like 'less' typing, so I always use, so I copy these into a 
directory, that is in my PATH evn variable. Then to build the project

$ bm - unix terminal
> bm - windows command

Just two keys... bm...

=================================================================
cmake-clean - unix sh script
cmake-clean.bat - windows batch file

It is useful to occasionally fully clean up the 'build' directory.
When in the 'build' directory just run -
../scripts/cmake-clean - unix terminal
..\scripts\cmake-clean - windows prompt

=================================================================
slippydirs - unix sh script
slippydirs.bat - windows batch file

This started as a exploration into slippy map paths. I wanted to 
be able to enter either -
../scripts/slippydirs 37.618674211 -122.375007609 10 - unix terminal
..\scripts\slippydirs 37.618674211 -122.375007609 10 - windows prompt

And find out airport KSFO is in the slippy map file /10/163/396.png

And conversely running the reverse

../scripts/slippydirs /10/163/396.png - unix terminal
..\scripts\slippydirs /10/163/396.png - windows prompt

should yield the same/similar information...

=================================================================
20141012:

Added dem1files.zip. This zip contains the batch files, and some
text files concerning the download and unzipping of USGS and 
Ferranti DEM1 files.

These were used to construct the F:\data\dem1 folder, in case 
that drive fails again. 

=================================================================

# eof
