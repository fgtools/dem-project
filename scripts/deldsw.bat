@setlocal
@echo Delete *.dsw and *.dsp files...
@set TEMP1=
@if EXIST *.dsw set TEMP1=%TEMP1% *.dsw
@if EXIST *.dsp set TEMP1=%TEMP1% *.dsp
@if "%TEMP1%x" == "x" goto NOTHING

@echo Will delete [%TEMP1%] files ...
@if "%1x" == "x" goto DOASK
@if /I "%1x" == "NOPAUSEx" goto DNCHECK
@echo.
@echo ERROR: Only input allowed is NOPAUSE! Not [%1]?
@echo.
@goto END

:DOASK
@echo Continue? Only Ctrl+C to abort ... All other keys continue...
@pause

:DNCHECK
@if EXIST *.dsw @echo del *dsw
@if EXIST *.dsw @del *dsw
@if EXIST *.dsp @echo del *.dsp
@if EXIST *.dsp @del *.dsp
@goto END

:NOTHING
@echo Appears ***NO*** dsw/dsp files to delete...
:END
