@setlocal
@set VCVERS=14
@set TMPPRJ=dem_server
@set TMPSRC=..
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOCM
@set TMPLOG=bldlog-1.txt

@if NOT EXIST Z:\nul goto NOZDIR
@if NOT EXIST X:\nul goto NOXDIR

@set TG_INSTALL=X:\install\msvc140-64\terragear-fork
@set TG_ARRAY=%TG_INSTALL%\include\tg\array.hxx
@set TG_LIB=%TG_INSTALL%\lib\array.lib
@if NOT EXIST %TG_ARRAY% goto NOTGI
@if NOT EXIST %TG_LIB% goto NOTGL
@REM ###########################################
@REM NOTE: Specific install location
@REM ###########################################
@set TMPINST=Z:\software.x64
@set TMPOPTS=
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TG_INSTALL%;X:\install\msvc140-64\SimGear
@REM set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=F:/FG/18/install/msvc100/simgear;Z:/software;F:/FG/18/3rdParty
@REM set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=X:/install/msvc100/simgear;X:/3rdParty.x64

@REM ###########################################
@REM ############################################
@REM NOTE: MSVC 10 INSTALL LOCATION
@REM Adjust to suit your environment
@REM ##########################################
@set GENERATOR=Visual Studio %VCVERS% Win64
@set TMPOPTS=%TMPOPTS% -G "%GENERATOR%"
@set VS_PATH=C:\Program Files (x86)\Microsoft Visual Studio %VCVERS%.0
@set VC_BAT=%VS_PATH%\VC\vcvarsall.bat
@if NOT EXIST "%VS_PATH%" goto NOVS
@if NOT EXIST "%VC_BAT%" goto NOBAT
@set BUILD_BITS=%PROCESSOR_ARCHITECTURE%
@IF /i %BUILD_BITS% EQU x86_amd64 (
    @REM set "RDPARTY_ARCH=x64"
) ELSE (
    @IF /i %BUILD_BITS% EQU amd64 (
        @REM set "RDPARTY_ARCH=x64"
    ) ELSE (
        @echo Appears system is NOT 'x86_amd64', nor 'amd64'
        @echo Can NOT build the 64-bit version! Aborting
        @exit /b 1
    )
)
@set CURRDIR=%CD%
@echo Setting environment - CALL "%VC_BAT%" %BUILD_BITS%
@call "%VC_BAT%" %BUILD_BITS%
@if ERRORLEVEL 1 goto NOSETUP
@cd %CURRDIR%

@call chkmsvc %TMPPRJ%

:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Begin project %TMPPRJ% %DATE% %TIME% > %TMPLOG%

@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' output to %TMPLOG%
@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' >>%TMPLOG%
cmake %TMPSRC% %TMPOPTS% >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo Doing: 'cmake --build . --config Debug' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Debug' >>%TMPLOG%
cmake --build . --config Debug >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing: 'cmake --build . --config Release' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Release' >>%TMPLOG%
cmake --build . --config Release >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo Appears a successful build...

@echo No install at this time...
@goto END

@echo Continue with install? Only Ctrl+C aborts...
@pause
@echo Doing: 'cmake --build . --config Debug --target INSTALL' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Debug --target INSTALL' >>%TMPLOG%
cmake --build . --config Debug --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Doing: 'cmake --build . --config Release --target INSTALL' output to %TMPLOG%
@echo Doing: 'cmake --build . --config Release --target INSTALL' >>%TMPLOG%
cmake --build . --config Release --target INSTALL >>%TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR5

@echo Appears a successful build and install... see %TMPLOG% for details

@goto END

:NOZDIR
@echo Error: Z: drive NOT setup!
@goto ISERR

:NOXDIR
@echo Error: X: drive NOT setup!
@goto ISERR

:NOSRC
@echo Can NOT locate source %TMPSRC%! *** FIX ME ***
@goto ISERR

:NOCM
@echo Can NOT locate source %TMPSRC%\CMakeLists.txt! *** FIX ME ***
@goto ISERR

:ERR4
:ERR5
@echo See %TMPLOG% for error
@goto ISERR

:Err1
@echo cmake config, gen error
@goto ISERR

:Err2
@echo debug build error
@goto ISERR

:Err3
@echo release build error
@goto ISERR

:NOVS
@echo Error: Can NOT locate Visual Studio path "%VS_PATH%"! *** FIX ME ***
@goto ISERR

:NOSETUP
@echo Error: In setting up the Visual Studio 64-bit environment! *** FIX ME ***
@goto ISERR

:NOBAT
@echo Error: Can NOT locate the batch "%VC_BAT%"! *** FIX ME ***
@goto ISERR

:NOTGI
@echo NOT EXIST %TG_ARRAY% - *** FIX ME ***
@goto ISERR

:NOTGL
@echo NOT EXIST %TG_LIB% - *** FIX ME ***
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
