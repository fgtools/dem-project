@setlocal
@set TMPEXE=Release\dem_tests.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@if "%~1x" == "x" goto HELP

%TMPEXE% %*

@goto END
:HELP
%TMPEXE% -s
%TMPEXE% -?
@goto END

:NOEXE
@echo Can NOT locate %TMPEXE%! *** FIX ME ***
@goto END

:END
