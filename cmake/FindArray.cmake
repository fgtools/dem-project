# FindArray.cmake - 20180601
# Find TG Array library
# This module defines
#  ARRAY_INCLUDE_DIR, where to find array.h, etc.
#  ARRAY_LIBRARIES, libraries to link against to use Array.
#  ARRAY_FOUND, If false, do not try to use TIFF.
# also defined, but not for general use are
#  ARRAY_LIBRARY, where to find the Array library.

find_path(ARRAY_INCLUDE_DIR tg/array.hxx)

set(ARRAY_NAMES ${ARRAY_NAMES} array)
if (MSVC)
    find_library(ARRAY_LIB_DBG NAMES arrayd )
    find_library(ARRAY_LIB_REL NAMES array )
    if (ARRAY_LIB_DBG AND ARRAY_LIB_REL)
        set(ARRAY_LIBRARY
            debug ${ARRAY_LIB_DBG}
            optimized ${ARRAY_LIB_REL}
            )
    elseif (ARRAY_LIB_REL)
        set(ARRAY_LIBRARY ${ARRAY_LIB_REL})
    endif ()
else ()
    find_library(ARRAY_LIBRARY NAMES ${TIFF2_NAMES} )
endif ()

# handle the QUIETLY and REQUIRED arguments and set TIFF_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ARRAY
                                  REQUIRED_VARS ARRAY_LIBRARY ARRAY_INCLUDE_DIR)

if(ARRAY_FOUND)
  set( ARRAY_LIBRARIES ${ARRAY_LIBRARY} )
endif()

mark_as_advanced(ARRAY_INCLUDE_DIR ARRAY_LIBRARY)

# eof
