# - Find the native OpenCV includes and library
#
# It defines the following variables
#  CV2_INCLUDE_DIRS, where to find imgproc.hpp, etc.
#  CV2_LIBRARIES, the libraries to link against to use CV2.
#  CV2_FOUND, If false, do not try to use OpenCV2.
#
#find_path(CV2_INCLUDE_DIR version.hpp
#    PATH_SUFFIXES opencv2/core opencv2
#    )

find_path(CV2_INCLUDE_DIR opencv2/imgproc/imgproc.hpp)
##    PATH_SUFFIXES opencv2/imgproc opencv2
##    )

set(CV2_NAMES ${CV2_NAMES}  opencv_core300 opencv_highgui300 opencv_imgproc300)
if (MSVC)
###################################################################################
### TODO: Detect the PATH just like OpenCV cmake
## cmake\OpenCVDetectTBB.cmake elseif(MSVC10) set(_TBB_LIB_PATH "${_TBB_LIB_PATH}/vc10")
#        if(MSVC80)
#          set(_TBB_LIB_PATH "${_TBB_LIB_PATH}/vc8")
#        elseif(MSVC90)
#          set(_TBB_LIB_PATH "${_TBB_LIB_PATH}/vc9")
#        elseif(MSVC10)
#          set(_TBB_LIB_PATH "${_TBB_LIB_PATH}/vc10")
#        elseif(MSVC11)
#          set(_TBB_LIB_PATH "${_TBB_LIB_PATH}/vc11")
#        endif()
## cmake\OpenCVconfig.cmake
#if(MSVC)
  if(CMAKE_CL_64)
    set(OpenCV_ARCH x64)
    set(OpenCV_TBB_ARCH intel64)
  else()
    set(OpenCV_ARCH x86)
    set(OpenCV_TBB_ARCH ia32)
  endif()
  if(MSVC_VERSION EQUAL 1400)
    set(OpenCV_RUNTIME vc8)
  elseif(MSVC_VERSION EQUAL 1500)
    set(OpenCV_RUNTIME vc9)
  elseif(MSVC_VERSION EQUAL 1600)
    set(OpenCV_RUNTIME vc10)
  elseif(MSVC_VERSION EQUAL 1700)
    set(OpenCV_RUNTIME vc11)
  elseif(MSVC_VERSION EQUAL 1800)
    set(OpenCV_RUNTIME vc12)
  endif()
#elseif(MINGW)
#  set(OpenCV_RUNTIME mingw)
#  execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpmachine
#                  OUTPUT_VARIABLE OPENCV_GCC_TARGET_MACHINE
#                  OUTPUT_STRIP_TRAILING_WHITESPACE)
#  if(CMAKE_OPENCV_GCC_TARGET_MACHINE MATCHES "64")
#    set(MINGW64 1)
#    set(OpenCV_ARCH x64)
#  else()
#    set(OpenCV_ARCH x86)
#  endif()
#endif()
set(OpenCV_SUFFIX ${OpenCV_ARCH}/${OpenCV_RUNTIME})
message(STATUS "*** PATH_SUFFIX ${OpenCV_SUFFIX}")

# find DEBUG
find_library(CV2_CORE_LIB_DBG opencv_core300d
    PATH_SUFFIXES x86/vc10/lib )
find_library(CV2_HIGH_LIB_DBG opencv_highgui300d
    PATH_SUFFIXES x86/vc10/lib )
find_library(CV2_IMG_LIB_DBG opencv_imgproc300d
    PATH_SUFFIXES x86/vc10/lib )
    
# find RELEASE
find_library(CV2_CORE_LIB_REL opencv_core300
    PATH_SUFFIXES x86/vc10/lib )
find_library(CV2_HIGH_LIB_REL opencv_highgui300
    PATH_SUFFIXES x86/vc10/lib )
find_library(CV2_IMG_LIB_REL opencv_imgproc300
    PATH_SUFFIXES x86/vc10/lib )
    
if (CV2_CORE_LIB_DBG AND CV2_CORE_LIB_REL)
    set(CV2_CORE_LIBRARY optimized ${CV2_CORE_LIB_REL} debug ${CV2_CORE_LIB_DBG})
else ()
    set(CV2_CORE_LIBRARY ${CV2_CORE_LIB_REL})
endif ()
if (CV2_HIGH_LIB_DBG AND CV2_HIGH_LIB_REL)
    set(CV2_HIGH_LIBRARY optimized ${CV2_HIGH_LIB_REL} debug ${CV2_HIGH_LIB_DBG})
else ()
    set(CV2_HIGH_LIBRARY ${CV2_HIGH_LIB_REL})
endif ()
if (CV2_IMG_LIB_DBG AND CV2_IMG_LIB_REL)
    set(CV2_IMG_LIBRARY optimized ${CV2_IMG_LIB_REL} debug ${CV2_IMG_LIB_DBG})
else ()
    set(CV2_IMG_LIBRARY ${CV2_IMG_LIB_REL})
endif ()
###################################################################################
else ()
###################################################################################
find_library(CV2_CORE_LIBRARY opencv_core300
    PATH_SUFFIXES x86/vc10/lib )
find_library(CV2_HIGH_LIBRARY opencv_highgui300
    PATH_SUFFIXES x86/vc10/lib )
# opencv_imgproc300
find_library(CV2_IMG_LIBRARY opencv_imgproc300
    PATH_SUFFIXES x86/vc10/lib )
###################################################################################
endif ()

if (CV2_CORE_LIBRARY AND CV2_HIGH_LIBRARY AND CV2_IMG_LIBRARY)
    set(CV2_LIBRARIES ${CV2_CORE_LIBRARY} ${CV2_HIGH_LIBRARY} ${CV2_IMG_LIBRARY})
endif ()

if (CV2_LIBRARIES AND CV2_INCLUDE_DIR)
    message(STATUS "*** Found CV2 lib ${CV2_LIBRARIES} inc ${CV2_INCLUDE_DIR}")
endif()

# handle the QUIETLY and REQUIRED arguments and set CV2_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args( CV2 DEFAULT_MSG CV2_LIBRARIES CV2_INCLUDE_DIR )
### mark_as_advanced( CV2_INCLUDE_DIR CV2_LIBRARY )

# eof
