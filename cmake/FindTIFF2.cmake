# FindTIFF2.cmake - localized MSVC version - 20140417
# Find TIFF library
# Find the native TIFF includes and library
# This module defines
#  TIFF2_INCLUDE_DIR, where to find tiff.h, etc.
#  TIFF2_LIBRARIES, libraries to link against to use TIFF.
#  TIFF2_FOUND, If false, do not try to use TIFF.
# also defined, but not for general use are
#  TIFF_LIBRARY, where to find the TIFF library.

#=============================================================================
# Copyright 2002-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

find_path(TIFF2_INCLUDE_DIR tiff.h)

set(TIFF2_NAMES ${TIFF_NAMES} tiff libtiff tiff3 libtiff3)
if (MSVC)
    find_library(TIFF2_LIB_DBG NAMES libtiffd tiffd )
    find_library(TIFF2_LIB_REL NAMES libtiff tiff )
    if (TIFF2_LIB_DBG AND TIFF2_LIB_REL)
        set(TIFF2_LIBRARY
            debug ${TIFF2_LIB_DBG}
            optimized ${TIFF2_LIB_REL}
            )
    elseif (TIFF2_LIB_REL)
        set(TIFF2_LIBRARY ${TIFF2_LIB_REL})
    endif ()
else ()
    find_library(TIFF2_LIBRARY NAMES ${TIFF2_NAMES} )
endif ()

if(TIFF2_INCLUDE_DIR AND EXISTS "${TIFF2_INCLUDE_DIR}/tiffvers.h")
    file(STRINGS "${TIFF2_INCLUDE_DIR}/tiffvers.h" tiff_version_str
         REGEX "^#define[\t ]+TIFFLIB_VERSION_STR[\t ]+\"LIBTIFF, Version .*")

    string(REGEX REPLACE "^#define[\t ]+TIFFLIB_VERSION_STR[\t ]+\"LIBTIFF, Version +([^ \\n]*).*"
           "\\1" TIFF2_VERSION_STRING "${tiff_version_str}")
    unset(tiff_version_str)
endif()

# handle the QUIETLY and REQUIRED arguments and set TIFF_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TIFF2
                                  REQUIRED_VARS TIFF2_LIBRARY TIFF2_INCLUDE_DIR
                                  VERSION_VAR TIFF2_VERSION_STRING)

if(TIFF2_FOUND)
  set( TIFF2_LIBRARIES ${TIFF2_LIBRARY} )
endif()

mark_as_advanced(TIFF2_INCLUDE_DIR TIFF2_LIBRARY)

# eof
