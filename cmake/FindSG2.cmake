#
# Find the SimGear (SG2) includes and library
#
# It defines the following variables
#  SG2_INCLUDE_DIRS, where to find <simgear/SGMath.hxx>, etc.
#  SG2_LIBRARIES, the libraries to link against to use SimGear.
#  SG2_FOUND, If false, do not try to use SimGear.
#  SG2_CORE_LIBRARY_DEPENDENCIES, additional dependencies
if (NOT SG2_DEBUG)
    set(tmp $ENV{SG2_DEBUG})
    if (tmp)
        set(SG2_DEBUG ON)
    endif ()
endif ()
if (SG2_DEBUG)
	set(tmp $ENV{SG_ROOT})
	if (tmp)
		message(STATUS "*** Found SG_ROOT in ENV = ${tmp}" )
	else ()
		message(STATUS "*** SG_ROOT NOT found in ENV" )
	endif ()
endif ()

find_path(SG2_INC_DIR
    NAMES simgear/math/SGMath.hxx
    HINTS ${SG_ROOT} $ENV{SG_ROOT}
    PATHS ${SG_ROOT} $ENV{SG_ROOT}
    PATH_SUFFIXES include simgear
    DOC "Finding SGMaths path"
    )
if (SG2_DEBUG)
	if (SG2_INC_DIR)
		message(STATUS "*** Found SG2_INC_DIR=${SG2_INC_DIR}" )
	else ()
		message(STATUS "*** NOT Found SG2_INC_DIR" )
	endif ()
endif ()
set(SG2_NAMES ${SG2_NAMES} SimGearCore)
if (MSVC)
    find_library(SG2_LIB_DBG NAMES SimGearCored
        HINTS ${SG_ROOT} $ENV{SG_ROOT}
    	PATHS ${SG_ROOT} $ENV{SG_ROOT}
    	PATH_SUFFIXES lib simgear
        DOC "Finding SG Debug library"
        )
    find_library(SG2_LIB_REL NAMES SimGearCore
        HINTS ${SG_ROOT} $ENV{SG_ROOT}
    	PATHS ${SG_ROOT} $ENV{SG_ROOT}
    	PATH_SUFFIXES lib simgear
        DOC "Finding SG Release library"
        )
    if (SG2_LIB_DBG AND SG2_LIB_REL)
        set(SG2_LIBRARY
            debug ${SG2_LIB_DBG}
            optimized ${SG2_LIB_REL})
    elseif (SG2_LIB_REL)
        set(SG2_LIBRARY ${SG2_LIB_REL})
    endif ()
else ()
    find_package(Threads)
    find_library(SG2_LIBRARY
	NAMES ${SG2_NAMES} 
	HINTS ${SG_ROOT} $ENV{SG_ROOT}
    	PATHS ${SG_ROOT} $ENV{SG_ROOT}
    	PATH_SUFFIXES lib simgear
        DOC "Finding SG library"
	)
endif ()
if (SG2_DEBUG)
	if (SG2_LIBRARY)
		message(STATUS "*** Found SG2_LIBRARY=${SG2_LIBRARY}" )
	else ()
		message(STATUS "*** NOT Found SG2_LIBRARY" )
	endif ()
        if (NOT MSVC)
		if (Threads_FOUND)
			message(STATUS "*** Threads_FOUND CMAKE_THREAD_LIBS_INIT=${CMAKE_THREAD_LIBS_INIT}" )
		else ()
			message(STATUS "*** Threads NOT FOUND" )
		endif ()
	endif ()
endif ()

if (SG2_LIBRARY AND SG2_INC_DIR)
    set(SG2_INCLUDE_DIRS ${SG2_INC_DIR} )
    SET(SG2_LIBRARIES ${SG2_LIBRARY} )
    if(WIN32)
        list(APPEND SG2_CORE_LIBRARY_DEPENDENCIES ws2_32.lib Shlwapi.lib)
    endif(WIN32)
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SG2  DEFAULT_MSG  SG2_LIBRARY SG2_INC_DIR)

# eof

