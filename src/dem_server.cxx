
/*\
    dem_server.cxx

    see slippystuff.cxx for the sub-directory maze

    HGT files from 'http://www.viewfinderpanoramas.org' site
    Got 26151 .hgt N|S file to process...
    Got 26151 lat,lon pairs, on a maximum of 64800 pairs, missing 38649 (38649)
    Missing HGT written to file C:\GTools\perl\tempmiss.txt

    RESIZING AN IMAGE
    =================
    from : http://stackoverflow.com/questions/873976/fastest-c-c-image-resizing-library
    So far my options are:
        OpenCV - http://opencv.org/ - (Open Source Computer Vision Library) - https://github.com/Itseez/opencv (hopencv[m]z.bat)
        CImg - http://cimg.sourceforge.net/ (hcimgz.bat)
        ImageMagick - http://cimg.sourceforge.net/ (him.bat)
        GraphicsMagick (it's said to be fast)
        DevIL
        GIL from Boost
        CxImage
        Imlib2 (it's said to be fast)
        g3DGMV - http://g3dgmv.sourceforge.net/ - 
            data from : http://edcwww.cr.usgs.gov/doc/edchome/ndcdb/ndcdb.html

    Interesting TEST locations - see ELEVTESTS elevtests below
    ==========================================================
    Marianas Trench - 10,911 meters - 11 19 N   142 15 E    = 11.317, 142.25  = N11E142
        Later found 'deepest' in SRTM file is - 68.625,142.425 - TRY THAT
        known as the Challenger Deep - 5,960 fathoms (35,760 ft).
    Mount Everest   -  8,848 meters - 27 59 17 N 86 55 31 E = 27.988, 86.9253 = N27E086
    Highest Mountain - South America - Andes, Argentina.
    Cerro Aconcagua : Elevation: 22,841 feet (6,962 meters), 
    Coordinates: 32 39 20 S 70 00 57 W (-32.6555555555556, -70.0158333333333)
    Highest Mountains - South Africa
    Mafadi, Drakensberg KwaZulu-Natal 3,450 m -29.202, 29.357 
    Seweweekspoortpiek, Western Cape  2,325 m -33.398, 21.368 
    Kilimanjaro (Kibo Summit) 5,895 (19,341 ft) Tanzania 03 04 33 S 37 21 12 E (-3.0758333333, 37.353333333)
    Australia - Mount Kosciuszko - 2,228 metres (7,310 ft) - 36 27 21.53 S 148 15 48 E (-36.4559806, 148.2633333)
    Hawaii -  Mauna Kea 13,796 ft 4205 m  19 49 14 N 155 28 05 W  (19.820555556,-155.46805555556)
    Tristan da Cunha, Queen Mary's Peak -  2,062 metres (6,765 ft) - 37 05 40 S 12 17 19 W (-37.09444444,-12.288611111)
    J28\S38W013.hgt
    Mount McKinley, Alaska, 6168 m	20,174 ft 63.0690, -151.0063
    Antarctica - Highest Mountains
    Ellsworth Mountains  Antartica,  5,140 m -85 , -78.75
    Sentinel Range  Antartica,  5,140 m -85.5, -78.167
    Mount Lamlam - 406 meters (1,332 ft) - 13.33861,144.662778 13 20 19 N 144 39 46 E
       on image is about x,y 798,795 = y = 1 - (dlat - ilat) * 1201 = 794 x = dlon - ilon * 1201 = 796
    Mont Blanc  45.833611111, 6.865, 4810 - N45E006.hgt
    Continental USA
    Mount McKinley (Denali)	20,236 ft 6168 m 63.0690, -151.0063
    Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920

    ===================================================================================================
    from : https://hc.app.box.com/shared/1yidaheouv
    DWN: 17/04/2014  14:44       676,666,211 SRTM_W_250m_ASCII.rar
    DWN: 17/04/2014  14:59       664,446,414 SRTM_W_250m_TIF.rar
        GeoTIFF is a public domain metadata standard which allows georeferencing information to be embedded 
        within a TIFF file. The potential additional information includes map projection, coordinate systems, 
        ellipsoids, datums, and everything else necessary to establish the exact spatial reference for the 
        file. The GeoTIFF format is fully compliant with TIFF 6.0, so software incapable of reading and 
        interpreting the specialized metadata will still be able to open a GeoTIFF format file.[
    ===================================================================================================
    from : http://en.wikipedia.org/wiki/List_of_GIS_data_sources
    1: http://www.naturalearthdata.com/ - some created from SRTM Plus, some from http://www.evl.uic.edu/pape/data/WDB/
         CIA World DataBank II 
    2: http://www.opentopography.org/ - SRTM Version 3.0 Global (90m) and United States (30m) Elevation Data 
    *: http://gis.ess.washington.edu/data/raster/tenmeter/onebytwo10/index.html - DWN: concrete.zip, which contains
        364MB concrete.bil, concrete.blw, concrete.hdr, concrete.stx - ...
    Lots more searching, but nothing NEW found, especially at high resolution

    ===================================================================================================

\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string>
#include <climits>
#include <time.h>
#ifdef _MSC_VER
#include <Windows.h> // needed for Sleep(ms)
#else
#include <string.h> // for strcat(), strlen(), ...
#include <stdlib.h> // for atoi(), ...
#include <unistd.h> // for usleep(), ...
#define stricmp strcasecmp
#endif

#include "utils.hxx"
#include "sprtf.hxx"
#include "dem_utils.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "color.hxx"
//#ifdef ADD_TEST_MODULES
//#include "all_tests.hxx"
//#endif
#include "utils.hxx"
#include "dir_utils.hxx"
#include "cache_utils.hxx"
#include "http_utils.hxx"
#include "pollkbd.hxx"
#include "slippystuff.hxx"
#include "color.hxx"
#include "dem_server.hxx"

#ifdef _MSC_VER
#pragma comment (lib, "Ws2_32.lib")
//#pragma comment (lib, "Mswsock.lib")
//#pragma comment (lib, "AdvApi32.lib")
//#pragma comment (lib, "Winmm.lib") // __imp__timeGetTime@0
#endif

static const char *module = "dem_server";

#ifndef SPRTF
#define SPRTF printf
#endif

#ifndef DEM_VERSION
#define DEM_VERSION "0.0.9"
#endif

#ifndef EndBuf
#define EndBuf(a) ( a + strlen(a) )
#endif

#ifndef DEF_SERVER_PORT
#define DEF_SERVER_PORT 5555
#endif
#ifndef DEF_SERVER_ADDRESS
#define DEF_SERVER_ADDRESS		"127.0.0.1"
#endif
#ifndef DEF_SLEEP_MS
#define DEF_SLEEP_MS 100
#endif
// for select timeout
#ifndef DEF_TIMEOUT_MS
#define DEF_TIMEOUT_MS 500
#endif

#ifndef SLEEP
#ifdef _MSC_VER
#define SLEEP(x) Sleep(x)
#else // !_MSC_VER
#define SLEEP(x) usleep( x * 1000 )
#endif // _MSC_VER y/n
#endif // SLEEP

static const char *log_file = "tempdem.log";
static int port = DEF_SERVER_PORT;
static const char *servaddr = DEF_SERVER_ADDRESS;
// options
static int sleep_ms = DEF_SLEEP_MS;
static int timeout_ms = DEF_TIMEOUT_MS;
static int verbosity = 0;

/////////////////////////////////////////////////////////////////////////////////
// Handle Server Resposes
static const char *html_head =
    "<html>\n"
    "<head>\n"
    "<title>Path information</title>\n"
    "</head>\n";

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

static int get_my_max_vpath_len();
static char *get_my_valid_paths_stg(int len);

static int showOptions(struct mg_connection *conn, bool verb)
{
    int iret = MG_TRUE;
    std::string c(html_head);
    c += "<body>\n";

    c += "<h1>Path information</h1>\n";

    c += "<pre>";
    c += get_my_valid_paths_stg(get_my_max_vpath_len());
    c += "</pre>\n";

    c += "<p><b>All other will return 400 - command error, or 404 - file not found</b></p>\n";


    //char *tmp = Get_Browser_Type_Stg();
    //if (*tmp) {
    //    c += "<pre><b>Browser Types</b>\n";
    //    c += tmp;
    //    c += "</pre>\n";
    //}

    // anything else to add
    c += "<p align=\"right\">Update: ";
    c += Get_Current_UTC_Time_Stg();
    c += " UTC</p>\n";

    c += "</body>\n";
    c += "</html>\n";
    mg_send_header(conn,"Content-Type","text/html");
    send_extra_headers(conn);
    mg_send_data(conn,c.c_str(),(int)c.size());
    if (verb) {
        SPRTF("%s: Sent HTML reply, %d bytes\n", module, (int)c.size());
    }
    return iret;
}

static int event_handler(struct mg_connection *conn, enum mg_event ev) 
{
    int iret = MG_FALSE;
    int x,y,z;
    bool verb = (VERB1) ? true : false;
    const char *q = (conn->query_string && *conn->query_string) ? "?" : "";
    if (ev == MG_AUTH) {
        return MG_TRUE;   // Authorize all requests
    } else if (ev == MG_REQUEST) {
        SPRTF("%s: got URI %s%s%s\n", module,
            conn->uri,q,
            ((q && *q) ? conn->query_string : "") );
        if (strcmp(conn->uri,"/") == 0) {
            iret = showOptions(conn,verb);
        } else if ((strcmp(conn->uri,"/elev") == 0) && conn->query_string) {
            iret = sendElevation2( conn );
#ifdef ADD_MAPPY_TILE
        } else if (isSlippyPath(conn->uri, &x, &y, &z)) {
            iret = sendSlippyTile( conn, x, y, z );
#endif // ADD_MAPPY_TILE
        } else if (strcmp(conn->uri,"/colors") == 0) {
            iret = sendColorTable(conn);
        }
    }
    return iret;
}

////////////////////////////////////////////////////////////////////////////////


char hgt_info[] =
"The SRTM data files have names like \"N34W119.hgt\".\n" 
"\n"    
"Each data file covers a one-degree-of-latitude by one-degree-of-longitude block of Earth's surface.\n"
"The first seven characters indicate the southwest corner of the block, with N, S, E, and W referring\n" 
"to north, south, east, and west.\n"
"\n"    
"Thus, the \"N34W119.hgt\" file covers latitudes 34 to 35 North and longitudes 118-119 West (this file\n" 
"includes downtown Los Angeles, California). The filename extension \".hgt\" simply stands for the word \n"
"\"height\", meaning elevation. It is NOT a format type. \n"
"\n"    
"These files are in \"raw\" format (no headers and not compressed), 16-bit signed integers, elevation \n"
"measured in meters above sea level, in a \"geographic\" (latitude and longitude array) projection.\n"
"\n"    
"The 16-bit signed integers are store in little endian format, thus must be byte 'swapped' on \n"
"an Intel CPU, or others that store memory in big endian format.\n"
"\n"    
"Data voids indicated by -32768.\n"
"\n"    
"International 3-arc-second files have 1201 columns and 1201 rows of data, with a total file size of \n"
"2,884,802 bytes ( = 1201 x 1201 x 2). \n"
"\n"    
"United States 1-arc-second files have 3601 columns and 3601 rows of data, with a total filesize of \n"
"25,934,402 bytes ( = 3601 x 3601 x 2) are not yet handled.\n" ;

static VALIDPATHS valid_paths[] = {
    { "/", "ie no path - Return this information block" },
    { "/elev?lat=27.9&lon=86.9", "Return the elevation of this lat,lon, if available." },
#ifdef ADD_MAPPY_TILE
    { "/5/23/13.png", "Return slippy map.png, zoom/xlon/ylat, if available." },
#endif // ADD_MAPPY_TILE
    { "/colors",         "Return current color table in html." },
    { 0, 0 }
};

static int get_my_max_vpath_len()
{
    int len, max_len = 0;
    PVALIDPATHS pvp = valid_paths;
    while (pvp->info) {
        len = (int)strlen(pvp->path);
        if (len > max_len)
            max_len = len;
        pvp++;
    }
    return max_len;
}

static char *get_my_valid_paths_stg(int len)
{

    PVALIDPATHS pvp = valid_paths;
    char *tmp = GetNxtBuf();
    char *paths = GetNxtBuf();
    *paths = 0;
    while (pvp->info) {
        strcpy(tmp,pvp->path);
        while ((int)strlen(tmp) < len)
            strcat(tmp," ");
        sprintf(EndBuf(paths), "%s = %s\n", tmp, pvp->info);
        pvp++;
    }
    return paths;
}

void show_my_valid_paths()
{
    int len = get_my_max_vpath_len();
    char *cp = get_my_valid_paths_stg(len);
    SPRTF("%s",cp);
}

static void give_help( char *name, int plus )
{
    printf("\n");
    printf("%s - version " DEM_VERSION "\n", name);
    printf("\n");
    printf("Usage: %s [options]\n", name);
    printf("\n");
    printf("Options:\n");
    //      123456789112345678921
    printf(" --help   (-h or -?) = This help and exit(2)\n");
    printf(" --cache <num>  (-c) = Set image cache MB size. (def=%dMB)\n", (int)max_cache_mb);
    printf(" --port <num>   (-p) = Set port (def=%d)\n", port);
#ifdef USE_SIMGEAR_LIB
    printf(" --addr <ip>    (-a) = Set the IP address. (def=%s)\n", servaddr );
#endif
    printf(" --log <file>   (-l) = Set log file. (def=%s, in CWD)\n", log_file);
    printf(" --sleep <ms>   (-s) = Set milliseconds sleep in loop. 0 for none. (def=%d)\n", sleep_ms);
    printf(" --timeout <ms> (-t) = Set milliseconds timeout for select(). (def=%d)\n", timeout_ms);
    printf(" --verb[num]    (-v) = Bump or set verbosity. (def=%d)\n", verbosity);
    dem_path_help();
    printf("\n");
    printf("Will establish a HTTP server on the addr:port, and respond to GET with '/paths'\n");
    printf("\n");
    show_my_valid_paths();
    printf("All others will return 400 - command error, or 404 - file not found\n");
    printf("\n");
    printf("All output will be written to stdout, and the log file.\n");
    printf("Will exit on ESC keying, if in foreground\n");
    if (plus) {
        printf("\n%s\n",hgt_info);
    }
}

#ifndef ISNUM
#define ISNUM(a) ((a >= '0')&&(a <= '9'))
#endif
#ifndef ADDED_IS_DIGITS
#define ADDED_IS_DIGITS

static int is_digits(char * arg)
{
    size_t len,i;
    len = strlen(arg);
    for (i = 0; i < len; i++) {
        if ( !ISNUM(arg[i]) )
            return 0;
    }
    return 1; /* is all digits */
}

#endif // #ifndef ADDED_IS_DIGITS


static int parse_commands( int argc, char **argv )
{
    int iret = 0;
    int i, c, i2;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help( get_file_name(argv[0]), InStr(sarg,(char *)"+") );
                return 2;
#ifdef USE_SIMGEAR_LIB
            case 'a':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    servaddr = strdup(sarg);
                    SPRTF("%s: Set server address to %s\n", module, servaddr);
                } else {
                    SPRTF("%s: Expected IP address value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
#endif
            case 'c':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    max_cache_mb = atoi(sarg);
                    SPRTF("%s: Set cache size to %dMB\n", module, max_cache_mb);
                } else {
                    SPRTF("%s: Error: Expected MB cahce size to follow!\n", module );
                    goto Bad_CMD;
                }
                break;
            case 'd':
                if (i2 < argc) {
                    i++;
                    iret = set_new_directory(argv[i]);
                    if (iret)
                        return iret;
                } else {
                    SPRTF("%s: Error: Expected name:directory to follow!\n", module );
                    goto Bad_CMD;
                }
                break;
            case 'l':
                i++;    // log file already checked and handled
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    port = atoi(sarg);
                    SPRTF("%s: Set port to %d\n", module, port);
                } else {
                    SPRTF("%s: Expected port value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    sleep_ms = atoi(sarg);
                    SPRTF("%s: Set sleep to %d ms\n", module, sleep_ms);
                } else {
                    SPRTF("%s: Expected ms sleep value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    timeout_ms = atoi(sarg);
                    SPRTF("%s: Set select timeout to %d ms\n", module, timeout_ms);
                } else {
                    SPRTF("%s: Expected ms timeout value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 'v':
                sarg++; // skip the -v
                if (*sarg) {
                    // expect digits
                    if (is_digits(sarg)) {
                        verbosity = atoi(sarg);
                    } else if (*sarg == 'v') {
                        verbosity++; /* one inc for first */
                        while(*sarg == 'v') {
                            verbosity++;
                            arg++;
                        }
                    } else
                        goto Bad_CMD;
                } else
                    verbosity++;
                if (VERB1) printf("%s: Set verbosity to %d\n", module, verbosity);
                break;
            default:
                goto Bad_CMD;
                break;
            }
        } else {
Bad_CMD:
            SPRTF("%s: Unknown command %s\n", module, arg );
            return 1;
        }
    }

    return iret;
}

int check_log_file(int argc, char **argv)
{
    int i,fnd = 0;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            if (*sarg == 'l') {
                if ((i + 1) < argc) {
                    i++;
                    sarg = argv[i];
                    log_file = strdup(sarg);
                    fnd = 1;
                } else {
                    printf("%s: Expected a file name to follow [%s]\n", module, arg );
                    return 1;
                }
            }
        } else {

        }
    }
    set_log_file((char *)log_file, false);
    if (fnd)
        SPRTF("%s: Set LOG file to [%s]\n", module, log_file);
    return 0;
}

int main( int argc, char **argv )
{
    int res;
    time_t curr,next;

    res = check_log_file(argc,argv);
    if (res)
        return res;

    res = parse_commands(argc,argv);
    if (res)
        return res;

    if (http_init(servaddr,port,event_handler)) {
        SPRTF("%s: Server FAILED! Aborting...\n", module );
        return 1;
    }
    SPRTF("%s: Waiting on %s:%d... ESC to exit\n", module, servaddr, port );

    curr = time(0);
    while (1) {
        res = test_for_input();
        if (res) {
            if (res == 0x1b) {
                SPRTF("%s: Got ESC exit key...\n", module );
                break;
            } else {
                SPRTF("%s: Got unknown key %X!\n", module, res );
            }
        }
        http_poll(timeout_ms);    // server->poll();
        next = time(0);
        if (next != curr) {
            curr = next;
            // any one seconds tasks???
#ifdef DO_JSON_BUMP
            bump_json();
#endif
            if (sleep_ms > 0) {
                SLEEP(sleep_ms);
            }
        }
    }

    http_close();   // delete server;

    return 0;
}


// eof
