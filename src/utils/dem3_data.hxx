/*\
 * dem3_data.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _DEM3_DATA_HXX_
#define _DEM3_DATA_HXX_

extern bool is_dem3_ocean_tile( char *file, bool verb = false );


#endif // #ifndef _DEM3_DATA_HXX_
// eof - dem3_data.hxx
