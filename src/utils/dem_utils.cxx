// dem_utils.cxx

#include <stdio.h>
#include <sstream>
#ifdef _MSC_VER
#include <Windows.h>
#else // !_MSC_VER
#include <stdlib.h> // for exit(), ...
#include <string.h> // for strlen(), ...
#include "bmptypes.h"
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "color.hxx"
#include "dir_utils.hxx"
#include "dem_utils.hxx"

static const char *module = "dem_utils";

int do_byte_swap = 1;

// from : http://www.viewfinderpanoramas.org - Jonathan de Ferranti
//

// 1 arc-sec 
// de Ferranti, some europe - usgs, usa only
const char *srtm1_dir = "F:\\data\\dem1\\srtm1";

// 3 arc-sec
// de Ferranti
const char *ferr3_dir  = "F:\\data\\dem3\\hgt";
// usgs
const char *usgs3_dir = "F:\\data\\dem3\\usgs";

const char *mt_everest =  "F:\\data\\dem3\\hgt\\N27E086.hgt";
//static const char *san_fran =    "F:\\data\\dem3\\hgt\\N37W123.hgt";
//static const char *norway_rose = "F:\\data\\dem3\\hgt\\N60E006.hgt";
// Israel - Dead Sea shoreline - 408 meters below sea level
const char *dead_sea =     "F:\\data\\dem3\\hgt\\N31E035.hgt";
const char *usgs_dead_sea = "F:\\data\\dem3\\usgs\\N31E035.hgt";
// Tristan da Cunha, Peak -  2,062 metres (6,765 ft) - -37.09444444,-12.288611111
const char *tristan = "F:\\data\\dem3\\hgt\\S38W013.hgt";
// Mount Lamlam - 406 meters (1,332 ft) - 13.33861,144.662778 13 20 19 N 144 39 46 E
const char *guam = "F:\\data\\dem3\\hgt\\N13E144.hgt";

// 30 arc-sec
// each file of the form 'e020n40.Bathymetry.srtm'
//static const char *ferr30_dir = "D:\\SRTM\\scripps\\srtm30\\data";
const char *ferr30_dir = "F:\\data\\srtm30_plus"; // Bathymetry.srtm
const char *usgs30_dir = "F:\\data\\SRTM30\\data"; // 27 USGS DEM3, like E140N90.DEM

const char *stopo30_fil = "D:\\SRTM\\scripps\\topo30\\topo30"; // single world file

const char *netcdf1 = "D:\\SRTM\\scripps\\srtm30\\grd\\e020n40.nc";
const char *netcdf2 = "D:\\SRTM\\scripps\\srtm30\\grd\\w100s10.nc"; // Aconcagua
const char *ncdf30_dir = "D:\\SRTM\\scripps\\srtm30\\grd";

//const char *hgt_tests[] = { mt_everest, san_fran, norway_rose, dead_sea, usgs_dead_sea, 0 };
//const char *ncdf_tests[] = { netcdf1, netcdf2, 0 }; 

const char *globe30_dir = "F:\\data\\dem3\\SRTM30\\all10";
const char *gtopo_a = "F:\\data\\dem3\\SRTM30\\all10\\a10g";


typedef struct tagDEMPATHS {
    const char **dir;
    const char *name;
    const char *example1;
    const char *example2;
}DEMPATHS, *PDEMPATHS;

DEMPATHS demPaths[] = {
    // 1"
    { &srtm1_dir,   "srtm1",     "N36W119.hgt", "N36W122.hgt" },
    // 3"
    { &ferr3_dir,   "ferr3",     "N27E086.hgt", "N31E035.hgt" },
    { &usgs3_dir,   "usgs3",    "N27E086.hgt", "N31E035.hgt" },
    // 30"
    { &ferr30_dir,  "ferrb30",   "e060n40.Bathymetry.srtm", "w060s60.Bathymetry.srtm" },
    { &usgs30_dir,  "usgs30",   "E060N40.DEM", "W060S10.DEM" },
    { &ncdf30_dir,  "netcdf30",  "e020n40.nc", "w100s10.nc" },   // CDF format
    { &stopo30_fil, "stopo30",   0, 0 }, // single whole world file
    { &globe30_dir, "globe30",    "a10g", "h10g" },
    // terminating entry
    { 0, 0, 0, 0 }
};

std::string get_path_help_str()
{
    std::stringstream c;
    const char *nm;
    const char *path;
    std::string status, f, exam;
    PDEMPATHS pdp = demPaths; 
    size_t len, min = 0;
    char *tmp = GetNxtBuf();
    while (pdp->name) {
        path = *pdp->dir;
        len = strlen(path);
        if (len > min)
            min = len;
        pdp++;
    }
    strcpy(tmp,"directory");
    while(strlen(tmp) < min)
         strcat(tmp," ");
    //   " 12345678
    c << "     name:";
    c << tmp;
    c << " status, examples found\n";
    pdp = demPaths; 
    while (pdp->name) {
        nm = pdp->name;
        path = *pdp->dir;
        status = "";
        strcpy(tmp,path);
        while(strlen(tmp) < min)
            strcat(tmp," ");
        if (strcmp(path,"none") == 0) {
            status = "disabled";
        } else if (strcmp(nm,"stopo30")) {
            if (is_file_or_directory((char *)path) == 2) {
                status += "ok";
                f = path;
                f += PATH_SEP;
                exam = pdp->example1;
                f += exam;
                if (is_file_or_directory((char *)f.c_str()) == 1) {
                    status += ", ";
                    status += exam;
                } else {
                    status += ", NOT FOUND ";
                    status += exam;
                }
                f = path;
                f += PATH_SEP;
                exam = pdp->example2;
                f += exam;
                if (is_file_or_directory((char *)f.c_str()) == 1) {
                    status += ", ";
                    status += exam;
                } else {
                    status += ", NOT FOUND ";
                    status += exam;
                }
            } else {
                status = "NOT VALID PATH";
            }
        } else {
            if (is_file_or_directory((char *)path) == 1) {
                status += "ok, found single world file.";
            } else {
                status = "FILE NOT FOUND!";
            }
        }
        f = nm;
        while (f.size() < 8) {
            f = " "+f;
        }
        c << " ";
        c << f;
        c << ":";
        c << tmp;
        c << " ";
        c << status;
        c << "\n";
        pdp++;
    }
    return c.str();

}

///////////////////////////////////////////////////////////////////////////////
// set the paths to various DEM/HGT file sets
void dem_path_help()
{
    printf(" --dir name:dir (-d) = Set a PATH to a set of HGT/DEM files. Current -\n");
    printf("%s", get_path_help_str().c_str());
    printf("Each new directory given will be checked for the 'example' files, or abort.\n");
    printf("A special -d <name>:none to disable that particular directory use.\n");
}

int set_new_directory( char *nm_path, bool verb )
{
    size_t ii, len;
    char *path = 0;
    char *cp = GetNxtBuf();
    int c;
    PDEMPATHS pdp = demPaths; 

    strcpy(cp,nm_path);
    len = strlen(cp);
    for (ii = 0; ii < len; ii++) {
        c = cp[ii];
        if (c == ':') {
            cp[ii] = 0;
            ii++;
            path = &cp[ii];
            break;
        }
    }
    if (!path) {
        SPRTF("%s: Error: Colon separator between name and directory NOT found!\n", module );
        return 1;
    }
    while (pdp->name) {
        if (strcmp(cp,pdp->name) == 0) {
            break;
        }
        pdp++;
    }
    if (!pdp->name) {
        SPRTF("%s: Error: [%s] name does not match one of [ ", module, cp);
        pdp = demPaths;
        while (pdp->name) {
            SPRTF("%s ",pdp->name);
            pdp++;
        }
        SPRTF("]\n");
        return 1;
    }
    if (strcmp(path,"none")) { // special use of 'none' to disable that directory
        // else validity the directory (or file)
        if (strcmp(pdp->name,"stopo30")) {
            // expect a directory
            if (is_file_or_directory(path) != 2) {
                SPRTF("%s: Error: Path [%s] NOT VALID!\n", module, path);
                return 1;
            }
            std::string f;
            f = path;
            f += PATH_SEP;
            f += pdp->example1;
            if (is_file_or_directory((char *)f.c_str()) != 1) {
                SPRTF("%s: Error: Path [%s] VALID, but file [%s] NOT FOUND!\n", module, path,
                    pdp->example1);
                return 1;
            }
            f = path;
            f += PATH_SEP;
            f += pdp->example2;
            if (is_file_or_directory((char *)f.c_str()) != 1) {
                SPRTF("%s: Error: Path [%s] VALID, but file [%s] NOT FOUND!\n", module, path,
                    pdp->example2);
                return 1;
            }
        } else {
            // expect single file
            if (is_file_or_directory(path) != 1) {
                SPRTF("%s: Error: File [%s] NOT FOUND!\n", module, path);
                return 1;
            }
        }
    }
    if (strcmp(*pdp->dir,path)) {
        *pdp->dir = strdup(path);
        if (verb) SPRTF("%s: For [%s] set new path [%s]\n", module, cp, path );
    } else {
        if (verb) SPRTF("%s: For [%s] path [%s] unchanged.\n", module, cp, path );
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
static const char *out_dir = "image_cache";
static const char *out_bmp = "image_cache" PATH_SEP "tempdem3.bmp";
static const char *out_png = "image_cache" PATH_SEP "tempdem3.png";

const char *get_image_out_dir()
{
    if (out_dir)
        return out_dir;
    return ".";
}

void set_image_out_dir(const char *dir) { out_dir = strdup(dir); }

ELEVTESTS elevtests[] = {
    // Later found 'deepest' in SRTM file is - 68.625,142.425 - TRY THAT
    // { "Marianas Trench", 68.625, 142.425, -10911 }, - NO WRONG
    { "Marianas Trench", 11.317, 142.25, -10911 },
    { "Mount Everest",  27.988, 86.9253, 8848   },
    { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt // usgs30: MAX 4536, at lat,lon 45.833333,6.858333, offset y,x 5300,3223
    { "Honzon Deep", -23.266667, -174.75, -10800 },
    { "Cerro Aconcagua", -32.6555555555556, -70.0158333333333, 6962 },
    { "Mt McKinley, Alaska", 63.0690, -151.0063, 6168 },
    { "Dead Sea, Jordan", 31.758333, 35.508333, -427 }, // gs30_tests: MIN -415, y,x 989,1861 USGS30 E020N40.DEM
    { "Mafadi, SA", -29.202, 29.357, 3450 },
    { "Seweweekspoortpiek", -33.398, 21.368, 2325 },
    { "Kilimanjaro, Tanzania", -3.0758333333, 37.353333333, 5895 },
    { "Mount Kosciuszko", -36.4559806, 148.2633333,  2228 },
    { "Uluru / Ayers Rock", -25.345, 131.0361, 863 },
    { "Mauna Kea,Hawaii", 19.820555556,-155.46805555556, 4205 },
    { "Tristan da Cunha", -37.09444444,-12.288611111, 2062 },
    { "Mount Lamlam, Guam", 13.33861,144.662778, 406 },
    { "Ellsworth, Antartic", -78.75, -85 , 5140 },
    { "Sentinel, Antartic", -78.167, -85.5, 5140 }, 

    // end of table
    { 0, 0, 0, 0 }
};

static int next_test = 0;
PELEVTESTS get_first_test()
{
    next_test = 0;
    return elevtests;
}
PELEVTESTS get_next_test()
{
    next_test++;
    PELEVTESTS pet = &elevtests[next_test];
    if (pet->name)
        return pet;
    next_test--;
    return 0;
}


// **************************************************************************

int check_me()
{
    int i;
    SPRTF("%s: Any key to continue!\n", module);
    getchar();
#ifndef _WIN64
#if defined(_MSC_VER) && !defined(NDEBUG)
    __asm int 3;
#endif
#endif
    i = 0;
    return i;
}

// ***************************************************************************

bool no_image_overwrite = true;
char *check_overwrite(char *file)
{
    if (!no_image_overwrite)
        return file; 
    if (is_file_or_directory(file) == 0)
        return file;
    std::string dir = get_directory(file,true);
    std::string f = get_file_name(file);
    size_t pos = f.rfind('.');
    std::string ext;
    if (pos > 0) {
        ext = f.substr(pos);
        f = f.substr(0,pos);
    }
    std::string tmp;
    char buf[32];
    int cnt = 0;
    tmp = dir;
    tmp += f;
    tmp += ext;
    while (is_file_or_directory((char *)tmp.c_str()) != 0) {
        cnt++;
        sprintf(buf,"%d",cnt);
        tmp = dir;
        tmp += f;
        tmp += buf;
        tmp += ext;
    }
    char *nf = GetNxtBuf();
    strcpy(nf,tmp.c_str());
    return nf;
}

char *get_curr_out_bmp() { return (char *)out_bmp; }
char *get_out_bmp()
{
    char *file = get_curr_out_bmp();
    return check_overwrite(file);
}

void set_out_png( char *file )
{
    out_png = strdup(file);
}
void set_out_bmp( char *file )
{
    out_bmp = strdup(file);
}

char *get_cache_file_name( const char *file, bool noext )
{
    char *cp = GetNxtBuf();
    int c, i, len = strlen(file);
    int off = 0;
    for (i = 0; i < len; i++) {
        c = file[i];
        if ((c == '/') || (c == '\\')) {
            if (i == 0)
                continue;
            c = '_';    // get_cache_file_name(file,true);
        } else if (( c == '.' ) && noext)
            break;
        cp[off++] = (char)c;
    }
    cp[off] = 0;
    return cp;
}


void set_slippy_file_name( char *file )
{
    std::string f("images" PATH_SEP);
    f += get_cache_file_name(file);
#ifdef USE_PNG_LIB
    set_out_png((char *)f.c_str());
#else
    // switch to .bmp
    size_t len = f.rfind('.');
    if (len > 0) {
        f = f.substr(0,len+1);
        f += "bmp";
    }
    set_out_bmp((char *)f.c_str());
#endif
}

char *get_curr_out_png() { return (char *)out_png; }
char *get_out_png() 
{ 
    char *file = get_curr_out_png();
    return check_overwrite(file);
}

int set_out_files( std::string in_file )
{
    char buf[16];
    int count;
    std::string file = get_file_name_only(in_file);
    std::string tmp;

    tmp = "";
    if (out_dir && *out_dir) {
        if (is_file_or_directory((char *)out_dir) == 2) {
            tmp += out_dir;
            tmp += PATH_SEP;
        } else {
            SPRTF("%s: Warning: Can not 'stat' out directory %s!\n", module, out_dir);
        }
    }

    // get base name
    file = tmp + file;

    // deal with bmp
    tmp = file + ".bmp";
    count = 0;
    while (is_file_or_directory((char *)tmp.c_str()) == 1) {
        count++;
        sprintf(buf,"%d",count);
        tmp = file + "-";
        tmp += buf;
        tmp += ".bmp";
    }
    out_bmp = strdup(tmp.c_str());

    // deal with PNG
    tmp = file + ".png";
    count = 0;
    while (is_file_or_directory((char *)tmp.c_str()) == 1) {
        count++;
        sprintf(buf,"%d",count);
        tmp = file + "-";
        tmp += buf;
        tmp += ".png";
    }
    out_png = strdup(tmp.c_str());

    return 0;
}

// The Intel x86 and x86-64 series of processors use the little-endian format
// Big-endian is the most common convention in data networking (including IPv6), hence its pseudo-synonym network byte order
// The Motorola 6800 and 68k series of processors use the big-endian format
void set_endian( bool verb )
{
    if (test_endian()) {
        do_byte_swap = 0;
        if (verb) SPRTF("%s: Running on big endian processor\n", module);
    } else {
        do_byte_swap = 1;
        if (verb) SPRTF("%s: Running on little endian processor (Intel)\n", module);
    }

    if (verb) {
        SPRTF("%s: SIZEOF short %d, int %d, size_t %d, float %d, double %d\n", module,
            sizeof(short), sizeof(int), sizeof(size_t), sizeof(float), sizeof(double) );

        byte r = 0x11;
        byte g = 0x22;
        byte b = 0x33;

        unsigned int colr = rgb(r,g,b);

        SPRTF("%s: Color #%02X%02X%02X is laid out as %s in memory, value %d (0x%08X)\n", module,
            r,g,b,
            get_hex_str((unsigned char *)&colr, 4).c_str(),
            colr, colr );

        const char *msg;
        if ((sizeof(BITMAPFILEHEADER) == 14) && (sizeof(BITMAPINFOHEADER) == 40)) {
            msg = "ok";
            colr = 0;
        } else {
            msg = "BAD STRUCTURE ALIGNMENT - FIX ME!";
            colr = 1;
        }


        SPRTF("%s: sizeof BITMAPFILEHEADER %d (14), BITMAPINFOHEADER %d (40) %s\n", module, 
            sizeof(BITMAPFILEHEADER), sizeof(BITMAPINFOHEADER), msg );

        if (colr) {
            SPRTF("%s: Aborting to FIX structure alignment!\n", module);
            exit(1);
        }
    } else {
        // put back this essential test of some structures that need 2-byte alignment
        if ((sizeof(BITMAPFILEHEADER) != 14) || (sizeof(BITMAPINFOHEADER) != 40)) {
            SPRTF("%s: sizeof BITMAPFILEHEADER %d (14), BITMAPINFOHEADER %d (40)\n", module, 
                sizeof(BITMAPFILEHEADER), sizeof(BITMAPINFOHEADER) );

            SPRTF("%s: Aborting to FIX structure alignment!\n", module);
            exit(1);
        }

    }
}

//////////////////////////////////////////////////////////////////////////////////
char *get_tile_path(const char*file)
{
    char *tmp = GetNxtBuf();
    strcpy(tmp,file);
    size_t ii,len = strlen(tmp);
    int c;
    for (ii = 0; ii < len; ii++) {
        c = tmp[ii];
        if (c == '_') // get_tile_path(file);
            tmp[ii] = '/';
    }
    return tmp;
}

char *get_web_path(const char *path)
{
    char *tmp = GetNxtBuf();
    strcpy(tmp,path);
    size_t ii, len = strlen(tmp);
    int c;
    for (ii = 0; ii < len; ii++) {
        c = tmp[ii];
        if (c == '\\')
            tmp[ii] = '/';
    }
    return tmp;
}

std::string get_image_file_path( char *cp )
{
    char *tmp = get_cache_file_name(cp,true);
    std::string img = get_image_out_dir();
    img += PATH_SEP;
    img += tmp;
#ifdef USE_PNG_LIB
    img += ".png";
#else // !USE_PNG_LIB
    img += ".bmp";
#endif // USE_PNG_LIB y/n
    return img;
}


void write_html_view(vLZ &vlz, int zoom_min, int zoom_max, const char *in_title, const char *html_file)
{
    const char *out_file = html_file;
    const char *title = in_title;
    size_t ii, max = vlz.size();
    if (!max) {
        SPRTF("%s: NO LOCZOOM items to write to HTML!\n", module);
        return;
    }
    if (out_file == 0)
        out_file = "tempview.html";
    if (title == 0)
        title = "Image Viewer";
    size_t i2,m2;
    std::stringstream html;
    std::string f, tp;
    std::string attr = "width=\"64\" height=\"64\"";
    int zoom;
    LOCZOOM lz;
    unsigned int colr;
    byte r,g,b;
    char *tmp = GetNxtBuf();
    html << "<html>" EOL;
    html << " <head>" EOL;
    html << "  <title>";
    html << title;
    html << "</title>" EOL;
    html << " </head>" EOL;
    html << " <body>" EOL;
    html << " <div align=\"center\">" EOL;
    html << " <h1 align=\"center\">" << title << "</h1>" EOL;
    html << "  <table border\"1\" align=\"center\" summary=\"view of images\">" EOL;
    html << "  <caption>Test location";
    if (max > 1) {
        html << "s";
    }
    html << " at zooms " << zoom_min << " to " << (zoom_max - 1) << "</caption>" EOL;
    lz = vlz[0];
    m2 = lz.files.size();
    html << "    <tr>" EOL;
    html << "     <th>Location</th>" EOL;
    for (zoom = zoom_min; zoom < zoom_max; zoom++) {
        html << "     <th>" << zoom << "</th>" EOL;
    }
    html << "     <th>Lat</th>" EOL;
    html << "     <th>Lon</th>" EOL;
    html << "     <th>Elev(m)</th>" EOL;
    html << "    </tr>" EOL;
    for (ii = 0; ii < max; ii++) {
        lz = vlz[ii];
        html << "    <tr>" EOL;
        html << "     <td>" << lz.loc << "</td>" EOL;
        m2 = lz.files.size();
        for (i2 = 0; i2 < m2; i2++) {
            f = lz.files[i2];
            f = get_file_name(f);
            tp = get_tile_path(f.c_str());
            html << "     <td><a target=\"_blank\" href=\"" << f << "\"><img src=\"" << f << "\" " << attr << " alt=\"tile " << tp << "\"></a></td>" EOL;
        }
        html << "     <td align=\"right\">" << lz.lat << "</td>" EOL;
        html << "     <td align=\"right\">" << lz.lon << "</td>" EOL;
        colr = get_BGR_color(lz.exp);
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        sprintf(tmp,"#%02X%02X%02X", b, g, r );
        if (( r + g + b ) < 32) {
            html << "     <td align=\"right\" bgcolor=\"" << tmp << "\"><font color=\"#ffffff\">" << lz.exp << "</font></td>" EOL;
        } else {
            html << "     <td align=\"right\" bgcolor=\"" << tmp << "\">" << lz.exp << "</td>" EOL;
        }
        // html << "     <td align=\"right\">" << lz.exp << "</td>" EOL;
        html << "    </tr>" EOL;
    }

    html << "   </table>" EOL;
    html << " </div>" EOL;
    char *utc = Get_Current_UTC_Time_Stg();
    html << " <p align=\"right\">Generated " << utc << " UTC</p>" EOL;
    html << " <!-- Generated ";
    html << utc;
    html << " -->" EOL;
    html << " </body>" EOL;
    html << "</html>" EOL;
    f = get_image_out_dir();
    f += PATH_SEP;
    f += out_file;
    FILE *fp = fopen(f.c_str(),"w");
    if (!fp)
        return;
    fwrite(html.str().c_str(), html.str().size(), 1, fp);
    fclose(fp);
    SPRTF("%s: html viewer written to %s\n", module, f.c_str() );
}


// eof
