/*\
 * cache_utils.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include "sprtf.hxx"
#include "dir_utils.hxx"
#include "dem_utils.hxx"
#include "cache_utils.hxx"
// other includes

static const char *module = "cache_utils";

#ifndef DEF_CACHE_MB
#define DEF_CACHE_MB 5000
#endif

size_t max_cache_mb = DEF_CACHE_MB;

// implementation
char *get_cache_info()
{
    char *cp = GetNxtBuf();
    const char *dir = get_image_out_dir();
    long long total = 0;
    if (get_directory_size(dir,&total)) {
        int used = (int)(total / 1024);
        int avail = ((int)max_cache_mb - used);
        sprintf(cp, "Max. %dMB, Used. %dMB, Avail. %dMB", 
            (int)max_cache_mb, used, avail );
    } else {
        sprintf(cp,"Max. %dMB, Used not available\n", (int)max_cache_mb );
    }
    return cp;
}

void show_cache_stats() 
{
    char *cp = get_cache_info();
    SPRTF("%s: %s\n", module, cp );
}

// eof = cache_utils.cxx
