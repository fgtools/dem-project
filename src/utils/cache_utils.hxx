/*\
 * cache_utils.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _CACHE_UTILS_HXX_
#define _CACHE_UTILS_HXX_

extern size_t max_cache_mb; // def = 500;
extern void show_cache_stats();
extern char *get_cache_info();

#endif // #ifndef _CACHE_UTILS_HXX_
// eof - cache_utils.hxx
