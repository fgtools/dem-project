/*\
 * json_load.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _JSON_LOAD_HXX_
#define _JSON_LOAD_HXX_
#include <string>

extern size_t max_json;
extern size_t next_json;
extern bool get_next_block( std::string &j );
extern const char *json_file;
extern int load_json_file(const char *in_file);

#endif // #ifndef _JSON_LOAD_HXX_
// eof - json_load.hxx
