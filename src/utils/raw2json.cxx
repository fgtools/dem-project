// raw2json.cxx

#include <stdio.h>
#include <time.h>
#include <vector>
#include <fstream>
#include <string>
#ifndef _MSC_VER
#include <stdlib.h> // for exit(), ...
#include <string.h> // for strlen(), ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "cf_packet.hxx"
#include "dir_utils.hxx"

static const char *module = "raw2json";
#define mod_name module

static const char *rawlog = "C:\\Users\\user\\Downloads\\logs\\fgx-cf\\cf_raw.log";

#ifndef MAX_PACKET_SIZE
#define MAX_PACKET_SIZE 1200
#endif

// ==================================================
typedef struct tagPKTSTR {
    Packet_Type pt;
    const char *desc;
    int count;
    int totals;
    void *vp;
}PKTSTR, *PPKTSTR;

static PKTSTR sPktStr[pkt_Max] = {
    { pkt_Invalid, "Invalid",     0, 0, 0 },
    { pkt_InvLen1, "InvLen1",     0, 0, 0 },
    { pkt_InvLen2, "InvLen2",     0, 0, 0 },
    { pkt_InvMag, "InvMag",       0, 0, 0 },
    { pkt_InvProto, "InvPoto",    0, 0, 0 },
    { pkt_InvPos, "InvPos",       0, 0, 0 },
    { pkt_InvHgt, "InvHgt",       0, 0, 0 },
    { pkt_InvStg1, "InvCallsign", 0, 0, 0 },
    { pkt_InvStg2, "InvAircraft", 0, 0, 0 },
    { pkt_First, "FirstPos",      0, 0, 0 },
    { pkt_Pos, "Position",        0, 0, 0 },
    { pkt_Chat, "Chat",           0, 0, 0 },
    { pkt_Other, "Other",         0, 0, 0 },
    { pkt_Discards, "Discards",   0, 0, 0 }
};

PPKTSTR Get_Pkt_Str() { return sPktStr; }

//////////////////////////////////////////////////////////////////////
// feed up a raw JSON log
FILE *raw_log = 0;
size_t file_size, read_len, read_cnt;
unsigned char *raw_buf = 0;
typedef struct tagPKTS {
    char *bgn;
    int cnt;
}PKTS, *PPKTS;

typedef std::vector<PKTS> vPKTS;

// static vPKTS vPackets;

static vSTG vJson;

#ifndef PFX64
#ifdef _MSC_VER
#define PFX64 "I64u"
#else
#define PFX64 PRIu64
#endif
#endif

void simple_stat()
{
    int i, PacketCount, Bad_Packets, DiscardCount;
    PPKTSTR pps = Get_Pkt_Str();

    PacketCount = 0;
    Bad_Packets = 0;
    DiscardCount = pps[pkt_Discards].count;
    for (i = 0; i < pkt_Max; i++) {
        PacketCount += pps[i].count;
        if (i < pkt_First)
            Bad_Packets += pps[i].count;
    }
    char *tb = GetNxtBuf();
    sprintf(tb,"%s: ", mod_name);
    //sprintf(EndBuf(tb),"%" PFX64, ui64ByteCnt);
    sprintf(EndBuf(tb)," PktCnt=%d Bad=%d Discarded=%d", 
                PacketCount, Bad_Packets, DiscardCount );
    SPRTF("%s\n",tb);
}


vSIMTM vSimTim;

#define SAMEPILOT(a,b) ((strcmp(a.callsign,b.callsign) == 0)&&(strcmp(a.aircraft,b.aircraft) == 0))

void load_raw_packets()
{
    if (is_file_or_directory((char *)rawlog) != 1) {
        raw_log = (FILE *)-1;
        SPRTF("%s: stat of %s file failed!\n",module,rawlog);
        return;
    }
    file_size = get_last_file_size();
    if (file_size < (MAX_PACKET_SIZE * 3)) {
        // not worth the effort is less than about 3 packets
        SPRTF("%s: file %s too small %d bytes!\n",module,rawlog, (int)file_size);
        return;
    }
    raw_log = fopen(rawlog,"rb");
    if (!raw_log) {
        raw_log = (FILE *)-1;
        SPRTF("%s: Can NOT open %s!\n", module, rawlog);
        return;
    }
    if (raw_log == (FILE *)-1) {
        SPRTF("%s: Can NOT locate %s!\n", module, rawlog);
        return;
    }
    // get the next message from the LOG, format as JSON, and pop it out
    // spoof the date/time... if end of file restart at beginning
    raw_buf = new unsigned char[file_size];
    if (!raw_buf) {
        SPRTF("%s: memory FAILED!\n", module);
        fclose(raw_log);
        raw_log = (FILE *)-1;
        return;
    }
    read_len = fread(raw_buf,1,file_size,raw_log);
    fclose(raw_log);
    if (read_len != file_size) {
        SPRTF("%s: Read of file %s FAILED!\n", module, rawlog);
        raw_log = (FILE *)-1;
        delete raw_buf;
        raw_buf = 0;
    }
    size_t i, cnt, max, ii = 0;
    unsigned char *tb = raw_buf;
    unsigned char *pbgn = 0;
    time_t last = time(0);
    SIMTIME st, st2;
    double cum_time = 0.0;
    bool had_10_secs;
    size_t packets_done = 0;
    double exp_time = 12.0;
    int res;
    const char *tmp_json = "tempr2j.json";
    bool verb = false;
    for (i = 0; i < read_len; i++) {
        if ((tb[i+0] == 0x53)&&(tb[i+1] == 0x46)&&(tb[i+2] == 0x47)&&(tb[i+3] == 0x46)&&
            (tb[i+4] == 0x00)&&(tb[i+5] == 0x01)&&(tb[i+6] == 0x00)&&(tb[i+7] == 0x01)&&
            (tb[i+8] == 0x00)&&(tb[i+9] == 0x00)&&(tb[i+10]== 0x00)&&(tb[i+11]== 0x07)) {
            if (pbgn && cnt) {
                // we have a complete packet
                Packet_Type pt = Deal_With_Packet((char *)pbgn,cnt);
                if (pt < pkt_Max) sPktStr[pt].count++;  // set the packet stats
                if (Get_Sim_Time((char *)pbgn, cnt, &st )) {
                    max = vSimTim.size();
                    for (ii = 0; ii < max; ii++) {
                        st2 = vSimTim[ii];
                        if (SAMEPILOT(st,st2)) {
                            break;
                        }
                    }
                    if (ii < max) {
                        st2.sim_time = st.sim_time;
                        cum_time = st.sim_time - st2.first_sim_time;
                        if (verb) SPRTF("CUM: %s Sim time %lf\n", st.callsign, cum_time);
                        vSimTim[ii].sim_time = st.sim_time;
                        vSimTim[ii].exp_sim_time = st.sim_time + 10.0;
                        vSimTim[ii].updated = true;
                    } else {
                        st.first_sim_time = st.sim_time;
                        st.prev_sim_time = st.sim_time;
                        st.exp_sim_time = st.sim_time + 10.0;   // time to die
                        if (verb) SPRTF("NEW: %s Sim time %lf\n", st.callsign, st.sim_time);
                        st.expired = false;
                        st.updated = true;
                        vSimTim.push_back(st);
                    }
                }
                packets_done++;
                had_10_secs = false;
                max = vSimTim.size();
                for (ii = 0; ii < max; ii++) {
                    st2 = vSimTim[ii];
                    cum_time = st2.sim_time - st2.prev_sim_time;
                    if (cum_time > exp_time) {
                        had_10_secs = true;
                        break;
                    }
                }
                // if any cumulation > 10 secs
                if (had_10_secs) {
                    // update em ALL
                    size_t exp = 0;
                    for (ii = 0; ii < max; ii++) {
                        if (!vSimTim[ii].updated) {
                            vSimTim[ii].expired = true;
                            exp++;
                        }
                        vSimTim[ii].prev_sim_time = vSimTim[ii].sim_time;
                        vSimTim[ii].updated = false;
                    }
                    if (exp) {
                        res = Expire_Pilots2( vSimTim );
                        Write_JSON();
                        char *pbuf;
                        int len = Get_JSON( &pbuf );
                        if (len && pbuf) {
                            vJson.push_back(pbuf);
                        }
                        if (res & 0x8000) {
                            res &= ~0x8000;
                            vSZT vst;
                            for (ii = 0; ii < max; ii++) {
                                if (vSimTim[ii].expired) {
                                    vst.push_back(ii);
                                }
                            }
                            res = vst.size();
                            while (!vst.empty()) {
                                ii = vst.back();
                                vst.pop_back();
                                vSimTim.erase(vSimTim.begin() + ii);
                            }
                            ii = vSimTim.size();
                            SPRTF("%s: Removed %d expired pilots from vector. Was %d, now %d\n", mod_name,
                                (int) res, (int) max, (int) ii );

                        }
                    }
                }
                pbgn = &tb[i];
                cnt = 0;
            } else {
                pbgn = &tb[i];
                cnt = 0;
            }
        }
        cnt++;
    }

    simple_stat();

    cnt = vJson.size();
    SPRTF("%s: Have %d json strings\n", module, cnt);
    std::string s;
    FILE *fp = fopen(tmp_json,"w");
    if (fp) {
        for (i = 0; i < cnt; i++) {
            s = vJson[i];
            //SPRTF("%s\n", s.c_str());
            fwrite(s.c_str(),1,s.size(), fp);
        }
        fclose(fp);
        SPRTF("%s JSON written to %s\n", module, tmp_json);
    } else {
        SPRTF("%s: Failed to open %s\n", module,tmp_json);
    }
    delete raw_buf;
    //exit(1);
}

static const char *json_file = "C:\\Users\\user\\Downloads\\logs\\fgx-cf\\all.json";
vSTG vJson_stgs;
static int load_json_file(const char *in_file, vSTG &vs)
{
    std::ifstream file(in_file);
    std::string str;
    std::string file_contents;
    size_t lines = 0;
    while (std::getline(file, str))
    {
        if (str.find("{\"success\":true") == 0) {
            if (file_contents.size() > 0) {
                vs.push_back(file_contents);
                lines++;
            }
            file_contents = str;
        } else if (file_contents.size()) {
            file_contents += str;
        }
    }
    SPRTF("%s: Got %d JSON sets...\n", module, (int)lines);
    return lines;
}

int check_json_file()
{
    if (is_file_or_directory((char *)json_file) != 1) {
        SPRTF("%s: Failed to 'stat' file %s\n", module, json_file);
        return 1;
    }
    file_size = get_last_file_size();
    int res = load_json_file(json_file,vJson_stgs);

    return 0;
}



int main( int argc, char **argv )
{
    set_log_file((char *)"tempr2j.txt",false);
    load_raw_packets();
    //check_json_file();
	return 0;
}

// eof
