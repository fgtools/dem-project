// http_utils.hxx
#ifndef _HTTP_UTILS_HXX_
#define _HTTP_UTILS_HXX_
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <fstream>
#include <string>
#ifndef USE_SIMGEAR_LIB
#include "mongoose.h"
#endif
//using namespace std;

#define ADD_MAPPY_TILE

typedef struct tagVALIDPATHS {
    const char *path;
    const char *info;
}VALIDPATHS, *PVALIDPATHS;

enum cont_typ {
    ct_appjson,
    ct_txtcsv,
    ct_txthtml,
    ct_txtxml,
    ct_txtplain,
    ct_imgpng,
    ct_imgbmp,
    ct_max
};

extern void show_valid_paths(int len);
extern char *get_valid_paths_stg(int len);
extern int get_max_vpath_len();


// setup http on address(IP) and port
#ifdef USE_SIMGEAR_LIB
extern int http_init( const char *addr, int port );
#else
extern int http_init( const char *addr, int port, mg_handler_t handler = 0 );
extern int sendNextJSON(struct mg_connection *conn, bool use_text = false, bool verb = false);
extern void send_extra_headers(struct mg_connection *conn);
#endif
// poll for http events - uses select with ms timeout
extern void http_poll(int timeout_ms);
// close the http server
extern void http_close();

extern const char *Set_Browser_Type( const char *pua ); // pointer to "User-Agent" string
extern char *Get_Browser_Type_Stg();

#ifdef ADD_MAPPY_TILE
// expect /zoom/x/y.png 
extern bool isSlippyPath( std::string path, int *px, int *py, int *pz );
extern int sendSlippyTile( struct mg_connection *conn, int x, int y, int z, bool verb = false );
#endif
extern int sendColorTable( struct mg_connection *conn );
extern int sendElevation2( struct mg_connection *conn, bool verb = false );
extern int sendElev( struct mg_connection *conn, double lat, double lon, bool verb = false );

#endif // #ifndef _HTTP_UTILS_HXX_
// eof - http_utils.hxx
