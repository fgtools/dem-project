/*\
 * sg_wgs_84.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _SG_WGS_84_HXX_
#define _SG_WGS_84_HXX_

extern int fg_geo_inverse_wgs_84( double lat1, double lon1, double lat2, double lon2, 
                                    double *az1, double *az2, double *s );
extern int fg_geo_direct_wgs_84 ( double lat1, double lon1, double az1, double s,
                                    double *lat2, double *lon2, double *az2 );


#endif // #ifndef _SG_WGS_84_HXX_
// eof - sg_wgs_84.hxx
