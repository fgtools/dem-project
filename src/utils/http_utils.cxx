// http_utils.cxx

#include <stdio.h>
#ifdef _MSC_VER
#define snprintf _snprintf
#else
#include <string.h> // for strncmp(), ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "demFile.hxx"
#include "json_load.hxx"
#include "color.hxx"
#include "slippystuff.hxx"
#include "dir_utils.hxx"
#include "cache_utils.hxx"
#include "http_utils.hxx"

static const char *module = "http_utils";

#if 0 // table not used - remove eventually
static const char *content_types[ct_max] = {
     "application/json" ,
     "text/csv" ,
     "text/html" ,
     "text/xml" ,
     "text/plain" ,
     "image/png" ,
     "image/bmp"
};
#endif // 0 - not used

static const char *html_head =
    "<html>\n"
    "<head>\n"
    "<title>Path information</title>\n"
    "</head>\n";

//typedef struct tagVALIDPATHS {
//    const char *path;
//    const char *info;
//}VALIDPATHS, *PVALIDPATHS;

static VALIDPATHS valid_paths[] = {
    { "/", "ie no path - Return this information block" },
    { "/elev?lat=27.9&lon=86.9", "Return the elevation of this lat,lon, if available." },
    { "/flight.json", "If a valid json file with json blocks, return next block." },
#ifdef ADD_MAPPY_TILE
    { "/5/23/13.png", "Return slippy map.png, zoom/xlon/ylat, if available." },
#endif // ADD_MAPPY_TILE
#ifdef USE_SIMGEAR_LIB
    { "/test1", "Will return canned strings, not closing the connection." },
    { "/test_close", "As above, but will close the connection after dispath." },
    { "/test_args?", "Accept foo=abc, bar=1234 or username=johndoe, and similarly" },
    { "", "return one of the 2 canned strings." },
#else // !USE_SIMGEAR_LIB
#endif // USE_SIMGEAR_LIB y/n
    { "/colors",         "Return current color table in html." },
    { "/elev=27.9,86.9", "Depreciated: get elevation of this lat,lon, if available." },
    { 0, 0 }
};

int get_max_vpath_len()
{
    int len, max_len = 0;
    PVALIDPATHS pvp = valid_paths;
    while (pvp->info) {
        len = (int)strlen(pvp->path);
        if (len > max_len)
            max_len = len;
        pvp++;
    }
    return max_len;
}

char *get_valid_paths_stg(int len)
{

    PVALIDPATHS pvp = valid_paths;
    char *tmp = GetNxtBuf();
    char *paths = GetNxtBuf();
    *paths = 0;
    while (pvp->info) {
        strcpy(tmp,pvp->path);
        while ((int)strlen(tmp) < len)
            strcat(tmp," ");
        sprintf(EndBuf(paths), "%s = %s\n", tmp, pvp->info);
        pvp++;
    }
    return paths;
}


void show_valid_paths(int len)
{
    PVALIDPATHS pvp = valid_paths;
    char *tmp = GetNxtBuf();
    while (pvp->info) {
        strcpy(tmp,pvp->path);
        while ((int)strlen(tmp) < len)
            strcat(tmp," ");
        SPRTF("%s = %s\n", tmp, pvp->info);
        pvp++;
    }
}
//////////////////////////////////////////////////////////////////////////////////////

#ifdef ADD_MAPPY_TILE
// expect /zoom/x/y.png 
bool isSlippyPath( std::string path, int *px, int *py, int *pz )
{
    bool bret = false;
    std::string::size_type pos = path.find('.');
    if ((pos == std::string::npos)||(pos < 5)) {
        return false;
    }
    std::string dirs = path.substr(1,pos-1);
    vSTG argv = string_split(dirs, "/");
    if (argv.size() == 3) {
        *pz = atoi(argv[0].c_str());
        *px = atoi(argv[1].c_str());
        *py = atoi(argv[2].c_str());
        bret = true;
    }
    return bret;
}
#endif // ADD_MAPPY_TILE

#ifdef USE_SIMGEAR_LIB
#include "http_sg.cxx"
#else // !USE_SIMGEAR_LIB
////////////////////////////////////////////////////////////////////////////////
// NOT SimGear - Use mongoose server
#include "mongoose.h"
#ifndef MVER
#define MVER MONGOOSE_VERSION
#endif

static char server_name[64];        // Set by init_server_name()
static struct mg_server *server;    // Set by start_mongoose()
void send_extra_headers(struct mg_connection *conn)
{
    mg_send_header(conn,"Access-Control-Allow-Origin","*");
    mg_send_header(conn,"Access-Control-Allow-Methods","OPTIONS, POST, GET");
    mg_send_header(conn,"Access-Control-Allow-Headers","Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token");
}

int showOptions(struct mg_connection *conn)
{
    std::string contentStr(html_head);
    contentStr += "<body>\n";

    contentStr += "<h1>Path information</h1>\n";
    contentStr += "<pre>";
    contentStr += get_valid_paths_stg(get_max_vpath_len()+1);
    contentStr += "</pre>\n";

    contentStr += "<p><b>All other will return 400 - command error, or 404 - file not found</b></p>\n";

    contentStr += "<h2>File and Directory Information</h2>\n";
    contentStr += "<p>These are the current configured directories, and the status of some sample files.</p>\n";

    contentStr += "<pre>";
    contentStr += get_path_help_str();
    contentStr += "</pre>\n";

    contentStr += "<p>";
    char *tmp = GetNxtBuf();
    if (max_json) {
        sprintf(tmp,"Have %d json strings loaded from file %s.</p>\n", (int)max_json, json_file);
    } else {
        sprintf(tmp,"Have NO json strings loaded from file %s.</p>\n", json_file);
    }
    contentStr += tmp;
    contentStr += "</p>\n";

    // Image cache information
    contentStr += "<p>";
    contentStr += "<b>Image Cache Information</b>";
    contentStr += "<br>";
    contentStr += "Cache Dir: ";
    contentStr += get_image_out_dir();
    contentStr += "<br>";
    contentStr += "Dir Stats: ";
    contentStr += get_cache_info();
    contentStr += "</p>\n";

    // anything else to add
    contentStr += "<p align=\"right\">Update: ";
    contentStr += Get_Current_UTC_Time_Stg();
    contentStr += " UTC</p>\n";

    contentStr += "</body>\n";
    contentStr += "</html>\n";
    mg_send_header(conn,"Content-Type","text/html");
    //send_extra_headers(conn);
    mg_send_data(conn,contentStr.c_str(),(int)contentStr.size());
    SPRTF("%s: Sent info text html, %d bytes\n", module, (int)contentStr.size() );
    return MG_TRUE;
}

int sendNextJSON(struct mg_connection *conn, bool use_text, bool verb) 
{
    int iret = MG_FALSE;
    std::string j;
#ifdef HAVE_MEMORY_GALORE
    if (next_json < max_json) {
        j = vJson[next_json];
#else
    if ((next_json < max_json) && get_next_block(j)) {
#endif
        if (use_text)
            mg_send_header(conn,"Content-Type","text/plain");
        else
            mg_send_header(conn,"Content-Type","application/json");
        send_extra_headers(conn);
        mg_send_data(conn,j.c_str(),(int)j.size());
        next_json++;
        if (next_json >= max_json)
            next_json = 0;
        iret = MG_TRUE;
        if (verb) SPRTF("%s: Sent JSON string, len %d\n", module, (int)j.size());
    } else {
        mg_send_header(conn,"Content-Type","text/plain");
        mg_printf_data(conn, "%s", "No json available");
        iret = MG_TRUE;   // Mark as processed
    }
    return iret;
}

#define elev_failed_bit 0x8000

int sendElev( struct mg_connection *conn, double lat, double lon, bool verb )
{
    short elev;
    int len, failed = 0;
    std::stringstream c,d;
    c << "{\"success\":true";
    c << ",\"update\":\"";
    c <<  Get_Current_UTC_Time_Stg();
    c << "\"";
    c << ",\"lat\":";
    c << lat;
    c << ",\"lon\":";
    c << lon;
    c << ",\"elev_m\":";
    if (get_best_elevation( lat, lon, &elev, 0 )) {
        const char *ccp = get_evevation_file();
        c << elev;
        if (ccp && *ccp) {
            c << ",\"file\":\"";
            c << ccp;
            c << "\"";
        }
    } else {
        c << "\"not available\"";
        failed = elev_failed_bit;
    }
    c << "}\n";
    mg_send_header(conn,"Content-Type","application/json");
    send_extra_headers(conn);
    len = (int)c.str().size();
    mg_send_data(conn,c.str().c_str(),len);
    if (failed)
        len |= failed;
    return len;
}


int sendElevation( struct mg_connection *conn, std::string &latlon )
{
    int iret = MG_FALSE;
    vSTG argv = string_split(latlon, ",");
    if (argv.size() == 2) {
        double lat = atof(argv[0].c_str());
        double lon = atof(argv[1].c_str());
        if (in_world_range(lat,lon)) {
            sendElev( conn, lat, lon );
        } else {
            mg_send_header(conn,"Content-Type","text/plain");
            mg_printf_data(conn, "No elevation - lat,lon out of world %s", conn->uri);
        }
        iret = MG_TRUE;
    } else {
        mg_send_header(conn,"Content-Type","text/plain");
        mg_printf_data(conn, "No elevation - mal formed uri %s", conn->uri);
        iret = MG_TRUE;   // Mark as processed
    }
    return iret;
}

int sendElevation2( struct mg_connection *conn, bool verb )
{
    int iret = MG_FALSE;
    int len;
    vSTG argv = string_split(conn->query_string, "&");
    if (argv.size() == 2) {
        vSTG vlat = string_split(argv[0],"=");
        vSTG vlon = string_split(argv[1],"=");
        if ((vlat.size() == 2) && (vlon.size() == 2)) {
            double lat = atof(vlat[1].c_str());
            double lon = atof(vlon[1].c_str());
            if (in_world_range(lat,lon)) {
                len = sendElev( conn, lat, lon, verb );
                if (verb) {
                    if (len & elev_failed_bit) {
                        len &= ~elev_failed_bit;
                        SPRTF("%s: Sent JSON %d byte, that get elevation failed!\n", module, len);
                    } else {
                        SPRTF("%s: Sent JSON of %d byte with elevation.\n", module, len);
                    }
                }
            } else {
                mg_send_header(conn,"Content-Type","text/plain");
                mg_printf_data(conn, "No elevation - lat,lon out of world %s?%s", conn->uri, conn->query_string);
                if (verb)
                    SPRTF("%s: Sent plain/text lat,lon out of world!\n", module);
            }
            iret = MG_TRUE;
        }
    }
    if (iret != MG_TRUE) {
        mg_send_header(conn,"Content-Type","text/plain");
        mg_printf_data(conn, "No elevation - mal formed uri %s?%s", conn->uri, conn->query_string);
        iret = MG_TRUE;   // Mark as processed
        if (verb)
            SPRTF("%s: Sent plain/text mal formed URI!\n", module);
    }
    return iret;
}


#ifdef ADD_MAPPY_TILE
// expect /zoom/x/y.png 
int sendSlippyTile( struct mg_connection *conn, int x, int y, int z, bool verb )
{
    int iret = MG_FALSE;
    int iverb = 3;
    char *cp = 0;
    size_t len = 0;
    if ( !xy_in_range_for_zoom( x, y, z ) ) {
        cp = GetNxtBuf();
        len = sprintf(cp,"Values x,y,z %d,%d,%d Out of Range - %s", x, y, z,
            get_slippy_range_stg(z));
        mg_send_header(conn,"Content-Type","text/plain");
        mg_send_data(conn, cp, (int)len);
        if (verb)
            SPRTF("%s: Sent response '%s'\n", module, cp );
        return MG_TRUE;
    }

    if (get_slippy_tile(x,y,z,&cp,iverb) && cp && (is_file_or_directory(cp) == 1)) {
        len = get_last_file_size();
        char *buf = new char[len];
        if (buf) {
            FILE *fp = fopen(cp,"rb");
            if (fp) {
                size_t res = fread(buf,1,len,fp);
                if (res == len) {
                    iret = MG_TRUE;
                    mg_send_header(conn,"Content-Type","image/png");
                    send_extra_headers(conn);
                    mg_send_data(conn,buf,(int)len);
                    if (verb)
                        SPRTF("%s: Sent '%s', len %d\n", module, cp, (int)len );
                    iret = MG_TRUE;
                }
                fclose(fp);
            }
            delete buf;
        }
    }
    if (iret == MG_FALSE) {
        cp = GetNxtBuf();
        len = sprintf(cp,"Unable to load slippy tile for %s", conn->uri);
        mg_send_header(conn,"Content-Type","text/plain");
        mg_send_data(conn, cp, (int)len);
        if (verb)
            SPRTF("%s: Sent response '%s'\n", module, cp );
        iret = MG_TRUE;   // Mark as processed
    }
    return iret;
}
#endif // ADD_MAPPY_TILE

int sendColorTable( struct mg_connection *conn )
{
    std::string html = get_color_http();
    mg_send_header(conn,"Content-Type","text/html");
    send_extra_headers(conn);
    mg_send_data(conn,html.c_str(),(int)html.size());
    return MG_TRUE;
}

// Mount Everest - 8,848 meters - /elev=27.988,86.9253 - N27E086
// slippy path = /5/23/13.png
static int event_handler(struct mg_connection *conn, enum mg_event ev) 
{
    int iret = MG_FALSE;
    int x,y,z;
    const char *q = (conn->query_string && *conn->query_string) ? "?" : "";
    if (ev == MG_AUTH) {
        return MG_TRUE;   // Authorize all requests
    } else if (ev == MG_REQUEST) {
        SPRTF("%s: got URI %s%s%s\n", module,
            conn->uri,q,
            ((q && *q) ? conn->query_string : "") );
        if (strcmp(conn->uri,"/") == 0) {
            iret = showOptions(conn);
        } else if (strncmp(conn->uri,"/elev=",6) == 0) {
            std::string latlon = &conn->uri[6];
            iret = sendElevation( conn, latlon );
        } else if ((strcmp(conn->uri,"/elev") == 0) && conn->query_string) {
            iret = sendElevation2( conn );
        } else if (strcmp(conn->uri,"/flights.json") == 0) {
            iret = sendNextJSON(conn);
#ifdef ADD_MAPPY_TILE
        } else if (isSlippyPath(conn->uri, &x, &y, &z)) {
            iret = sendSlippyTile( conn, x, y, z, true );
#endif // ADD_MAPPY_TILE
        } else if (strcmp(conn->uri,"/colors") == 0) {
            iret = sendColorTable(conn);
        }
    }
    return iret;
}

#define EV_HANDLER event_handler

static void init_server_name(void)
{
  const char *descr = " for DEM project";
  snprintf(server_name, sizeof(server_name), "Mongoose web server v.%s%s",
           MVER, descr);
}


int http_init( const char *addr, int port, mg_handler_t handler )
{
    init_server_name();
    if (handler)
        server = mg_create_server(NULL, handler);
    else
        server = mg_create_server(NULL, EV_HANDLER);
    if (!server) {
        SPRTF("%s: mg_create_server(NULL, event_handler) FAILED!\n", module );
        return 1;
    }
    char *tmp = GetNxtBuf();
    sprintf(tmp,"%u",port);
    const char *msg = mg_set_option(server, "listening_port", tmp);
    if (msg) {
        http_close();
        SPRTF("%s: Failed to set listening port %u - %s!\n", module, port, msg);
        return 1;
    }

    // there is NO document root here mg_get_option(server, "document_root")
    SPRTF("%s: %s on port %s\n", module,
         server_name, 
         mg_get_option(server, "listening_port"));

    return 0;
}

// poll for http events - uses select with ms timeout
void http_poll(int timeout_ms)
{
    if (server) {
        mg_poll_server(server, timeout_ms);
    }
}

// close the http server
void http_close()
{
    if (server)
        mg_destroy_server(&server);
    server = 0;
    SPRTF("%s: destroyed mongoose server.\n", module);
}


#endif // USE_SIMGEAR_LIB y/n

/////////////////////////////////////////////////////////////////////////
// I like to know the browser type
// ===============================
enum BrType {
    bt_unknown = 0,
    bt_msie,
    bt_chrome,
    bt_firefox,
    bt_ns,
    bt_opera,
    bt_safari,
    bt_moz,
    bt_lynx,
    bt_wget,
    bt_max
};

static bool done_btc_init = false;
static int bt_count[bt_max] = { 0 };

typedef struct tagBRNAME {
    BrType bt;
    const char *name;
}BRNAME, *PBRNAME;

static BRNAME BrName[] = {
    { bt_unknown, "Unknown" },
    { bt_msie, "MSIE" },
    { bt_chrome, "Chrome" },
    { bt_firefox, "Firefox" },
    { bt_ns, "Netscape" },
    { bt_opera, "Opera" },
    { bt_safari, "Safari" },
    { bt_moz, "Mozilla" },
    { bt_lynx, "Lynx" },
    { bt_wget, "Wget" },

    // always LAST
    { bt_max, 0 }
};

const char *get_BrType_Name( BrType bt )
{
    PBRNAME pbn = &BrName[0];
    while( pbn->name ) {
        if (pbn->bt == bt)
            return pbn->name;
        pbn++;
    }
    return "Unknown";
}


// got the User-Agent string,
// attempt to get browser type
BrType Get_Browser_Type(const char *pua)
{
    char *prot = (char *)pua;
    BrType bt = bt_unknown;
    if (InStr(prot,(char *)"MSIE"))
        bt = bt_msie;
    else if (InStr(prot,(char *)"Chrome"))
        bt = bt_chrome;
    else if (InStr(prot,(char *)"Opera"))
        bt = bt_opera;
    else if (InStr(prot,(char *)"Safari"))
        bt = bt_safari;
    else if (InStr(prot,(char *)"Navigator"))
        bt = bt_ns;
    else if (InStr(prot,(char *)"Firefox"))
        bt = bt_firefox;
    else if (InStr(prot,(char *)"Lynx"))
        bt = bt_lynx;
    else if (InStr(prot,(char *)"Wget"))
        bt = bt_wget;
    else if (InStr(prot,(char *)"Mozilla"))
        bt = bt_moz;

    return bt;
}

const char *Set_Browser_Type( const char *pua ) // pointer to "User-Agent" string
{
    BrType bt;
    if (!done_btc_init) {
        done_btc_init = true;
        for (int i = bt_unknown; i < bt_max; i++) {
            bt_count[i] = 0;
        }
    }
    bt = Get_Browser_Type(pua);
    bt_count[bt]++;
    return get_BrType_Name(bt);
}

char *Get_Browser_Type_Stg()
{
    char *cp = GetNxtBuf();
    int i, cnt, total = 0;
    const char *bt;
    *cp = 0;
    for (i = bt_unknown; i < bt_max; i++) {
        cnt = bt_count[i];
        if (cnt) {
            bt = get_BrType_Name((BrType)i);
            sprintf(EndBuf(cp),"%3d %s\n", cnt, bt);
            total += cnt;
        }
    }

    if (total)
        sprintf(EndBuf(cp),"%3d Total\n", total);

    return cp;
}

/////////////////////////////////////////////////////////////////////////

// eof
