// http_sg.cxx
// Only included #ifdef USE_SIMGEAR_LIB

///////////////////////////////////////////////////////////////////////////////////////
// SimGear implementation
#include <simgear/io/raw_socket.hxx>
#include <simgear/io/sg_netChannel.hxx>
#include <simgear/io/sg_netBuffer.hxx>
#include <simgear/io/sg_netChat.hxx>
#include <simgear/misc/strutils.hxx>
#include <simgear/timing/timestamp.hxx>

using namespace simgear;

const char* BODY1 = "The quick brown fox jumps over a lazy dog.";
const char* BODY3 = "Cras ut neque nulla. Duis ut velit neque, sit amet "
"pharetra risus. In est ligula, lacinia vitae congue in, sollicitudin at "
"libero. Mauris pharetra pretium elit, nec placerat dui semper et. Maecenas "
"magna magna, placerat sed luctus ac, commodo et ligula. Mauris at purus et "
"nisl molestie auctor placerat at quam. Donec sapien magna, venenatis sed "
"iaculis id, fringilla vel arcu. Duis sed neque nisi. Cras a arcu sit amet "
"risus ultrices varius. Integer sagittis euismod dui id varius. Cras vel "
"justo gravida metus.";

static bool add_app_content = false;

class HTTPServerChannel : public NetChat
{
public:  
    enum State
    {
        STATE_IDLE = 0,
        STATE_HEADERS,
        STATE_CLOSING,
        STATE_REQUEST_BODY
    };
    
    HTTPServerChannel();
    virtual void collectIncomingData(const char* s, int n);
    virtual void foundTerminator(void);
    void parseArgs(const string& argData);
    void addHeaders( stringstream & d );
    void sendNextJSON();
    void set_header( stringstream &d, int code, cont_typ ct, size_t len );
    bool isElevation( std::string path );
#ifdef ADD_MAPPY_TILE
    // expect /zoom/x/y.png 
    bool sendSlippyTile( int x, int y, int z );
#endif
    void showOptions();
    void receivedRequestHeaders();
    void closeAfterSending();
    void receivedBody();
    void sendErrorResponse(int code, bool close, string content);
    string reasonForCode(int code);
    
    State state;
    string buffer;
    string method;
    string path;
    string httpVersion;
    std::map<string, string> requestHeaders;
    std::map<string, string> args;
    int requestContentLength;

};


class HTTPServer : public NetChannel
{
    simgear::NetChannelPoller _poller;

public:
    HTTPServer();
    HTTPServer( const char *addr, int port );
    virtual ~HTTPServer();
    virtual bool writable (void) { return false ; }
    virtual void handleAccept (void);
   
    void poll()
    {
        _poller.poll();
    }
    bool ok;
    bool acc;
};


HTTPServerChannel::HTTPServerChannel()
{
    state = STATE_IDLE;
    setTerminator("\r\n");
}


void HTTPServerChannel::collectIncomingData(const char* s, int n)
{
    buffer += string(s, n);
}

void HTTPServerChannel::foundTerminator(void)
{
    if (state == STATE_IDLE) {
        state = STATE_HEADERS;
        string_list line = strutils::split(buffer, NULL, 3);
        if (line.size() < 3) {
            SPRTF("%s: malformed request: %s\n", module, buffer.c_str());
            exit(-1);
        }
            
        method = line[0];
        path = line[1];
            
        string::size_type queryPos = path.find('?');
        if (queryPos != string::npos) {
            parseArgs(path.substr(queryPos + 1));
            path = path.substr(0, queryPos);
        }
            
        httpVersion = line[2];
        requestHeaders.clear();
        buffer.clear();
    } else if (state == STATE_HEADERS) {
        string s = strutils::simplify(buffer);
        if (s.empty()) {
            buffer.clear();
            receivedRequestHeaders();
            return;
        }
            
        string::size_type colonPos = buffer.find(':');
        if (colonPos == string::npos) {
            SPRTF("%s: malformed HTTP response header: %s\n", module, buffer.c_str());
            buffer.clear();
            return;
        }

        string key = strutils::simplify(buffer.substr(0, colonPos));
        string value = strutils::strip(buffer.substr(colonPos + 1));
        requestHeaders[key] = value;
        buffer.clear();
    } else if (state == STATE_REQUEST_BODY) {
        receivedBody();
        setTerminator("\r\n");
    } else if (state == STATE_CLOSING) {
        // ignore!
    }
}  

void HTTPServerChannel::parseArgs(const string& argData)
{
    string_list argv = strutils::split(argData, "&");
    for (unsigned int a=0; a<argv.size(); ++a) {
        string::size_type eqPos = argv[a].find('=');
        if (eqPos == string::npos) {
            SPRTF("%s: malformed HTTP argument: %s\n", module, argv[a].c_str());
            continue;
        }

        string key = argv[a].substr(0, eqPos);
        string value = argv[a].substr(eqPos + 1);
        args[key] = value;
    }
}

void HTTPServerChannel::addHeaders( stringstream & d ) 
{
    if (add_app_content) {
        d << "Content-Type: application/json; charset=UTF-8" << "\r\n";
    } else {
        d << "Content-Type: text/plain; charset=UTF-8" << "\r\n";
    }
    d << "Access-Control-Allow-Origin: *" << "\r\n";
    d << "Access-Control-Allow-Methods: OPTIONS, POST, GET" << "\r\n";
    d << "Access-Control-Allow-Headers: Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token" << "\r\n";
}

void HTTPServerChannel::sendNextJSON()
{
    std::string j;
#ifdef HAVE_MEMORY_GALORE
    if (next_json < max_json) {
        j = vJson[next_json];
#else
    if ((next_json < max_json) && get_next_block(j)) {
#endif
        stringstream d;
        d << "HTTP/1.1 " << 200 << " " << reasonForCode(200) << "\r\n";
        d << "Content-Length:" << j.size() << "\r\n";
        addHeaders(d);
        d << "\r\n"; // final CRLF to terminate the headers
        d << j;
        SPRTF("%s: returning json block %d of %d, len %d\n", module, (int)(next_json + 1),
            (int)max_json,
            (int)j.size());
        next_json++;
        if (next_json >= max_json)
            next_json = 0;
        push(d.str().c_str());
        closeAfterSending();    // == set state closing AND closeWhenDone();
    } else {
        sendErrorResponse(404, true, "no json available");
    }
}

void HTTPServerChannel::set_header( stringstream &d, int code, cont_typ ct, size_t len )
{
    d << "HTTP/1.1 " << code << " " << reasonForCode(code) << "\r\n";
    d << "Content-Length:" << len << "\r\n";
    d << "Content-Type: ";
    d << content_types[ct];
    d << "\r\n";
    //addHeaders(d);
    d << "\r\n"; // final CRLF to terminate the headers
}

bool HTTPServerChannel::isElevation( std::string path )
{
    bool bret = false;
    if (strncmp(path.c_str(),"/elev=", 6) == 0) {
        std::string latlon = path.substr(6);
        string_list argv = strutils::split(latlon, ",");
        if (argv.size() == 2) {
            double lat = atof(argv[0].c_str());
            double lon = atof(argv[1].c_str());
            char *ts = Get_Current_UTC_Time_Stg();
            short elev;
            stringstream c,d;
            c << "{\"success\":true";
            c << ",\"update\":\"";
            c << ts;
            c << "\"";
            c << ",\"lat\":";
            c << lat;
            c << ",\"lon\":";
            c << lon;
            if (get_best_elevation( lat, lon, &elev, 0 )) {
                const char *ccp = get_evevation_file();
                c << ",\"elev_m\":";
                c << elev;
                if (ccp && *ccp) {
                    c << ",\"file\":\"";
                    c << ccp;
                    c << "\"";
                }
            } else {
                c << ",\"elevation\":";
                c << "\"not available\"";
            }
            c << "}\n";
            set_header( d, 200, ct_txtplain, c.str().size() );
            d << c.str();
            SPRTF("%s: returning %s\n", module, d.str().c_str());
            push(d.str().c_str());
            closeAfterSending();
            bret = true;
        }
    }
    return bret;
}

#ifdef ADD_MAPPY_TILE
// expect /zoom/x/y.png 
bool HTTPServerChannel::sendSlippyTile( int x, int y, int z )
{
    bool bret = false;
    int iverb = 3;
    char *cp = 0;
    size_t len = 0;
    if (get_slippy_tile(x,y,z,&cp,iverb) && cp && (is_file_or_directory(cp) == 1)) {
        len = get_last_file_size();
        char *buf = new char[len];
        if (buf) {
            FILE *fp = fopen(cp,"rb");
            if (fp) {
                size_t res = fread(buf,1,len,fp);
                if (res == len) {
                    //std::vector<unsigned char> vect;
                    stringstream d;
                    std::string c(buf,len);
                    set_header( d, 200, ct_imgpng, c.size() );
                    d << c; // hope this works with binary data
                    SPRTF("%s: returning length %d\n", module, (int)d.str().size());
                    push(d.str().c_str());
                    closeAfterSending();
                    bret = true;
                }
                fclose(fp);
            }
            delete buf;
        }
    }
    return bret;
}
#endif // ADD_MAPPY_TILE

void HTTPServerChannel::showOptions()
{
    char *tmp = GetNxtBuf();
    char *ts = Get_Current_UTC_Time_Stg();
    string contentStr(html_head);
    contentStr += "<body>\n";

    contentStr += "<h1>Path information</h1>\n";
    contentStr += "<pre>";
    contentStr += get_valid_paths_stg();
    contentStr += "</pre>\n";

    contentStr += "<p><b>All other will return 400 - command error, or 404 - file not found</b></p>\n";

    contentStr += "<h2>File and Directory Information</h2>\n";
    contentStr += "<p>These are the current configured directories, and the status of some sample files.</p>\n";

    contentStr += "<pre>";
    contentStr += get_path_help_str();
    contentStr += "</pre>\n";

    contentStr += "<p>";
    if (max_json) {
        sprintf(tmp,"Have %d json strings loaded from file %s.</p>\n", (int)max_json, json_file);
    } else {
        sprintf(tmp,"Have NO json strings loaded from file %s.</p>\n", json_file);
    }
    contentStr += tmp;
    contentStr += "</p>\n";

    contentStr += "<p align=\"right\">Update: ";
    contentStr += ts;
    contentStr += " UTC</p>\n";

    contentStr += "</body>\n";
    contentStr += "</html>\n";
    stringstream d;
    set_header( d, 200, ct_txthtml, contentStr.size() );
    d << contentStr;
    SPRTF("%s: returning %s\n", module, d.str().c_str());
    push(d.str().c_str());
    closeAfterSending();
}

void HTTPServerChannel::receivedRequestHeaders()
{
    static const char *last = BODY1;
    state = STATE_IDLE;
    int x,y,z;
    if ((path == "")||(path =="/")) {
        showOptions();
    } else if (path == "/flights.json") {
        sendNextJSON();
    } else if (isElevation(path)) {
        // all done
#ifdef ADD_MAPPY_TILE
    } else if (isSlippyPath(path, &x, &y, &z)) {
        if (!sendSlippyTile( x, y, z )) {
            SPRTF("%s: no slippy tile for path %s! Sending 404\n", module, path.c_str());
            sendErrorResponse(404, false, "");
        }
#endif // ADD_MAPPY_TILE
    } else if (path == "/test1") {
        string contentStr(last);
        stringstream d;
        d << "HTTP/1.1 " << 200 << " " << reasonForCode(200) << "\r\n";
        d << "Content-Length:" << contentStr.size() << "\r\n";
        //addHeaders(d);
        d << "\r\n"; // final CRLF to terminate the headers
        d << contentStr;
        SPRTF("%s: returning %s\n", module, d.str().c_str());
        push(d.str().c_str());
        if (last == BODY1)
            last = BODY3;
        else
            last = BODY1;
    } else if (path == "/test_close") {
        string contentStr(last);
        stringstream d;
        d << "HTTP/1.1 " << 200 << " " << reasonForCode(200) << "\r\n";
        d << "Connection: close\r\n";
        //addHeaders(d);
        d << "\r\n"; // final CRLF to terminate the headers
        d << contentStr;
        SPRTF("%s: returning %s\n", module, d.str().c_str());
        push(d.str().c_str());
        closeAfterSending();
        if (last == BODY1)
            last = BODY3;
        else
            last = BODY1;
    } else if (path == "/test_args") {
        string contentStr(last);
        // if ((args["foo"] != "abc") || (args["bar"] != "1234") || (args["username"] != "johndoe"))
        if (args["foo"] == "abc") {
            contentStr.push_back('\n');
            contentStr += "got foo=abc";
        } else if (args["bar"] == "1234") {
            contentStr.push_back('\n');
            contentStr += "got bar=1234";
        } else if (args["username"] == "johndoe") {
            contentStr.push_back('\n');
            contentStr += "got username=johndoe";
        } else {
            SPRTF("%s: return 400 'bad_argument'\n", module);
            sendErrorResponse(400, true, "bad arguments");
            return;
        }
        stringstream d;
        d << "HTTP/1.1 " << 200 << " " << reasonForCode(200) << "\r\n";
        d << "Content-Length:" << contentStr.size() << "\r\n";
        //addHeaders(d);
        d << "\r\n"; // final CRLF to terminate the headers
        d << contentStr;
        SPRTF("%s: returning %s\n", module, d.str().c_str());
        push(d.str().c_str());
        if (last == BODY1)
            last = BODY3;
        else
            last = BODY1;
    } else if (path == "/test_post") {
        if (requestHeaders["Content-Type"] != "application/x-www-form-urlencoded") {
            SPRTF("%s: bad content type: '%s' - return 400\n", module, requestHeaders["Content-Type"].c_str());
                sendErrorResponse(400, true, "bad content type");
                return;
        }
            
        requestContentLength = strutils::to_int(requestHeaders["Content-Length"]);
        setByteCount(requestContentLength);
        state = STATE_REQUEST_BODY;
    } else {
        SPRTF("%s: unknown path %s! Sending 404\n", module, path.c_str());
        sendErrorResponse(404, false, "");
    }
}

void HTTPServerChannel::closeAfterSending()
{
    state = STATE_CLOSING;
    closeWhenDone();
}
  
void HTTPServerChannel::receivedBody()
{
    state = STATE_IDLE;
    if (method == "POST") {
        parseArgs(buffer);
    }
        
    if (path == "/test_post") {
        if ( !((args["foo"] == "abc") || (args["bar"] == "1234") || (args["username"] == "johndoe")) ) {
            SPRTF("%s: sending 400, bad argument\n", module);
            sendErrorResponse(400, true, "bad arguments");
            return;
        }
            
        stringstream d;
        d << "HTTP/1.1 " << 204 << " " << reasonForCode(204) << "\r\n";
        d << "\r\n"; // final CRLF to terminate the headers
        SPRTF("%s: returning %s\n", module, d.str().c_str());
        push(d.str().c_str());
            
    }
}

void HTTPServerChannel::sendErrorResponse(int code, bool close, string content)
{
    stringstream headerData;
    headerData << "HTTP/1.1 " << code << " " << reasonForCode(code) << "\r\n";
    headerData << "Content-Length:" << content.size() << "\r\n";
    headerData << "\r\n"; // final CRLF to terminate the headers
    SPRTF("%s: sending header %s\n", module, headerData.str().c_str());
    push(headerData.str().c_str());
    SPRTF("%s: sending content %s\n", module, content.c_str());
    push(content.c_str());
        
    if (close) {
        closeWhenDone();
    }
}

    
    
string HTTPServerChannel::reasonForCode(int code) 
{
    switch (code) {
        case 200: return "OK";
        case 204: return "no content";
        case 400: return "bad argument";
        case 404: return "not found";
        default: return "unknown code";
    }
}

void HTTPServer::handleAccept (void)
{
    simgear::IPAddress addr ;
    int handle = accept ( &addr ) ;
    SPRTF("%s: did accept from %s:%d\n", module,
        addr.getHost(), addr.getPort() );
    HTTPServerChannel *chan = new HTTPServerChannel();
    chan->setHandle(handle);
    _poller.addChannel(chan);
}

HTTPServer::HTTPServer()
{
    ok = false;
    acc = false;
}

HTTPServer::HTTPServer( const char *addr, int port )
{ 
    acc = false;
    ok = open();
    if (!ok) {
        SPRTF("%s: Open sockets FAILED\n", module );
        return;
    }
    if ( bind(addr, port) ) { // addr, port
        ok = false;
        PERROR("bind failed");
    }
    listen(5);
    _poller.addChannel(this);
}

HTTPServer::~HTTPServer()
{
    SPRTF("%s: HTTPServer::~HTTPServer()\n", module );
}


/////////////////////////////////////////////////////////////////////////////////////

// init the http server
// return 0 = success, else an error inidicator
static HTTPServer *server = 0;

int http_init( const char *addr, int port )
{
    server = new HTTPServer(addr,port);
    if (!server->ok) {
        SPRTF("%s: Server FAILED! Aborting...\n", module );
        return 1;
    }
    return 0;
}

// poll for http events - uses select with ms timeout
// what is simgear default, and how to change it???
void http_poll(int timeout_ms)
{
    if (server)
        server->poll();
}


// close the http server
void http_close()
{
    // TODO: this causes an assert - vector iterators incompatible ?????????????
    //if (server)
    //    delete server;
    //server = 0;
}




// eof
