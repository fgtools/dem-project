// dem_utils.hxx
#ifndef _DEM_UTILS_HXX_
#define _DEM_UTILS_HXX_
#include <string>
#include "utils.hxx"

extern int check_me();

typedef struct tagTILELIMITS {
    double max_lat, min_lat, max_lon, min_lon;
} TILELIMITS, *PTILELIMITS;

typedef struct tagELEVTESTS {
    const char *name;
    double lat,lon;
    short expected;
}ELEVTESTS, *PELEVTESTS;

extern ELEVTESTS elevtests[];

// Location              zoom level
// 
typedef struct tagLOCZOOM {
    std::string loc;
    double lat,lon;
    short exp;
    vSTG files;
}LOCZOOM, *PLOCZOOM;

typedef std::vector<LOCZOOM> vLZ;

#ifndef EOL
#define EOL << std::endl
#endif

typedef struct tagMPOS {
    double lat,lon;
    short elev;
}MPOS, *PMPOS;


extern void write_html_view(vLZ &vlz, int zoom_min, int zoom_max, const char *title = 0,
    const char *html_file = 0 );
extern char *get_tile_path(const char*file);
extern char *get_web_path(const char *path);
extern char *get_cache_file_name( const char *slippy_path, bool noext = false );
extern std::string get_image_file_path( char *cp );

// DEFAULT DIRECTORIES
// 1"
extern const char *srtm1_dir;   // = "F:\\data\\dem1\\srtm1";
// 3"
extern const char *ferr3_dir;   //  = "F:\\data\\dem3\\hgt";
extern const char *usgs3_dir;   // = "F:\\data\\dem3\\usgs";
// 30"
extern const char *ferr30_dir;  // = "F:\\data\\srtm30_plus"; // Bathymetry.srtm
extern const char *usgs30_dir;  // = "F:\\data\\SRTM30\\data"; // 27 USGS DEM3, like E140N90.DEM
extern const char *ncdf30_dir;  // = "D:\\SRTM\\scripps\\srtm30\\grd";
extern const char *stopo30_fil; // = "D:\\SRTM\\scripps\\topo30\\topo30"; // single world file
extern const char *globe30_dir; // = "F:\\data\\dem3\\SRTM30\\all10";

// INDIVIDUAL FILES
extern const char *gtopo_a;
extern const char *dead_sea; // "F:\\data\\dem3\\hgt\\N31E035.hgt";
extern const char *usgs_dead_sea; // "F:\\data\\dem3\\usgs\\N31E035.hgt";
// Mount Lamlam - 406 meters (1,332 ft) - 13.33861,144.662778 13 20 19 N 144 39 46 E
extern const char *guam;    // = "F:\\data\\dem3\\hgt\\N13E144.hgt";
// Tristan da Cunha, Peak -  2,062 metres (6,765 ft) - -37.09444444,-12.288611111
extern const char *tristan;        // = "F:\\data\\dem3\\hgt\\S38W013.hgt";
extern const char *mt_everest;     // =  "F:\\data\\dem3\\hgt\\N27E086.hgt";
extern const char *netcdf1;     // = "D:\\SRTM\\scripps\\srtm30\\grd\\e020n40.nc";
extern const char *netcdf2;     // = "D:\\SRTM\\scripps\\srtm30\\grd\\w100s10.nc"; // Aconcagua

extern void dem_path_help();
extern std::string get_path_help_str();

extern int set_new_directory( char *nm_path, bool verb = true );

// expose the image output directory
extern const char *get_image_out_dir(); // { return out_dir; }
extern void set_image_out_dir(const char *dir); // { out_dir = strdup(dir); }


extern int do_byte_swap;
extern int set_out_files( std::string in_file );
extern char *get_out_bmp(); // NOTE: This checks for OVERWRITE, if no_overwrite == true
extern char *get_out_png(); // NOTE: This checks for OVERWRITE, if no_overwrite == true
extern char *get_curr_out_png(); // just return current value
extern char *get_curr_out_bmp(); // just return current value

extern bool no_image_overwrite; // default == true;
extern char *check_overwrite(char *file);


extern void set_out_bmp( char *file );
extern void set_out_png( char *file );
extern void set_slippy_file_name( char *file );
extern void set_endian( bool verb = false );

#endif // #ifndef _DEM_UTILS_HXX_
// eof - dem_utils.hxx

