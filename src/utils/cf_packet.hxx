// cf_packet.hxx
#ifndef _CF_PACKET_HXX_
#define _CF_PACKET_HXX_
#include <vector>
#include "mpMsgs.hxx"

enum Packet_Type {
    pkt_Invalid,    // not used
    pkt_InvLen1,    // too short for header
    pkt_InvLen2,    // too short for position
    pkt_InvMag,     // Magic value error
    pkt_InvProto,   // not right protocol
    pkt_InvPos,     // linear value all zero - never seen one!
    pkt_InvHgt,     // alt <= -9990 feet - always when fgs starts
    pkt_InvStg1,    // no callsign after filtering
    pkt_InvStg2,    // no aircraft
    // all ABOVE are INVALID packets
    pkt_First,      // first pos packet
    pkt_Pos,        // pilot position packets
    pkt_Chat,       // chat packets
    pkt_Other,      // should be NONE!!!
    pkt_Discards,   // valid, but due to no time/dist...
    pkt_Max
};

extern Packet_Type Deal_With_Packet( char *packet, int len );
extern Packet_Type Deal_With_Packet( char *packet, int len );
extern int Write_JSON();
extern int Get_JSON( char **pbuf );

typedef struct tagSIMTIME {
    double  sim_time, prev_sim_time, first_sim_time; // sim time from packet
    double  exp_sim_time;
    char    callsign[MAX_CALLSIGN_LEN];
    char    aircraft[MAX_MODEL_NAME_LEN];
    bool    expired, updated;
}SIMTIME, *PSIMTIME;

typedef std::vector<SIMTIME> vSIMTM;
typedef std::vector<size_t> vSZT;

extern bool Get_Sim_Time( char *packet, int len, PSIMTIME pst );
extern int Expire_Pilots2( vSIMTM & vst );


#endif // #ifndef _CF_PACKET_HXX_
// eof - cf_packet.hxx

