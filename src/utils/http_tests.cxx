// http_tests.cxx

#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <fstream>
#include <string>
#ifdef _MSC_VER
#include <WinSock2.h>
#else // !_MSC_VER
#include <unistd.h>
#include <stdio.h> // for perror()
#include <errno.h> // for perror()
#include <string.h> // for strdup()
#endif // _MSC_VER y/n

#ifdef USE_SIMGEAR_LIB
#include <simgear/io/raw_socket.hxx>
#include <simgear/io/sg_netChannel.hxx>
#include <simgear/io/sg_netBuffer.hxx>
#include <simgear/io/sg_netChat.hxx>
#include <simgear/misc/strutils.hxx>
#include <simgear/timing/timestamp.hxx>
#endif // USE_SIMGEAR_LOB
#include <time.h>

#include "demFile.hxx"
#include "sprtf.hxx"
#include "utils.hxx"
#include "pollkbd.hxx"
#include "http_utils.hxx"
#include "json_load.hxx"
#include "dir_utils.hxx"
#include "dem_utils.hxx"
#include "cache_utils.hxx"

#ifdef _MSC_VER
#pragma comment (lib, "Ws2_32.lib")
//#pragma comment (lib, "Mswsock.lib")
//#pragma comment (lib, "AdvApi32.lib")
//#pragma comment (lib, "Winmm.lib") // __imp__timeGetTime@0
#endif

// avoid, if poss
// using namespace std;

#ifndef DEF_SERVER_PORT
#define DEF_SERVER_PORT 5555
#endif
#ifndef DEF_SERVER_ADDRESS
#define DEF_SERVER_ADDRESS		"127.0.0.1"
#endif
#ifndef DEF_SLEEP_MS
#define DEF_SLEEP_MS 100
#endif
// for select timeout
#ifndef DEF_TIMEOUT_MS
#define DEF_TIMEOUT_MS 500
#endif

#ifndef SLEEP
#ifdef _MSC_VER
#define SLEEP(x) Sleep(x)
#else // !_MSC_VER
#define SLEEP(x) usleep( x * 1000 )
#endif // _MSC_VER y/n
#endif // SLEEP

static const char *module = "http_tests";

static int port = DEF_SERVER_PORT;
static const char *servaddr = DEF_SERVER_ADDRESS;
// options
static int sleep_ms = DEF_SLEEP_MS;
static int timeout_ms = DEF_TIMEOUT_MS;

#ifdef USE_SIMGEAR_LIB
using namespace simgear;
#endif // USE_SIMGEAR_LIB

static const char *log_file = "temphttp.log";

static void give_help( char *name )
{
    printf("\n");
    printf("%s - version 1.0.0\n", name);
    printf("\n");
    printf("Usage: %s [options]\n", name);
    printf("\n");
    printf("Options:\n");
    //      123456789112345678921
    printf(" --help   (-h or -?) = This help and exit(2)\n");
    printf(" --cache <num>  (-c) = Set image cache MB size. (def=%dMB)\n", (int)max_cache_mb);
    printf(" --port <num>   (-p) = Set port (def=%d)\n", port);
    printf(" --json <file>  (-j) = Set the json file to use. (def=%s)\n", get_file_name((char *)json_file));
#ifdef USE_SIMGEAR_LIB
    printf(" --addr <ip>    (-a) = Set the IP address. (def=%s)\n", servaddr );
#endif
    printf(" --log <file>   (-l) = Set log file. (def=%s, in CWD)\n", log_file);
    printf(" --sleep <ms>   (-s) = Set milliseconds sleep in loop. 0 for none. (def=%d)\n", sleep_ms);
    printf(" --timeout <ms> (-t) = Set milliseconds timeout for select(). (def=%d)\n", timeout_ms);
    dem_path_help();
    printf("\n");
    printf("Will establish a HTTP server on the addr:port, and respond to GET with '/paths'\n");
    printf("\n");
    show_valid_paths(20);
    printf("All others will return 400 - command error, or 404 - file not found\n");
    printf("\n");
    printf("All output will be written to stdout, and the log file.\n");
    printf("The current json file [%s] %s\n", json_file, 
        ((is_file_or_directory((char *)json_file) == 1) ? "appears ok." : "IS NOT VALID!") );
    printf("Will exit on ESC keying, if in foreground\n");
}

static int parse_commands( int argc, char **argv )
{
    int iret = 0;
    int i, c, i2;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help( get_file_name(argv[0]) );
                return 2;
#ifdef USE_SIMGEAR_LIB
            case 'a':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    servaddr = strdup(sarg);
                    SPRTF("%s: Set server address to %s\n", module, servaddr);
                } else {
                    SPRTF("%s: Expected IP address value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
#endif
            case 'c':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    max_cache_mb = atoi(sarg);
                    SPRTF("%s: Set cache size to %dMB\n", module, max_cache_mb);
                } else {
                    SPRTF("%s: Error: Expected MB cahce size to follow!\n", module );
                    goto Bad_CMD;
                }
                break;
            case 'd':
                if (i2 < argc) {
                    i++;
                    iret = set_new_directory(argv[i]);
                    if (iret)
                        return iret;
                } else {
                    SPRTF("%s: Error: Expected name:directory to follow!\n", module );
                    goto Bad_CMD;
                }
                break;
            case 'l':
                i++;    // log file already checked and handled
                break;
            case 'j':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    json_file = strdup(sarg);
                    if (is_file_or_directory((char *)json_file) != 1) {
                        SPRTF("%s: Unable to 'stat' file [%s[!\n", module, json_file);
                        SPRTF("%s: Check name and location\n");
                        return 1;
                    }
#ifdef HAVE_MEMORY_GALORE
                    load_json_file(json_file,vJson);
#else // !HAVE_MEMORY_GALORE
                    load_json_file(json_file);
#endif // HAVE_MEMORY_GALORE y/n
                    if (max_json == 0) {
                        SPRTF("%s: json file [%s] yielded NO json!\n", module, json_file);
                        return 1;
                    }

                    SPRTF("%s: Loaded json file [%s] with %d records.\n", module, json_file,
                        (int)max_json);

                } else {
                    SPRTF("%s: Expected file name to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    port = atoi(sarg);
                    SPRTF("%s: Set port to %d\n", module, port);
                } else {
                    SPRTF("%s: Expected port value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    sleep_ms = atoi(sarg);
                    SPRTF("%s: Set sleep to %d ms\n", module, sleep_ms);
                } else {
                    SPRTF("%s: Expected ms sleep value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    timeout_ms = atoi(sarg);
                    SPRTF("%s: Set select timeout to %d ms\n", module, timeout_ms);
                } else {
                    SPRTF("%s: Expected ms timeout value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            default:
                goto Bad_CMD;
                break;
            }
        } else {
Bad_CMD:
            SPRTF("%s: Unknown command %s\n", module, arg );
            return 1;
        }
    }

    return iret;
}

int check_log_file(int argc, char **argv)
{
    int i,fnd = 0;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            if (*sarg == 'l') {
                if ((i + 1) < argc) {
                    i++;
                    sarg = argv[i];
                    log_file = strdup(sarg);
                    fnd = 1;
                } else {
                    printf("%s: Expected a file name to follow [%s]\n", module, arg );
                    return 1;
                }
            }
        } else {

        }
    }
    set_log_file((char *)log_file, false);
    if (fnd)
        SPRTF("%s: Set LOG file to [%s]\n", module, log_file);
    return 0;
}

int main( int argc, char **argv )
{
    int res;
    time_t curr,next;

    res = check_log_file(argc,argv);
    if (res)
        return res;

    res = parse_commands(argc,argv);
    if (res)
        return res;

#ifdef HAVE_MEMORY_GALORE
    load_json_file(json_file,vJson);
#else
    load_json_file(json_file);
#endif

    if (http_init(servaddr,port)) {
        SPRTF("%s: Server FAILED! Aborting...\n", module );
        return 1;
    }
    SPRTF("%s: Waiting on %s:%d... ESC to exit\n", module, servaddr, port );

    curr = time(0);
    while (1) {
        res = test_for_input();
        if (res) {
            if (res == 0x1b) {
                SPRTF("%s: Got ESC exit key...\n", module );
                break;
            } else {
                SPRTF("%s: Got unknown key %X!\n", module, res );
            }
        }
        http_poll(timeout_ms);    // server->poll();
        next = time(0);
        if (next != curr) {
            curr = next;
            // any one seconds tasks???
#ifdef DO_JSON_BUMP
            bump_json();
#endif
            if (sleep_ms > 0) {
                SLEEP(sleep_ms);
            }
        }
    }

    http_close();   // delete server;

    return 0;
}

// eof - http_tests.cxx
