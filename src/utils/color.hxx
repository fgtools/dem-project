// color.hxx
#ifndef _COLOR_HXX_
#define _COLOR_HXX_
#include <string>

#ifndef byte
typedef unsigned char byte;
#endif

#ifndef MAX_TABLE_ELEV
#define MAX_TABLE_ELEV 9000
#endif
#ifndef MIN_TABLE_ELEV
#define MIN_TABLE_ELEV -11000
#endif

#ifdef _WINDOWS_
#define rgb RGB
#define getRVal GetRValue
#define getGVal GetGValue
#define getBVal GetBValue
#else
#define rgb(r,g,b) ((unsigned int)(((byte)(r)|((unsigned short)((byte)(g))<<8))|(((unsigned int)(byte)(b))<<16)))
#define loByte(w)           ((byte)(((unsigned int)(w)) & 0xff))
#define hiByte(w)           ((byte)((((unsigned int)(w)) >> 8) & 0xff))

#define getRVal(rgb)      (loByte(rgb))
#define getGVal(rgb)      (loByte(((unsigned short)(rgb)) >> 8))
#define getBVal(rgb)      (loByte((rgb)>>16))
#endif

typedef struct tagHGT2COLOR {
    short min;
    short max;
    unsigned int color;
    unsigned int freq;
    unsigned int res1;
    unsigned int res2;
    unsigned int res3;
}HGT2COLOR, *PHGT2COLOR;

#define MAX_COLORS 26
#define ZERO_COLOR 20

extern void clear_used_colors();
extern unsigned int get_color( short elev, size_t coff );
extern unsigned int get_BGR_color( short elev );
extern short set_void_elevation(short nodata, bool v = true);   // { elev_void = nodata; }
extern unsigned int set_void_elev_color(unsigned int colr, bool v = true ); // set color, return current
extern short set_zero_elevation(short zero);     // { elev_zero = zero; }
extern PHGT2COLOR get_current_table();
extern std::string get_color_http();

extern void out_used();

// just a testing of the gen_color_range service
extern int test_gen_colors();

typedef unsigned int (*GETCOLOR)(short); // indirect service

extern GETCOLOR set_get_color(GETCOLOR ngc, bool verb = false);
extern GETCOLOR set_default_get_color(bool verb = false);
extern GETCOLOR set_linear_get_color(bool verb = false);
// default service
extern unsigned int get_per_color_table( short elev );
// linear color generator
extern unsigned int get_generated_color( short elev );
// the indirect service
extern GETCOLOR getColor;
extern bool is_per_table_color(); // is per the default generated color table

#endif // _COLOR_HXX_
// eof
