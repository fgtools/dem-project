/*\
 * json_load.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <vector>
#include <string>
#include <stdio.h>
#ifndef _MSC_VER
#include <stdlib.h> // for malloc(), ...
#include <string.h> // for strncmp(), ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "dir_utils.hxx"
#include "json_load.hxx"

static const char *module = "json_load";

// implementation
const char *json_file = "C:\\Users\\user\\Downloads\\logs\\fgx-cf\\all.json";
static bool dn_json_load = false;
size_t max_json = 0;
size_t next_json = 0;

#ifdef HAVE_MEMORY_GALORE
static vSTG vJson;
static size_t json_file_size = 0;
static void bump_json()
{
    if (next_json < max_json) {
        next_json++;
        if (next_json >= max_json)
            next_json = 0;
    }
}

int load_json_file(const char *in_file, vSTG &vs)
{
    if (dn_json_load) {
        return (int)max_json;
    }
    if (is_file_or_directory((char *)in_file) != 1) {
        return 0;
    }
    json_file_size = get_last_file_size();
    std::ifstream file(in_file);
    std::string str;
    std::string file_contents;
    size_t lines = 0;
    int getline_cnt = 0;
    SPRTF("%s: Moment, reading file of %s bytes...\n", module,
        get_NiceNumberStg(json_file_size));
    while (std::getline(file, str))
    {
        getline_cnt++;
        if (str.find("{\"success\":true") == 0) {
            if (file_contents.size() > 0) {
                vs.push_back(file_contents);
                lines++;
            }
            file_contents = str;
        } else if (file_contents.size()) {
            file_contents.push_back('\n');
            file_contents += str;
        }
    }
    max_json = vs.size();
    SPRTF("%s: Got %d (%d) JSON sets... from %s bytes, %d line reads\n", module, (int)lines,
        (int)max_json, (int)get_NiceNumberStg(json_file_size), getline_cnt );
    dn_json_load = true;
    return lines;
}
#else // !HAVE_MEMORY_GALORE

typedef struct tagJSONBLKS {
    long bgnoff, endoff;
}JSONBLKS, *PJSONBLKS;

typedef std::vector<JSONBLKS> vJBLKS;

static vJBLKS vJsonBlks;

static int maximumLineLength = 1024;
static char *lineBuffer = 0;
static FILE *fp = 0;
static size_t json_file_size;
///////////////////////////////////////////////////////////////////////
// I like re-invetning wheels ;=))
// my own readLine(FILE *)
char *readLine(FILE *file, long *pbl, long *pel) 
{
    if (file == 0) {
        SPRTF("%s: File pointer is null!\n", module);
        return 0;
    }
    if (feof(file)) {
        SPRTF("%s: Reached EOF!\n", module);
        return 0;
    }

    if (lineBuffer == 0) {
        lineBuffer = (char *)malloc(sizeof(char) * maximumLineLength);
        if (!lineBuffer) {
            SPRTF("%s: memory allocation FAILED on %d bytes\n", module, maximumLineLength);
            return 0;
        }
    }

    int count = 0;
    long offset = ftell(file);
    int ch = getc(file);
    while ((ch <= ' ') && (ch != EOF)) {
        offset = ftell(file);
        ch = getc(file);
    }
    if (ch == EOF) {
        SPRTF("%s: Read to EOF!\n", module);
        return 0;
    }

    while ((ch != '\n') && (ch != '\r') && (ch != EOF)) {
        if (count >= maximumLineLength) {
            maximumLineLength += 1024;
            lineBuffer = (char *)realloc(lineBuffer, maximumLineLength);
            if (!lineBuffer) {
                SPRTF("%s: memory reallocation FAILED on %d bytes\n", module, maximumLineLength);
                return 0;
            }
        }
        lineBuffer[count++] = ch;
        ch = getc(file);
    }

    lineBuffer[count] = 0;
    if (pbl)
        *pbl = offset;   // pass back BEGINNING of line offset
    if (pel)
        *pel = ftell(file);
    return lineBuffer;
}

int load_json_file(const char *in_file)
{
    if (dn_json_load) {
        return (int)max_json;
    }
    if (is_file_or_directory((char *)in_file) != 1) {
        return 0;
    }
    json_file_size = get_last_file_size();
    fp = fopen(in_file,"rb");
    if (!fp) {
        SPRTF("%s: Failed to 'open' file [%s]\n", module, in_file);
        return 0;
    }
    char *cp;
    long boff,eoff;
    long bgnoff,endoff;
    //std::string s;
    JSONBLKS jb;
    size_t lines = 0;
    int getline_cnt = 0;
    int cnt = 0;
    SPRTF("%s: Moment, reading file of %s bytes...\n", module,
        get_NiceNumberStg(json_file_size));
    double bgn = get_seconds();
    while (( cp = readLine(fp,&boff,&eoff) ) != 0 ) {
        getline_cnt++;
        if (strncmp(cp,"{\"success\":",11) == 0) {
            //if (s.size()) {
            if (cnt) {
                jb.bgnoff = bgnoff;
                jb.endoff = endoff;
                vJsonBlks.push_back(jb);
                lines++;
            }
            bgnoff = boff;
            endoff = eoff;
            //s = cp;
            cnt++;
        } else {
            //s.push_back('\n');
            //s += cp;
            endoff = eoff;
        }
    }
    // maybe missing the last block

    max_json = vJsonBlks.size();
    SPRTF("%s: Got %d (%d) JSON sets... %s line reads in %s\n", module, (int)lines,
        (int)max_json, get_NiceNumberStg(getline_cnt), get_elapsed_stg(bgn) );
    dn_json_load = true;
    return lines;
}

bool get_next_block( std::string &j )
{
    JSONBLKS jb;
    jb = vJsonBlks[next_json];
    int res = fseek(fp,jb.bgnoff,SEEK_SET);
    if (res) {
        SPRTF("%s: Failed in seek to %ld\n", module, jb.bgnoff);
        return false;
    }
    char *cp;
    long boff,eoff;
    // long bgnoff,endoff;
    int cnt = 0;
    while (( cp = readLine(fp,&boff,&eoff) ) != 0 ) {
        if (strncmp(cp,"{\"success\":",11) == 0) {
            //if (j.size()) {
            if (cnt) {
                return true;
            }
            //bgnoff = boff;
            //endoff = eoff;
            j = cp;
            cnt++;
            if (eoff >= jb.endoff) {
                return true;
            }
        } else {
            j.push_back('\n');
            j += cp;
            //endoff = eoff;
            if (eoff >= jb.endoff) {
                return true;
            }
        }
    }
    return false;
}

#endif // HAVE_MEMORY_GALORE y/n

// eof = json_load.cxx
