// color.cxx

// This is a topic on its own

#include <stdio.h>
#include <vector>
#include <sstream>
#ifndef _MSC_VER
#include <stdlib.h> // for exit(), ...
#include <string.h> // for memset(), ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "dem_utils.hxx"
#include "color.hxx"


#define ELEV_VOID -32768

static const char *module = "color";

bool set_color_expansion = true;

short elev_void = ELEV_VOID;
short elev_zero = 0;

HGT2COLOR hgtVoid[1] = {
        { ELEV_VOID,ELEV_VOID, 0, 0, 0, 0, 0 }
};

// add grey above 5000 meters - see SNOWLINE table
// TODO: Maybe this should be less than 5000, maybe 3000???
HGT2COLOR hgtColor3[MAX_COLORS] = {
    // =====================================
    { 7549, MAX_TABLE_ELEV, 0xFFFFFF, 0, 0, 0, 0 },
    { 6493, 7548	, 0xCCCCCC, 0, 0, 0, 0 },
    { 5662, 6492    , 0x999999, 0, 0, 0, 0 },
    { 4977, 5661	, 0x666666, 0, 0, 0, 0 },
    // above 5000 - go gray to white
    { 4395, 4976	, 0xFF0090, 0, 0, 0, 0 },
    { 3888, 4394	, 0xFF0060, 0, 0, 0, 0 },
    { 3439, 3887	, 0xFF0040, 0, 0, 0, 0 },
    { 3037, 3438	, 0xFF0010, 0, 0, 0, 0 },
    { 2672, 3036	, 0xFF0000, 0, 0, 0, 0 },
    { 2338, 2671	, 0xFF1000, 0, 0, 0, 0 },
    // ------------------------------------
    { 2031, 2337	, 0xFF1800, 0, 0, 0, 0 },
    { 1746, 2030	, 0xFF2000, 0, 0, 0, 0 },
    { 1480, 1745	, 0xFF2800, 0, 0, 0, 0 },
    { 1232, 1479	, 0xFF3800, 0, 0, 0, 0 },
    {  998, 1231	, 0xFF4000, 0, 0, 0, 0 },
    {  777,  997	, 0xFF4C00, 0, 0, 0, 0 },
    {  568,  776	, 0xFF7E00, 0, 0, 0, 0 },
    { 370,  567	, 0xFFB000, 0, 0, 0, 0 },
    { 181, 369	, 0xFFFF00, 0, 0, 0, 0 },
    { 1, 180	, 0x00FF00, 0, 0, 0, 0 },
    // ======================================
    { 0, 0	, 0x0000FF, 0, 0, 0, 0 },
    // ======================================
    { -100, -1	 , 0x3300FF, 0, 0, 0, 0 },
    { -200, -101 , 0x6600FF, 0, 0, 0, 0 },
    { -400, -201 , 0x9900FF, 0, 0, 0, 0 },
    //{ -800, -401 , 0xCC00FF, 0, 0, 0, 0 },
    //{ -11000,-801, 0xFF00FF, 0, 0, 0, 1 }
    { -2000, -401 , 0xCC00FF, 0, 0, 0, 0 },
    { MIN_TABLE_ELEV,-2001, 0x000000, 0, 0, 0, 1 }  // this headed towards BLACK depths looks GOOD!!!
};

void zero_freq_in_fixed_table()
{
    PHGT2COLOR pct = hgtColor3;
    int i;
    for (i = 0; i < MAX_COLORS; i++) {
        pct->freq = 0;
        pct++;
    }
}

// an idea to exxpand the table
bool done_table_expansion = false;
static PHGT2COLOR pct = hgtColor3;
typedef struct tagMAXMINCOLOR {
    int max, min;
    unsigned int colr;
}MAXMINCOLOR, *PMAXMINCOLOR;

typedef std::vector<MAXMINCOLOR> vMMC;

static int pc_table_cnt = MAX_COLORS;
static int pc_zero_cnt = ZERO_COLOR;

void do_table_expansion()
{
    bool verb = false;
    PHGT2COLOR ct = hgtColor3;
    int cnt = 0;
    int ranges = 0;
    int rng;
    //int last;
    unsigned char r,g,b;
    unsigned char nr,ng,nb;
    unsigned char pr,pg,pb,tmp;
    int rd,gd,bd,md,i;
    unsigned int colr;
    //unsigned int prev;
    int rdec,gdec,bdec;

    short cmin,cmax,pmin,pmax;
    short nxcmin, nxcmax;
    short nmin,nmax;

    int frng;
    int colrdiff = 0;
    double unit;
    colr = 0;
    rng = 0;
    r = g = b = 0;
    cmin = cmax = 0;
    vMMC vmmc;
    MAXMINCOLOR mmc;
    vMMC table;
    int max, ii;
    if (!set_color_expansion)
        return;

    while (1) {
        pr = getRVal(colr);
        pg = getGVal(colr);
        pb = getBVal(colr);
        //prev = colr;
        pmin = cmin;
        pmax = cmax;
        //last = rng;
        // stop at the ZERO ENTRY
        if (ct->min == 0)
            break;
        cmax = ct->max;
        cmin = ct->min;
        rng = (cmax - cmin);
        colr = ct->color;
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        if (cnt) {
            // how many color steps are there between this color and the previous
            // keeping the same relative colors...
            if (r > pr) {
                rdec = 1;
                rd = r - pr;
            } else {
                rdec = 0;
                rd = pr - r;
            }
            if (g > pg) {
                gdec = 1;
                gd = g - pg;
            } else {
                gdec = 0;
                gd = pg - g;
            }
            if (b > pb) {
                bdec = 1;
                bd = b - pb;
            } else {
                bdec = 0;
                bd = pb - b;
            }
            if (rd > gd)
                md = rd;
            else
                md = gd;
            if (bd > md)
                md = bd;
            // range is from cmin to pmax
            if (cnt == 1) {
                nxcmin = cmin;
                nxcmax = pmax;
            } else {
                nxcmin = cmin;
                //nxcmax = cmax;
                nxcmax = pmin;
            }

            frng = nxcmax - nxcmin;
            if (verb) SPRTF("%s: maxd %d, from %02X%02X%02X to %02X%02X%02X range %d-%d-%d-%d, %d steps (%d,%d)\n", module,
                md,
                r,g,b,
                pr,pg,pb,
                cmin, cmax, pmin, pmax, frng,
                nxcmin, nxcmax);
            colrdiff += md;
            unit = ((double)frng / (double)(md+1));
            nmin = nxcmin;
            nmax = nmin + (int)(unit + 0.5);
            if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, r, g, b );
            vmmc.clear();
            mmc.max = nmax;
            mmc.min = nmin;
            mmc.colr = rgb(b,g,r);
            vmmc.push_back(mmc);
            for (i = 1; i < md; i++) {
                nmin = nxcmin + (int)((unit * (double)i) + 0.5);
                nmax = nmin + (int)(unit + 0.5);
                nr = r;
                if (rd) {
                    // move r to towards pr by an incremental amount
                    tmp = (int)((((double)rd / (double)md) * (double)i) + 0.5);
                    if (rdec)
                        nr = r - tmp;
                    else
                        nr = r + tmp;
                }
                ng = g;
                if (gd) {
                    // move g to towards pg by an incremental amount
                    tmp = (int)((((double)gd / (double)md) * (double)i) + 0.5);
                    if (gdec)
                        ng = g - tmp;
                    else
                        ng = g + tmp;
                }
                nb = b;
                if (bd) {
                    // move b to towards pb by an incremental amount
                    tmp = (int)((((double)bd / (double)md) * (double)i) + 0.5);
                    if (bdec)
                        nb = b - tmp;
                    else
                        nb = b + tmp;

                }
                if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, nr, ng, nb );
                mmc.max = nmax;
                mmc.min = nmin;
                mmc.colr = rgb(nb,ng,nr);
                vmmc.push_back(mmc);
            }
            nmax = nxcmax;
            nmin = nmax - (int)(unit + 0.5);
            if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, pr, pg, pb );
            mmc.max = nmax;
            mmc.min = nmin;
            mmc.colr = rgb(pb,pg,pr);
            vmmc.push_back(mmc);
            // DRAT, now need to reverse order
            max = (int)vmmc.size();
            for (ii = max - 1; ii >= 0; ii--) {
                mmc = vmmc[ii];
                if ((mmc.max == 0)&&(mmc.min == 0)) {
                    SPRTF("%s: Ugh! A zero entry!\n",module);
                    check_me();
                    continue;
                }
                table.push_back(mmc);
            }

            if (md && verb)
                SPRTF("\n");
        }
        ranges += rng;

        ct++;
        cnt++;
    }

    if (verb) SPRTF("%s: Adding the zero and negative ranges...\n", module );
    while (1) {
        // store the previous
        pr = getRVal(colr);
        pg = getGVal(colr);
        pb = getBVal(colr);
        //prev = colr;
        pmin = cmin;
        pmax = cmax;

        cmax = ct->max;
        cmin = ct->min;
        rng = (cmax - cmin);
        colr = ct->color;
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        if (cmax == 0) {
            // just ADD this zero value
            mmc.max = cmax;
            mmc.min = cmin;
            mmc.colr = rgb(b,g,r);
            table.push_back(mmc);
        } else {
            // scale between previous and this current
            if (r > pr) {
                rdec = 1;
                rd = r - pr;
            } else {
                rdec = 0;
                rd = pr - r;
            }
            if (g > pg) {
                gdec = 1;
                gd = g - pg;
            } else {
                gdec = 0;
                gd = pg - g;
            }
            if (b > pb) {
                bdec = 1;
                bd = b - pb;
            } else {
                bdec = 0;
                bd = pb - b;
            }
            if (rd > gd)
                md = rd;
            else
                md = gd;
            if (bd > md)
                md = bd;
            // range is from cmin to pmax
            nxcmin = cmin;
            //nxcmax = cmax;
            nxcmax = pmin;

            frng = nxcmax - nxcmin;
            if (verb) SPRTF("%s: maxd %d, from %02X%02X%02X to %02X%02X%02X range %d-%d-%d-%d, %d steps (%d,%d)\n", module,
                md,
                r,g,b,
                pr,pg,pb,
                cmin, cmax, pmin, pmax, frng,
                nxcmin, nxcmax);
            colrdiff += md;
            unit = ((double)frng / (double)(md+1));
            nmin = nxcmin;
            nmax = nmin + (int)(unit + 0.5);
            if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, r, g, b );
            vmmc.clear();
            mmc.max = nmax;
            mmc.min = nmin;
            mmc.colr = rgb(b,g,r);
            vmmc.push_back(mmc);
            for (i = 1; i < md; i++) {
                nmin = nxcmin + (int)((unit * (double)i) + 0.5);
                nmax = nmin + (int)(unit + 0.5);
                nr = r;
                if (rd) {
                    // move r to towards pr by an incremental amount
                    tmp = (int)((((double)rd / (double)md) * (double)i) + 0.5);
                    if (rdec)
                        nr = r - tmp;
                    else
                        nr = r + tmp;
                }
                ng = g;
                if (gd) {
                    // move g to towards pg by an incremental amount
                    tmp = (int)((((double)gd / (double)md) * (double)i) + 0.5);
                    if (gdec)
                        ng = g - tmp;
                    else
                        ng = g + tmp;
                }
                nb = b;
                if (bd) {
                    // move b to towards pb by an incremental amount
                    tmp = (int)((((double)bd / (double)md) * (double)i) + 0.5);
                    if (bdec)
                        nb = b - tmp;
                    else
                        nb = b + tmp;

                }
                if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, nr, ng, nb );
                mmc.max = nmax;
                mmc.min = nmin;
                mmc.colr = rgb(nb,ng,nr);
                vmmc.push_back(mmc);
            }
            nmax = nxcmax;
            nmin = nmax - (int)(unit + 0.5);
            if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, pr, pg, pb );
            mmc.max = nmax;
            mmc.min = nmin;
            mmc.colr = rgb(pb,pg,pr);
            vmmc.push_back(mmc);
            // DRAT, now need to reverse order
            max = (int)vmmc.size();
            for (ii = max - 1; ii >= 0; ii--) {
                mmc = vmmc[ii];
                if ((mmc.max == 0)&&(mmc.min == 0)) {
                    SPRTF("%s: Ugh! A zero entry!\n",module);
                    check_me();
                    continue;
                }
                table.push_back(mmc);
            }

            if (md && verb)
                SPRTF("\n");
        }
        ranges += rng;

        if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", cmin, cmax, r, g, b );
        cnt++;
        if (ct->res3 == 1)  // NOTE: special terminator
            break;
        ct++;
    }

    max = (int)table.size();
    if (verb) SPRTF("%s: Output of expanded table of %d elements\n", module, max);
    nxcmin = nxcmax = 0;
    cnt = 0;
    for (ii = 0; ii < max; ii++) {
        cmin = nxcmin;
        cmax = nxcmax;
        mmc = table[ii];
        nxcmin = mmc.min;
        nxcmax = mmc.max;
        if ((nxcmin == cmin)&&
            (nxcmax == cmax))
            continue;
        colr = mmc.colr;
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nxcmin, nxcmax, r, g, b );
        cnt++;
    }

    if (verb) SPRTF("%s: Will build table with total %d records...\n", module, cnt );

    PHGT2COLOR phgtc = new HGT2COLOR[cnt];
    if (!phgtc) {
        SPRTF("%s: Memory allocation FAILED!\n");
        check_me();
        return;
    }
    // clear it all to zero
    memset(phgtc,0,sizeof(HGT2COLOR) * cnt);

    ct = phgtc; // using moving pointer
    cnt = 0;
    int zpos = 0;
    for (ii = 0; ii < max; ii++) {
        cmin = nxcmin;
        cmax = nxcmax;
        mmc = table[ii];    // get generated entry
        nxcmin = mmc.min;
        nxcmax = mmc.max;
        if ((nxcmin == cmin)&&
            (nxcmax == cmax)) {
            continue;
        }
        colr = mmc.colr;
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        // FIX20140502 - Wow how did this BUG continue
        //NOT ct->min = cmin; ct->max = cmax; BUT
        ct->min = nxcmin;
        ct->max = nxcmax;
        // =====================================================
        // TODO: One day MUST fix this stupid REVERSAL of COLORS
        // =====================================================
        ct->color = rgb(b,g,r);
        // ct->colr = colr;
        // SPRTF("min/max %d %d %02X%02X%02X\n", nxcmin, nxcmax, r, g, b );
        if ((cmin == 0)&&(cmax == 0))
            zpos = cnt;
        cnt++;
        ct++;
    }

    // *********************************
    // ready to esablish new table
    if (is_per_table_color()) {
        // only if using this color table
        SPRTF("%s: Expanded original table from %d entries to %d entries.\n", module, pc_table_cnt, cnt);
    } // else silent
    pc_table_cnt = cnt;
    pc_zero_cnt = zpos;
    pct = phgtc;
    // *********************************
    done_table_expansion = true;
}


PHGT2COLOR get_current_table() 
{
    if (!done_table_expansion) {
        done_table_expansion = true;
        do_table_expansion();
    }
    return pct; 
}

short set_void_elevation(short nodata, bool v) 
{ 
    short curr = elev_void;
    elev_void = nodata;
    hgtVoid[0].max = nodata;
    hgtVoid[0].min = nodata;
    if (v) SPRTF("%s: Replaced 'void' value %d, with %d\n", module, curr, nodata);
    return curr;
}
unsigned int set_void_elev_color(unsigned int colr, bool v) 
{
    unsigned int curr = hgtVoid[0].color;
    hgtVoid[0].color = colr;
    if (v) SPRTF("%s: Replaced 'void' color %06X, with %06X\n", module, curr, colr);
    return curr;
}


short set_zero_elevation(short zero) 
{
    short curr = elev_zero;
    elev_zero = zero;
    sprtf("%s: Replaced 'zero' value %d, with %d\n", module, curr, zero);
    return curr;
}

unsigned int color_table[MAX_COLORS] = {
    rgb(0xFF,0,0), rgb(0xFF,0x33,0), rgb(0xFF,0x66,0), rgb(0xFF,0x99,0), rgb(0xFF,0xCC,0), rgb(0xFF,0xFF,0),
    rgb(0xCC,0xFF,0), rgb(0x99,0xFF,0), rgb(0x66,0xFF,0), rgb(0x33,0xFF,0), rgb(0,0xFF,0),
    rgb(0,0xFF,0x33), rgb(0,0xFF,0x66), rgb(0,0xFF,0x99), rgb(0,0xFF,0xCC), rgb(0,0xFF,0xFF),
    rgb(0,0xCC,0xFF), rgb(0,0x99,0xFF), rgb(0,0x66,0xFF), rgb(0,0x33,0xFF), rgb(0,0,0xFF),
    rgb(0x33,0,0xFF), rgb(0x66,0,0xFF), rgb(0x99,0,0xFF), rgb(0xCC,0,0xFF), rgb(0xFF,0,0xFF) };

unsigned int colors_used[MAX_COLORS];

void clear_used_colors() 
{
    int i;
    PHGT2COLOR pc = get_current_table();
    memset(colors_used,0,sizeof(colors_used)); 

    if (!done_table_expansion) {
        done_table_expansion = true;
        do_table_expansion();
    }

    for (i = 0; i < pc_table_cnt; i++) {
        pc[i].freq = 0;
    }
    hgtVoid[0].freq = 0;

    if (pc != hgtColor3)
        zero_freq_in_fixed_table();

}

bool have_used()
{
    int i,cnt = 0;
    for (i = 0; i < MAX_COLORS; i++) {
        cnt = colors_used[i];
        if (cnt)
            break;
    }
    if (cnt)
        return true;

    PHGT2COLOR pc = get_current_table();
    for (i = 0; i < pc_table_cnt; i++) {
        cnt = pc[i].freq;
        if (cnt)
            break;
    }
    if (cnt)
        return true;

    return false;
}

void add_cnt_for_min_max( int cnt, short min, short max )
{
    PHGT2COLOR pc = hgtColor3;
    int i;
    short smin,smax;
    for (i = 0; i < MAX_COLORS; i++) {
        smin = pc[i].min;
        smax = pc[i].max;
        if ((min >= smin)&&
            (max <= smax)) {
            pc[i].freq += cnt;
            return;
        }
    }
    for (i = 0; i < MAX_COLORS; i++) {
        smin = pc[i].min;
        smax = pc[i].max;
        if ((min >= smin)&&
            (min <= smax)) {
            pc[i].freq += cnt;
            return;
        }
    }
    for (i = 0; i < MAX_COLORS; i++) {
        smin = pc[i].min;
        smax = pc[i].max;
        if ((max >= smin)&&
            (max <= smax)) {
            pc[i].freq += cnt;
            return;
        }
    }

    SPRTF("%s: Unable to match min %d, max %d in fixed table\n", module,
        min,max);
    check_me();

}

void out_used()
{
    int i, cnt;
    unsigned int colr;
    int max, min;

    if (have_used())
        SPRTF("%s: Display of used colors... if any... only colors with a count...\n", module);

    for (i = 0; i < MAX_COLORS; i++) {
        cnt = colors_used[i];
        if (cnt)
            break;
    }
    if (i < MAX_COLORS) {
        for (i = 0; i < MAX_COLORS; i++) {
            cnt = colors_used[i];
            colr = color_table[i];
            SPRTF("Color %08X: %d\n", colr, cnt);
        }
    }

    // ******************************************************************
    // note: this can be a allocated table, much larger than the original
    // But now do NOT like the expanded color report ;=((
    // Maybe I could transfer the entries back to the old table????
    PHGT2COLOR pc = get_current_table();
    bool show_old_table = (pc != hgtColor3) ? true : false;

    for (i = 0; i < pc_table_cnt; i++) {
        cnt = pc[i].freq;
        if (cnt)
            break;
    }
    byte r,g,b;
    if (i < pc_table_cnt) {
        for (i = 0; i < pc_table_cnt; i++) {
            cnt = pc[i].freq;
            if (cnt == 0)
                continue;
            max = pc[i].max;
            min = pc[i].min;
            if (show_old_table) {
                add_cnt_for_min_max( cnt, min, max );
            } else {
                colr = pc[i].color;
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                SPRTF("Color #%02X%02X%02X: %7d  min %6d - max %6d\n", r, g, b, cnt, min, max);
            }
        }
        if (show_old_table) {
            pc = hgtColor3;
            for (i = 0; i < MAX_COLORS; i++) {
                cnt = pc[i].freq;
                if (cnt == 0)
                    continue;
                max = pc[i].max;
                min = pc[i].min;
                colr = pc[i].color;
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // TODO: Again note color REVERSAL here
                SPRTF("Color #%02X%02X%02X: %7d  min %6d - max %6d\n", b, g, r, cnt, min, max);
                //SPRTF("Color #%02X%02X%02X: %7d  min %6d - max %6d (%#06X)\n", b, g, r, cnt, min, max, colr);
            }
        }
    }

    if (hgtVoid[0].freq) {
        pc = hgtVoid;
        i = 0;
        cnt = pc[i].freq;
        colr = pc[i].color;
        max = pc[i].max;
        min = pc[i].min;
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        SPRTF("Color #%02X%02X%02X: %7d  void %6d\n", r, g, b, cnt, elev_void);
    }
}

GETCOLOR set_default_get_color(bool verb) 
{
    GETCOLOR gc = getColor;
    if (gc != get_per_color_table) {
        if (verb) SPRTF("%s: Set getColor to 'default' get_per_color_table\n", module);
        getColor = get_per_color_table;
    }
    return gc;
}
GETCOLOR set_get_color(GETCOLOR ngc, bool verb) 
{
    GETCOLOR gc = getColor;
    if (gc != ngc) {
        if (verb) SPRTF("%s: Set new getColor service\n", module);
        getColor = ngc;
    }
    return gc;
}

bool is_per_table_color()
{
    return (getColor == get_per_color_table) ? true : false;
}

unsigned int get_generated_color( short elev )
{
    int elevation = elev;
    unsigned int colr;
    byte r,g,b;
    byte *bp = (byte *)&colr;
    //if (elev > 0)
    //    r = 0;
    r = (( elevation & 0xff0000 ) >> 16 );
    g = (( elevation & 0x00ff00 ) >>  8 );
    b = (( elevation & 0x0000ff )       );
    // TODO: this probably needs FIXING for endianess
	bp[0] = r;
	bp[1] = g;
    bp[2] = b;
    bp[3] = 0;
	//bp[0] = b;
	//bp[1] = g;
    //bp[2] = r;
    //bp[3] = 0;
    return colr;
}

GETCOLOR set_linear_get_color(bool verb)
{
    GETCOLOR gc = getColor;
    if (gc != get_generated_color) {
        if (verb) SPRTF("%s: set getColor to get_generated_color()\n", module );
        getColor = get_generated_color;
    }
    return gc;
}


unsigned int get_per_color_table( short elev )
{
    static bool shown_not_found = false;
    static bool shown_out_of_range = false;
    static unsigned int last_color = 0;
    int i;
    short min, max;
    if ((elev > MAX_TABLE_ELEV)||
        (elev < MIN_TABLE_ELEV)) {
        if (!shown_out_of_range) {
            shown_out_of_range = true;
            SPRTF("%s: WARNING: OUT OF RANGE elevation %d. Table is max,min %d,%d meters!\n", module,
                elev,
                MAX_TABLE_ELEV, MIN_TABLE_ELEV );
            SPRTF("Returning last color %08X\n", last_color);
        }
        //return 0x0000ff;    // return PURE blue == NOT FOUND
        return last_color;
    }

    PHGT2COLOR pc = get_current_table();
    if (elev == elev_zero) {
        pc[pc_zero_cnt].freq++;
        last_color = pc[pc_zero_cnt].color;
        return last_color;
    }
    if (elev == elev_void) {
        hgtVoid[0].freq++;
        last_color = hgtVoid[0].color;
        return last_color;
    }

    if (elev > 0) {
        for (i = 0; i < pc_table_cnt; i++) {
            min = pc[i].min;
            max = pc[i].max;
            if ((elev >= min) && (elev <= max)) {
                pc[i].freq++;
                last_color = pc[i].color;
                return last_color;
            }
        }
    } else {
        // negative elevation
        //for (i = pc_zero_cnt + 1; i < pc_table_cnt; i++) {
        for (i = pc_zero_cnt; i < pc_table_cnt; i++) {
            min = pc[i].min;
            max = pc[i].max;
            //if ((elev <= min) && (elev >= max)) {
            if ((elev >= min) && (elev <= max)) {
                pc[i].freq++;
                last_color = pc[i].color;
                return last_color;
            }
        }
    }
    if (!shown_not_found) {
        shown_not_found = true;
        if (elev < 0) {
            SPRTF("%s: Ranges: min,max ", module);
            for (i = pc_zero_cnt; i < pc_table_cnt; i++) {
                min = pc[i].min;
                max = pc[i].max;
                SPRTF("%d,%d  ", min, max);
            }
            SPRTF("\n");
        }
        SPRTF("%s: WARNING: Elevation %d NOT found\n", module, (int)elev);
        SPRTF("Returning last color %08X\n", last_color);
        //check_me();
    }
    //return 0x0000ff;    // return PURE blue == NOT FOUND
    return last_color;
}

GETCOLOR getColor = get_per_color_table;

unsigned int get_BGR_color( short elev )
{
    return getColor(elev);
}

// only tried in load-hgt(), a depreciated service
// javascript - height = ( addSpace * elevations[ index++ ] + 0xffffffff + 1 ).toString( 16 ).slice( -6 );
static int addSpace = 80;
unsigned int get_color( short elev, size_t coff )
{
    unsigned int colr;
    char buf[32];
    unsigned long long height;
    if (coff >= MAX_COLORS) {
        SPRTF("%s: YEEK: With elev %d, exceeding color %d on %d\n", module,
            elev, (int)coff, MAX_COLORS );
        check_me();
        exit(1);
    }
    height = ((addSpace * elev) + 0xffffffff + 1);
    sprintf(buf,"%06" W64 "X", height); 
    colr = (unsigned int)height;
    //colr = color_table[coff];
    //colors_used[coff]++;
    if (colr == 0) {
        colr = rgb(0,0,255);
    }
    return colr;
}

/////////////////////////////////////////////////////////////////////////////
// from : http://en.wikipedia.org/wiki/Snow_line 
// Levels of the climatic snow line:
typedef struct tagSNOWLINE {
    const char *place;
    int lat;
    short min_m, max_m;
}SNOWLINE, *PSNOWLINE;

SNOWLINE snowline[] = {
    { "Svalbard", 78, 300, 600 },
    { "Scandinavia at the polar circle", 67, 1000, 1500 }, 
    { "Iceland", 65, 700, 1100 },
    { "Eastern Siberia", 63, 2300, 2800 },
    { "southern Scandinavia", 62, 1200, 2200 },
    { "Alaska Panhandle", 58, 1000, 1500 },
    { "Kamchatka (coastal)", 55, 700, 1500 },
    { "Kamchatka (interior)", 55, 2000, 2800 },
    { "Alps (northern slopes)", 48, 2500, 2800 },
    { "Central Alps", 47, 2900, 3200 },
    { "Alps (southern slopes)", 46, 2700, 2800 }, 
    { "Pyrenees", 43, 2600, 2900 },
    { "Caucasus", 43, 2700, 3800 },
    { "Pontic Mountains", 42, 3800, 4300 },
    { "Karakoram", 36, 5400, 5800 },
    { "Transhimalaya", 32, 6300, 6500 },
    { "Himalaya", 30, 4800, 6000 },
    { "Mount Kenya", 0, 4600, 4700 },
    { "New Guinea", -2, 4600, 4700 },
    { "Andes in Ecuador", -2, 4800, 5000 }, 
    { "Kilimanjaro", -3, 5500, 5600 },
    { "Andes in Bolivia", -18, 6000, 6500 },
    { "Andes in Chile", -30, 5800, 6500 },
    { "North Island, New Zealand", -37, 2500, 2700 },
    { "South Island, New Zealand", -43, 1600, 2700 }, 
    { "Tierra del Fuego", -54, 800, 1300 },
    { "Antarctica", -70, 0, 400 },
    // end of table
    { 0, 0, 0, 0 }
};

/////////////////////////////////////////////////////////////////////////////
// color tables NOT USED

#if 0 // to be removed one day

HGT2COLOR hgtColorP_NOT_USED[] = {
    { 1, 500 , 0x80f31f , 0, 0, 0, 0 },
    { 501, 1000 , 0x86ed07 , 0, 0, 0, 0 },
    { 1001, 1500  , 0x8ce601 , 0, 0, 0, 0 },
    { 1501, 2000  , 0x92de0f , 0, 0, 0, 0 },
    { 2001, 2500  , 0x99d52f , 0, 0, 0, 0 },
    { 2501, 3000  , 0x9fcc5c , 0, 0, 0, 0 },
    { 3001, 3500  , 0xa5c18e , 0, 0, 0, 0 },
    { 3501, 4000  , 0xabb6be , 0, 0, 0, 0 },
    { 4001, 4500  , 0xb1aae4 , 0, 0, 0, 0 },
    { 4501, 5000  , 0xb79efa , 0, 0, 0, 0 },
    { 5001, 5500  , 0xbc91fd , 0, 0, 0, 0 },
    { 5501, 6000  , 0xc285ec , 0, 0, 0, 0 },
    { 6001, 6500  , 0xc778ca , 0, 0, 0, 0 },
    { 6501, 7000  , 0xcc6b9c , 0, 0, 0, 0 },
    { 7001, 7500  , 0xd15f69 , 0, 0, 0, 0 },
    { 7501, 8000  , 0xd6533a , 0, 0, 0, 0 },
    { 8001, 8500  , 0xdb4716 , 0, 0, 0, 0 },
    { 8501, 9000  , 0xdf3c03 , 0, 0, 0, 0 }
};

// Generally this LOOKS GOOD, but needs improvement
HGT2COLOR hgtColor3_OK[MAX_COLORS] = {
    // =====================================
    { 7549, 9000	, 0xFF0090, 0, 0, 0, 0 },
    { 6493, 7548	, 0xFF0080, 0, 0, 0, 0 },
    { 5662, 6492    , 0xFF0060, 0, 0, 0, 0 },
    { 4977, 5661	, 0xFF0050, 0, 0, 0, 0 },
    { 4395, 4976	, 0xFF0040, 0, 0, 0, 0 },
    { 3888, 4394	, 0xFF0030, 0, 0, 0, 0 },
    { 3439, 3887	, 0xFF0020, 0, 0, 0, 0 },
    { 3037, 3438	, 0xFF0010, 0, 0, 0, 0 },
    { 2672, 3036	, 0xFF0000, 0, 0, 0, 0 },
    { 2338, 2671	, 0xFF1000, 0, 0, 0, 0 },
    // ------------------------------------
    { 2031, 2337	, 0xFF1800, 0, 0, 0, 0 },
    { 1746, 2030	, 0xFF2000, 0, 0, 0, 0 },
    { 1480, 1745	, 0xFF2800, 0, 0, 0, 0 },
    { 1232, 1479	, 0xFF3800, 0, 0, 0, 0 },
    {  998, 1231	, 0xFF4000, 0, 0, 0, 0 },
    {  777,  997	, 0xFF4C00, 0, 0, 0, 0 },
    {  568,  776	, 0xFF7E00, 0, 0, 0, 0 },
    { 370,  567	, 0xFFB000, 0, 0, 0, 0 },
    { 181, 369	, 0xFFFF00, 0, 0, 0, 0 },
    { 1, 180	, 0x00FF00, 0, 0, 0, 0 },
    // ======================================
    { 0, 0	, 0x0000FF, 0, 0, 0, 0 },
    { -1, -20	, 0x3300FF, 0, 0, 0, 0 },
    { -21, -40	, 0x6600FF, 0, 0, 0, 0 },
    { -41, -60	, 0x9900FF, 0, 0, 0, 0 },
    { -61, -80	, 0xCC00FF, 0, 0, 0, 0 },
    { -81, -11000 , 0xFF00FF, 0, 0, 0, 1 }
};

HGT2COLOR hgtColor1_NOT_USED[MAX_COLORS] = {
    { 8550, 9000	, 0xFF0000, 0, 0, 0, 0 },
    { 8100, 8549	, 0xFF3300, 0, 0, 0, 0 },
    { 7650, 8099	, 0xFF6600, 0, 0, 0, 0 },
    { 7200, 7649	, 0xFF9900, 0, 0, 0, 0 },
    { 6750, 7199	, 0xFFCC00, 0, 0, 0, 0 },
    { 6300, 6749	, 0xFFFF00, 0, 0, 0, 0 },
    { 5850, 6299	, 0xCCFF00, 0, 0, 0, 0 },
    { 5400, 5849	, 0x99FF00, 0, 0, 0, 0 },
    { 4950, 5399	, 0x66FF00, 0, 0, 0, 0 },
    { 4500, 4949	, 0x33FF00, 0, 0, 0, 0 },
    { 4050, 4499	, 0x00FF00, 0, 0, 0, 0 },
    { 3600, 4049	, 0x00FF33, 0, 0, 0, 0 },
    { 3150, 3599	, 0x00FF66, 0, 0, 0, 0 },
    { 2700, 3149	, 0x00FF99, 0, 0, 0, 0 },
    { 2250, 2699	, 0x00FFCC, 0, 0, 0, 0 },
    { 1800, 2249	, 0x00FFFF, 0, 0, 0, 0 },
    { 1350, 1799	, 0x00CCFF, 0, 0, 0, 0 },
    { 900, 1349	, 0x0099FF, 0, 0, 0, 0 },
    { 450, 899	, 0x0066FF, 0, 0, 0, 0 },
    { 1, 449	, 0x0033FF, 0, 0, 0, 0 },
    { 0, 0	, 0x0000FF, 0, 0, 0, 0 },
    { -1, -20	, 0x3300FF, 0, 0, 0, 0 },
    { -21, -40	, 0x6600FF, 0, 0, 0, 0 },
    { -41, -60	, 0x9900FF, 0, 0, 0, 0 },
    { -61, -80	, 0xCC00FF, 0, 0, 0, 0 },
    { -81, -11000 , 0xFF00FF, 0, 0, 0, 0 }
};


// I want to make the first 20 entries go from red to green
// That is from #ff0000 to #00ff00
HGT2COLOR hgtColor4_NOT_USED[MAX_COLORS] = {
    // =====================================
    { 7549, 9000	, 0xFF0000, 0, 0, 0, 0 },
    { 6493, 7548	, 0xFF1A00, 0, 0, 0, 0 },
    { 5662, 6492    , 0xFF3200, 0, 0, 0, 0 },
    { 4977, 5661	, 0xFF4C00, 0, 0, 0, 0 },
    { 4395, 4976	, 0xFF6400, 0, 0, 0, 0 },
    { 3888, 4394	, 0xFF7E00, 0, 0, 0, 0 },
    { 3439, 3887	, 0xFF9600, 0, 0, 0, 0 },
    { 3037, 3438	, 0xFFB000, 0, 0, 0, 0 },
    { 2672, 3036	, 0xFFC800, 0, 0, 0, 0 },
    { 2338, 2671	, 0xFFE200, 0, 0, 0, 0 },
    // ------------------------------------
    { 2031, 2337	, 0xE2FF00, 0, 0, 0, 0 },
    { 1746, 2030	, 0xC8FF00, 0, 0, 0, 0 },
    { 1480, 1745	, 0xB0FF00, 0, 0, 0, 0 },
    { 1232, 1479	, 0x96FF00, 0, 0, 0, 0 },
    {  998, 1231	, 0x7EFF00, 0, 0, 0, 0 },
    {  777,  997	, 0x64FF00, 0, 0, 0, 0 },
    {  568,  776	, 0x4CFF00, 0, 0, 0, 0 },
    { 370,  567	, 0x32FF00, 0, 0, 0, 0 },
    { 181, 369	, 0x1AFF00, 0, 0, 0, 0 },
    { 1, 180	, 0x00FF00, 0, 0, 0, 0 },
    // ======================================
    { 0, 0	, 0x0000FF, 0, 0, 0, 0 },
    { -1, -20	, 0x3300FF, 0, 0, 0, 0 },
    { -21, -40	, 0x6600FF, 0, 0, 0, 0 },
    { -41, -60	, 0x9900FF, 0, 0, 0, 0 },
    { -61, -80	, 0xCC00FF, 0, 0, 0, 0 },
    { -81, -11000 , 0xFF00FF, 0, 0, 0, 0 }
};

#endif // 0

///////////////////////////////////////////////////////////////////////
// Experiments in generating a color range
// Say we have 0xFF00FF, morph to 0xCC00FF 
//typedef std::vector<unsigned int> vUINT;

bool gen_color_range( unsigned int colr1, unsigned int colr2, int max, int min,
                        vMMC &v, int verb )
{
    MAXMINCOLOR mmc;
    byte r1,g1,b1;
    byte r2,g2,b2;
    byte nr,ng,nb;
    int rdec,gdec,bdec,frng;
    int nmin,nmax, lmin,lmax;
    byte rd,gd,bd,md,i,tmp;
    double unit;
    // bool isneg = (max < 0) ? true : false;

    if (min >= max) {
        if (verb) SPRTF("%s: min %d is GTE max %d\n", module, min, max);
        return false;
    } else if ((min == 0)||(max == 0)) {
        if (verb) SPRTF("%s: Service does NOT handle 0! min %d or max %d\n", module, min, max);
        return false;
    } else if ((min < 0)&&(max > 0)) {
        if (verb) SPRTF("%s: Service does NOT handle +/-! min %d or max %d\n", module, min, max);
        return false;
    }
    r1 = getRVal(colr1);
    g1 = getGVal(colr1);
    b1 = getBVal(colr1);
    r2 = getRVal(colr2);
    g2 = getGVal(colr2);
    b2 = getBVal(colr2);
    if (r1 > r2) {
        rdec = 1;
        rd = r1 - r2;
    } else {
        rdec = 0;
        rd = r2 - r1;
    }
    if (g1 > g2) {
        gdec = 1;
        gd = g1 - g2;
    } else {
        gdec = 0;
        gd = g2 - g1;
    }
    if (b1 > b2) {
        bdec = 1;
        bd = b1 - b2;
    } else {
        bdec = 0;
        bd = b2 - b1;
    }
    if (rd > gd)
        md = rd;
    else
        md = gd;
    if (bd > md)
        md = bd;

    if (md == 0) {
        if (verb) SPRTF("%s: Found NO color difference! 1:%02X%02X%02X 2:%02X%02X%02X \n", module, r1,g1,b1,r2,g2,b2 );
        return false;
    }
    frng = max - min;
    if (verb) SPRTF("%s: maxd %d, from %02X%02X%02X to %02X%02X%02X range %d - %d, %d steps\n", module,
        md,
        r1,g1,b1,
        r2,g2,b2,
        min, max,
        (frng + 1));
     unit = ((double)frng / (double)(md+1));
     //nmin = min;
     //nmax = nmin + (int)(unit + 0.5);
     nmax = max;
     nmin = nmax - (int)(unit + 0.5);
     if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, r1, g1, b1 );
     mmc.colr = rgb(r1,g1,b1);
     mmc.max  = nmax;
     mmc.min  = nmin;
     v.push_back(mmc);
     lmin = min;
     lmax = lmin + (int)(unit + 0.5);
     //lmax = max;
     //lmin = nmax - (int)(unit + 0.5);
    for (i = 1; i < md; i++) {
        nmax = max - (int)((unit * (double)i) + 0.5);
        nmin = nmax - (int)(unit + 0.5);
        //nmin = min + (int)((unit * (double)i) + 0.5);
        //nmax = nmin + (int)(unit + 0.5);
        nr = r1;
        if (rd) {
            // move r to towards pr by an incremental amount
            tmp = (int)((((double)rd / (double)md) * (double)i) + 0.5);
            if (rdec)
                nr = r1 - tmp;
            else
                nr = r1 + tmp;
        }
        ng = g1;
        if (gd) {
            // move g to towards pg by an incremental amount
            tmp = (int)((((double)gd / (double)md) * (double)i) + 0.5);
            if (gdec)
                ng = g1 - tmp;
            else
                ng = g1 + tmp;
        }
        nb = b1;
        if (bd) {
            // move b to towards pb by an incremental amount
            tmp = (int)((((double)bd / (double)md) * (double)i) + 0.5);
            if (bdec)
                nb = b1 - tmp;
            else
                nb = b1 + tmp;

        }
        if ((mmc.max == nmax)&&(mmc.min == nmin)) {
            if (verb > 1) SPRTF("SKIPPED %d %d %02X%02X%02X\n", nmin, nmax, nr, ng, nb );
        } else if ((lmax == nmax)&&(lmin == nmin)) {
            if (verb > 1) SPRTF("skipped %d %d %02X%02X%02X\n", nmin, nmax, nr, ng, nb );
        } else {
            if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, nr, ng, nb );
            mmc.max = nmax;
            mmc.min = nmin;
            mmc.colr = rgb(nb,ng,nr);
            v.push_back(mmc);
        }
    }


     nmin = lmin;
     nmax = lmax;
     //nmin = min;
     //nmax = nmin + (int)(unit + 0.5);
     //nmax = max;
     //nmin = nmax - (int)(unit + 0.5);
     if (verb) SPRTF("min/max %d %d %02X%02X%02X\n", nmin, nmax, r2, g2, b2 );
     mmc.colr = rgb(b2,g2,r2);
     mmc.max  = nmax;
     mmc.min  = nmin;
     v.push_back(mmc);

     return true;
}

int test_gen_colors()
{
    vMMC v;
    int min = 500;
    int max = 1000;
    int verb = 1;
    unsigned int colr1 = 0xFF00FF; // morph to 
    unsigned int colr2 = 0xCC00FF;

    SPRTF("\n");
    SPRTF("%s: === test_gen_color() =====================\n", module);

    if (! gen_color_range( colr1, colr2, max, min, v, verb )) {
        SPRTF("%s: Failed in color gen!\n", module);
        return 1;
    }
    SPRTF("%s: gen_color_range returned %d ranges\n", module, (int)v.size());

    v.clear();
    colr1 = 0x80f31f;
    colr2 = 0x86ed07;
    min = 1;
    max = 180;
    if (! gen_color_range( colr1, colr2, max, min, v, verb )) {
        SPRTF("%s: Failed in color gen!\n", module);
        return 1;
    }
    SPRTF("%s: gen_color_range returned %d ranges\n", module, (int)v.size());

    //{ -1, -20	, 0x3300FF, 0, 0, 0, 0 },
    //{ -21, -40	, 0x6600FF, 0, 0, 0, 0 },
    v.clear();
    min = -40;
    max = -1;
    colr1 = 0x6600FF;
    colr2 = 0x3300FF;
    if (! gen_color_range( colr1, colr2, max, min, v, verb )) {
        SPRTF("%s: Failed in color gen!\n", module);
        return 1;
    }
    SPRTF("%s: gen_color_range returned %d ranges\n", module, (int)v.size());

    SPRTF("%s: Try shades of grey...\n", module );
    v.clear();
    min = 1;
    max = 3000;
    colr1 = 0xFFFFFF;
    colr2 = 0x000000;
    if (! gen_color_range( colr1, colr2, max, min, v, verb )) {
        SPRTF("%s: Failed in color gen!\n", module);
        return 1;
    }
    SPRTF("%s: gen_color_range returned %d ranges\n", module, (int)v.size());

    SPRTF("%s: Try shades of grey... with minimal range...\n", module );
    v.clear();
    min = 1;
    max = 20;
    colr1 = 0xFFFFFF;
    colr2 = 0x000000;
    if (! gen_color_range( colr1, colr2, max, min, v, verb )) {
        SPRTF("%s: Failed in color gen!\n", module);
        return 1;
    }
    SPRTF("%s: gen_color_range returned %d ranges\n", module, (int)v.size());

    v.clear();
    min = 100;
    max = 1000;
    colr1 = 0xFF00CC;
    colr2 = 0xCC00FF;
    if (! gen_color_range( colr1, colr2, max, min, v, verb )) {
        SPRTF("%s: Failed in color gen!\n", module);
        return 1;
    }
    SPRTF("%s: gen_color_range returned %d ranges\n", module, (int)v.size());
    return 0;
}

#ifndef EOL
#define EOL << std::endl
#endif

std::string get_color_http()
{
    PHGT2COLOR pct = get_current_table();
    int i, min, max;
    byte r,g,b;
    unsigned int colr;
    std::stringstream html;
    html << "<html>" EOL;
    html << " <head>" EOL;
    html << "  <title>Current Color Table</title>" EOL;
    html << "  <script language=\"JavaScript\" type=\"text/javascript\">" EOL;
    html << "// change background" EOL;
    html << "  function sbgc(hex) { document.bgColor=hex; }" EOL;
    html << "  </script>" EOL;
    html << " </head>" EOL;
    html << " <body>" EOL;
    html << " <div align=\"center\">" EOL;
    html << "  <table border\"1\" align=\"center\" summary=\"view of images\">" EOL;
    html << "  <caption>Current Color Table</caption>" EOL;
    char *cp = GetNxtBuf();
    char *tx = GetNxtBuf();
    html << "    <tr>" EOL;
    html << "     <th>Minimum</th>" EOL;
    html << "     <th>Maximum</th>" EOL;
    html << "     <th>Color</th>" EOL;
    html << "    </tr>" EOL;
    for (i = 0; i < pc_table_cnt; i++) {
        colr = pct[i].color;
        min =  pct[i].min;
        max =  pct[i].max;
        r = getRVal(colr);
        g = getGVal(colr);
        b = getBVal(colr);
        //sprintf(cp,"#%02X%02X%02X", r, g, b);
        //sprintf(tx,"#%02X%02X%02X", 255 - r, 255 - g, 255 - b);
        sprintf(cp,"#%02X%02X%02X", b, g, r);
        sprintf(tx,"#%02X%02X%02X", 255 - b, 255 - g, 255 - r);
        html << "    <tr>" EOL;
        html << "     <td align=\"right\">" << min << "</td>" EOL;
        html << "     <td align=\"right\">" << max << "</td>" EOL;
        html << "     <td bgcolor=\"" << cp << "\">" EOL;
        html << "      <a href=\"#\" onmouseover=\"sbgc('" << cp << "')\" onmouseout=\"sbgc('#FFFFFF')\">" EOL;
        html << "        <tt><font color=\"" << tx << "\">***** " << cp << " ******</font></tt></a>" EOL;
        html << "     </td>" EOL;
        html << "    </tr>" EOL;
    }
    html << "   </table>" EOL;
    html << " </div>" EOL;
    html << " </body>" EOL;
    html << "</html>" EOL;
    return html.str();
}

// eof
