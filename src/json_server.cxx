/*\
 * json_server.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#ifdef _MSC_VER
#include <Windows.h>
#else
#include <string.h> // for strcmp(), ...
#include <unistd.h> // for usleep(), ...
#endif
#include <time.h>
#include "sprtf.hxx"
#include "dir_utils.hxx"
#include "json_load.hxx"
#include "pollkbd.hxx"
#include "http_utils.hxx"
#include "json_server.hxx"

static const char *module = "json_server";

#ifndef DEF_SERVER_PORT
#define DEF_SERVER_PORT 5555
#endif
#ifndef DEF_SLEEP_MS
#define DEF_SLEEP_MS 100
#endif
// for select timeout
#ifndef DEF_TIMEOUT_MS
#define DEF_TIMEOUT_MS 500
#endif

#ifndef SLEEP
#ifdef _MSC_VER
#define SLEEP(x) Sleep(x)
#else // !_MSC_VER
#define SLEEP(x) usleep( x * 1000 )
#endif // _MSC_VER y/n
#endif // SLEEP

static int port = DEF_SERVER_PORT;
// options
static int verbosity = 0;
static int sleep_ms = DEF_SLEEP_MS;
static int timeout_ms = DEF_TIMEOUT_MS;
static const char *log_file = "tempjson.log";
static const char *html_head =
    "<html>\n"
    "<head>\n"
    "<title>Path information</title>\n"
    "</head>\n";

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

static int showOptions(struct mg_connection *conn)
{
    int iret = MG_TRUE;
    std::string c(html_head);
    c += "<body>\n";

    c += "<h1>Path information</h1>\n";

    c += "<pre>";
    c += "/             - ie no path. This information block\n";
    c += "/flights.json - next json block, if available\n";
    c += "</pre>\n";

    c += "<p><b>All other will return 400 - command error, or 404 - file not found</b></p>\n";

    c += "<p>";
    char *tmp = GetNxtBuf();
    if (max_json) {
        sprintf(tmp,"Have %d json strings loaded from file %s. Next is %d</p>\n", (int)max_json, json_file, (int)next_json);
    } else {
        sprintf(tmp,"Have NO json strings loaded from file %s.</p>\n", json_file);
    }
    c += tmp;
    c += "</p>\n";

    tmp = Get_Browser_Type_Stg();
    if (*tmp) {
        c += "<pre><b>Browser Types</b>\n";
        c += tmp;
        c += "</pre>\n";
    }

    // anything else to add
    c += "<p align=\"right\">Update: ";
    c += Get_Current_UTC_Time_Stg();
    c += " UTC</p>\n";

    c += "</body>\n";
    c += "</html>\n";
    mg_send_header(conn,"Content-Type","text/html");
    send_extra_headers(conn);
    mg_send_data(conn,c.c_str(),(int)c.size());
    if (VERB1) {
        SPRTF("%s: Sent HTML reply, %d bytes\n", module, (int)c.size());
    }
    return iret;
}

static int event_handler(struct mg_connection *conn, enum mg_event ev) 
{
    int iret = MG_FALSE;
    int i;
    bool verb = (VERB1) ? true : false;
    const char *q = (conn->query_string && *conn->query_string) ? "?" : "";
    if (ev == MG_AUTH) {
        return MG_TRUE;   // Authorize all requests
    } else if (ev == MG_REQUEST) {
        SPRTF("%s: got URI %s%s%s\n", module,
            conn->uri,q,
            ((q && *q) ? conn->query_string : "") );
        const char *pua = mg_get_header(conn, "User-Agent");
        if (pua && *pua) {
            const char *bt = Set_Browser_Type(pua); // pointer to "User-Agent" string
            if (VERB1) {
                SPRTF("%s: User-Agent: %s '%s'\n",module, bt, pua);
            }
        }
        if (VERB9) {
            if (conn->num_headers) {
                SPRTF("%s: Show of %d headers...\n", module, conn->num_headers);
                for (i = 0; i < conn->num_headers; i++) {
                    //struct mg_header *p = &conn->http_headers[i];
                    const char *n = conn->http_headers[i].name;
                    const char *v = conn->http_headers[i].value;
                    SPRTF(" %s: %s\n", n, v );
                }
            }
        }
        if (strcmp(conn->uri,"/") == 0) {
            iret = showOptions(conn);
        } else if (strcmp(conn->uri,"/flights.json") == 0) {
            iret = sendNextJSON(conn,true,verb);
        }
    }
    if ( (ev == MG_REQUEST) && (iret == MG_FALSE) ) {
        if (VERB1) {
            SPRTF("%s: No repsonse sent! Returning MG_FALSE to mongoose.\n", module );
        }
    }
    return iret;
}

#ifndef ISNUM
#define ISNUM(a) ((a >= '0')&&(a <= '9'))
#endif
#ifndef ADDED_IS_DIGITS
#define ADDED_IS_DIGITS

static int is_digits(char * arg)
{
    size_t len,i;
    len = strlen(arg);
    for (i = 0; i < len; i++) {
        if ( !ISNUM(arg[i]) )
            return 0;
    }
    return 1; /* is all digits */
}

#endif // #ifndef ADDED_IS_DIGITS


static void give_help( char *name )
{
    printf("\n");
    printf("%s - version 1.0.0\n", name);
    printf("\n");
    printf("Usage: %s [options]\n", name);
    printf("\n");
    printf("Options:\n");
    //      123456789112345678921
    printf(" --help   (-h or -?) = This help and exit(2)\n");
    printf(" --port <num>   (-p) = Set port (def=%d)\n", port);
    printf(" --json <file>  (-j) = Set the json file to use. (def=%s)\n", get_file_name((char *)json_file));
    printf(" --log <file>   (-l) = Set log file. (def=%s, in CWD)\n", log_file);
    printf(" --sleep <ms>   (-s) = Set milliseconds sleep in loop. 0 for none. (def=%d)\n", sleep_ms);
    printf(" --timeout <ms> (-t) = Set milliseconds timeout for select(). (def=%d)\n", timeout_ms);
    printf(" --verb[num]    (-v) = Bump or set verbosity. (def=%d)\n", verbosity);
    printf("\n");
    printf("Will establish a HTTP server on the addr:port, and respond to GET with '/paths'\n");
    printf("\n");
    printf(" /flights.json       = return next available json block.\n");
    printf("All others will return 400 - command error, or 404 - file not found\n");
    printf("\n");
    printf("All output will be written to stdout, and the log file.\n");
    printf("The current json file [%s] %s\n", json_file, 
        ((is_file_or_directory((char *)json_file) == 1) ? "appears ok." : "IS NOT VALID!") );
    printf("Will exit on ESC keying, if in foreground\n");
}

static int parse_commands( int argc, char **argv )
{
    int iret = 0;
    int i, c, i2;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help( get_file_name(argv[0]) );
                return 2;
            case 'l':
                i++;    // log file already checked and handled
                break;
            case 'j':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    json_file = strdup(sarg);
                    if (is_file_or_directory((char *)json_file) != 1) {
                        SPRTF("%s: Unable to 'stat' file [%s[!\n", module, json_file);
                        SPRTF("%s: Check name and location\n");
                        return 1;
                    }
#ifdef HAVE_MEMORY_GALORE
                    load_json_file(json_file,vJson);
#else // !HAVE_MEMORY_GALORE
                    load_json_file(json_file);
#endif // HAVE_MEMORY_GALORE y/n
                    if (max_json == 0) {
                        SPRTF("%s: json file [%s] yielded NO json!\n", module, json_file);
                        return 1;
                    }

                    SPRTF("%s: Loaded json file [%s] with %d records.\n", module, json_file,
                        (int)max_json);

                } else {
                    SPRTF("%s: Expected file name to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    port = atoi(sarg);
                    SPRTF("%s: Set port to %d\n", module, port);
                } else {
                    SPRTF("%s: Expected port value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    sleep_ms = atoi(sarg);
                    SPRTF("%s: Set sleep to %d ms\n", module, sleep_ms);
                } else {
                    SPRTF("%s: Expected ms sleep value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    timeout_ms = atoi(sarg);
                    SPRTF("%s: Set select timeout to %d ms\n", module, timeout_ms);
                } else {
                    SPRTF("%s: Expected ms timeout value to follow %s!\n", module, arg );
                    goto Bad_CMD;
                }
                break;
            case 'v':
                sarg++; // skip the -v
                if (*sarg) {
                    // expect digits
                    if (is_digits(sarg)) {
                        verbosity = atoi(sarg);
                    } else if (*sarg == 'v') {
                        verbosity++; /* one inc for first */
                        while(*sarg == 'v') {
                            verbosity++;
                            arg++;
                        }
                    } else
                        goto Bad_CMD;
                } else
                    verbosity++;
                if (VERB1) printf("%s: Set verbosity to %d\n", module, verbosity);
                break;
            default:
                goto Bad_CMD;
                break;
            }
        } else {
Bad_CMD:
            SPRTF("%s: Unknown command %s\n", module, arg );
            return 1;
        }
    }

    return iret;
}

int check_log_file(int argc, char **argv)
{
    int i,fnd = 0;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            if (*sarg == 'l') {
                if ((i + 1) < argc) {
                    i++;
                    sarg = argv[i];
                    log_file = strdup(sarg);
                    fnd = 1;
                } else {
                    printf("%s: Expected a file name to follow [%s]\n", module, arg );
                    return 1;
                }
            }
        } else {

        }
    }
    set_log_file((char *)log_file, false);
    if (fnd)
        SPRTF("%s: Set LOG file to [%s]\n", module, log_file);
    return 0;
}

int main( int argc, char **argv )
{
    int res;
    time_t curr,next;

    res = check_log_file(argc,argv);
    if (res)
        return res;

    res = parse_commands(argc,argv);
    if (res)
        return res;

#ifdef HAVE_MEMORY_GALORE
    load_json_file(json_file,vJson);
#else
    load_json_file(json_file);
#endif

    if (http_init(0,port,event_handler)) {
        SPRTF("%s: Server FAILED! Aborting...\n", module );
        return 1;
    }
    SPRTF("%s: Waiting on %d... ESC to exit\n", module, port );

    curr = time(0);
    while (1) {
        res = test_for_input();
        if (res) {
            if (res == 0x1b) {
                SPRTF("%s: Got ESC exit key...\n", module );
                break;
            } else {
                SPRTF("%s: Got unknown key %X!\n", module, res );
            }
        }
        http_poll(timeout_ms);    // server->poll();
        next = time(0);
        if (next != curr) {
            curr = next;
            // any one seconds tasks???
#ifdef DO_JSON_BUMP
            bump_json();
#endif
            if (sleep_ms > 0) {
                SLEEP(sleep_ms);
            }
        }
    }

    http_close();   // delete server;

    return 0;
}


// eof = json_server.cxx
