/*\
 * dem_track.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
 * Given a 'track' bgn-lat bgn-lon end-lat end-lon, or a special xg track file to get the 'track'
 * get sample elevations along the 'track'
 *
\*/

#include <stdio.h>
#include <string.h> // for strlen(), ...
#include <math.h> // for atof(), ...
#include <cctype>
#include <vector>
#include <fstream>
#include <sstream> // for #include <iostream>
#include <algorithm> // tranform()
#include <locale>   // for isspace
#include <iostream>
#include <functional> // for ptr_fun
#include "hgt3File.hxx"
#include "dir_utils.hxx"
#include "utils.hxx"
#include "sprtf.hxx"
#include "sg_wgs_84.hxx"
#include "dem_track.hxx"

#ifndef SPRTF
#define SPRTF printf
#endif

#ifndef DEF_DEM3_PATH
#define DEF_DEM3_PATH "F:\\data\\dem3\\hgt"
#endif

#ifndef METER2FEET
#define METER2FEET 3.28084
#endif

#ifndef FEET2METER
#define FEET2METER 0.3048
#endif

#ifndef EOL
#define EOL std::endl
#endif

#ifndef DEF_LOG_FILE
#define DEF_LOG_FILE "tempdem.txt"
#endif

#ifndef TRACK_VERSION
#define TRACK_VERSION "1.0.0 12 Nov 2014"
#endif

typedef struct tagVERT {
    double x,y;
}VERT, *PVERT;

typedef std::vector<VERT> vVERTS;
typedef std::vector<vVERTS> vSEGS;

static const char *module = "dem_track";

// forward refs
static void set_default_log();

static const char *dem3_path = DEF_DEM3_PATH;
static double bgn_lat,bgn_lon,end_lat,end_lon;
static int verbosity = 0;
static bool extra_debug = false;
static bool do_full_bbox = false;
static bool got_segments = false;
static const char *xg_file = "temptrack.xg";
static const char *xg_file2 = "tempelev.xg";
// double arb_dist = 0.005; // with this still got 48,799 points, 18.6 secs
// double arb_dist = 0.0005; // with this got 4,886 points, in 2.3 secs...
static double arb_dist = 0.0004; // with this got 3,909 points in 2.0 secs              
static char *usr_args = 0;
static double vert_factor = 1.0;    // 1:1 can be very flat - try 10.0; // was 100.0 in dev
static vSEGS segs;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

static void out_version()
{
    printf(" %s: Version %s\n", module, TRACK_VERSION);
}

static void give_help()
{
    bool valid_dem_path = (is_file_or_directory((char *)dem3_path) == 2) ? true : false;
    set_default_log();
    printf("\n");
    printf("%s [options] bgn_lat bgn_lon end_lat end_lon\n", module );
    printf("\n");
    printf("options:\n");
    printf(" --help    (-h or -?) = This help and exit(2)\n");
    printf(" --dem3 <path>   (-d) = Set path the DEM3 hgt files. (def=%s) %s\n",
        dem3_path, valid_dem_path ? "ok" : "NOT VALID" );
    printf(" --verb[num]     (-v) = Bump or set verbosity. (def=%d)\n", verbosity);
    printf(" --log <file>    (-l) = Set log file.\n (def=%s)\n", get_log_file());
    printf(" --map <file>    (-m) = Set the output of the xg height map. (def=%s)\n", xg_file2);
    printf(" --factor <dbl>  (-f) = Set vertical exageration divisor. (def=%f)\n", vert_factor);
    printf(" --path <file>   (-p) = Set ouput of the sample track xg file. (def=%s)\n", xg_file);
    printf(" --spread <degs> (-s) = Set max distance from track when sampling. (def=%lf degs)\n", arb_dist );
    printf(" --track <file>  (-t) = Use a track xg file, to set a series of segments.\n");
    printf(" --version            = Out version, and exit(0)\n");
    printf("\n");
    out_version();
    printf(" Given a begin lat,lon and an end lat,lon, generate a height map\n");
    printf(" for when flying this track. This is done by sampling elevation\n");
    printf(" on or near the track. The sampling distance is abt 5m, and the elev are in meters,\n");
    printf(" so can appear very flat. Set the vertical factor to say '-f 100.0', to see up and\n");
    printf(" down greatly exagerated. Can give a better idea of the terrain along the track.\n");
    printf(" And an xg of the sample path, of points sampled, ie 'lon lat ; elev', is also output.\n");
    printf(" The 'sample' path corresponds to points sampled in the DEM3 file.\n");

}

#ifndef ISDIGIT
#define ISDIGIT(a) (( a >= '0' ) && ( a <= '9' ))
#endif
#ifndef ADDED_IS_DIGITS
#define ADDED_IS_DIGITS
#define ISNUM ISDIGIT
static int is_digits(char * arg)
{
    size_t len,i;
    len = strlen(arg);
    for (i = 0; i < len; i++) {
        if ( !ISNUM(arg[i]) )
            return 0;
    }
    return 1; /* is all digits */
}

#endif // #ifndef ADDED_IS_DIGITS

bool is_decimal(char *cp) 
{
    size_t ii, len = strlen(cp);
    int c, dcnt = 0, dots = 0;
    for (ii = 0; ii < len; ii++) {
        c = cp[ii];
        if (c == '-') {
            if (ii)
                return false;
        } else if (ISDIGIT(c)) {
            dcnt++;
        } else if (c == '.') {
            if (dots)
                return false;
            dots++;
        } else {
            return false;
        }
    }
    return true;
}

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

const char *spacey = " \t\r\n";
void trim_trailing_space( std::string & str )
{
    size_t endpos = str.find_last_not_of(spacey);
    if( std::string::npos != endpos ) {
        str = str.substr( 0, endpos+1 );
    }
}
void trim_leading_space( std::string & str )
{
    size_t startpos = str.find_first_not_of(spacey);
    if( std::string::npos != startpos ) {
        str = str.substr( startpos );
    }
}

void trim_all( std::string & str )
{
    trim_trailing_space(str);
    trim_leading_space(str);
}

typedef std::vector<std::string> vSTG;

vSTG split_whitespace( const std::string &str, int maxsplit = 0 );

vSTG split_whitespace( const std::string &str, int maxsplit )
{
    vSTG result;
    std::string::size_type len = str.length();
    std::string::size_type i = 0;
    std::string::size_type j;
    int countsplit = 0;
    while (i < len) {
        while (i < len && isspace((unsigned char)str[i])) {
		    i++;
		}
        j = i;  // next non-space
        while (i < len && !isspace((unsigned char)str[i])) {
		    i++;
		}

		if (j < i) {
            result.push_back( str.substr(j, i-j) );
		    ++countsplit;
		    while (i < len && isspace((unsigned char)str[i])) {
                i++;
            }

		    if (maxsplit && (countsplit >= maxsplit) && i < len) {
                result.push_back( str.substr( i, len-i ) );
                i = len;
		    }
		}
    }
    return result;
}

template <class Type>
class ConvValue
{
   private:
      Type Factor;   // The value to multiply by
   public:
      // Constructor initializes the value to multiply by
      ConvValue ( const Type& _Val ) : Factor ( _Val ) {
      }

      // The function call for the element to be multiplied
      Type operator ( ) ( Type& elem ) const 
      {
          if (elem == ',')
              return ' ';
          else
              return elem;
      }
};

// this will load a xg file, setting a beginning and ending track to sample
static int load_track_xg(char *file)
{
    int iret = 0;
    if (is_file_or_directory(file) != 1) {
        SPRTF("%s: Unable to 'stat' file '%s'!\n", module, file);
        return 1;
    }
    size_t size = get_last_file_size();
    if (size == 0) {
        SPRTF("%s: File '%s' is EMPTY!\n", module, file);
        return 1;
    }
    std::ifstream infile(file);
    std::string line,s;
    std::string color("color");
    std::string anno("color");
    std::string cc("yellow");
    std::string yell("yellow");
    std::string next("next");
    size_t len,off,vcount = 0;
    vVERTS verts;
    VERT vert;
    while (std::getline(infile, line)) {
        off = line.find('#');
        if( std::string::npos != off ) {
            line = line.substr(0,off);
        }
        trim_all(line);
        if (line.size() == 0) continue;
        if (line[0] == '#') continue;
        transform(line.begin(), line.end(), line.begin(), ConvValue<char> (',') );

        transform(line.begin(), line.end(), line.begin(), ::tolower);
        vSTG v = split_whitespace(line);
        len = v.size();
        s = v[0];
        if (s == color) {
            for (off = 1; off < len; off++) {
                s = v[off];
                if (s == "=") continue;
                cc = s; // change in color
                break;
            }

        } else if (s == anno) {

        } else if (s == next) {
            if (verts.size()) {
                segs.push_back(verts);
                verts.clear();
            }
        } else {
            std::istringstream iss_xy (line);
            if ( iss_xy >> vert.x >> vert.y ) {
                if  (cc == yell) {
                    if (vcount == 0) {
                        bgn_lat = vert.y;
                        bgn_lon = vert.x;
                    }
                    verts.push_back(vert);
                    vcount++;
                }
            }
        }
    }
    if (verts.size()) {
        segs.push_back(verts);
        verts.clear();
    }
    len = 0;
    if (vcount) {
        end_lat = vert.y;
        end_lon = vert.x;
        len = segs.size();
    }
    if (len) {
        SPRTF("%s: Got %d segments from '%s'\n", module, (int)len, file);
        got_segments = true;
    } else {
        SPRTF("%s: Got NO segments from '%s'\n", module, file);
        iret = 1;
    }
    return iret;
}

static void set_default_log()
{
    int c;
    char *tb = GetNxtBuf();
    char *env;
    size_t len;
    *tb = 0;
#ifdef WIN32
    env = getenv("APPDATA");
    if (!env)
        env = getenv("LOCALAPPDATA");
    if (env) {
        strcpy(tb,env);
    }
#else
    env = getenv("HOME");
    if (env) {
        strcpy(tb,env);
        len = strlen(tb);
        if (len) {
            c = tb[len - 1];
            if ( !(( c == '\\' ) || ( c =='/')) )
                strcat(tb,"\\");
        }
    }
#endif
    len = strlen(tb);
    if (len) {
        c = tb[len - 1];
        if ( !(( c == '\\' ) || ( c =='/')) ) {
#ifdef WIN32
            strcat(tb,"\\");
#else
            strcat(tb,"/");
#endif
        }
    }
    strcat(tb,DEF_LOG_FILE);
    set_log_file(tb,false);
}

//////////////////////////////////////////////////////////////////////
// int scan_for_log_file( int argc, char **argv, std::string &argsv )
//
// Need a location that is user writable, so in UNIX
// use $HOME/tempdem.txt, in Windows use %APPDATA%\tempdem.txt,
// which is C:\Users\<users_name>\AppData\Roaming
/////////////////////////////////////////////////////////////////////
static int scan_for_log_file( int argc, char **argv, std::string &argsv )
{
    int c, i;
    int i2 = 0;
    char *arg, *sarg;
    set_default_log();
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            if (c == 'l') {
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_log_file(sarg,false);
                    argsv += arg;
                    argsv += " ";
                    argsv += sarg;
                    argsv += " ";
                } else {
                    printf("%s: Expected log file name to follow '%s'! Aborting...\n", module, arg );
                    return 1;
                }
            }
        }
    }
    return 0;
}


// examples
// -v9 -31.696563 148.636306 -33.946816 151.176132 
// -t F:\Projects\gshhg\build\per-syd.xg
// -t C:\Users\user\Documents\FG\LEBL-LEIG\LEBL-LEIG.xg
// -t D:\FG\d-and-c\tracks\VHSK-01.xg
static int parse_commands(int argc, char **argv)
{
    int i, c, i2;
    bool isdec;
    char *arg, *sarg;
    std::string argsv;

    bgn_lat = bgn_lon = end_lat = end_lon = 400;
    if (scan_for_log_file( argc, argv, argsv ))
        return 1;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        argsv += arg;
        argsv += " ";
        isdec = is_decimal(arg);
        if ((*arg == '-') && !isdec) {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help();
                return 2;
            case 'd':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    if (is_file_or_directory(sarg) != 2) {
                        SPRTF("%s: Path '%s' NOT a valid directory! Aborting...\n", module, sarg);
                        return 1;
                    }
                    dem3_path = strdup(sarg);
                    argsv += sarg;
                    argsv += " ";
                } else {
                    SPRTF("%s: Expected valid path to dem1 hgt files to follow '%s'! Aborting...\n", module, arg );
                    return 1;
                }
                break;
            case 'f':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    vert_factor = atof(sarg);
                    if (vert_factor <= 0.0) {
                        SPRTF("%s: Error: From '%s' got invalid veretical factor %lf! Aborting...\n", module, sarg, vert_factor);
                        return 1;
                    }
                    argsv += sarg;
                    argsv += " ";
                }
                else {
                    SPRTF("%s: Expected double vertical factor to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'l':
                i++;    // already actioned
                break;
            case 'm':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    xg_file2 = strdup(sarg);
                    argsv += sarg;
                    argsv += " ";
                } else {
                    SPRTF("%s: Expected file name to follow '%s'! Aborting...\n", module, arg );
                    return 1;
                }
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    xg_file = strdup(sarg);
                    argsv += sarg;
                    argsv += " ";
                } else {
                    SPRTF("%s: Expected file name to follow '%s'! Aborting...\n", module, arg );
                    return 1;
                }
                break;
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    arb_dist = atof(sarg);
                    if (arb_dist <= 0.0) {
                        SPRTF("%s: Error: From '%s' got invalid arbitrary degrees %lf! Aborting...\n", module, sarg, arb_dist);
                        return 1;
                    }
                    argsv += sarg;
                    argsv += " ";
                } else {
                    SPRTF("%s: Expected file name to follow '%s'! Aborting...\n", module, arg );
                    return 1;
                }
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    if (load_track_xg(sarg)) {
                        SPRTF("%s: Error during load of track.xg file '%s'! Aborting...\n", module, sarg);
                        return 1;
                    }
                    argsv += sarg;
                    argsv += " ";
                } else {
                    SPRTF("%s: Expected file name to follow '%s'! Aborting...\n", module, arg );
                    return 1;
                }
                break;
            case 'v':
                if (strcmp(arg,"--version") == 0) {
                    out_version();
                    exit(0);
                }
                sarg++; // skip the -v
                if (*sarg) {
                    // expect digits
                    if (is_digits(sarg)) {
                        verbosity = atoi(sarg);
                    } else if (*sarg == 'v') {
                        verbosity++; /* one inc for first */
                        while(*sarg == 'v') {
                            verbosity++;
                            arg++;
                        }
                    } else
                        goto Bad_CMD;
                } else
                    verbosity++;
                if (VERB1) printf("%s: Set verbosity to %d\n", module, verbosity);
                break;
            default:
Bad_CMD:
                SPRTF("%s: Unknown option '%s'! Aborting...\n", module, arg);
                return 1;
            }
        } else if (isdec){
            if (bgn_lat == 400) {
                bgn_lat = atof(arg);
            } else if (bgn_lon == 400) {
                bgn_lon = atof(arg);
            } else if (end_lat == 400) {
                end_lat = atof(arg);
            } else if (end_lon == 400) {
                end_lon = atof(arg);
            } else {
                SPRTF("%s: Already have lat/lon begin %lf,%lf, end %lf,%lf!\nWhat is this '%s'? Aborting...\n", module,
                    bgn_lat, bgn_lon, end_lat, end_lon, arg);
                return 1;
            }
        }
    }
    if ((bgn_lat == 400) || (bgn_lon == 400) || (end_lat == 400) || (end_lon == 400)) {
        SPRTF("%s 4 items bgn_lat bgn_lon end_lat end_lon NOT found in command! Aborting...\n", module );
        return 1;
    }
    if (!in_world_range(bgn_lat,bgn_lon)) {
        SPRTF("%s: Begin lat/lon %lf,%lf NOT in world range! Aborting...\n", module,
            bgn_lat, bgn_lon);
        return 1;
    }
    if (!in_world_range(end_lat,end_lon)) {
        SPRTF("%s: End lat/lon %lf,%lf NOT in world range! Aborting...\n", module,
            end_lat, end_lon);
        return 1;
    }
    if (is_file_or_directory((char *)dem3_path) != 2) {
        SPRTF("%s: Path '%s' NOT a valid directory! Aborting...\n", module, dem3_path);
        return 1;
    }
    if (argsv.size())
        usr_args = strdup(argsv.c_str());

    return 0;
}

typedef struct tagTRKPT {
    double lat,lon;
    short elev;
}TRKPT, *PTRKPT;

typedef std::vector<TRKPT> vTPTS;

vTPTS *pvPts = 0;
/* -----------------------------------------------------------------
    Hmmm, this WORKS but take a long time, even for a modest track
    say -31.696563 148.636306 -33.946816 151.176132 YGIL to YSSY
    Given XDIM/YDIM value, this is 3049x2702, or 8,238,398 elevations
    That took nearly 8 minutes...
    Really need to ONLY get the points along the track, somehow...
   ----------------------------------------------------------------- */
int get_bbox_heights( hgt3File &s, double min_lon, double min_lat, double max_lon, double max_lat )
{
    int iret = 0;
    double xdiff = max_lon - min_lon;
    double ydiff = max_lat - min_lat;
    double xfactor = xdiff / HGT3_XDIM;
    double yfactor = ydiff / HGT3_YDIM;
    double lat,lon;
    TRKPT tpt;
    short elev;
    size_t max;
    int xdim = (int)ceil(xfactor+1);
    int ydim = (int)ceil(yfactor+1);
    bool debug = (VERB9 && extra_debug) ? true : false;
    short min_elev = 9999;
    short max_elev = -9999;
    int void_count = 0;
    double bgn;
    char *elap;
    int failed_count = 0;
    int elev_count = 0;
    s.verb = debug;
    if (VERB1) {
        SPRTF("Got xdiff %lf, XDIM %lf, f %lf, width %d\n    ydiff %lf, YDIM %lf, f %lf, height %d\n", 
            xdiff, HGT3_XDIM, xfactor, xdim, ydiff, HGT3_YDIM, yfactor, ydim);
    }
    pvPts = new vTPTS;
    bgn = get_seconds();
    for (lat = min_lat; lat <= max_lat; lat += HGT3_YDIM) {
        for (lon = min_lon; lon <= max_lon; lon += HGT3_XDIM) {
            if (!s.get_elevation(lat,lon,&elev)) {
                SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon));
                //iret = 1;
                //goto func_Exit;
                failed_count++;
                continue;
            }
            if (elev == HGT_VOID) {
                void_count++;
            } else {
                elev_count++;
                if (elev > max_elev)
                    max_elev = elev;
                if (elev < min_elev)
                    min_elev = elev;
            }
            tpt.elev = elev;
            tpt.lat  = lat;
            tpt.lon  = lon;
            pvPts->push_back(tpt);
            if (debug) {
                SPRTF("%s: For lat,lon %s,%s got elev %d m, %d feet.\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    elev, (int)(elev * METER2FEET));
            }
        }
        if (!s.get_elevation(lat,lon,&elev)) {
            SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                get_trim_double(lat),
                get_trim_double(lon));
            failed_count++;
            continue;
            //iret = 1;
            //goto func_Exit;
        }
        if (elev == HGT_VOID) {
            void_count++;
        } else {
            elev_count++;
            if (elev > max_elev)
                max_elev = elev;
            if (elev < min_elev)
                min_elev = elev;
        }
        tpt.elev = elev;
        tpt.lat  = lat;
        tpt.lon  = lon;
        pvPts->push_back(tpt);
        if (debug) {
            SPRTF("%s: For lat,lon %s,%s got elev %d m, %d feet.\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                elev, (int)(elev * METER2FEET));
        }
    }
    // one more row
    for (lon = min_lon; lon <= max_lon; lon += HGT3_XDIM) {
        if (!s.get_elevation(lat,lon,&elev)) {
            SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                get_trim_double(lat),
                get_trim_double(lon));
            failed_count++;
            continue;
            //iret = 1;
            //goto func_Exit;
        }
        if (elev == HGT_VOID) {
            void_count++;
        } else {
            elev_count++;
            if (elev > max_elev)
                max_elev = elev;
            if (elev < min_elev)
                min_elev = elev;
        }
        tpt.elev = elev;
        tpt.lat  = lat;
        tpt.lon  = lon;
        pvPts->push_back(tpt);
        if (debug) {
            SPRTF("%s: For lat,lon %s,%s got elev %d m, %d feet.\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                elev, (int)(elev * METER2FEET));
        }
    }
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        failed_count++;
        //iret = 1;
        //goto func_Exit;
    } else {
        if (elev == HGT_VOID) {
            void_count++;
        } else {
            elev_count++;
            if (elev > max_elev)
                max_elev = elev;
            if (elev < min_elev)
                min_elev = elev;
        }
        tpt.elev = elev;
        tpt.lat  = lat;
        tpt.lon  = lon;
        pvPts->push_back(tpt);
        if (debug) {
            SPRTF("%s: For lat,lon %s,%s got elev %d m, %d feet.\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                elev, (int)(elev * METER2FEET));
        }
    }
    max = pvPts->size();
    elap = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Got %d elevs voids %d max %d m, %d feet, min %d m, %d feet, in %s.\n", module,
        (int)max, void_count,
        max_elev, (int)(max_elev * METER2FEET),
        min_elev, (int)(min_elev * METER2FEET),
        elap );
// func_Exit:
    return iret;
}

void write_xg_file(std::stringstream &xg)
{
    size_t len = xg.str().size();
    if (len) {
        rename_file_to_bak(xg_file);
        FILE *fp = fopen(xg_file,"w");
        if (fp) {
            size_t res = fwrite(xg.str().c_str(),1,len,fp);
            fclose(fp);
            if (res == len) {
                SPRTF("%s: Written %s\n", module, xg_file);
            }
        }
    }
}

void write_xg_file2(std::stringstream &xg)
{
    size_t len = xg.str().size();
    if (len) {
        rename_file_to_bak(xg_file2);
        FILE *fp = fopen(xg_file2,"w");
        if (fp) {
            size_t res = fwrite(xg.str().c_str(),1,len,fp);
            fclose(fp);
            if (res == len) {
                SPRTF("%s: Written %s\n", module, xg_file2);
            }
        }
    }
}


/* -------------------------------------------------------------------------
    Walk the track from bgn_lat,bgn_lon to end_lat,end_lon
    only getting elevations on/near the actual track.
    Setting the arb_dist lower reduces the number of elevation fetches
   ------------------------------------------------------------------------- */
int failed_count = 0;
int elev_count = 0;
short min_elev = 9999;
short max_elev = -9999;
int void_count = 0;
TRKPT tpt, max_tpt, min_tpt;
std::stringstream xg;

int process_current_bgn_end( hgt3File &s )
{
    int iret = 0;
    double lat,lon,dist;
    short elev;
    if (bgn_lat <= end_lat) {
        // increment from begin to end
        for (lat = bgn_lat; lat <= end_lat; lat += HGT3_YDIM) {
            if (bgn_lon <= end_lon) {
                // increment
                for (lon = bgn_lon; lon <= end_lon; lon += HGT3_YDIM) {
                    // now must test this point is on or near the track
                    dist = DistanceFromLine(lon, lat,
                        bgn_lon, bgn_lat, end_lon, end_lat );
                    if (dist < arb_dist) {
                        if (!s.get_elevation(lat,lon,&elev)) {
                            SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                                get_trim_double(lat),
                                get_trim_double(lon));
                            failed_count++;
                            continue;
                            //iret = 1;
                            //goto func_Exit;
                        }
                        tpt.elev = elev;
                        tpt.lat  = lat;
                        tpt.lon  = lon;
                        if (elev == HGT_VOID) {
                            void_count++;
                        } else {
                            elev_count++;
                            if (elev > max_elev) {
                                max_tpt = tpt;
                                max_elev = elev;
                            }
                            if (elev < min_elev) {
                                min_tpt = tpt;
                                min_elev = elev;
                            }
                        }
                        xg << lon << " " << lat << " # " << elev << EOL;
                        pvPts->push_back(tpt);
                        if (VERB5) {
                            SPRTF("inc/inc: point lat,lon %lf,%lf, distance %s, elev %d m. %d feet.\n",
                                lat,lon, get_trim_double(dist),
                                elev, (int)(elev * METER2FEET) );
                        }
                    }
                }
            } else {
                // decrement
                for (lon = bgn_lon; lon >= end_lon; lon -= HGT3_YDIM) {
                    // now must test this point is on or near the track
                    dist = DistanceFromLine(lon, lat,
                        bgn_lon, bgn_lat, end_lon, end_lat );
                    if (dist < arb_dist) {
                        if (!s.get_elevation(lat,lon,&elev)) {
                            SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                                get_trim_double(lat),
                                get_trim_double(lon));
                            failed_count++;
                            continue;
                            //iret = 1;
                            //goto func_Exit;
                        }
                        tpt.elev = elev;
                        tpt.lat  = lat;
                        tpt.lon  = lon;
                        if (elev == HGT_VOID) {
                            void_count++;
                        } else {
                            elev_count++;
                            if (elev > max_elev) {
                                max_tpt = tpt;
                                max_elev = elev;
                            }
                            if (elev < min_elev) {
                                min_tpt = tpt;
                                min_elev = elev;
                            }
                        }
                        xg << lon << " " << lat << " # " << elev << EOL;
                        pvPts->push_back(tpt);
                        if (VERB5) {
                            SPRTF("inc/dec: point lat,lon %lf,%lf, distance %s, elev %d m. %d feet.\n",
                                lat,lon, get_trim_double(dist),
                                elev, (int)(elev * METER2FEET) );
                        }
                    }
                }
            }
        }
    } else {
        // decrement from begin to end
        for (lat = bgn_lat; lat >= end_lat; lat -= HGT3_YDIM) {
            if (bgn_lon <= end_lon) {
                // increment
                for (lon = bgn_lon; lon <= end_lon; lon += HGT3_YDIM) {
                    // now must test this point is on or near the track
                    dist = DistanceFromLine(lon, lat,
                        bgn_lon, bgn_lat, end_lon, end_lat );
                    if (dist < arb_dist) {
                        if (!s.get_elevation(lat,lon,&elev)) {
                            SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                                get_trim_double(lat),
                                get_trim_double(lon));
                            failed_count++;
                            continue;
                            //iret = 1;
                            //goto func_Exit;
                        }
                        tpt.elev = elev;
                        tpt.lat  = lat;
                        tpt.lon  = lon;
                        if (elev == HGT_VOID) {
                            void_count++;
                        } else {
                            elev_count++;
                            if (elev > max_elev) {
                                max_tpt = tpt;
                                max_elev = elev;
                            }
                            if (elev < min_elev) {
                                min_tpt = tpt;
                                min_elev = elev;
                            }
                        }
                        pvPts->push_back(tpt);
                        xg << lon << " " << lat << " # " << elev << EOL;
                        if (VERB5) {
                            SPRTF("dec/inc: point lat,lon %lf,%lf, distance %s, elev %d m. %d feet.\n",
                                lat,lon, get_trim_double(dist),
                                elev, (int)(elev * METER2FEET) );
                        }
                    }
                }
            } else {
                // decrement
                for (lon = bgn_lon; lon >= end_lon; lon -= HGT3_YDIM) {
                    // now must test this point is on or near the track
                    dist = DistanceFromLine(lon, lat,
                        bgn_lon, bgn_lat, end_lon, end_lat );
                    if (dist < arb_dist) {
                        if (!s.get_elevation(lat,lon,&elev)) {
                            SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
                                get_trim_double(lat),
                                get_trim_double(lon));
                            failed_count++;
                            continue;
                            //iret = 1;
                            //goto func_Exit;
                        }
                        tpt.elev = elev;
                        tpt.lat  = lat;
                        tpt.lon  = lon;
                        if (elev == HGT_VOID) {
                            void_count++;
                        } else {
                            elev_count++;
                            if (elev > max_elev) {
                                max_tpt = tpt;
                                max_elev = elev;
                            }
                            if (elev < min_elev) {
                                min_tpt = tpt;
                                min_elev = elev;
                            }
                        }
                        pvPts->push_back(tpt);
                        xg << lon << " " << lat << " # " << elev << EOL;
                        if (VERB5) {
                            SPRTF("dec/dec: point lat,lon %lf,%lf, distance %s, elev %d m. %d feet.\n",
                                lat,lon, get_trim_double(dist),
                                elev, (int)(elev * METER2FEET) );
                        }
                    }
                }
            }
        }
    }

    return iret;
}
int get_track_elevations( hgt3File &s, PTRKPT ptpt2 )
{
    int iret = 0;
    double dist;
    pvPts = new vTPTS;
    double bgn = get_seconds();
    PTRKPT ptpt, ptpt1;
    size_t ii, max;
    char *elap;
    double xdiff = (bgn_lon < end_lon) ? end_lon - bgn_lon : bgn_lon - end_lon;
    double ydiff = (bgn_lat < end_lat) ? end_lat - bgn_lat : bgn_lat - end_lat;
    double hypot = sqrt( (xdiff * xdiff) + (ydiff * ydiff) );
    double factor = hypot / HGT3_XDIM;
    int ifact = (int)(ceil(factor)+1);
    if (got_segments) {
        size_t i,len;
        max = segs.size();
        for (ii = 0; ii < max; ii++) {
            vVERTS *pvv = &segs[ii];
            len = pvv->size();
            for (i = 1; i < len; i++) {
                PVERT pv1 = &pvv->at(i-1);
                PVERT pv2 = &pvv->at(i);
                bgn_lat = pv1->y;
                bgn_lon = pv1->x;
                end_lat = pv2->y;
                end_lon = pv2->x;
                iret |= process_current_bgn_end( s );
            }
        }
    } else {
        iret = process_current_bgn_end( s );
    }

    max = pvPts->size();
    elap = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Got %d elevs, targ %d, voids %d, failed %d, good %d\n", module,
        (int)max, ifact, void_count, failed_count, elev_count );
    SPRTF("%s: Elevations: max %d m, %d feet, min %d m, %d feet, in %s.\n", module,
        max_elev, (int)(max_elev * METER2FEET),
        min_elev, (int)(min_elev * METER2FEET),
        elap );
    if (xg.str().size()) {
        xg << "NEXT" << EOL;
        xg << "# generated " << max << " segments, using arb. dist " << arb_dist << EOL;
        xg << "anno " << ptpt2[0].lon << " " << ptpt2[0].lat << " BGN elev " << ptpt2[0].elev << " m, " << (int)(ptpt2[0].elev * METER2FEET) << " feet." << EOL;
        xg << "anno " << ptpt2[1].lon << " " << ptpt2[1].lat << " END elev " << ptpt2[1].elev << " m, " << (int)(ptpt2[1].elev * METER2FEET) << " feet." << EOL;
        xg << "anno " << min_tpt.lon << " " << min_tpt.lat << " MIN elev " << min_tpt.elev << " m, " << (int)(min_tpt.elev * METER2FEET) << " feet." << EOL;
        xg << "anno " << max_tpt.lon << " " << max_tpt.lat << " MAX elev " << max_tpt.elev << " m, " << (int)(max_tpt.elev * METER2FEET) << " feet."  << EOL;
        xg << "color red" << EOL;
        // xg << bgn_lon << " " << bgn_lat << EOL;
        // xg << end_lon << " " << end_lat << EOL;
        xg << ptpt2[0].lon << " " << ptpt2[0].lat << EOL;
        xg << ptpt2[1].lon << " " << ptpt2[1].lat << EOL;
        xg << "# generated " << get_date_time_stg() << ", by " << module << EOL;
        if (usr_args) {
            xg << "# " << usr_args << EOL;
        }
        write_xg_file(xg);
    }
    if (max) {
        double az1,az2;
        short prev_elev;
        short diff, min_diff = 5;
        double div = vert_factor;  // 10.0; // was 100.0
        xg.str("");
        for (ii = 0; ii < max; ii++) {
            ptpt = &pvPts->at(ii);
            if (ii) {
                if (ptpt->elev != HGT_VOID) {
                    diff = (prev_elev > ptpt->elev) ? prev_elev - ptpt->elev : ptpt->elev - prev_elev;
                    if (diff >= min_diff) {
                        fg_geo_inverse_wgs_84( ptpt1->lat, ptpt1->lon, ptpt->lat, ptpt->lon, &az1, &az2, &dist );
                        dist /= div;
                        xg << dist << " " << ptpt->elev << EOL;
                        prev_elev = ptpt->elev;
                    }
                }
            } else {
                xg << 0 << " " << ptpt->elev << EOL;
                prev_elev = ptpt->elev;
                ptpt1 = ptpt;
            }
        }
        xg << "NEXT" << EOL;
        xg << "color gray" << EOL;
        xg << 0 << " " << 0 << EOL;
        xg << dist << " " << 0 << EOL;
        xg << dist << " " << max_elev << EOL;
        xg << 0 << " " << max_elev << EOL;
        xg << 0 << " " << 0 << EOL;
        xg << "NEXT" << EOL;
        write_xg_file2(xg);

    }
//func_Exit:
    return iret;
}

int get_track_heights() 
{
    int iret = 0;
    double lat,lon;
    double min_lat,min_lon,max_lat,max_lon;
    short elev;
    TRKPT tpts[2];
    hgt3File s;
    if (!s.set_hgt_dir(dem3_path)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(dem3_path);
        return 1;
    }
    s.verb = VERB1 ? true : false;
    lat = bgn_lat;
    lon = bgn_lon;
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }
    tpts[0].elev = elev;
    tpts[0].lat  = lat;
    tpts[0].lon  = lon;
    min_lat = max_lat = lat;
    min_lon = max_lon = lon;
    if (VERB1) {
        SPRTF("%s: For lat,lon %s,%s got elev %d m, %d feet.\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev, (int)(elev * METER2FEET));
    }
    lat = end_lat;
    lon = end_lon;
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }
    tpts[1].elev = elev;
    tpts[1].lat  = lat;
    tpts[1].lon  = lon;
    if (VERB1) {
        SPRTF("%s: For lat,lon %s,%s got elev %d m, %d feet.\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev, (int)(elev * METER2FEET));
    }
    if (lat > max_lat)
        max_lat = lat;
    if (lat < min_lat)
        min_lat = lat;
    if (lon > max_lon)
        max_lon = lon;
    if (lon < min_lon)
        min_lon = lon;
    if (do_full_bbox) {     // this is really WAY TOO SLOW
        // returning MUCH MORE than is required.
        iret = get_bbox_heights( s, min_lon, min_lat, max_lon, max_lat );
    } else {
        // this is a faster, better service, only fetching elevation on or near the track
        iret = get_track_elevations( s, tpts );
    }
    return iret;
}

#if 0 // 000000000000000000000000000000000000000000
void test()
{
    std::string s1("1 2 # comment");
    std::string s2("# all comment");
    size_t off = s1.find('#');
    if( std::string::npos != off ) {
        s1 = s1.substr(0,off);
        trim(s1);
    }
    off = s2.find('#');
    if( std::string::npos != off ) {
        s2 = s2.substr(0,off);
        trim(s2);
    }
    exit(1);
}
#endif // 000000000000000000000000000000000000000

// main() OS entry
int main( int argc, char **argv )
{
    //test();
    int iret = parse_commands(argc,argv);
    if (iret)
        return iret;
    iret = get_track_heights();
    if (pvPts) {
        pvPts->clear();
        delete pvPts;
        pvPts = 0;
    }
    return iret;
}


// eof = dem1_track.cxx
