/*\
 * arr_test.cxx
 *
 * Copyright (c) 2017 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <string.h> // for strdup(), ...
#include <tg/array.hxx>
#include <sstream> // for #include <iostream>
#include <algorithm> // tranform()
#include "sprtf.hxx"
// other includes

static const char *module = "arr_test";

static const char *usr_input = 0;
static const char *def_log = "temparr.txt";

void give_help( char *name )
{
    printf("%s: usage: [options] usr_input\n", module);
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(0)\n");
    // TODO: More help
}

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            // TODO: Other arguments
            default:
                printf("%s: Unknown argument '%s'. Try -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                printf("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(arg);
        }
    }
    if (!usr_input) {
        printf("%s: No user input found in command!\n", module);
        return 1;
    }
    return 0;
}

std::string get_base_name(const char *path)
{
    std::string base;
    size_t ii, max = strlen(path);
    int c;
    if (max) {
        for (ii = max - 1; ii >= 0; ii--) {
            c = path[ii];
            if ((c == '\\') || (c == '/')) {
                if (ii < (max - 1)) {
                    base = &path[ii + 1];
                    break;
                }
            } if (ii == 0) {
                base = path;
            }
        }
    }
    return base;
}

char *get_directory_name(const char *path)
{
    char *cp = GetNxtBuf();
    size_t ii, max = strlen(path);
    int c;
    *cp = 0;
    if (max) {
        for (ii = max - 1; ii >= 0; ii--) {
            c = path[ii];
            if ((c == '\\') || (c == '/')) {
                if (ii < (max - 1)) {
                    if (ii) {
                        strncpy(cp,path,ii);
                        cp[ii] = 0;
                        break;
                    }
                }
            }
        }
    }
    return cp;
}

#ifndef ISDIGIT
#define ISDIGIT(a) ((a >= '0') && (a <= '9'))
#endif

bool is_all_digits(std::string &base)
{
    int c;
    for (std::string::iterator it = base.begin(); it != base.end(); ++it) {
        c = *it;
        if (!ISDIGIT(c))
            return false;
    }

    return true;
}

int arr_test()
{
    int iret = 0;
    std::stringstream out;
    SGBucket b;
    //double lat = -33.943392; 
    //double lon = 151.179773; // YSSY Sydney Intl  tile=e150s30
    //SGBucket b(SGGeod::fromDeg(lon, lat));
    //out << "input: lon=" << lon << ", lat=" << lat <<
    //    ", gives Bucket = " << b;
    //SPRTF("%s\n", out.str().c_str());
    //out.str(""); // stringstream clear the stream
    TGArray arr(usr_input);
    if (!arr.is_open()) {
        SPRTF("%s: Failed to open base '%s'\n", module, usr_input);
        return 1;
    }
    SPRTF("%s: Loaded '%s'\n", module, usr_input);
    std::string base = get_base_name(usr_input);
    std::string dir = get_directory_name(usr_input);
    if (is_all_digits(base)) {
        int index = atoi(base.c_str());
        SGBucket b2(index);
        b = b2;
        out << "base name '" << base << "' indicates Bucket " << b << ", path " << b.gen_base_path();
        SPRTF("%s\n", out.str().c_str());
        out.str(""); // stringstream clear the stream
    }

    arr.parse(b);
    double ox = arr.get_originx();  // const { return originx; }
    double oy = arr.get_originy();      // const { return originy; }
    int cols = arr.get_cols();          // const { return cols; }
    int rows = arr.get_rows();          // const { return rows; }
    double col_step = arr.get_col_step();   // const { return col_step; }
    double row_step = arr.get_row_step();   // const { return row_step; }
    ox /= 3600;
    oy /= 3600;
    SPRTF("ox=%f, oy=%f, cols=%d, rows=%d, col_step=%f, row_step=%f\n", ox, oy, cols, rows, col_step, row_step);
    int void_elev = -32768;
    int row, col, elev;
    int min_elev = 32768;
    int max_elev = -32768;
    int void_count = 0;
    int zero_count = 0;
    int elements = 0;
    out.str(""); // stringstream clear the stream
    for (row = 0; row < rows; row++) {
        out << "Row " << (row + 1) << ": ";
        for (col = 0; col < cols; col++) {
            elements++;
            elev = arr.get_array_elev(col, row);
            if (elev <= void_elev) {
                out << "VOID ";
                void_count++;
            }
            else {
                out << elev << " ";
                if (elev > max_elev)
                    max_elev = elev;
                if (elev < min_elev)
                    min_elev = elev;
                if (elev == 0)
                    zero_count++;
            }
        }
        SPRTF("%s\n", out.str().c_str());
        out.str(""); // stringstream clear the stream
    }
    out << "Scanned " << elements << ", void " << void_count << ", zero " << zero_count <<
        ", max " << max_elev << ", min " << min_elev;
    SPRTF("%s\n", out.str().c_str());
    out.str(""); // stringstream clear the stream
    return iret;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)def_log, false);
    iret = parse_args(argc,argv);
    if (iret) {
        if (iret == 2)
            iret = 0;
        return iret;
    }

    iret = arr_test(); // actions of app

    return iret;
}


// eof = arr_test.cxx
