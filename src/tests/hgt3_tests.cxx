// hgt3_tests.cxx

#include <stdio.h>
#include <string>
#include <algorithm>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#endif
#ifdef USE_SIMGEAR_LIB
#include <simgear/constants.h>
#include <simgear/math/SGMath.hxx>
#endif // #ifdef USE_SIMGEAR_LIB
#include "sprtf.hxx"
#include "utils.hxx"
#include "hgt3File.hxx"
#include "dem_utils.hxx"
#include "dem_tests.hxx"
#include "slippystuff.hxx"
#include "color.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "dir_utils.hxx"
#include "hgt3_tests.hxx"

static const char *module = "hgt3_tests";

std::string get_slippy_image_file( char * tile )
{
    char *tmp = get_cache_file_name(tile,true);
    std::string img = get_image_out_dir();
    img += PATH_SEP;
    img += tmp;
#ifdef USE_PNG_LIB
    img += ".png";
#else // !USE_PNG_LIB
    img += ".bmp";
#endif // USE_PNG_LIB y/n
    return img;
}


int do_hgt3_test1() // write ferr3 N31E035.hgt to image, and report on voids
{
    int iret = 0;
    SPRTF("\n");
    SPRTF("%s: === do_hgt3 test1 =====================\n", module);

    //std::string path(san_fran);
    //std::string path(mt_everest);
    //std::string path(norway_rose);
    std::string path(dead_sea);
    //std::string path(usgs_dead_sea);
    set_out_files(path);
    hgt3File hgt;
    hgt.report = true;  // report 'void'
    hgt.alt_file = usgs_dead_sea;
    hgt.gtopo_dir = globe30_dir;
    hgt.set_file(path);
    if (!hgt.load())
        return 1;
    iret = hgt.hgt_to_image();
    return iret;
}

////////////////////////////////////////////////////////////////////////////
// run the full set of elevation tests - PELEVTESTS pet = elevtests;
// Using both the de Ferranti hgt_dir, and the previous USGS usgs_dir
///////////////////////////////////////////////////////////////////////////
int do_hgt3_test7() // elevtests; de Ferranti hgt_dir, and USGS usgs_dir
{
    int iret = 0;
    vSTG missed_files, void_files, failed_files;
    char *cp;
    std::string f;
    int ilat,ilon;

    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test7 =====================\n", module);
    PELEVTESTS pet = elevtests;
    hgt3File h;

    double lat,lon;
    short elev;
    int count = 0;
    int i, failed_cnt, void_cnt, missing_cnt;
    while (pet->name) {
        count++;
        pet++;
    }
    SPRTF("\n");
    SPRTF("%s: %d 'test' elevations using [%s].\n", module,
        count,
        ferr3_dir );

    // set the directory
    if (!h.set_hgt_dir(ferr3_dir)) {
        SPRTF("%s: Failed to set_hgt_dir(%s0!\n", module,
            ferr3_dir);
        return 1;
    }


    pet = elevtests; // reset 'pet' pointer
    missing_cnt = failed_cnt = void_cnt = 0;
    while (pet->name) {
        lat = pet->lat;
        lon = pet->lon;
        cp = get_hgt_file_name( lat, lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: FAILED to get file name for lat,lon %s,%s\n",module,
                get_trim_double(lat),
                get_trim_double(lon) );
            check_me();
            return 1;
        }
        f = ferr3_dir;
        f += PATH_SEP;
        f += cp;
        if (h.verb)
            SPRTF("\n");
        if (is_file_or_directory((char *)f.c_str()) != 1) {
            missing_cnt++;
            f = cp;
            if (!string_in_vec( missed_files, f )) {
                missed_files.push_back(f);
            }
            SPRTF("%s: For lat,lon %s,%s, %s, FAILED to find file %s\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                cp );
        } else {
            if (h.get_elevation( lat, lon, &elev)) {
                SPRTF("%s: For lat,lon %s,%s, %s, got elev ", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    pet->name );
                if (elev == HGT_VOID) {
                    SPRTF("*VOID*");
                    void_cnt++;
                    f = get_file_name(h.file);
                    if (!string_in_vec( void_files, f )) {
                        void_files.push_back(f);
                    }
                } else {
                    SPRTF("%d",elev);
                }
                SPRTF(", expected %d.\n", pet->expected);
            } else {
                failed_cnt++;
                f = cp;
                if (!string_in_vec( failed_files, f )) {
                    failed_files.push_back(f);
                }
                SPRTF("%s: For lat,lon %s,%s, %s, FAILED, expected %d.\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    pet->name,
                    pet->expected);
            }
        }
        pet++;
    }

    missing_cnt = (int)missed_files.size();
    failed_cnt = (int)failed_files.size();
    void_cnt = (int)void_files.size();
    if (missing_cnt || failed_cnt || void_cnt) {
        if (h.verb)
            SPRTF("\n");
        SPRTF("%s: Problems with %d files, directory %s\n", module,
            (missing_cnt + failed_cnt + void_cnt),
            ferr3_dir );
        if (missing_cnt) {
            SPRTF("   Missing %d: ", missing_cnt);
            for (i = 0; i < missing_cnt; i++) {
                SPRTF("%s ", missed_files[i].c_str());
            }
            SPRTF("\n");
        }
        if (failed_cnt) {
            SPRTF("   Failed %d: ", failed_cnt);
            for (i = 0; i < failed_cnt; i++) {
                SPRTF("%s ", failed_files[i].c_str());
            }
            SPRTF("\n");
        }
        if (void_cnt) {
            SPRTF("   With void %d: ", void_cnt);
            for (i = 0; i < void_cnt; i++) {
                SPRTF("%s ", void_files[i].c_str());
            }
            SPRTF("\n");
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // USGS files
    ///////////////////////////////////////////////////////////////////////////////////////
    SPRTF("\n");
    SPRTF("%s: Same %d 'test' elevations using [%s].\n", module,
        count,
        usgs3_dir );

    pet = elevtests; // reset 'pet' pointer
    // set the directory
    if (!h.set_hgt_dir(usgs3_dir)) {
        SPRTF("%s: Failed to set_hgt_dir(%s0!\n", module,
            usgs3_dir);
        return 1;
    }
    missed_files.clear();
    void_files.clear();
    failed_files.clear();
    pet = elevtests; // reset 'pet' pointer
    missing_cnt = failed_cnt = void_cnt = 0;
    while (pet->name) {
        lat = pet->lat;
        lon = pet->lon;
        cp = get_hgt_file_name( lat, lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: FAILED to get file name for lat,lon %s,%s\n",module,
                get_trim_double(lat),
                get_trim_double(lon) );
            check_me();
            return 1;
        }
        f = usgs3_dir;
        f += PATH_SEP;
        f += cp;
        if (h.verb)
            SPRTF("\n");
        if (is_file_or_directory((char *)f.c_str()) != 1) {
            missing_cnt++;
            f = cp;
            if (!string_in_vec( missed_files, f )) {
                missed_files.push_back(f);
            }
            SPRTF("%s: For lat,lon %s,%s, %s, FAILED to find file %s\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                cp );
        } else {
            if (h.get_elevation( lat, lon, &elev)) {
                SPRTF("%s: For lat,lon %s,%s, %s, got elev ", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    pet->name );
                if (elev == HGT_VOID) {
                    SPRTF("*VOID*");
                    void_cnt++;
                    f = get_file_name(h.file);
                    if (!string_in_vec( void_files, f )) {
                        void_files.push_back(f);
                    }
                } else {
                    SPRTF("%d",elev);
                }
                SPRTF(", expected %d.\n", pet->expected);
            } else {
                failed_cnt++;
                f = cp;
                if (!string_in_vec( failed_files, f )) {
                    failed_files.push_back(f);
                }
                SPRTF("%s: For lat,lon %s,%s, %s, FAILED, expected %d.\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    pet->name,
                    pet->expected);
            }
        }
        pet++;
    }
    missing_cnt = (int)missed_files.size();
    failed_cnt = (int)failed_files.size();
    void_cnt = (int)void_files.size();
    if (missing_cnt || failed_cnt || void_cnt) {
        if (h.verb)
            SPRTF("\n");
        SPRTF("%s: Problems with %d files, directory %s\n", module,
            (missing_cnt + failed_cnt + void_cnt),
            usgs3_dir );
        if (missing_cnt) {
            SPRTF("   Missing %d: ", missing_cnt);
            for (i = 0; i < missing_cnt; i++) {
                SPRTF("%s ", missed_files[i].c_str());
            }
            SPRTF("\n");
        }
        if (failed_cnt) {
            SPRTF("   Failed %d: ", failed_cnt);
            for (i = 0; i < failed_cnt; i++) {
                SPRTF("%s ", failed_files[i].c_str());
            }
            SPRTF("\n");
        }
        if (void_cnt) {
            SPRTF("   With void %d: ", void_cnt);
            for (i = 0; i < void_cnt; i++) {
                SPRTF("%s ", void_files[i].c_str());
            }
            SPRTF("\n");
        }
    }

    return iret;
}

// Which is it N27 or N28...
// Mount Everest - 8,848 meters - 27.988, 86.9253 = N27E086.hgt
// const char *mt_ever = "F:\\data\\dem3\\hgt\\N27E086.hgt";
// const char *tristan = "F:\\data\\dem3\\hgt\\S38W013.hgt";
// Tristan da Cunha, Peak -  2,062 metres (6,765 ft) - -37.09444444,-12.288611111
// is in S38W013.hgt so this tile covers -37 to -38 - -13 to -12
// in the 1201x1201 image, its center is about x,y 856,130 with top/left being 0,0
// 856 / 1201 = 0.712739 = (-12.2886 - -13) * 1201 = 854
 // Guam -  N13E144.hgt
// const char *guam = "F:\\data\\dem3\\hgt\\N13E144.hgt";
// Mount Lamlam - 406 meters (1,332 ft) - 13.33861,144.662778 13 20 19 N 144 39 46 E
// on image is x,y 798,795 =
// y = 1 - (dlat - ilat) * 1201 = 794
// x = dlon - ilon * 1201 = 796
int do_hgt3_test3() // Mt Lamlam, Guam, elev, N13E144 to img, Tristan, elev, S38W013 to img 
{
    int iret = 0;
    std::string file1(mt_everest);
    std::string file2(tristan);
    std::string file3(guam);
    double lat = 27.0;
    double lon = 86.0;
    short elev;
    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test3 =====================\n", module);
    hgt3File h;

    // set the directory
    if (!h.set_hgt_dir(ferr3_dir)) {
        SPRTF("%s: Failed to set_hgt_dir(%s0!\n", module,
            ferr3_dir);
        return 1;
    }
    lat = 13.33861;
    lon = 144.662778;
    if (!h.get_elevation( lat, lon, &elev ))
        return 1;
    SPRTF("%s: For lat,lon %s,%s, got elevation %d, offset x,y %s,%s\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev,
        get_NiceNumberStg(h.x_off),
        get_NiceNumberStg(h.y_off) );
    //return iret;


    if (!h.load_file(file3))
        return 1;
    iret = h.hgt_to_image();
    //return iret;

    lat = -37.09444444;
    lon = -12.288611111;
    if (!h.get_elevation( lat, lon, &elev ))
        return 1;
    SPRTF("%s: For lat,lon %s,%s, got elevation %d, offset x,y %s,%s\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev,
        get_NiceNumberStg(h.x_off),
        get_NiceNumberStg(h.y_off) );

    //h.set_file(file);
    if (!h.load_file(file2))
        return 1;
    iret = h.hgt_to_image();
    return iret;

    if (!h.scan_hgt_file(file1,lat,lon))
        return 1;

    // set the directory
    if (!h.set_hgt_dir(ferr3_dir)) {
        SPRTF("%s: Failed to set_hgt_dir(%s0!\n", module,
            ferr3_dir);
        return 1;
    }

    if (!h.get_elevation( h.max_lat, h.max_lon, &elev ))
        return 1;

    SPRTF("%s: For lat,lon %s,%s, got elevation %d\n", module,
        get_trim_double(h.max_lat),
        get_trim_double(h.max_lon),
        elev );

    if (!h.get_elevation( h.min_lat, h.min_lon, &elev ))
        return 1;

    SPRTF("%s: For lat,lon %s,%s, got elevation %d\n", module,
        get_trim_double(h.min_lat),
        get_trim_double(h.min_lon),
        elev );


    return iret;

}

// e060n40 
// dem_tests -t 512
// Mt Everest N27E086.hgt write 256x256 image of BBOX
int do_hgt3_test4()
{
    int iret = 0;
    // Mt Everest - 8,848 meters - 27.988, 86.9253 = N27E086
    double lat = 27.988;
    double lon = 86.9253;
    //int zoom = 5; // need to alter the bounding box to be within the DEM3 N27E086, and renders the WHOLE 1x1 degree DEM3
    //int zoom = 6; // essentially renders the WHOLE 1x1 degree file 
    //int zoom = 7; // essentially renders the WHOLE 1x1 degree file
    // hgt3_tests: NOTE WELL: CHANGED box to BBOX lat,lon min 27.059126,86 max 28,87 span 0.940874,1

    //int zoom = 8; // again renders the WHOLE 1x1 degree file
    // hgt3_tests: NOTE WELL: CHANGED box to BBOX lat,lon min 27.059126,86 max 28,87 span 0.940874,1

    //int zoom = 9; // different files, SO NOTE WELL: CHANGED box to BBOX lat,lon min 27.683528,86.484375 max 28,87 span 0.316472,0.515625
    // so renders about 1/3 of the DEM3 file

    int zoom = 12; // different files SO NOTE WELL: CHANGED box to BBOX lat,lon min 27.916767,86.923828 max 27.994401,87 span 0.077635,0.076172
    // This is about the BEST DEM3 can give - it spans x,y 91,93 points, to get a 256x256 image.

    //int zoom = 15; //

    // int zoom = 16;  // but this only writes a 256x256 image based on ONE POINT from the DEM3 file!!! BASICALLY POINTLESS
    // hgt3_tests: Z=16 BBOX lat,lon min 27.9847,86.923828 max 27.989551,86.929321 covers x,y 1109,12 to 1116,18, span 7,6 points
    // hgt3_tests: Extract w,h 256,256 from x,y bgn 1109,12, span 1,1 (0.027344,0.023438) from N27E086.hgt.

    int width = 256;
    int height = 256;
    double flat,flon;   // lat,lon from file name

    short elev;
    std::string efile, file1, file2;
    int x_eve, y_eve, x_bgn, y_bgn, x_end, y_end;
    char *slippy_file;

    BNDBOX bb;
    PBNDBOX pbb = &bb;
    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test4() =====================\n", module);

    if (!get_slippy_path( lat, lon, zoom, &slippy_file)) {
        SPRTF("%s: HUH! Failed to get slippy file for lat,lon,zoom %s,%s,%d!\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return 1;
    }

    if (!get_map_bounding_box( lat, lon, zoom, pbb )) {
        SPRTF("%s: Failed to get bounding box for lat,lon,zoom %s,%s,%d!\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return 1;
    }

#if 0 // ok, this served the purpose of 'fixing' the above get_bounding_box()
    ////////////////////////////////////////////////////////////////////////
    BoundingBox bb2;
    PBoundingBox pbb2 = &bb2;
    if (!llz2bb( lat, lon, zoom, pbb2 )) {
        SPRTF("%s: Failed to get bounding box for lat,lon,zoom %s,%s,%d!\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return 1;
    }
    SPRTF("%s: For lat,lon,zoom %s,%s,%d %s.\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom,
            get_BoundingBox_stg(pbb2,true));
    // SANITY
    if (( lat < pbb2->south )||
        ( lat > pbb2->north )||
        ( lon < pbb2->west  )||
        ( lon > pbb2->east  )) {
        SPRTF("%s: AAARRGGHH! BAH!!!\n", module );
        return 1;
    }
    ///////////////////////////////////////////////////////////////////////////
#endif // 0

    // sanity check only - BBOX min bb_min_lat,bb_min_lon max bb_max_lat,bb_max_lon
    if (( lat < pbb->bb_min_lat )||
        ( lat > pbb->bb_max_lat )||
        ( lon < pbb->bb_min_lon )||
        ( lon > pbb->bb_max_lon )) {
        SPRTF("%s: What lat,lon %s,%s NOT in %s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            get_bounding_box_stg(pbb,true) );
        return 1;
    }
    SPRTF("%s: For lat,lon %s,%s got %s.\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        get_bounding_box_stg(pbb,true) );


    hgt3File s;
    s.verb = false; // set to run silent
    if (!s.set_hgt_dir(ferr3_dir)) {
        s.verb = true;
        s.set_hgt_dir(ferr3_dir);
        return 1;
    }

    // get Mt Everest, just to CHECK
    if (!s.get_elevation( lat, lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return 1;
    }
    SPRTF("%s: for lat,lon %s,%s reported elevation %d, expected 8848 meters.\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev );

    efile = s.file;
    x_eve = s.x_off;
    y_eve = s.y_off;
    if (get_hgt_lat_lon( efile, &flat, &flon )) {
        int chg = 0;
        // TODO: CHECK ME for negative values
        double mflat = flat + 1.0;
        double mflon = flon + 1.0;
        SPRTF("%s: File [%s], with lat,lon %s,%s to %s,%s, off y,x %d,%d.\n", module,
            efile.c_str(),
            get_trim_double(flat),
            get_trim_double(flon),
            get_trim_double(mflat),
            get_trim_double(mflon),
            y_eve, x_eve );
        /* we know the HGT only covers 1 x 1 degrees, so could potentially ADJUST BBOX to suit
            sprintf(cp,"BBOX lat,lon min %s,%s max %s,%s",
                get_trim_double(pbb->bb_min_lat),
                get_trim_double(pbb->bb_min_lon),
                get_trim_double(pbb->bb_max_lat),
                get_trim_double(pbb->bb_max_lon) );
            if (add_span) {
                sprintf(EndBuf(cp), " span %s,%s",
                    get_trim_double(pbb->bb_max_lat - pbb->bb_min_lat),
                    get_trim_double(pbb->bb_max_lon - pbb->bb_min_lon));
            }
        */
        // TODO: This hanky panky is to ensure the BBOX fits in this tile, BUT should be able to handle MULTIPLE files
        // TODO: WATCH IT: Potentially ALTERING a bound box to be within the DEM FILE bound
        if (pbb->bb_min_lat < flat) {
            pbb->bb_min_lat = flat;
            chg++;
        }
        if (pbb->bb_max_lat >= mflat) {
            pbb->bb_max_lat = mflat - MY_EPSILON;
            chg++;
        }
        if (pbb->bb_min_lon < flon) {
            pbb->bb_min_lon = flon;
            chg++;
        }
        if (pbb->bb_max_lon >= mflon) {
            pbb->bb_max_lon = mflon - MY_EPSILON;
            chg++;
        }
        if (chg) {
            SPRTF("%s: NOTE WELL: CHANGED box to %s\n", module,
                get_bounding_box_stg(pbb,true));
        }

    }

    s.verb = true; // to see errors
    // now get upper left elevation
    if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon) );
        return 1;
    }
    file1 = s.file;
    x_bgn = s.x_off;
    y_bgn = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon),
            elev,
            file1.c_str(),
            x_bgn, y_bgn);
    // now get lower right elevation
    if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon) );
        return 1;
    }
    file2 = s.file;
    x_end = s.x_off;
    y_end = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon),
            elev,
            file2.c_str(),
            x_end, y_end);

    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;
    SPRTF("%s: Z=%d %s covers x,y %d,%d to %d,%d, span %d,%d points\n", module,
        zoom,
        get_bounding_box_stg(pbb),
        x_bgn,y_bgn,x_end,y_end,
        xcols, yrows );

    if (strcmp(file1.c_str(),file2.c_str())) {
        SPRTF("%s: Presently NOT handling %s in different files, but will...\n", module,
            get_bounding_box_stg(pbb) );
        return 1;
    }

    // set the slippy tile name replacing the tempdem3.png
    std::string img = get_slippy_image_file( slippy_file );
    iret = hgt3_write_image( width, height, yrows, xcols, y_bgn, x_bgn, file1, img.c_str() );

    return iret;
}

int hgt3_write_image( int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1, const char *img_file )
{
    int iret = 0;
    int x,y;
    short elev;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    size_t doff;
    unsigned char r,g,b;
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    unsigned int colr;
    SPRTF("%s: Extract w,h %d,%d from x,y bgn %d,%d, span %d,%d, for %d,%d (%s,%s) from %s.\n", module,
        width, height,
        x_bgn, y_bgn,
        spanx, spany,
        xcols, yrows,
        get_trim_double(dspanx),
        get_trim_double(dspany),
        get_file_name(file1).c_str());

    hgt3File s;
    s.verb = false; // set to run silent
    if (!s.set_hgt_dir(ferr3_dir)) {
        s.verb = true;
        s.set_hgt_dir(ferr3_dir);
        return 1;
    }
    s.verb = true;  // to 'see' any errors

    bool ok = s.begin_span_file(file1);
    if (!ok) {
        SPRTF("%s: Failed to begin_span_file(%s)!\n", module, file1.c_str() );
        return 1;
    }

    s.verb = true;  // to see any error
    clear_used_colors();
    double bgn = get_seconds();
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
            if (!ok) {
                SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                check_me();
                return 1;
            }
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    if (writePNGImage24((char *)img_file, width, height, bit_depth, color_type, buffer )) {
        iret |= 8;
    }
#else // !USE_PNG_LIB
    if (writeBMPImage24((char *)img_file, width, height, buffer, bsize)) {
        iret |= 4;
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        xcols, yrows,
        get_NiceNumberStg(xcols * yrows),
        width, height, secs );
    delete buffer;
    out_used();
    return iret;

}

#if 0 // compare a string, if needed
static bool compare(std::string a, std::string b)
{
    //std::cout << "compare(" << a << "," << b << ")" << std::endl;
    return (a.compare(b) < 0);
}
#endif // 0 

//     13 KSFO San Francisco Intl       37.618674211,-122.375007609
// get slippy map tile at various zoom level
/*\

hgt3_tests: Got slippy path 10\163\396.png for lat,lon,zoom 37.618674,-122.375008,10 (37.71859,-122.695313,10). tile x,y 163,396
hgt3_tests: Show nine tiles around this point...
y 395 lat 37.996163 x 162 lon -123.046875 x 163 lon -122.695313 x 164 lon -122.34375 
y 396 lat 37.71859 x 162 lon -123.046875 x 163 lon -122.695313 x 164 lon -122.34375 
y 397 lat 37.439974 x 162 lon -123.046875 x 163 lon -122.695313 x 164 lon -122.34375 

\*/

// KSFO 37.618,-122.375,10 get elev, show 9 boxes, write 10_163_396 img use gen.color
int do_hgt3_test5() // KSFO z=10 get elev, show 9 boxes, write 10_163_396 img use gen.color
{
    int iret = 0;
    int width = 256;
    int height = 256;
    //int height = 128;

    int zoom = 10;
    double lat = 37.618;
    double lon = -122.375; //

    //int zoom = 10;
    //int zoom = 11;  // span 423,168 points
    //int zoom = 12;  // span 211,84 points
    //double lat = 37.618674211;
    //double lon = -122.375007609; // at 10 covers TWO hgt files N37W124.hgt, N37W123.hgt
    //double lon = -122.427; // NOPE, try to right no loaded N37W124.hgt and N37W123.hgt
    //double lon = -122.35;  // no N37W124.hgt and N37W123.hgt
    //double lon = -122.345;  // N37W124.hgt and N37W123.hgt
    //double lon = -122.3445;  // N37W124.hgt and N37W123.hgt
    //double lon = -122.344;     // N37W124.hgt and N37W123.hgt
    //double lon = -122.3438;     // N37W124.hgt and N37W123.hgt
    //double lon = -122.34378;    // N37W124.hgt and N37W123.hgt
    //double lon = -122.343755;   // N37W124.hgt and N37W123.hgt
    //double lon = -122.34375005; // N37W124.hgt and N37W123.hgt
    //double lon = -122.3437500001;
    //double lon = -122.34375;   // N37W123.hgt and N37W122.hgt
    //double lon = -122.3437;     // N37W123.hgt and N37W122.hgt
    //double lon = -122.34365;    // N37W123.hgt and N37W122.hgt
    //double lon = -122.3436;     // N37W123.hgt and N37W122.hgt
    //double lon = -122.3435;  // N37W123.hgt and N37W122.hgt
    //double lon = -122.3425;  // N37W123.hgt and N37W122.hgt
    //double lon = -122.34;  // no N37W123.hgt and N37W122.hgt
    //double lon = -122.325;  // no N37W123.hgt and N37W122.hgt
    //double lon = -122.3;  // no N37W123.hgt and N37W122.hgt
    //double lon = -122.25;  // no N37W123.hgt and N37W122.hgt
    //double lon = -122.148; // further right no loaded N37W123.hgt and N37W122.hgt
    //double lon = -122.04; // no loaded N37W123.hgt and N37W122.hgt
    // ok, the lon span is 0.703125 on a 1.0 degree file span so quite close
    // double lon = -122.485; // no N37W124.hgt and N37W123.hgt
    //double lon = -122.99; // still N37W124.hgt and N37W123.hgt
    char *path;
    LLZ llz;
    PLLZ pllz = &llz;
    short elev;
    int x,y,cnt;
    int maxx,maxy;
    double nlat, nlon;
    //double llat, llon;
    BNDBOX bb, ksfobb;
    bool fnd = false;
    PBNDBOX pbb = &ksfobb;
    std::string file1, file2;
    int x_bgn, y_bgn, x_end, y_end;
    size_t ii,max;
    std::string f;
    vSTG files;

    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test5() =====================\n", module);
    hgt3File s;
    s.verb = false; // set to run silent
    if (!s.set_hgt_dir(ferr3_dir)) {
        s.verb = true;
        s.set_hgt_dir(ferr3_dir);
        return 1;
    }

    s.verb = true; // to see errors
    if (!s.get_elevation( lat, lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return 1;
    }
    file1 = s.file;
    x_bgn = s.x_off;
    y_bgn = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev,
            file1.c_str(),
            x_bgn, y_bgn);

    if (!get_slippy_path(lat,lon,zoom,&path)) {
        SPRTF("%s: Failed to get slippy path for lat,lon,zoom %s,%s,%d!\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return 1;
    }
    if (!path_to_llz( path, pllz )) {
        SPRTF("%s: Failed converting slippy path %s, for lat,lon,zoom %s,%s,%d back to LLZ!\n", module,
            path,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return 1;

    }

    SPRTF("%s: Got slippy path %s for lat,lon,zoom %s,%s,%d (%s,%s,%d). tile x,y %d,%d\n", module,
        path,
        get_trim_double(lat),
        get_trim_double(lon),
        zoom,
        get_trim_double(pllz->lat),
        get_trim_double(pllz->lon),
        pllz->zoom,
        pllz->tilex,
        pllz->tiley);
    maxx = lon2tilex(MAX_SLIPPY_LON,zoom);
    //int maxy = lon2tilex(MAX_SLIPPY_LAT,zoom);
    maxy = lon2tilex(MIN_SLIPPY_LAT,zoom);
    SPRTF("%s: Z=%d For min_lat,max_lon %s,%s get maxy,maxx %d,%d\n", module,
        zoom,
        get_trim_double(MIN_SLIPPY_LAT),
        get_trim_double(MAX_SLIPPY_LON),
        maxy,maxx );

    cnt = 0;
    if (!get_map_bounding_box( lat, lon, zoom, &bb, true )) {
        SPRTF("%s: OOPS! Failed to get 'map' bounding box!\n", module);
        return 1;
    }

    nlat = bb.bb_max_lat;
    nlon = bb.bb_max_lon; // previous is MIN lon
    files.clear();
    SPRTF("%s: Z=%d: Show nine tiles around this point...\n", module, zoom);
    for (y = pllz->tiley - 1; y <= pllz->tiley + 1; y++) {
        // as y INCREASE, lat decrease, so max_lat (bb_max_lat) is set
        //llat = nlat;
        bb.bb_max_lat = nlat;    // previous is MAX lat
        nlat = tiley2lat(y,zoom);
        bb.bb_min_lat = nlat;    // current is MIN lat
        SPRTF("%s: y %d lat %s - ", module, y, get_trim_double(nlat));
        for (x = pllz->tilex - 1; x <= pllz->tilex + 1; x++) {
            // as x increase, lon increase, min_lon (bb_max_lon) is set
            //llon = nlon;
            //llat = nlat;
            bb.bb_max_lon = nlon; // previous is MIN lon
            nlon = tilex2lon(x,zoom);
            bb.bb_min_lon = nlon; // current is MAX lon
            SPRTF("%d: x %d lon %s ", (cnt + 1), x, get_trim_double(nlon));
            if (!get_hgt3_file_names( &bb, files, false, false )) {
                SPRTF("%s: Failed to get hgt3 file names!\n", module);
                check_me();
                return 1;
            }
            if (lat_lon_in_bounding_box(lat,lon,&bb)) {
                if (fnd) {
                    SPRTF("%s: ARGH! Can NOT find TWO BOXES!\n", module);
                    return 1;
                } else {
                    ksfobb = bb;
                    fnd = true;
                    SPRTF("[FND] ");
                }
            }
            cnt++;
        }
        SPRTF("\n");
    }   // do nine tiles
    if (fnd) {
        x = lon2tilex(bb.bb_max_lon,zoom);   // Use MIN lon to get x
        y = lat2tiley(bb.bb_max_lat,zoom);   // Use MAX lat to get y
        SPRTF("%s: Found %s x,y %d,%d\n", module, get_bounding_box_stg(&ksfobb,true), x, y);
        //ksfobb.bb_max_lon += 0.05;
        //ksfobb.bb_min_lon += 0.05;
        //SPRTF("Adjusted to %s\n", get_bounding_box_stg(&ksfobb,true));
    } else {
        SPRTF("%s: OOPS! Can NOT find a BOX!\n", module);
        return 1;
    }
    // std::sort(files.begin(),files.end(),compare);
    std::sort(files.begin(),files.end());
    max = files.size();
    SPRTF("%s: Req %d files: ", module, (int)max);
    for (ii = 0; ii < max; ii++) {
        f = files[ii];
        SPRTF("%s ", f.c_str());
    }
    SPRTF("\n");

    // next test
    if (!get_map_bounding_box( lat, lon, zoom, pbb, true )) {
        SPRTF("%s: OOPS! Failed to get 'map' bounding box!\n", module);
        return 1;
    }
    // now get upper left elevation
    if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon) );
        return 1;
    }
    file1 = s.file;
    x_bgn = s.x_off;
    y_bgn = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon),
            elev,
            file1.c_str(),
            x_bgn, y_bgn);
    // now get lower right elevation
    if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon) );
        return 1;
    }
    file2 = s.file;
    x_end = s.x_off;
    y_end = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon),
            elev,
            file2.c_str(),
            x_end, y_end);

    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;
    SPRTF("%s: Z=%d %s covers x,y %d,%d to %d,%d, span %d,%d points\n", module,
        zoom,
        get_bounding_box_stg(pbb),
        x_bgn,y_bgn,x_end,y_end,
        xcols, yrows );

    if (strcmp(file1.c_str(),file2.c_str())) {
        SPRTF("%s: Can NOT handling 2 files, %s and %s, but will...\n", module,
            get_file_name(file1).c_str(),
            get_file_name(file2).c_str());
        return 1;
    }
    std::string img = get_slippy_image_file(path);
    set_linear_get_color(true);
    iret = hgt3_write_image( width, height, yrows, xcols, y_bgn, x_bgn, file1, img.c_str() );
    set_default_get_color(true);

    return iret;

}

// already tested - is all in a single file
int dem3_bounding_box_2_image( PBNDBOX pbb )
{
    int iret = 0;
    short elev;
    int width = 256;
    int height = 256;
    std::string file1, file2;
    int x_bgn,y_bgn,x_end,y_end;
    int zoom = pbb->llz.zoom;
    char *tile;
    if (!get_slippy_path( pbb->llz.lat, pbb->llz.lon, zoom, &tile) ) {
        SPRTF("%s: ZUTE! failed to get slippy path!\n", module );
        check_me();
        return 1;
    }
    hgt3File s;
    s.verb = false; // set to run silent
    if (!s.set_hgt_dir(ferr3_dir)) {
        s.verb = true;
        s.set_hgt_dir(ferr3_dir);
        return 1;
    }
    s.verb = true; // to see errors

    // now get upper left elevation
    if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
        SPRTF("%s: Failed elevation for TL lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon) );
        return 1;
    }
    file1 = s.file;
    x_bgn = s.x_off;
    y_bgn = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon),
            elev,
            file1.c_str(),
            x_bgn, y_bgn);

    // now get lower right elevation
    if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
        SPRTF("%s: Failed elevation for BR lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon) );
        return 1;
    }
    file2 = s.file;
    x_end = s.x_off;
    y_end = s.y_off;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon),
            elev,
            file2.c_str(),
            x_end, y_end);

    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;
    SPRTF("%s: Z=%d %s covers x,y %d,%d to %d,%d, span %d,%d points\n", module,
        zoom,
        get_bounding_box_stg(pbb),
        x_bgn,y_bgn,x_end,y_end,
        xcols, yrows );

    if (strcmp(file1.c_str(),file2.c_str())) {
        SPRTF("%s: Can NOT handling 2 files, %s and %s, but will...\n", module,
            get_file_name(file1).c_str(),
            get_file_name(file2).c_str());
        return 1;
    }

    if ( (xcols < MIN_SPAN) || (yrows < MIN_SPAN) ) {
        SPRTF("%s: At this ZOOM %d, on DEM3 data, the BBOX is LESS THAN MIN_SPAN %dx%d (ie %d,%d) points!\n", module,
            zoom, MIN_SPAN, MIN_SPAN,
            xcols, yrows);
        return 2;
    }
    std::string img = get_slippy_image_file(tile);
    iret = hgt3_write_image( width, height, yrows, xcols, y_bgn, x_bgn, file1, img.c_str() );
    return iret;
}

#define GTD2(a,b) get_trim_double(a),get_trim_double(b)

#define mff_Used 1

typedef struct tagMULTFILES {
    std::string file;
    int ilat, ilon;
    int flag;
}MULTFILES, *PMULTFILES;

typedef std::vector<MULTFILES> vMULT;

// de Ferranti DEM 3 file
// Its south western corner can be deduced from its file name: for example, N51E002.hgt 
// covers the area between N 51 E 2 and N 52 E 3, and S14W077.hgt covers S 14 W 77 to S 13 W 76.
// spans MULTIPLE DEM 3 files
// ==========================
int dem3_bbox_2_image( PBNDBOX pbb, vMULT &vMult, hgt3File &s )
{
    int iret = 0;
    const char *dir = ferr3_dir;
    size_t ii, max = vMult.size();
    MULTFILES mf;
    BNDBOX bb, fbb;
    int width = 256;
    int height = 256;
    size_t bsize = width * height * 3;
    unsigned char *buffer;
    int x_bgn, x_end, y_bgn, y_end;
    int xcols, yrows;
    double inc = 1.0 - MY_EPSILON;  // has to be slightly LESS than 1.0
    int min_ilat = 400;
    int max_ilat = -400;
    int min_ilon = 400;
    int max_ilon = -400;
    size_t cnt = 0;
    int ilat,ilon;
    int bwidth,bheight;
    std::string f;
    int zoom = pbb->llz.zoom;
    //double lat = pbb->llz.lat;
    //double lon = pbb->llz.lon;
    char *tile;

    if (!max) {
        SPRTF("%s: HEY! Seems no image files to process!\n",module);
        return 4;
    }

    if (!get_slippy_path( pbb->llz.lat, pbb->llz.lon, pbb->llz.zoom, &tile )) {
        SPRTF("%s: WHAT! failed to get slippy path!\n", module);
        check_me();
        return 1;
    }
    buffer = new unsigned char[bsize];
    if (!buffer) {
        SPRTF("%s: Memory FAILED %d bytes\n", module, (int)bsize );
        return 1;
    }

    memset(buffer,0,bsize); // all black to show MISSING slices

    std::string img = get_slippy_image_file(tile);

    for (ii = 0; ii < max; ii++) {
        mf = vMult[ii];
        if (mf.ilat < min_ilat)
            min_ilat = mf.ilat;
        if (mf.ilat > max_ilat)
            max_ilat = mf.ilat;
        if (mf.ilon < min_ilon)
            min_ilon = mf.ilon;
        if (mf.ilon > max_ilon)
            max_ilon = mf.ilon;
    }

    SPRTF("%s: Z=%d files %d: %s - ilat/ilon min %d,%d max %d,%d\n", module,
        zoom,
        (int)max,
        get_bounding_box_stg(pbb,true),
        min_ilat, min_ilon, max_ilat, max_ilon);

    double bb_lat_sp = pbb->bb_max_lat - pbb->bb_min_lat;
    double bb_lon_sp = pbb->bb_max_lon - pbb->bb_min_lon;
    double bbb_lat_sp, bbb_lon_sp, spdx, spdy;
    int x_off = 0;
    int y_off = 0;
    size_t total_bytes = 0;
    clear_used_colors();
    double bgn = get_seconds();
    char *secs;
    char *hgt_file;
    double dlat,dlon;
    int ilat2,ilon2;
    int bgn_x, bgn_y;
    unsigned int colr;
    short elev;
    int x,y;
    size_t doff;
    unsigned char r,g,b;
    int spany, spanx;
    double dspany, dspanx;

    // Do they need to be in the correct order to process?
    // ==================================================
    bheight = bwidth = 0;
    for (ilat = max_ilat; ilat >= min_ilat; ilat--) {
        x_off = 0;
        // maybe should STOP before max_ilon
        // OR MAYBE NOT
        for (ilon = min_ilon; ilon <= max_ilon; ilon++) {
        //for (ilon = min_ilon; ilon < max_ilon; ilon++) {
            dlat = (double)ilat;
            dlon = (double)ilon;
            hgt_file = get_hgt_file_name( dlat, dlon, &ilat2, &ilon2 );
            if (!hgt_file) {
                SPRTF("%s: EEK! No file name generated for lat,lon %s,%s! *** FIX ME ***\n", module,
                    get_trim_double(dlat),
                    get_trim_double(dlon));
                check_me();
                return 1;
            }
            for (ii = 0; ii < max; ii++) {
                mf = vMult[ii];
                if ((mf.ilat == ilat) && (mf.ilon == ilon)) {
                    if (mf.flag & mff_Used) {
                        SPRTF("%s: WHAT: This file %s, lat,lon %d,%d already used in this service!\n", module,
                            mf.file.c_str(), ilat, ilon );
                    } else {
                        mf.flag |= mff_Used;
                        vMult[ii] = mf;
                    }
                    break;
                }
            }
            if (ii >= max) {
                // need to TRY HARDER to match the file
                for (ii = 0; ii < max; ii++) {
                    mf = vMult[ii];
                    if (strcmp(mf.file.c_str(),hgt_file) == 0) {
                        if (mf.flag & mff_Used) {
                            SPRTF("%s: WHAT: This file %s, lat,lon %d,%d already used in this service!\n", module,
                                mf.file.c_str(), ilat, ilon );
                        } else {
                            mf.flag |= mff_Used;
                            vMult[ii] = mf;
                        }
                        break;
                    }
                }
                if (ii >= max) {
                    static bool done_one_check_me = false;
                    SPRTF("\n");
                    SPRTF("%s: CHECK ME: FAILED: file %s, ilat,ilon %d,%d in %d enties (%d,%d)\n", module,
                        hgt_file,
                        ilat, ilon, (int)max,
                        ilat2,ilon2 );
                    if (max) {
                        for (ii = 0; ii < max; ii++) {
                            mf = vMult[ii];
                            SPRTF(" %d: file %s - %d,%d\n", (int)(ii+1), mf.file.c_str(), mf.ilat, mf.ilon);
                        }
                    }
                    if (!done_one_check_me) {
                        done_one_check_me = true;
                        check_me();
                    }
                    continue;
                }
            }

            cnt++;
            bb = *pbb;
            f = dir;
            f += PATH_SEP;
            f += mf.file;

            // ilat,ilon is bottom left point = min lat,lon
            fbb.bb_min_lat = (double)mf.ilat;
            fbb.bb_max_lat = fbb.bb_min_lat + inc;

            fbb.bb_min_lon = (double)mf.ilon;
            fbb.bb_max_lon = fbb.bb_min_lon + inc;

            // fix bbox for this dem3 file
            if (fbb.bb_max_lat < bb.bb_max_lat)
                bb.bb_max_lat = fbb.bb_max_lat;

            if (fbb.bb_min_lat > bb.bb_min_lat)
                bb.bb_min_lat = fbb.bb_min_lat;

            if (fbb.bb_max_lon < bb.bb_max_lon)
                bb.bb_max_lon = fbb.bb_max_lon;

            if (fbb.bb_min_lon > bb.bb_min_lon)
                bb.bb_min_lon = fbb.bb_min_lon;

            bbb_lat_sp = bb.bb_max_lat - bb.bb_min_lat;
            bbb_lon_sp = bb.bb_max_lon - bb.bb_min_lon;

            SPRTF("%s: %d: %s (%s) - file %s\n", module, (int)cnt,
                get_bounding_box_stg(&bb,true), get_bounding_box_stg(&fbb), mf.file.c_str() );

            // now get upper left elevation
            if (!s.set_offsets( bb.bb_max_lat, bb.bb_min_lon )) {
                SPRTF("%s: BAH! Failed setting an offset!\n", module );
                iret |= 1;
                goto exit;
            }
            x_bgn = s.x_off;
            y_bgn = s.y_off;
            // now get lower right elevation
            if (!s.set_offsets( bb.bb_min_lat, bb.bb_max_lon )) {
                SPRTF("%s: BAH! Failed setting an offset!\n", module );
                iret |= 2;
                goto exit;
            }
            x_end = s.x_off;
            y_end = s.y_off;
            xcols = x_end - x_bgn;
            yrows = y_end - y_bgn;

            // FIX20140519: can NOT have ZERO rows or cols
            if (xcols == 0)
                xcols = 1;
            if (yrows == 0)
                yrows = 1;

            SPRTF("%s: Offsets x,y bgn %d,%d, end %d,%d, span %d,%d\n", module,
                x_bgn, y_bgn, x_end, y_end, xcols, yrows );
            //if ( (xcols < MIN_SPAN) || (yrows < MIN_SPAN) ) {
            //    SPRTF("%s: At this ZOOM %d, on DEM3 data, the BBOX is LESS THAN MIN_SPAN %dx%d (ie %d,%d) points!\n", module, zoom, MIN_SPAN, MIN_SPAN,
            //        xcols, yrows);
            //}
            // we know the total width, height of the final image, but need a partial width/height for this block
            // represented as this blocks span of the full bbox span
            spdx = bbb_lon_sp / bb_lon_sp;
            spdy = bbb_lat_sp / bb_lat_sp;
            bwidth = (int)(spdx * (double)width);
            bheight = (int)(spdy * (double)height);

            // FIX20140519: AVOID division by zero
            if (bwidth == 0)
                bwidth = 1;
            if (bheight == 0)
                bheight = 1;

            SPRTF("%s: Offsets x,y bgn %d,%d, end %d,%d, span %d,%d, w,h %d,%d, ow,oh %d,%d\n", module,
                x_bgn, y_bgn, x_end, y_end, xcols, yrows, bwidth, bheight, x_off, y_off );
            spany = yrows / bheight;
            if (yrows % bheight) 
                spany++;
            spanx = xcols / bwidth;
            if (xcols % bwidth) 
                spanx++;
            dspany = (double)yrows / (double)bheight;
            dspanx = (double)xcols / (double)bwidth;
            if (!s.begin_span_file(f)) {
                SPRTF("%s: Failed to begin_span_file(%s)!\n", module, f.c_str() );
                x_off += bwidth;
                continue;   // just file does NOT exist, so NOT return 1;
            }
            total_bytes += s.file_size;
            for (y = 0; y < bheight; y++) {
                bgn_y = y_bgn + (int)((double)y * dspany);
                for (x = 0; x < bwidth; x++) {
                    bgn_x = x_bgn + (int)((double)x * dspanx);
                    if (!s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev)) {
                        SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                        check_me();
                        return 1;
                    }
                    colr = get_BGR_color(elev); // convert to a color
                    doff = ((height - 1 - (y_off + y)) * width * 3) + ((x_off + x) * 3);
                    //doff = ((height - 1 - y) * width * 3) + (x * 3);
                    if ((doff + 3) > bsize) {
                        SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                            x, y, (int)doff, (int)bsize );
                        check_me();
                        return 1;
                    }
                    r = getRVal(colr);
                    g = getGVal(colr);
                    b = getBVal(colr);
                    // is this the correct order????????
                    buffer[doff+0] = r;
                    buffer[doff+1] = g;
                    buffer[doff+2] = b;
                }
            }
            s.end_span_file();
            x_off += bwidth;
        }
        y_off += bheight;
    }
#ifdef USE_PNG_LIB
    //int bit_depth = 8;
    //int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    if (writePNGImage24((char *)img.c_str(), width, height, 8, 6, buffer )) {
       iret |= 8;
    }
#else // !USE_PNG_LIB
    if (writeBMPImage24((char *)img.c_str(), width, height, buffer, bsize)) {
       iret |= 4;
    }
#endif // USE_PNG_LIB y/n
    secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        x_off, y_off,
        get_NiceNumberStg(total_bytes),
        width, height, secs );
    // SANITY CHECK ONLY
    // Have ALL files passed been USED?
    for (ii = 0; ii < max; ii++) {
        mf = vMult[ii];
        if (mf.flag & mff_Used) {
            // this what is expected
        } else {
            SPRTF("%s: CHECK ME: This file %s, lat,lon %d,%d NOT USED this service!\n", module,
                mf.file.c_str(), mf.ilat, mf.ilon );
        }
    }
exit:
    delete buffer;
    return iret;
}


/*\
    { "Mont Blanc", 45.833611111, 6.865, 4810 },
\*/

int do_hgt3_test6() // Mont Blanc at various zooms
{
    int iret = 0;
    short elev;
    //int width = 256;
    //int height = 256;
    // UGH: N46E006.hgt and N45E007.hgt
    int zoom = 10;
    //int zoom = 11;  // span 423,168 points
    //int zoom = 12;  // span 211,84 points
    double lat = 45.833611111;
    double lon = 6.865; //
    short expect = 4810;
    std::string file1;
    int x_bgn,y_bgn;
    BNDBOX bb;
    PBNDBOX pbb = &bb;
    const char *dir = ferr3_dir;

    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test6() =====================\n", module);

    SPRTF("\n");
    SPRTF("%s: Get elevation Mont Blanc lat,lon %s,%s. Expect %d meters\n", module,
        GTD2(lat,lon),
        expect );

    if (is_file_or_directory((char *)dir) != 2) {
        SPRTF("%s: Can not 'stat' directory %s! Abandoning this test set.\n", module, dir);
        return 1;
    }

    hgt3File s;
    s.verb = false; // set to run silent
    if (!s.set_hgt_dir(dir)) {
        s.verb = true;
        s.set_hgt_dir(dir);
        return 1;
    }

    s.verb = true; // to see errors
    if (!s.get_elevation( lat, lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return 1;
    }
    file1 = s.file;
    x_bgn = s.x_off;
    y_bgn = s.y_off;
    SPRTF("%s: for lat,lon %s,%s, Mont Blanc, elev %d, expected %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev, expect,
            file1.c_str(),
            x_bgn, y_bgn);

    // CHOICE OF ZOOM RANGE FOR DEM 3 DATA
    // Below ZOOM 8 requires MULTIPLE file 
    // hgt3_tests: Z=4 Total 346 DEM3 files, found 312, missing 34
    // hgt3_tests: Z=5 Total 97 DEM3 files, found 88, missing 9
    // hgt3_tests: Z=6 Total 25 DEM3 files, found 25, missing 0
    // hgt3_tests: Z=7 Total 7 DEM3 files, found 7, missing 0
    // Seems evident should use DEM 15 or DEM 30 for these tiles

    // ZOOM 8 to 12, MAYBE to 16
    // hgt3_tests: Z=8 Total 3 DEM3 files, found 3, missing 0
    // hgt3_tests: Z=9 Total 2 DEM3 files, found 2, missing 0
    // hgt3_tests: Z=10 Total 2 DEM3 files, found 2, missing 0
    // hgt3_tests: Z=11 Total 2 DEM3 files, found 2, missing 0
    // hgt3_tests: Z=12 Total 1 DEM3 files, found 1, missing 0 - span 105,73 points
    // BUT somewhere around here the span expanded to 256 x 256 is getting rediculous ;=()
    // hgt3_tests: Z=13 Total 1 DEM3 files, found 1, missing 0 - span 53,37 points
    // hgt3_tests: Z=14 Total 1 DEM3 files, found 1, missing 0 - span 26,18 points
    // hgt3_tests: Z=15 Total 1 DEM3 files, found 1, missing 0 - span 13,9 points
    // hgt3_tests: Z=16 Total 1 DEM3 files, found 1, missing 0 - span 6,5 points

    // from ZOOM 17 UPWARDS, the point span is too small to generate an image - less than 4 x 4 expanded to 256 x 256
    // hgt3_tests: At this ZOOM 17, on DEM3 data, the BBOX is LESS THAN MIN_SPAN 4x4 (ie 3,3) points!
    // hgt3_tests: At this ZOOM 18, on DEM3 data, the BBOX is LESS THAN MIN_SPAN 4x4 (ie 1,1) points!
    // hgt3_tests: At this ZOOM 19, on DEM3 data, the BBOX is LESS THAN MIN_SPAN 4x4 (ie 0,0) points!
    // so this is too much - for (zoom = 4; zoom < 20; zoom++). Reduce test to
    for (zoom = 8; zoom < 17; zoom++) {

        SPRTF("\n");

        if (!get_map_bounding_box( lat, lon, zoom, pbb, true )) {
            SPRTF("%s: FAILED to get slippy map bounding box!\n", module);
            return 1;
        }
        SPRTF("%s: Z=%d: Write tile for lat,lon %s,%s - %s\n", module,
            zoom,
            GTD2(lat,lon),
            get_bounding_box_stg(pbb,true));

        // the de Ferranti DEM 3 data does NOT offer DEM 3 1x1 degree files for ocean only, so should check the files exist
        double dlat, dlon;
        char *cp;
        int ilat, ilon;
        std::string f;
        int found = 0;
        int missed = 0;
        vSTG files;
        MULTFILES mf;
        vMULT vMult;
        double lat_sp = 0.5;
        double lon_sp = 0.5;
        // 1: get files needed for the 4 corners
        // =====================================
        // TL - max_lat, min_lon
        // =====================
        dlat = pbb->bb_max_lat;
        dlon = pbb->bb_min_lon;
        if (!in_world_range(dlat,dlon)) {
            SPRTF("%s: UGH! From %s lat,lon %s,%s NOT IN WORLD! *** FIX ME ***\n", module,
                get_bounding_box_stg(pbb,true),
                GTD2(dlat,dlon));
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: EEK! No file name generated! *** FIX ME ***\n", module);
            check_me();
            return 1;
        }
        mf.file = cp;
        mf.flag = 0;
        if (!string_in_vec( files, mf.file )) {
            mf.ilat = ilat;
            mf.ilon = ilon;
            vMult.push_back(mf);
            files.push_back(cp);
            f = dir;
            f += PATH_SEP;
            f += cp;
            if (is_file_or_directory((char *)f.c_str()) == 1) {
                SPRTF("%s ok ", cp);
                found++;
            } else {
                SPRTF("%s NF ", cp);
                missed++;
            }
        }

        // TR - max_lat max_lon
        // ====================
        dlat = pbb->bb_max_lat;
        dlon = pbb->bb_max_lon;
        if (!in_world_range(dlat,dlon)) {
            SPRTF("%s: UGH! From %s lat,lon %s,%s NOT IN WORLD! *** FIX ME ***\n", module,
                get_bounding_box_stg(pbb,true),
                GTD2(dlat,dlon));
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: EEK! No file name generated! *** FIX ME ***\n", module);
            check_me();
            return 1;
        }
        mf.file = cp;
        mf.flag = 0;
        if (!string_in_vec( files, mf.file )) {
            mf.ilat = ilat;
            mf.ilon = ilon;
            vMult.push_back(mf);
            files.push_back(cp);
            f = dir;
            f += PATH_SEP;
            f += cp;
            if (is_file_or_directory((char *)f.c_str()) == 1) {
                SPRTF("%s ok ", cp);
                found++;
            } else {
                SPRTF("%s NF ", cp);
                missed++;
            }
        }

        // BR - min_lat max_lon
        // ====================
        dlat = pbb->bb_min_lat;
        dlon = pbb->bb_max_lon;
        if (!in_world_range(dlat,dlon)) {
            SPRTF("%s: UGH! From %s lat,lon %s,%s NOT IN WORLD! *** FIX ME ***\n", module,
                get_bounding_box_stg(pbb,true),
                GTD2(dlat,dlon));
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: EEK! No file name generated! *** FIX ME ***\n", module);
            check_me();
            return 1;
        }
        mf.file = cp;
        mf.flag = 0;
        if (!string_in_vec( files, mf.file )) {
            mf.ilat = ilat;
            mf.ilon = ilon;
            vMult.push_back(mf);
            files.push_back(cp);
            f = dir;
            f += PATH_SEP;
            f += cp;
            if (is_file_or_directory((char *)f.c_str()) == 1) {
                SPRTF("%s ok ", cp);
                found++;
            } else {
                SPRTF("%s NF ", cp);
                missed++;
            }
        }

        // BL - min_lat, min_lon
        // =====================
        dlat = pbb->bb_min_lat;
        dlon = pbb->bb_min_lon;
        if (!in_world_range(dlat,dlon)) {
            SPRTF("%s: UGH! From %s lat,lon %s,%s NOT IN WORLD! *** FIX ME ***\n", module,
                get_bounding_box_stg(pbb,true),
                GTD2(dlat,dlon));
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: EEK! No file name generated! *** FIX ME ***\n", module);
            check_me();
            return 1;
        }
        mf.file = cp;
        mf.flag = 0;
        if (!string_in_vec( files, mf.file )) {
            mf.ilat = ilat;
            mf.ilon = ilon;
            vMult.push_back(mf);
            files.push_back(cp);
            f = dir;
            f += PATH_SEP;
            f += cp;
            if (is_file_or_directory((char *)f.c_str()) == 1) {
                SPRTF("%s ok ", cp);
                found++;
            } else {
                SPRTF("%s NF ", cp);
                missed++;
            }
        }
        // 2: Get ALL the intermediate files needed
        // ========================================
        // for (dlat = pbb->bb_min_lat; dlat <= pbb->bb_max_lat; dlat += 1.0) {
        // should start at the top left, like
        // if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
        for (dlat = pbb->bb_max_lat; dlat >= pbb->bb_min_lat; dlat -= lat_sp) {
            for (dlon = pbb->bb_min_lon; dlon <= pbb->bb_max_lon; dlon += lon_sp) {
                if (!in_world_range(dlat,dlon)) {
                    SPRTF("%s: UGH! From %s lat,lon %s,%s NOT IN WORLD! *** FIX ME ***\n", module,
                        get_bounding_box_stg(pbb,true),
                        GTD2(dlat,dlon));
                    check_me();
                    return 1;
                }
                cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon );
                if (!cp) {
                    SPRTF("%s: EEK! No file name generated! *** FIX ME ***\n", module);
                    check_me();
                    return 1;
                }
                mf.file = cp;
                mf.flag = 0;
                if (!string_in_vec( files, mf.file )) {
                    mf.ilat = ilat;
                    mf.ilon = ilon;
                    vMult.push_back(mf);
                    files.push_back(cp);
                    f = dir;
                    f += PATH_SEP;
                    f += cp;
                    if (is_file_or_directory((char *)f.c_str()) == 1) {
                        SPRTF("%s ok ", cp);
                        found++;
                    } else {
                        SPRTF("%s NF ", cp);
                        missed++;
                    }
                }
            }
            SPRTF("\n");
        }
        SPRTF("%s: Z=%d Total %d DEM3 files, found %d, missing %d\n", module,
            zoom,
            (found + missed), found, missed );
        if (found && (missed == 0)) {
            if (found == 1) {
                iret |= dem3_bounding_box_2_image( pbb );
            } else {
                iret |= dem3_bbox_2_image( pbb, vMult, s );
                //// break; // TEMPORARY EXIT
            }
        }
    }
    return iret;
}

// { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
int do_hgt3_test2()
{
    int iret = 0;
    //int zoom = 15;
    double lat = 45.832704;
    double lon = 6.864797;
    short elev = 4810;

    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test2() =====================\n", module);

    hgt3File s;
    s.verb = false;
    if (!s.set_hgt_dir(ferr3_dir)) {
        s.verb = true;
        s.set_hgt_dir(ferr3_dir);
        return 1;
    }
    s.verb = true;

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }

    SPRTF("%s: For lat,lon %s,%s, Mont Blanc got elev %d, expected 4810\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev);
    std::string file = ferr3_dir;
    file += PATH_SEP;
    file += "N45E006.hgt";
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        SPRTF("%s: Can NOT locate file %s\n", module,
            file.c_str());
    }
    size_t file_size = get_last_file_size();
    SPRTF("\n");
    SPRTF("%s: Doing full scan of file %s, %s bytes...\n", module,
        file.c_str(),
        get_NiceNumberStg(file_size));

    double bgn = get_seconds();
    if (!s.scan_hgt_file(file, 45.0, 6.0)) {
        SPRTF("%s: hgt scan failed!\n", module);
        return 1;
    }
    SPRTF("%s: Scan of %s byte file took %s.\n", module,
        get_NiceNumberStg(file_size),
        get_elapsed_stg(bgn));

    SPRTF("%s: Expected highest 4810 at lat,lon %s,%s, got %d at %s,%s.\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        s.max_elev,
        get_trim_double(s.max_lat),
        get_trim_double(s.max_lon) );
#ifdef USE_SIMGEAR_LIB
    SGGeod p1,p2;
    double az1,az2,dist;
    p1.setLatitudeDeg(lat);
    p1.setLongitudeDeg(lon);
    p1.setElevationM(elev);
    p2.setLatitudeDeg(s.max_lat);
    p2.setLongitudeDeg(s.max_lon);
    p2.setElevationM(s.max_elev);
    SGGeodesy::inverse(p1,p2,az1,az2,dist);
    SPRTF("%s: SG Geodesy reports these are %s appart, on course %s\n", module,
        get_meters_stg( SG_FEET_TO_METER * dist ),
        get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
    //SPRTF("%s: SG Geodesy reports these are %s Km appart, on course %s\n", module,
    //    get_trim_double( (double)((int)(((SG_FEET_TO_METER * dist) / 1000.0) * 10.0)) / 10.0 ),
    //    get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
#endif // USE_SIMGEAR_LIB

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(s.max_lat),
            get_trim_double(s.max_lon));
    if (!s.get_elevation(s.max_lat,s.max_lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(s.max_lat),
            get_trim_double(s.max_lon));
        return 1;
    }
    SPRTF("%s: For lat,lon %s,%s, got elev %d, expected %d.\n", module,
        get_trim_double(s.max_lat),
        get_trim_double(s.max_lon),
        elev,
        s.max_elev);

    // another file load and scan on load
    SPRTF("\n");
    SPRTF("%s: Doing load of file %s...\n", module,
            file.c_str());
    bgn = get_seconds();
    if (!s.load_file(file)) {
        SPRTF("%s: Load of file %s FAILED!\n", module,
            file.c_str());
        return 1;
    }
    SPRTF("%s: Done load and scan of %s byte file took %s.\n", module,
        get_NiceNumberStg(file_size),
        get_elapsed_stg(bgn));
#ifdef USE_SIMGEAR_LIB
    p2.setLatitudeDeg(s.emax_lat);
    p2.setLongitudeDeg(s.emax_lon);
    p2.setElevationM(s.emax);
    SGGeodesy::inverse(p1,p2,az1,az2,dist);
    SPRTF("%s: SG Geodesy reports these are %s appart, on course %s\n", module,
        get_meters_stg( SG_FEET_TO_METER * dist ),
        get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
#endif // USE_SIMGEAR_LIB

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(s.emax_lat),
            get_trim_double(s.emax_lon));
    if (!s.get_elevation(s.emax_lat,s.emax_lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(s.emax_lat),
            get_trim_double(s.emax_lon));
        return 1;
    }
    SPRTF("%s: For lat,lon %s,%s, got elev %d, expected %d.\n", module,
        get_trim_double(s.emax_lat),
        get_trim_double(s.emax_lon),
        elev,
        s.emax);
    return iret;
}


int do_hgt3_test8() // elevtests for zoom range;
{
    int iret = 0;
    PELEVTESTS pet = elevtests;
    int zoom;
    double lat,lon;
    short expect;
    const char *name;
    char *sp;
    BNDBOX bb;
    PBNDBOX pbb = &bb;
    bool verb = true;
    vSTG files;
    std::string f, ff;
    std::string img;
    size_t max,ii,fnd;
    int min_zoom = 6;
    int max_zoom = MAX_SLIPPY_ZOOM;
    MULTFILES mf;
    vMULT vMult;
    LOCZOOM lz;
    vLZ vlz;

    SPRTF("\n");
    SPRTF("%s: === do_hgt3_test8 =====================\n", module);

    hgt3File s;
    if (!s.set_hgt_dir(ferr3_dir)) {
        return 1;
    }

    // NOTE: Should use single topo30 for zooms up to min_zoom
    pet = elevtests;
    while (pet->name) {
        lat = pet->lat;
        lon = pet->lon;
        expect = pet->expected;
        name = pet->name;
        pet++;
        lz.loc = name;
        lz.lat = lat;
        lz.lon = lon;
        lz.exp = expect;
        lz.files.clear();
        SPRTF("\n");
        SPRTF("%s: Location: '%s', lat,lon %s,%s, expect %d\n", module,
            name,
            get_trim_double(lat),
            get_trim_double(lon),
            expect );
        // now for EACH zoom level
        for (zoom = min_zoom; zoom < max_zoom; zoom++) {

            SPRTF("\n");
            if (!get_slippy_path(lat,lon,zoom,&sp)) {
                SPRTF("%s: Failed to get path for lat,lon %s,%s\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon));
                continue;
            }

            img = get_slippy_image_file(sp);
            lz.files.push_back(img);

            if (!get_map_bounding_box(lat,lon,zoom,pbb,verb)) {
                SPRTF("%s: Failed to get bbox for lat,lon,zoom %s,%s,%d\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    zoom);
                continue;

            }
            SPRTF("%s: Z=%d Write tile for lat,lon %s,%s\n", module, zoom,
                get_trim_double(lat),
                get_trim_double(lon));
            SPRTF("%s: %s\n", module,
                get_bounding_box_stg(pbb,verb));

            files.clear();
            // if (!get_hgt3_file_names( pbb, files, true, false )) {
            if (!get_hgt3_file_names( pbb, files, true, verb )) {
                SPRTF("%s: Failed to get files list for %s\n", module,
                    get_bounding_box_stg(pbb,verb));
                continue;
            }

            std::sort(files.begin(),files.end());
            max = files.size();
            fnd = 0;
            vMult.clear();
            for (ii = 0; ii < max; ii++) {
                f = files[ii];
                //SPRTF("%s ", f.c_str());
                ff = ferr3_dir;
                ff += PATH_SEP;
                ff += f;
                mf.file = f;
                mf.flag = 0;
                if (get_hgt_ilat_ilon(f, &mf.ilat, &mf.ilon )) {
                    vMult.push_back(mf);
                    if (is_file_or_directory((char *)ff.c_str()) == 1) {
                        fnd++;
                    }
                }
            }
            SPRTF("%s: Z=%d: Require %d files, found %d\n", module,
                zoom,
                (int)max,
                (int)fnd);

            //if ((max == 1)&&(fnd == 1)) {
            //    iret |= dem3_bounding_box_2_image( pbb );
            //} else if (fnd && (fnd == max)) {
            //    iret |= dem3_bbox_2_image( pbb, vMult, s );
            //} else if (max) {
                //SPRTF("%s: Presently can NOT handle missing %d hgt3 files, but SHOULD\n", module, 
                //    (int)(max - fnd));
                iret |= dem3_bbox_2_image( pbb, vMult, s );
            //}
        } // for each zoom for this location
        vlz.push_back(lz);
        // next location
    }

    // write a quick viewer
    write_html_view(vlz, min_zoom, max_zoom);

    return iret;


}

// eof - hgt3_tests.cxx
