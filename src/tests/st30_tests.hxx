// st30_tests.hxx
#ifndef _ST30_TESTS_HXX_
#define _ST30_TESTS_HXX_
#include "slippystuff.hxx" // BNDBOX, ...

extern int do_stopo30_test1(); // try limits, elevtests and group on single 2 GB file - D:\SRTM\scripps\topo30\topo30
extern int do_stopo30_test2(); // build a 1024x512 image of whole file
extern int do_stopo30_test3(); // write Mt Everest slippy maps for zooms 4-19
extern int do_stopo30_test5(); // write slippy maps for elevtests locs, zooms 0 - 14

#endif // #ifndef _ST30_TESTS_HXX_
// eof

