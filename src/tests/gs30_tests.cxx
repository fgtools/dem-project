/*\
 * gs30_tests.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <limits.h> // for SHRT_MIN, ...
#include "sprtf.hxx"
#include "utils.hxx"
#include "color.hxx"
#include "usgs30.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "dir_utils.hxx"
#include "slippystuff.hxx"
#include "gs30_tests.hxx"

static const char *module = "gs30_tests";


//     tf_usgs_test1 = 0x00400000, //iret |= do_usgs30_test1(); // Mount Everest, and run elevtests set
//     { "Mount Everest",  27.988, 86.9253, 8848   },
int do_usgs30_test1()
{
    int iret = 0;
    const char *dir = usgs30_dir;
    short elev, expected = 8848;
    double lat = 27.988;
    double lon = 86.9253;
    SPRTF("\n");
    SPRTF("%s: === do_usgs30_test1() =====================\n", module);

    usgs30 s;
    if (!s.set_file_dir(dir)) {
        SPRTF("%s: Failed to set file directory to %s!\n", module, dir);
        return 1;
    }
    if (!s.get_elevation( lat, lon, &elev )) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s!\n", module, 
            get_trim_double(lat),
            get_trim_double(lon) );
    }

    SPRTF("%s: For lat,lon %s,%s got elevation %d, expected %d\n", module, 
        get_trim_double(lat),
        get_trim_double(lon),
        elev,
        expected );

    PELEVTESTS pet = elevtests; // reset 'pet' pointer
    int missing_cnt, failed_cnt, void_cnt;
    int ilat, ilon;
    const char *cp;
    bool verb = true;
    std::string f;
    missing_cnt = failed_cnt = void_cnt = 0;
    while (pet->name) {
        lat = pet->lat;
        lon = pet->lon;
        SPRTF("\n");
        SPRTF("%s: Testing lat,lon %s,%s - %s - expect %d\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                pet->expected );
        cp = get_usgs30_tile_name(lat, lon, &ilat, &ilon, verb);
        if (!cp) {
            missing_cnt++;
            SPRTF("%s: No USGS tile for lat,lon %s,%s - %s\n",module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name );
            pet++;
            continue;
        }
        f = dir;
        f += PATH_SEP;
        f += cp;
        f += USGS_EXT;
        if (is_file_or_directory((char *)f.c_str()) != 1) {
            missing_cnt++;
            f = cp;
            SPRTF("%s: For lat,lon %s,%s, %s, FAILED to find file %s\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                cp );
        } else {
            if (s.get_elevation( lat, lon, &elev)) {
                SPRTF("%s: For lat,lon %s,%s, %s, got elev ", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    pet->name );
                if (elev == USGS_VOID) {
                    SPRTF("*VOID*");
                    void_cnt++;
                } else {
                    SPRTF("%d",elev);
                }
                SPRTF(", expected %d. diff %d\n", pet->expected,
                    ((elev < pet->expected) ? pet->expected - elev : elev - pet->expected) );

            } else {
                failed_cnt++;
                f = cp;
                SPRTF("%s: For lat,lon %s,%s, %s, FAILED, expected %d.\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    pet->name,
                    pet->expected);
            }
        }
        pet++;
    }
    SPRTF("%s: missing_cnt %d, failed_cnt %d, void_cnt %d\n", module,
        missing_cnt, failed_cnt, void_cnt );
    return iret;
}

// testtest.log extract
// gs30_tests: Scanned 27 of 27, 27 successful, in 40.814 secs, total 1,555,200,000 bytes.
// gs30_tests: MAX 8685, at lat,lon 27.983333,86.925, offset y,x 1442,3231
// gs30_tests: MAX File F:\data\SRTM30\data\E060N40.DEM
// gs30_tests: MIN -415, at lat,lon 31.758333,35.508333, offset y,x 989,1861
// gs30_tests: MIN File F:\data\SRTM30\data\E020N40.DEM
// all_tests: With bit flag 8388608, 0x00800000, did 1 tests in 40.824 secs

int do_usgs30_test2()   // scan all usgs30_dir/*.DEM files for max/min
{
    int iret = 0;
    const char *dir = usgs30_dir;
    SPRTF("\n");
    SPRTF("%s: === do_usgs30_test2() =====================\n", module);

    if (is_file_or_directory((char *)dir) != 2) {
        SPRTF("%s: Directory %s NOT a valid directory!\n", module, dir);
        return 1;
    }
    vSTG usgs30_files = get_USGS30_DEM_Files(dir, true);
    size_t ii, max = usgs30_files.size();
    if (!max) {
        SPRTF("%s: Directory %s has no valid USGS DEM files!\n", module, dir);
    }
    usgs30 s;
    std::string f;
    short maxe = SHRT_MIN;
    short mine = SHRT_MAX;

    double max_lat,max_lon;
    int max_x,max_y;
    std::string max_file;

    double min_lat,min_lon;
    int min_x,min_y;
    std::string min_file;
    size_t done = 0;
    size_t succ = 0;
    double bgn = get_seconds();
    for (ii = 0; ii < max; ii++) {
        f = usgs30_files[ii];
        done++;
        SPRTF("\n");
        SPRTF("%s: Scanning %s\n", module, f.c_str());
        if (s.scan(f)) {
            succ++;
            if (s.max_elev > maxe) {
                maxe = s.max_elev;
                max_lat = s.max_lat;
                max_lon = s.max_lon;
                max_x = s.max_x;
                max_y = s.max_y;
                max_file = f;
            }
            if (s.min_elev < mine) {
                mine = s.min_elev;
                min_lat = s.min_lat;
                min_lon = s.min_lon;
                min_x = s.min_x;
                min_y = s.min_y;
                min_file = f;

            }
        } else {
            SPRTF("%s: scan FAILED!\n", module);
        }
    }
    SPRTF("\n");
    SPRTF("%s: Scanned %d of %d, %d successful, in %s, total %s bytes.\n", module,
        (int)done, (int)max, (int)succ,
        get_elapsed_stg(bgn),
        get_NiceNumberStg( (succ * 57600000) ) );
    if (succ) {
        SPRTF("%s: MAX %d, at lat,lon %s,%s, offset y,x %d,%d\n", module,
            maxe,
            get_trim_double(max_lat),
            get_trim_double(max_lon),
            max_y, max_x );
        SPRTF("%s: MAX File %s\n", module, max_file.c_str());
        SPRTF("%s: MIN %d, at lat,lon %s,%s, offset y,x %d,%d\n", module,
            mine,
            get_trim_double(min_lat),
            get_trim_double(min_lon),
            min_y, min_x );
        SPRTF("%s: MIN File %s\n", module, min_file.c_str());
    }
    SPRTF("\n");
    return iret;
}

// beautiful, but can not be loaded together in a browser
int do_usgs30_test3_FULL_SIZE_4800_X_6000()   // write all usgs30_dir/*.DEM files to png/bmp images
{
    int iret = 0;
    const char *dir = usgs30_dir;
    SPRTF("\n");
    SPRTF("%s: === do_usgs30_test3() =====================\n", module);

    if (is_file_or_directory((char *)dir) != 2) {
        SPRTF("%s: Directory %s NOT a valid directory!\n", module, dir);
        return 1;
    }
    vSTG usgs30_files = get_USGS30_DEM_Files(dir, true);
    size_t ii, max = usgs30_files.size();
    if (!max) {
        SPRTF("%s: Directory %s has no valid USGS DEM files!\n", module, dir);
    }
    usgs30 s;
    std::string f, img;
    size_t cnt = 0;
    double bgn = get_seconds();
    for (ii = 0; ii < max; ii++) {
        f = usgs30_files[ii];
        img = get_image_out_dir();
        img += PATH_SEP;
        img += get_file_name(f);    // like Everest - "E060N40.DEM";
#ifdef USE_PNG_LIB
        img += ".png";
#else
        img += ".bmp";
#endif
        if (!s.usgs30_file_to_image(f,img.c_str())) {
            iret |= 1;
            SPRTF("%s: FAILED to write image!\n", module );
            check_me();
        } else {
            cnt++;
        }
    }
    SPRTF("%s: Written %d (giant)images in %s\n", module, (int)cnt, get_elapsed_stg(bgn));
    return iret;
}


// gs30_tests: Testing lat,lon 27.988,86.9253 - Mount Everest - expect 8848
// usgs30: Set F:\data\SRTM30\data\e060n40.DEM as input file, 57,600,000 bytes.
// gs30_tests: For lat,lon 27.988,86.9253, Mount Everest, got elev 8569, expected 8848. diff 279
int do_usgs30_test3()   // write all usgs30_dir/*.DEM files to png/bmp images
{
    int iret = 0;
    const char *dir = usgs30_dir;
    int width = 480;
    int height = 600;
    int x,y,rows, cols, spanx, spany;
    double dspanx, dspany;
    short elev;
    unsigned int colr;
    size_t bsize = width * height * 3;
    unsigned char *buffer;
    size_t doff;
    unsigned char r,g,b;
    int bgn_x,bgn_y;

    SPRTF("\n");
    SPRTF("%s: === do_usgs30_test3() =====================\n", module);

    buffer = new unsigned char[bsize];
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    if (is_file_or_directory((char *)dir) != 2) {
        SPRTF("%s: Directory %s NOT a valid directory!\n", module, dir);
        return 1;
    }
    vSTG usgs30_files = get_USGS30_DEM_Files(dir, true);
    size_t ii, max = usgs30_files.size();
    if (!max) {
        SPRTF("%s: Directory %s has no valid USGS DEM files!\n", module, dir);
    }
    usgs30 s;
    std::string f, img;
    double bgn = get_seconds();
    for (ii = 0; ii < max; ii++) {
        f = usgs30_files[ii];
        //if (!s.set_file(f)) {
        if (!s.begin_span_file(f)) {
            SPRTF("%s: OOPS! Failed to begin span file '%s'!\n", module, f.c_str());
            return 1;
        }
        rows = s.rows;
        cols = s.cols;
        spany = rows / height;
        if (rows % height) 
            spany++;
        spanx = cols / width;
        if (cols % width) 
            spanx++;
        dspany = (double)rows / (double)height;
        dspanx = (double)cols / (double)width;
        SPRTF("%s: Processing file %s, x,y %d,%d, %d bytes, span %d,%d, incs %s,%s\n", module,
            f.c_str(),
            cols, rows,
            (int) s.file_size,
            spanx, spany,
            get_trim_double(dspanx),
            get_trim_double(dspany) );
        bool ok = true;
        s.verb = true;  // to see any error
        clear_used_colors();
        for (y = 0; y < height; y++) {
            bgn_y = (int)((double)y * dspany);
            for (x = 0; x < width; x++) {
                bgn_x = (int)((double)x * dspanx);
                ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
                if (!ok) {
                    SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                    check_me();
                    return 1;
                }
                //SPRTF("%d ",elev);
                colr = get_BGR_color(elev); // convert to a color
                doff = ((height - 1 - y) * width * 3) + (x * 3);
                if ((doff + 3) > bsize) {
                    SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                        x, y, (int)doff, (int)bsize );
                    check_me();
                    return 1;
                }
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = r;
                buffer[doff+1] = g;
                buffer[doff+2] = b;
            }
            //SPRTF("\n");
            if (!ok)
                break;
        }
        s.end_span_file();
        out_used();
#ifdef USE_PNG_LIB
        img = get_image_out_dir();
        img += PATH_SEP;
        img += get_file_name(f);    // like Everest - "E060N40.DEM";
        img += ".png";
        if (writePNGImage24((char *)img.c_str(), width, height, 8, 6, buffer )) {
            iret |= 8;
        }
#endif // USE_PNG_LIB y/n
        img = get_image_out_dir();
        img += PATH_SEP;
        img += get_file_name(f);    // like Everest - "E060N40.DEM";
        img += ".bmp";
        if (writeBMPImage24((char *)img.c_str(), width, height, buffer, bsize)) {
            iret |= 4;
        }
    }

    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed %s bytes to %d %dx%d images in %s\n", module,
        get_NiceNumberStg(s.file_size * max),
        (int)(max * 2),
        width, height, secs );

    delete buffer;
    return iret;
}

int usgs30_out_image( int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1,  usgs30 & s, const char *img_file )
{
    int iret = 0;
    int x,y,cnt,ncnt;
    short elev;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    size_t doff;
    unsigned char r,g,b;
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    int last_bgn_x, last_bgn_y;
    unsigned int colr;
    SPRTF("%s: Extract w,h %d,%d from x,y bgn %d,%d, span %d,%d (%s,%s) from %s.\n", module,
        width, height,
        x_bgn, y_bgn,
        spanx, spany,
        get_trim_double(dspanx),
        get_trim_double(dspany),
        get_file_name(file1).c_str());

    clear_used_colors();
    double bgn = get_seconds();

    //hgt1File s;
    //s.verb = false; // set to run silent
    //s.set_hgt_dir(srtm1);
    //s.verb = true;  // to 'see' any errors
    bool ok = s.begin_span_file(file1);
    if (!ok) {
        SPRTF("%s: Failed to begin_span_file(%s)!\n", module, file1.c_str() );
        return 1;
    }
    ncnt = cnt = 0;
    last_bgn_x = last_bgn_y = -1;
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            if ((last_bgn_x != bgn_x)||(last_bgn_y != bgn_y)) {
                ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
                if (!ok) {
                    SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                    check_me();
                    return 1;
                }
                //SPRTF("%d ",elev);
                colr = get_BGR_color(elev); // convert to a color
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                ncnt++;
            }
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
            cnt++;
            last_bgn_x = bgn_x;
            last_bgn_y = bgn_y;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
    SPRTF("%s: Did %s of %s av. calls, in %s.\n", module,
        get_NiceNumberStg(ncnt),
        get_NiceNumberStg(cnt),
        get_elapsed_stg(bgn));
    out_used();

#ifdef USE_PNG_LIB
    if (writePNGImage24((char *)img_file, width, height, 8, 6, buffer )) {
       iret |= 8;
    }
#else // !USE_PNG_LIB
    if (writeBMPImage24((char *)img_file, width, height, buffer, bsize)) {
            iret |= 4;
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        xcols, yrows,
        get_NiceNumberStg(xcols * yrows),
        width, height, secs );
    delete buffer;
    return iret;
}

int process_usgs30_bbox( usgs30 &s, PBNDBOX pbb, int width, int height, const char *img_file )
{
    int iret = 0;
    int zoom = pbb->llz.zoom;
    int x_bgn, y_bgn, x_end, y_end;
    std::string file1, file2;
    TILELIMITS tl1,tl2;

    short elev;
    // now get upper left elevation
    if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon) );
        return 1;
    }
    file1 = s.file;
    x_bgn = s.x_col;
    y_bgn = s.y_row;
    // usgs30: Given lon,lat 40.979898,45, is tile e020n90, min 40,20, max 90,60
    tl1 = s.tl;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
        get_trim_double(pbb->bb_max_lat),
        get_trim_double(pbb->bb_min_lon),
        elev,
        file1.c_str(),
        x_bgn, y_bgn);

    // now get lower right elevation
    if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon) );
        return 1;
    }
    file2 = s.file;
    x_end = s.x_col;
    y_end = s.y_row;
    tl2 = s.tl;
    SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
        get_trim_double(pbb->bb_min_lat),
        get_trim_double(pbb->bb_max_lon),
        elev,
        file2.c_str(),
        x_end, y_end);

    if (file1 != file2) {
        // different files
        BNDBOX bb1, bb2;
        // we have the tile limits of 1 and 2, TL and BR
        // and the BBOX we want to render
        bb1 = *pbb;
        if (bb1.bb_min_lat < tl1.min_lat)
            bb1.bb_min_lat = tl1.min_lat;
        if (bb1.bb_max_lat > tl1.max_lat)
            bb1.bb_max_lat = tl1.max_lat;
        if (bb1.bb_min_lon < tl1.min_lon)
            bb1.bb_min_lon = tl1.min_lon;
        if (bb1.bb_max_lon > tl1.max_lon)
            bb1.bb_max_lon = tl1.max_lon;

        bb2 = *pbb;
        if (bb2.bb_min_lat < tl2.min_lat)
            bb2.bb_min_lat = tl2.min_lat;
        if (bb2.bb_max_lat > tl2.max_lat)
            bb2.bb_max_lat = tl2.max_lat;
        if (bb2.bb_min_lon < tl2.min_lon)
            bb2.bb_min_lon = tl2.min_lon;
        if (bb2.bb_max_lon > tl2.max_lon)
            bb2.bb_max_lon = tl2.max_lon;

        SPRTF("%s: File 1 covers lat,lon min %s,%s max %s,%s, %s\n", module,
            get_trim_double(tl1.min_lat),
            get_trim_double(tl1.min_lon),
            get_trim_double(tl1.max_lat),
            get_trim_double(tl1.max_lon),
            get_bounding_box_stg(&bb1,true));
        SPRTF("%s: File 2 covers lat,lon min %s,%s max %s,%s, %s\n", module,
            get_trim_double(tl2.min_lat),
            get_trim_double(tl2.min_lon),
            get_trim_double(tl2.max_lat),
            get_trim_double(tl2.max_lon),
            get_bounding_box_stg(&bb2,true));
        SPRTF("%s: Bounding Box spans different USGS DEM30 files! %s and %s\n", module,
            get_file_name(file1).c_str(),
            get_file_name(file2).c_str());
        return 1;
    }

    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;
    SPRTF("%s: Z=%d %s covers x,y %d,%d to %d,%d, span %d,%d points\n", module,
        zoom,
        get_bounding_box_stg(pbb,true),
        x_bgn,y_bgn,x_end,y_end,
        xcols, yrows );
    if ((xcols >= MIN_SPAN) && (yrows >= MIN_SPAN)) {
        iret = usgs30_out_image( width, height, yrows, xcols, y_bgn, x_bgn, file1, s, img_file );
    } else {
        SPRTF("%s: At this ZOOM %d, on DEM30 data, the BBOX is LESS THAN MIN_SPAN %dx%d (ie %d,%d) points!\n", module, zoom, MIN_SPAN, MIN_SPAN,
             xcols, yrows);
        iret |= 2;
    }
    return iret;

}


//     { "Mount Everest",  27.988, 86.9253, 8848   },
// slippy map directory zoom 6 = /6/47/26.png, 7 = /7/94/53.png, 8 = /8/189/107.png
int do_usgs30_test4()   // slippy map tile using usgs30_dir/*.DEM files
{
    int iret = 0;
    const char *dir = usgs30_dir;
    double lat = 27.988;
    double lon = 86.9253;
    int zoom = 6;
    char *cp;
    int width = 256;
    int height = 256;
    BNDBOX bb;
    PBNDBOX pbb = &bb;
    char *tmp;
    SPRTF("\n");
    SPRTF("%s: === do_usgs30_test4() =====================\n", module);

    if (is_file_or_directory((char *)dir) != 2) {
        SPRTF("%s: Directory %s NOT a valid directory!\n", module, dir);
        return 1;
    }
    usgs30 s;
    if (!s.set_file_dir(dir)) {
        SPRTF("%s: Failed to set file directory to %s!\n", module, dir);
        return 1;
    }
    std::string img;
    for (zoom = 3; zoom < 20; zoom++) {
        SPRTF("\n");
        if ( !get_slippy_path( lat, lon, zoom, &cp) ) {
            SPRTF("%s: WHAT! failed to get slippy map path for lat,lon,zoom %s,%s,%d!\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                zoom );

        }

        if (!get_map_bounding_box( lat, lon, zoom, pbb, false )) {
            SPRTF("%s: Failed to get bounding box for lat,lon,zoom %s,%s,%d\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    zoom );
            return 1;
        }

        SPRTF("%s: for lat,lon,zoom %s,%s,%d got %s, path %s\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                zoom,
                get_bounding_box_stg(pbb,true),
                cp);
        tmp = get_cache_file_name(cp,true);
        img = get_image_out_dir();
        img += PATH_SEP;
        img += tmp;
#ifdef USE_PNG_LIB
        img += ".png";
#else // !USE_PNG_LIB
        img += ".bmp";
#endif // USE_PNG_LIB y/n
        iret |= process_usgs30_bbox( s, pbb, width, height, img.c_str() );
    }

    return iret;
}

// eof = gs30_tests.cxx
