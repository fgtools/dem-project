// all_tests.hxx
#ifndef _ALL_TESTS_HXX_
#define _ALL_TESTS_HXX_

enum test_flags {
    tf_none = 0,
    // ====================================================================
    // 1 arc-secs
    tf_hgt1_test1 = 0x00000001, // Mont Blanc
    tf_hgt1_test2 = 0x00000002, // Mont Blanc - scan of file - both lat,lon positive - works well
    tf_hgt1_test3 = 0x00000004, // "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt - write images for zoom 15 - zoom 10
    tf_hgt1_test4 = 0x00000004, // Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
    tf_hgt1_test5 = 0x00000008, // Mount McKinley (Denali)	20,236 ft 6168 m 63.0690, -151.0063 see : F:/data/dem1/srtm1/index.html - Region_definition.jpg
    tf_hgt1_test6 = 0x00000010, // Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920  write all 9 images 
    tf_hgt1_test7 = 0x00000020, // whole file to image, like for //const char *hgt = "F:\\data\\dem1\\srtm1\\N36W119.hgt";
    // ====================================================================
    // 3 arc-secs
    tf_hgt3_test1 = 0x00000040, // do_hgt3_test1(): write ferr3 N31E035.hgt to image, and report on voids
    tf_hgt3_test2 = 0x00000080, // do_hgt3_test2(): Mont Blanc N45E006.hgt full set levation tests elevtests;
    tf_hgt3_test3 = 0x00000100, // do_hgt3_test3(); Mt Lamlam, Guam, elev, N13E144 to img, Tristan, elev, S38W013 to img 
    tf_hgt3_test4 = 0x00000200, // do_hgt3_test4(); Mt Everest N27E086.hgt write 256x256 image of BBOX
    tf_hgt3_test5 = 0x00000400, // do_hgt3_test5(); KSFO z=10 get elev, show 9 boxes, write 10_163_396 img use gen.color
    tf_hgt3_test6 = 0x00000800, // do_hgt3_test6(); Mont Blanc at various zooms
    tf_hgt3_test7 = 0x00001000, // do_hgt3_test7(); elevtests; de Ferranti hgt_dir, and USGS usgs_dir ;
    tf_hgt3_test8 = 0x00002000, // do_hgt3_test8(); elevtests for zoom range

    // =====================================================================
    // 30 arc-secs
    tf_sr30_test2 = 0x00004000, //iret |= do_srtm30_test2(); Mt Everest file e060n40 write 256x256 img bbox at zooms 
    tf_sr30_test3 = 0x00008000, // Using the de Ferranti ferr_dir, bathymetry 30-arcsec files //iret |= do_srtm30_test3();
    tf_sr30_test4 = 0x00010000, //iret |= do_netcdf_test();
    // =====================================================================
    // single large whole world 30 arc-sec file - SRTM land, bathemetric oceans
    // ************************************************************************
    tf_st30_test1 = 0x00020000, // do_stopo30_test1(); try limits, elevtests and group on single 2 GB file
    tf_st30_test2 = 0x00040000, //iret |= do_stopo30_test2(); // build a 1024x512 image of whole file
    tf_st30_test3 = 0x00080000, //iret |= do_stopo30_test3(); // write Mt Everest slippy maps for zooms 4-19
    //tf_st30_test4 = 0x00100000, // none 
    tf_st30_test5 = 0x00200000, // do_stopo30_test5(); write slippy maps for elevtests locs, zooms 0 - 14
#ifdef INCLUDE_GLOBE_DATA
    // ***********************************************************************
    // original OLD GLOBE 30 arc-sec set - 16 files - full of voids (-500)
    // ********************************************************************
    // seems so BAD, maybe NOT WORTH THE EFFORT!!!
    // gtopo30: NO DATA cnt 46,155,637, too high cnt 0, on 64,800,000 points.
    // =====================================================================
    tf_glob_test0 = 0x00400000, //iret |= do_globe_test(); // scan of file <gtopo_dir>\h10g get min,max... then run the elevtests
    tf_glob_test1 = 0x00800000, //iret |= do_globe_test1();
    tf_glob_test2 = 0x01000000, //iret |= do_globe_test2();
    tf_glob_test3 = 0x02000000, //iret |= do_globe_test3();
#else
    tf_usgs_test1 = 0x00400000, //iret |= do_usgs30_test1(); // Mount Everest, and run elevtests set
    tf_usgs_test2 = 0x00800000, //iret |= do_usgs30_test2(); // scan all usgs30_dir/*.DEM files for max/min
    tf_usgs_test3 = 0x01000000, //iret |= do_usgs30_test3(); // write all usgs30_dir/*.DEM files to png/bmp images
    tf_usgs_test4 = 0x02000000, //iret |= do_usgs30_test4(); // generate a slippy map tile from usgs30_dir/*.DEM files
#endif // INCLUDE_GLOBE_DATA
    // MISC
    // =====================================================================
    tf_misc_test1 = 0x04000000, //iret |= do_tile_test();
    tf_misc_test2 = 0x08000000, //do_dir_tests(); // just to check the services work - create dir WILL fail if there already exist a file of that name
    tf_misc_test3 = 0x10000000, //iret |= do_color_test();
    tf_misc_test4 = 0x20000000, //iret |= test_hgt_file_name();
    tf_misc_test5 = 0x40000000, //iret |= do_mappy_tile_test();
    tf_misc_test6 = 0x80000000, // iret |= get_elev_tests();
    // UGH - out of bits, but there are some, and could maybe remove GLOBE
    //iret |= do_dist_test();
};

extern int do_tests( test_flags flag );
extern void show_test_bits();
extern bool get_bit_from_name( char *sarg, unsigned int *pui);


#endif // _ALL_TESTS_HXX_
// eof - all_tests.hxx
