// hgt1_tests.hxx
#ifndef _HGT1_TESTS_HXX_
#define _HGT1_TESTS_HXX_

extern int do_hgt1_test1(); // Mont Blanc elev test, mult zoom imgs, html, plus 1024x1024
extern int do_hgt1_test2(); // Mont BLanc elev - scan of HGT file
// { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
extern int do_hgt1_test3();
// Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
extern int do_hgt1_test4();
// Mount McKinley (Denali)	20,236 ft 6168 m 63.0690, -151.0063
extern int do_hgt1_test5();
// Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
// write all 9 images 
extern int do_hgt1_test6();

// whole file to image, like for
// const char *hgt = "F:\\data\\dem1\\srtm1\\N36W119.hgt";
extern int do_hgt1_test7(const char *file);

#endif // #ifndef _HGT1_TESTS_HXX_
// eof

