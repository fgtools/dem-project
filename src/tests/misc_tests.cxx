// misc_tests.cxx
#include <stdio.h>
#include <string>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#include <math.h>  // for pow(), ...
#endif
#include "demFile.hxx"
#include "sprtf.hxx"
#include "utils.hxx"
#include "dir_utils.hxx"
#include "dem_tests.hxx"
#include "srtm30.hxx"
#include "slippystuff.hxx"
#include "misc_tests.hxx"

static const char *module = "misc_tests";

int get_elev_tests()
{
    int iret = 0;
    SPRTF("\n");
    SPRTF("%s: === get_elev_tests =====================\n", module);
    // Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
    short elev;
    double lat = 36.5786;
    double lon = -118.2920;
    //if (!get_elevation( 36.5786, -118.2920, &elev ))
    //    iret |= 1;

    //for ( ; lon >= -119.0; lon -= 0.1) {
    //    if (!get_elevation( lat, lon, &elev, false )) {
    //        iret = 1;
    //        get_elevation( lat, lon, &elev, true );
    //        break;
    //    }
    //}

    for (lon = -119.0; lon < -118.0; lon += 0.05) {
        if (!get_best_elevation( lat, lon, &elev, 0 )) {
            iret = 1;
            get_best_elevation( lat, lon, &elev, 3 );
            break;
        }
    }

    return iret;

}

void do_dir_tests() 
{
    SPRTF("\n");
    SPRTF("%s: === do_dir_tests =====================\n", module);
    //std::string dir("F:\\data\\dem3\\images\\temp1/temp2\\temp3");
    //dir_create(dir,0766);
    //std::string dir("F:\\data\\dem3\\hgt");
    std::string dir(ferr30_dir); // = "F:\\data\\srtm30_plus"; // Bathymetry.srtm
    //std::string mask("*.hgt");
    std::string mask("");
    //vSTG vFiles = read_directory( dir, mask, dm_FILE );
    vSTG vFiles = read_directory( dir, mask, 0, true );
    SPRTF("%s: read_directory returned %d files...\n", module, (int)vFiles.size());
    vFiles.clear();
    mask = "*.srtm";
    vFiles = read_directory( dir, mask, 0, true );
    SPRTF("%s: read_directory returned %d files...\n", module, (int)vFiles.size());

}


int do_tile_test()
{
    SPRTF("\n");
    SPRTF("%s: === do_tile_test() =====================\n", module);
    TILELIMITS tl;
    std::string msg;
    double lat = 37.618674211;
    double lon = -122.375007609;
    const char *tile = get_srmt30_tile_name(lat,lon);
    bool gotlim = false;
    if (tile)
        gotlim = get_srmt30_tile_limits(tile, &tl);
    SPRTF("%s: For lat %s, lon %s (SF) is in tile %s ", module,
        get_trim_double(lat),
        get_trim_double(lon), 
        (tile ? tile : "NOT FOUND!") );
    if (gotlim) {
        SPRTF(" limits TL %s,%s to BR %s,%s ",
            get_trim_double(tl.max_lat),
            get_trim_double(tl.min_lon),
            get_trim_double(tl.min_lat),
            get_trim_double(tl.max_lon));
    }
    SPRTF("\n");
    //     Cerro Aconcagua : Elevation: 22,841 feet (6,962 meters), 
    // Coordinates: 32 39 20 S 70 00 57 W (-32.6555555555556, -70.0158333333333)
    lat = -32.6555555555556;
    lon = -70.0158333333333;
    gotlim = false;
    tile = get_srmt30_tile_name(lat,lon);
    if (tile)
        gotlim = get_srmt30_tile_limits(tile, &tl);
    SPRTF("%s: For lat %lf, lon %lf (Aconcagua) is in tile %s ", module, lat,lon, 
        (tile ? tile : "NOT FOUND!") );
    if (gotlim) {
        if ((lat <= tl.max_lat) &&
            (lat >= tl.min_lat) &&
            (lon <= tl.max_lon) &&
            (lon >= tl.min_lon)) {
            msg = "ok";
        } else {
            msg = "CHECK RANGE!!!";
        }
            
        SPRTF(" limits TL %s,%s to BR %s,%s %s ",
            get_trim_double(tl.max_lat),
            get_trim_double(tl.min_lon),
            get_trim_double(tl.min_lat),
            get_trim_double(tl.max_lon),
            msg.c_str() );
    }
    SPRTF("\n");

    vSTG vs;
    std::string s;
    size_t cnt = 0;
    for (lat = -90.0; lat <= 90.0; lat += 10.0) {
        for (lon = -180.0; lon <= 180.0; lon += 10.0) {
            tile = get_srmt30_tile_name(lat,lon);
            if (tile) {
                s = tile;
                if (!string_in_vec( vs, s )) {
                    vs.push_back(s);
                    cnt++;
                    SPRTF("%s: %d: lat,lon %s,%s the tile is %s ", module, (int)cnt,
                        get_trim_double(lat),
                        get_trim_double(lon),
                        tile ); 
                    gotlim = get_srmt30_tile_limits(tile, &tl);
                    if (gotlim) {
                        SPRTF(" limits TL %s,%s to BR %s,%s ",
                            get_trim_double(tl.max_lat),
                            get_trim_double(tl.min_lon),
                            get_trim_double(tl.min_lat),
                            get_trim_double(tl.max_lon));
                    }
                    SPRTF("\n");
                }
            } else {
                SPRTF("%s: Why no tile for %s,%s???? *** CHECK ME ***\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon) );
            }
        }
    }
    return 0;
}

/* =================================================================
    OUTPUT: Using KSFO
    int zoom = 10;
    double lat = 37.618;
    double lon = -122.375;

misc_tests: === do_mappy_tile_test() =====================
misc_tests: Z=10 Current lat,lon 37.618,-122.375 = BBOX lat,lon min 37.439974,-122.695313 max 37.71859,-122.34375 span 0.278616,0.351563.
misc_tests: nbb_right lon: -122.695313 -122.34375 -121.992188 -121.640625 -121.289063 increases
misc_tests: nbb_bottom lat: 37.439974 37.160317 36.879621 36.597889 36.315125 decreasing
misc_tests: nbb_left lon: -122.695313 -123.046875 -123.398438 -123.75 -124.101563 decreasing
misc_tests: nbb_top lat: 37.439974 37.71859 37.996163 38.272689 38.548165 decreasing
misc_tests: nbb_lr lat,lon: 37.439974,-122.695313 36.879621,-121.992188 36.315125,-121.289063 lat/dec lon/inc
misc_tests: nbb_ll lat,lon: 37.439974,-122.695313 36.879621,-123.398438 36.315125,-124.101563 lat/dec lon/dec
misc_tests: nbb_tl lat,lon: 37.439974,-122.695313 37.996163,-123.398438 38.548165,-124.101563 lat/inc lon/dec
misc_tests: nbb_tr lat,lon: 37.439974,-122.695313 37.996163,-121.992188 38.548165,-121.289063 lat/inc lon/incc
misc_tests: Have an expanded bound box BBOX lat,lon min 36.315125,-124.101563 max 38.822591,-120.9375 span 2.507466,3.164063.
misc_tests: Successfuly PASSED move bbox test

   ================================================================= */

int do_mappy_tile_test()
{
    int iret = 0;
    // KSFO
    int zoom = 10;
    double lat = 37.618;
    double lon = -122.375;
    BNDBOX bb, bb2, bblast, bigbb;
    PBNDBOX pbb = &bb;
    PBNDBOX pbb2 = &bb2;
    int cnt, max_cnt = 3;
    double diff, diff2;
    vDBLE vd;
    bool verb = false;  // set true for LOTS of 'noise'
    size_t max, ii;
    int ilat, ilon, z;
    char *cp, *tmp, *sp;
    double file_lat,file_lon;
    vSTG files;
    std::string s;
    int max_line = 119;
    bool got_sp;
    double total;
    //double subtot, subbytes;
    double avtile = 2000.0;

    SPRTF("\n");
    SPRTF("%s: === do_mappy_tile_test() =====================\n", module);

    SPRTF("%s: HGT1_XDIM = %g, HGT3_XDIM = %g, STOPO30_DIM = %g, GTOPO_XDIM = %g, path = /z/x/y.png\n", module, 
        HGT1_XDIM, HGT3_XDIM, (360.0 / (double)STOPO30_COLS), GTOPO_XDIM);
    /////////////////////////////////////////////////////////////////////////////
    // zoom tests
    lon = MIN_SLIPPY_LON;
    lat = MIN_SLIPPY_LAT;
    total = 0.0;
    SPRTF("\n");
    SPRTF("%s: For MIN lat,lon %s,%s\n", module,
        get_trim_double(lat),
        get_trim_double(lon));
    for (z = 0; z <= MAX_SLIPPY_ZOOM; z++) {
        if (z) {
            diff = pow(2.0,z) * pow(2.0,z);
        } else {
            diff = 1.0;
        }
        total += diff;
        //subtot = total / 1024.0;
        //subbytes = (total * avtile)  / 1024.0;

        sp = 0;
        got_sp = get_slippy_path(lat,lon,z,&sp);
        cp = get_trim_double(diff);
        cp = get_NiceNumber(cp);
        tmp = GetNxtBuf();
        if (get_map_bounding_box( lat, lon, z, pbb, verb )) {
            sprintf(tmp,"%s: Z=%2d got %s ", module, z, 
                get_bounding_box_stg(pbb,true));
            if (got_sp && sp && *sp)
                strcat(tmp,sp);
            else
                strcat(tmp,"SP_FAILED");
            while( (int)strlen(tmp) < max_line)
                strcat(tmp," ");
            SPRTF("%s % 16s tiles.  ",tmp,cp);
            //SPRTF("T %sK, B %sK\n", get_NiceNumberStg((int)subtot), get_NiceNumberStg((int)subbytes));
            SPRTF("T %s, B %s\n", get_k_num((long long)total), get_k_bytes((long long)(total*avtile)) );
        } else {
            SPRTF("%s: Z=%d FAILED to get BBOX\n", module, z ); 
        }
    }
    cp = get_trim_double(total);
    cp = get_NiceNumber(cp);
    SPRTF("%s: That represents a total of %s tiles.\n", module,cp);

    lon = 0.0;
    lat = 0.0;
    SPRTF("\n");
    SPRTF("%s: For ZERO lat,lon...\n", module);
    for (z = 0; z <= MAX_SLIPPY_ZOOM; z++) {
        if (z) {
            diff = pow(2.0,z) * pow(2.0,z);
        } else {
            diff = 1.0;
        }
        sp = 0;
        got_sp = get_slippy_path(lat,lon,z,&sp);
        cp = get_trim_double(diff);
        cp = get_NiceNumber(cp);
        tmp = GetNxtBuf();
        if (get_map_bounding_box( lat, lon, z, pbb, verb )) {
            sprintf(tmp,"%s: Z=%2d got %s ", module, z, 
                get_bounding_box_stg(pbb,true));
            if (got_sp && sp && *sp)
                strcat(tmp,sp);
            else
                strcat(tmp,"SP_FAILED");
            while( (int)strlen(tmp) < max_line)
                strcat(tmp," ");
            SPRTF("%s % 16s tiles.\n",tmp,cp);
        } else {
            SPRTF("%s: Z=%d FAILED to get BBOX\n", module, z ); 
        }
    }
    //lon = MAX_SLIPPY_LON - 0.351563;
    //lat = MAX_SLIPPY_LAT - 0.030421;
    lon = MAX_SLIPPY_LON - MY_EPSILON;
    lat = MAX_SLIPPY_LAT - MY_EPSILON;
    SPRTF("\n");
    SPRTF("%s: For MAX lat,lon %s,%s\n", module,
        get_trim_double(lat),
        get_trim_double(lon));
    for (z = 0; z <= MAX_SLIPPY_ZOOM; z++) {
        if (z) {
            diff = pow(2.0,z) * pow(2.0,z);
        } else {
            diff = 1.0;
        }
        sp = 0;
        got_sp = get_slippy_path(lat,lon,z,&sp);
        cp = get_trim_double(diff);
        cp = get_NiceNumber(cp);
        tmp = GetNxtBuf();
        if (get_map_bounding_box( lat, lon, z, pbb, verb )) {
            sprintf(tmp,"%s: Z=%2d got %s ", module, z, 
                get_bounding_box_stg(pbb,true));
            if (got_sp && sp && *sp)
                strcat(tmp,sp);
            else
                strcat(tmp,"SP_FAILED");
            while( (int)strlen(tmp) < max_line)
                strcat(tmp," ");
            SPRTF("%s % 16s tiles.\n",tmp,cp);
        } else {
            SPRTF("%s: Z=%d FAILED to get BBOX\n", module, z ); 
        }
    }

    SPRTF("\n");
    /////////////////////////////////////////////////////////////////////////////
    lat = MIN_SLIPPY_LAT;
    lon = MAX_SLIPPY_LON - MY_EPSILON;
    SPRTF("%s: For MIN lat, MAX lon %s,%s\n", module,
        get_trim_double(lat),
        get_trim_double(lon));
    const char *msgx;
    const char *msgy;
    for (z = 0; z <= MAX_SLIPPY_ZOOM; z++) {
        int max_x = lon2tilex(lon,z); // get tilex
        int max_y = lat2tiley(lat,z); // get tiley
        msgx = "";
        if (max_x != get_max_tilex_for_zoom(z))
            msgx = "CHECK get_max_tilex_for_zoom() service!";
        msgy = "";
        if (max_y != get_max_tiley_for_zoom(z))
            msgy = "CHECK get_max_tiley_for_zoom() service!";

        SPRTF("%s: Z=%2d tilex 0 to %6u  tiley 0 to %6u %s %s\n", module,
            z,
            max_x, max_y,
            msgx, msgy );
    }

    SPRTF("\n");
    // Cerro Aconcagua : Elevation: 22,841 feet (6,962 meters), 
    // Coordinates: 32 39 20 S 70 00 57 W (-32.6555555555556, -70.0158333333333)
    lat = -32.6555555555556;
    lon = -70.0158333333333;
    zoom = 15;

    // seed bounding box
    //bigbb.bb_max_lat = lat;
    //bigbb.bb_max_lon = lon;
    //bigbb.bb_min_lat = lat;
    //bigbb.bb_min_lon = lon;
    if (!seed_bounding_box(lat,lon,&bigbb)) {
        SPRTF("%s: ARGH: How can 'seeding' be bad!\n", module);
        check_me();
        return 1;
    }

    // char *get_hgt_file_name( double dlat, double dlon, int *pilat, int *pilon, bool verb )
    cp = get_hgt_file_name( lat, lon, &ilat, &ilon, true );
    if (!cp) {
        SPRTF("%s: UGH: How did get file name FAIL?\n", module);
        check_me();
        return 1;
    }
    files.push_back(cp);

    if ( !get_hgt_lat_lon( cp, &file_lat, &file_lon ) ) {
        SPRTF("%s: UGH: How did get file lat,lon FAIL?\n", module);
        check_me();
        return 1;
    }

    if (!get_map_bounding_box( lat, lon, zoom, pbb, verb )) {
        SPRTF("%s: ZOOT: FAILED to get slippy map bounding box!\n", module);
        check_me();
        return 1;
    }

    SPRTF("%s: Z=%d Current lat,lon %s,%s = %s.\n", module,
        zoom,
        get_trim_double(lat),
        get_trim_double(lon),
        get_bounding_box_stg(pbb,true));


    /////////////////////////////////////////////////////////////////
    // ready for the tests
    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lon);
    while ( get_next_map_bbox( pbb2, nbb_right, verb )) {
        diff = bb2.bb_min_lon - bblast.bb_min_lon;
        if (diff <= 0.0) {
            SPRTF("\n%s: nbb_right FAILED! prev lon %s, curr lon %s, diff %s\n", module,
                get_trim_double(bblast.bb_min_lon),
                get_trim_double(bb2.bb_min_lon),
                get_trim_double(diff) );
            iret |= 1;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lon);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }

    if ( !(iret & 1) ) {
        SPRTF("%s: nbb_right lon: ", module);
        max = vd.size();
        for (ii = 0; ii < max; ii++) {
            diff = vd[ii];
            SPRTF("%s ", get_trim_double(diff));
        }
        SPRTF("increases\n");
    }


    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lat);
    while ( get_next_map_bbox( pbb2, nbb_bottom, verb )) {
        diff = bblast.bb_min_lat - bb2.bb_min_lat;
        if (diff <= 0.0) {
            SPRTF("\n%s: nbb_bottom (down) FAILED! prev lat %s, curr lat %s, diff %s\n", module,
                get_trim_double(bblast.bb_min_lat),
                get_trim_double(bb2.bb_min_lat),
                get_trim_double(diff) );
            iret |= 2;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lat);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }
    if ( !(iret & 2) ) {
        SPRTF("%s: nbb_bottom lat: ", module);
        max = vd.size();
        for (ii = 0; ii < max; ii++) {
            diff = vd[ii];
            SPRTF("%s ", get_trim_double(diff));
        }
        SPRTF("decreasing\n");
    }

    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lon);
    while ( get_next_map_bbox( pbb2, nbb_left, verb )) {
        diff = bblast.bb_min_lon - bb2.bb_min_lon;
        if (diff <= 0.0) {
            SPRTF("\n%s: nbb_left FAILED! prev lon %s, curr lon %s, diff %s\n", module,
                get_trim_double(bblast.bb_min_lon),
                get_trim_double(bb2.bb_min_lon),
                get_trim_double(diff) );
            iret |= 4;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lon);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }

    if ( !(iret & 4) ) {
        SPRTF("%s: nbb_left lon: ", module);
        max = vd.size();
        for (ii = 0; ii < max; ii++) {
            diff = vd[ii];
            SPRTF("%s ", get_trim_double(diff));
        }
        SPRTF("decreasing\n");
    }

    // top, up - ie lat INCREASES
    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lat);
    while ( get_next_map_bbox( pbb2, nbb_top, verb )) {
        diff = bb2.bb_min_lat - bblast.bb_min_lat;
        if (diff <= 0.0) {
            SPRTF("\n%s: nbb_top (up) FAILED! prev lat %s, curr lat %s, diff %s\n", module,
                get_trim_double(bblast.bb_min_lat),
                get_trim_double(bb2.bb_min_lat),
                get_trim_double(diff) );
            iret |= 8;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lat);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }
    if ( !(iret & 8) ) {
        SPRTF("%s: nbb_top lat: ", module);
        max = vd.size();
        for (ii = 0; ii < max; ii++) {
            diff = vd[ii];
            SPRTF("%s ", get_trim_double(diff));
        }
        SPRTF("decreasing\n");
    }

    // diagonal - lower right - ie lat DECREASE, lon INCREASE
    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lat);
    vd.push_back(bb2.bb_min_lon);
    while ( get_next_map_bbox( pbb2, nbb_lr, verb )) {
        diff = bblast.bb_min_lat - bb2.bb_min_lat;
        diff2 = bb2.bb_min_lon - bblast.bb_min_lon;
        if ((diff <= 0.0) || (diff2 <= 0.0)) {
            SPRTF("\n%s: nbb_lr (lower right) FAILED! prev lat,lon %s,%s curr lat,lon %s,%s lat diff %s lon diff %s\n", module,
                get_trim_double(bblast.bb_min_lat),
                get_trim_double(bblast.bb_min_lon),
                get_trim_double(bb2.bb_min_lat),
                get_trim_double(bb2.bb_min_lon),
                get_trim_double(diff),
                get_trim_double(diff2) );
            iret |= 16;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lat);
        vd.push_back(bb2.bb_min_lon);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }
    if ( !(iret & 16) ) {
        SPRTF("%s: nbb_lr lat,lon: ", module);
        max = vd.size() / 2;
        for (ii = 0; ii < max; ii += 2) {
            diff = vd[ii*2];
            diff2 = vd[(ii*2)+1];
            SPRTF("%s,%s ", get_trim_double(diff), get_trim_double(diff2));
        }
        SPRTF("lat/dec lon/inc\n");
    }

    // diagonal - lower left - ie lat DECREASE, lon DECREASES
    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lat);
    vd.push_back(bb2.bb_min_lon);
    while ( get_next_map_bbox( pbb2, nbb_ll, verb )) {
        diff = bblast.bb_min_lat - bb2.bb_min_lat;
        diff2 = bblast.bb_min_lon - bb2.bb_min_lon;
        if ((diff <= 0.0) || (diff2 <= 0.0)) {
            SPRTF("\n%s: nbb_ll (lower left) FAILED! prev lat,lon %s,%s curr lat,lon %s,%s lat diff %s lon diff %s\n", module,
                get_trim_double(bblast.bb_min_lat),
                get_trim_double(bblast.bb_min_lon),
                get_trim_double(bb2.bb_min_lat),
                get_trim_double(bb2.bb_min_lon),
                get_trim_double(diff),
                get_trim_double(diff2) );
            iret |= 32;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lat);
        vd.push_back(bb2.bb_min_lon);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }
    if ( !(iret & 32) ) {
        SPRTF("%s: nbb_ll lat,lon: ", module);
        max = vd.size() / 2;
        for (ii = 0; ii < max; ii += 2) {
            diff = vd[ii*2];
            diff2 = vd[(ii*2)+1];
            SPRTF("%s,%s ", get_trim_double(diff), get_trim_double(diff2));
        }
        SPRTF("lat/dec lon/dec\n");
    }

    // diagonal - upper left - ie lat INCREASE, lon DECREASES
    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lat);
    vd.push_back(bb2.bb_min_lon);
    while ( get_next_map_bbox( pbb2, nbb_tl, verb )) {
        diff = bb2.bb_min_lat - bblast.bb_min_lat;
        diff2 = bblast.bb_min_lon - bb2.bb_min_lon;
        if ((diff <= 0.0) || (diff2 <= 0.0)) {
            SPRTF("\n%s: nbb_tl (top left) FAILED! prev lat,lon %s,%s curr lat,lon %s,%s lat diff %s lon diff %s\n", module,
                get_trim_double(bblast.bb_min_lat),
                get_trim_double(bblast.bb_min_lon),
                get_trim_double(bb2.bb_min_lat),
                get_trim_double(bb2.bb_min_lon),
                get_trim_double(diff),
                get_trim_double(diff2) );
            iret |= 64;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lat);
        vd.push_back(bb2.bb_min_lon);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }
    if ( !(iret & 64) ) {
        SPRTF("%s: nbb_tl lat,lon: ", module);
        max = vd.size() / 2;
        for (ii = 0; ii < max; ii += 2) {
            diff = vd[ii*2];
            diff2 = vd[(ii*2)+1];
            SPRTF("%s,%s ", get_trim_double(diff), get_trim_double(diff2));
        }
        SPRTF("lat/inc lon/dec\n");
    }

    // diagonal - upper right - ie lat INCREASE, lon INCREASES
    bb2 = bb;
    bblast = bb2;
    cnt = 0;
    vd.clear();
    vd.push_back(bb2.bb_min_lat);
    vd.push_back(bb2.bb_min_lon);
    while ( get_next_map_bbox( pbb2, nbb_tr, verb )) {
        diff = bb2.bb_min_lat - bblast.bb_min_lat;
        diff2 = bb2.bb_min_lon - bblast.bb_min_lon;
        if ((diff <= 0.0) || (diff2 <= 0.0)) {
            SPRTF("\n%s: nbb_tr (top right) FAILED! prev lat,lon %s,%s curr lat,lon %s,%s lat diff %s lon diff %s\n", module,
                get_trim_double(bblast.bb_min_lat),
                get_trim_double(bblast.bb_min_lon),
                get_trim_double(bb2.bb_min_lat),
                get_trim_double(bb2.bb_min_lon),
                get_trim_double(diff),
                get_trim_double(diff2) );
            iret |= 128;
            break;
        }
        if (!expand_bounding_box_by_box( pbb2, &bigbb, verb)) {
            SPRTF("%s: Failed in EXPANDING bounding box!\n", module);
            if (!verb)
                expand_bounding_box_by_box( pbb2, &bigbb, true);
            check_me();
            return 1;
        }
        cp = get_hgt_file_name( bb2.bb_min_lat, bb2.bb_min_lon, &ilat, &ilon );
        if (!cp) {
            SPRTF("%s: UGH: How did get file name FAIL?\n", module);
            check_me();
            return 1;
        }
        s = cp;
        if (!string_in_vec( files, s ))
            files.push_back(s);
        vd.push_back(bb2.bb_min_lat);
        vd.push_back(bb2.bb_min_lon);
        bblast = bb2;
        cnt++;
        if (cnt > max_cnt)
            break;
    }
    if ( !(iret & 128) ) {
        SPRTF("%s: nbb_tr lat,lon: ", module);
        max = vd.size() / 2;
        for (ii = 0; ii < max; ii += 2) {
            diff = vd[ii*2];
            diff2 = vd[(ii*2)+1];
            SPRTF("%s,%s ", get_trim_double(diff), get_trim_double(diff2));
        }
        SPRTF("lat/inc lon/inc\n");
    }

    if (iret) {
        SPRTF("%s: FAILED move bbox test\n", module);
    } else {
        SPRTF("%s: Have an expanded bound box %s.\n", module,
            get_bounding_box_stg(&bigbb,true));
        max = files.size();
        SPRTF("%s: BBOX requires %d files: ", module, (int)max);
        for (ii = 0; ii < max; ii++) {
            s = files[ii];
            SPRTF("%s ", s.c_str());
        }
        SPRTF("\n");
        SPRTF("%s: Successfuly PASSED move bbox test\n", module);
    }
    return iret;
}

/*
    OUTPUT
misc_tests: === test_hgt_file_name() =====================
utils: lat,lon 25,36 is in file N25E036.hgt.
utils: lat,lon 25.99999,36.99999 is in file N25E036.hgt.
utils: lat,lon -25,-36 is in file S25W036.hgt.
utils: lat,lon -25.5,-36.5 is in file S24W035.hgt.
misc_tests: Passed file name test.

 */

int test_hgt_file_name()
{
    char *cp, *cp2;
    int ilat, ilon;
    double dlat = 25.0;
    double dlon = 36.0;
    double llat,llon;
    SPRTF("\n");
    SPRTF("%s: === test_hgt_file_name() =====================\n", module);

    cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon, true );
    if (!cp) {
        return 1;
    }
    cp2 = cp;
    llat = dlat;
    llon = dlon;
    dlat = 25.99999;
    dlon = 36.99999;
    cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon, true );
    if (!cp) {
        SPRTF("%s: Failed to get file name for lat,lon %s,%s\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return 1;
    }
    if (strcmp(cp,cp2)) {
        SPRTF("%s: Error: Got DIFFERENT file names for lat,lon %s,%s %s for %s,%s %s\n", module,
            get_trim_double(llat),
            get_trim_double(llon),
            cp2,
            get_trim_double(dlat),
            get_trim_double(dlon),
            cp );
        return 1;

    }

    dlat = -25.0;
    dlon = -36.0;
    cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon, true );
    if (!cp) {
        SPRTF("%s: Failed to get file name for lat,lon %s,%s\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return 1;
    }

    cp2 = cp;
    llat = dlat;
    llon = dlon;
    dlat = -25.5;
    dlon = -36.5;
    cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon, true );
    if (!cp) {
        SPRTF("%s: Failed to get file name for lat,lon %s,%s\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return 1;
    }
    if (!strcmp(cp,cp2)) {
        SPRTF("%s: Error: Got SAME file names for lat,lon %s,%s %s for %s,%s %s\n", module,
            get_trim_double(llat),
            get_trim_double(llon),
            cp2,
            get_trim_double(dlat),
            get_trim_double(dlon),
            cp );
        return 1;

    }
    SPRTF("%s: Passed file name test.\n", module);
    return 0;
}

int do_dist_test()
{
    char *cp;
    double m;
    SPRTF("\n");
    SPRTF("%s: === do_dist_test() =====================\n", module);

    cp = get_meters_stg( -1.0 );
    SPRTF("%s - Negative result\n",cp);
    m = 0.0;
    cp = get_meters_stg(m);
    SPRTF("%s - Zero result\n",cp);
    m = (MY_EPSILON / 10.0);
    cp = get_meters_stg(m);
    SPRTF("%s - Close to zero result\n",cp);
    m = MY_EPSILON;
    cp = get_meters_stg(m);
    SPRTF("%s - MY_EPSILON result\n",cp);
    m = 0.0015;
    cp = get_meters_stg(m);
    SPRTF("%s - 1.5 mm result\n",cp);
    m = 1.0015;
    cp = get_meters_stg(m);
    SPRTF("%s - 1 m + .0015 mm result\n",cp);
    m = 1.1;
    cp = get_meters_stg(m);
    SPRTF("%s - 1.1 m result\n",cp);
    m = 999.9999;
    cp = get_meters_stg(m);
    SPRTF("%s - 999.9999 m result\n",cp);
    m = 1000.000001;
    cp = get_meters_stg(m);
    SPRTF("%s - 1000.000001 m result\n",cp);
    m = 1234.567;
    cp = get_meters_stg(m);
    SPRTF("%s - 1234.567 m result\n",cp);
    return 0;
}

// eof
