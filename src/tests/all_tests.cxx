// all_tests.cxx


#include <stdio.h>
#include <string.h> // for strcmp, ... in unix
#include "gt30_tests.hxx"
#include "hgt1_tests.hxx"
#include "hgt3_tests.hxx"
#include "misc_tests.hxx"
#include "ncdf_tests.hxx"
#include "sr30_tests.hxx"
#include "st30_tests.hxx"
#include "gs30_tests.hxx"   // USGS30 DEM files
#include "color.hxx"
#include "sprtf.hxx"
#include "utils.hxx"
#include "all_tests.hxx"

static const char *module = "all_tests";

int do_color_test()
{
    const char *out = "tempcolor.html";
    //PHGT2COLOR pt = get_current_table();
    test_gen_colors();
    std::string html = get_color_http();
    FILE *fp = fopen(out,"w");
    if (!fp) {
        SPRTF("%s: Failed to open %s!\n", module, out );
        return 1;
    }
    fwrite(html.c_str(), html.size(), 1, fp);
    fclose(fp);
    SPRTF("%s: Writen color table to %s.\n", module, out );
    return 0;
}

int do_tests( test_flags flag )
{
	int iret = 0;
    int cnt = 0;
    double bgn = get_seconds();
    // ====================================================================
    // 1 arc-sec
    if (flag & tf_hgt1_test1) {
        cnt++;
        iret |= do_hgt1_test1(); // Mont Blanc elev test, mult zoom imgs, html, plus 1024x1024
    }
    if (flag & tf_hgt1_test2) {
        cnt++;
        iret |= do_hgt1_test2(); // Mont Blanc - scan of file - both lat,lon positive - works well
    }
    // { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
    if (flag & tf_hgt1_test3) {
        cnt++;
        iret |= do_hgt1_test3(); // write images for zoom 15 - zoom 10
    }
    // Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
    if (flag & tf_hgt1_test4) {
        cnt++;
        iret |= do_hgt1_test4(); // Mount Whitney - fix this negative lon, but it is a VOID!!!
    }
    // this test also write images at zoom 15 down until 2 files, or file not found
    // and repeats those images for the lowest elevation found in scan...
    // TODO: BUT how come the scan did NOT turn up the big negatives???
    // hgt1_tests: Extract w,h 256,256 from x,y bgn 2517,1448, span 2,1 (1.238281,0.992188) from N36W119.hgt.
    // color: Elevation -14401 NOT found... color: Elevation -14572 NOT found - this needs investigation

    // ARG! USGS DEM1 ONLY GO TO 60 degrees N - Region_07.txt - top is N59W165.hgt.zip
    // Mount McKinley (Denali)	20,236 ft 6168 m 63.0690, -151.0063
    if (flag & tf_hgt1_test5) {
        cnt++; 
        iret |= do_hgt1_test5(); // see : F:/data/dem1/srtm1/index.html - Region_definition.jpg
    }
    // Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
    // write all 9 images 
    if (flag & tf_hgt1_test6) {
        cnt++;
        iret |= do_hgt1_test6();
    }
    // whole file to image, like for
    if (flag & tf_hgt1_test7) {
        //const char *hgt = "F:\\data\\dem1\\srtm1\\N36W119.hgt";
        // const char *hgt = "F:\\data\\dem1\\srtm1\\N36W122.hgt";
        // Theo's favorite
        const char *hgt = "F:\\data\\dem1\\srtm1\\N37W123.hgt";
        iret |= do_hgt1_test7(hgt);
        cnt++;
    }

    // ====================================================================
    // 3 arc-secs
    if (flag & tf_hgt3_test1) {
        cnt++;
        iret |= do_hgt3_test1(); // write ferr3 N31E035.hgt to image, and report on voids
    }
    // { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
    if (flag & tf_hgt3_test2) {
        cnt++; 
        iret |= do_hgt3_test2(); // Mont Blanc N45E006.hgt iret |= do_hgt3_test2(); full set levation tests elevtests;
    }
    if (flag & tf_hgt3_test3) {
        cnt++; 
        iret |= do_hgt3_test3(); // Mt Lamlam, Guam, elev, N13E144 to img, Tristan, elev, S38W013 to img 
    }
    if (flag & tf_hgt3_test4) {
        cnt++; 
        iret |= do_hgt3_test4(); // Mt Everest N27E086.hgt write 256x256 image of BBOX
    }
    if (flag & tf_hgt3_test5)  {
        cnt++;
        iret |= do_hgt3_test5(); // KSFO z=10 get elev, show 9 boxes, write 10_163_396 img use gen.color
    }
    if (flag & tf_hgt3_test6)  {
        cnt++;
        iret |= do_hgt3_test6(); // Mont Blanc at various zooms
    }
    // run the full set of elevation tests - PELEVTESTS pet = elevtests;
    // Using both the de Ferranti hgt_dir, and the previous USGS usgs_dir
    if (flag & tf_hgt3_test7)  {
        cnt++;
        iret |= do_hgt3_test7(); // elevtests; de Ferranti hgt_dir, and USGS usgs_dir
    }
    if (flag & tf_hgt3_test8)  {
        cnt++;
        iret |= do_hgt3_test8(); // elevtests, for various zooms
    }
    // =====================================================================
    //if (flag & tf_sr30_test1)
    //    iret |= do_srtm30_test();
    if (flag & tf_sr30_test2) {
        cnt++;
        iret |= do_srtm30_test2(); //  Mt Everest file e060n40 write 256x256 img bbox at zooms
    }
    // run the full set of elevation tests - PELEVTESTS pet = elevtests;
    // Using the de Ferranti ferr_dir, bathymetry 30-arcsec files
    if (flag & tf_sr30_test3) {
        cnt++;
        iret |= do_srtm30_test3();
    }
    if (flag & tf_sr30_test4) {
        cnt++;
        iret |= do_netcdf_test();
    }
    // =====================================================================

    // single large whole world 30 arc-sec file - SRTM land, bathemetric oceans
    // ************************************************************************
    if (flag & tf_st30_test1) {
        cnt++;
        iret |= do_stopo30_test1(); // try limits, elevtests and group on single 2 GB file
    }
    if (flag & tf_st30_test2) {
        cnt++;
        iret |= do_stopo30_test2(); // build a 1024x512 image of whole file
    }
    if (flag & tf_st30_test3) {
        cnt++;
        iret |= do_stopo30_test3(); // write Mt Everest slippy maps for zooms 4-19
    }
    //tf_st30_test4 = 0x00100000, // none 
    if (flag & tf_st30_test5) {
        cnt++;
        iret |= do_stopo30_test5(); // write slippy maps for elevtests locs, zooms 0 - 14
    }

#ifdef INCLUDE_GLOBE_DATA
    // original OLD GLOBE 30 arc-sec set - 16 files - full of voids (-500)
    // ********************************************************************
    // seems so BAD, maybe NOT WORTH THE EFFORT!!!
    // gtopo30: NO DATA cnt 46,155,637, too high cnt 0, on 64,800,000 points.
    // =====================================================================
    if (flag & tf_glob_test0) {
        cnt++;
        iret |= do_globe_test(); // scan of file <gtopo_dir>\h10g get min,max... then run the elevtests
    }
    if (flag & tf_glob_test1) {
        cnt++;
        iret |= do_globe_test1();
    }
    if (flag & tf_glob_test2) {
        cnt++;
        iret |= do_globe_test2();
    }
    if (flag & tf_glob_test3) {
        cnt++;
        iret |= do_globe_test3();
    }
#else
    if (flag & tf_usgs_test1) {
        cnt++;
        iret |= do_usgs30_test1(); // Mount Everest, and run elevtests set
    }
    if (flag & tf_usgs_test2) {
        cnt++;
        iret |= do_usgs30_test2(); // scan all usgs30_dir/*.DEM files for max/min
    }
    if (flag & tf_usgs_test3) {
        cnt++;
        iret |= do_usgs30_test3(); // write all usgs30_dir/*.DEM files to png/bmp images
    }
    if (flag & tf_usgs_test4) {
        cnt++;
        iret |= do_usgs30_test4(); // slippy map tile using usgs30_dir/*.DEM files
    }
#endif // #ifdef INCLUDE_GLOBE_DATA

    // MISC
    // =====================================================================
    if (flag & tf_misc_test1) {
        cnt++;
        iret |= do_tile_test();
    }
    if (flag & tf_misc_test2) {
        cnt++;
        do_dir_tests(); // just to check the services work - create dir WILL fail if there already exist a file of that name
    }
    if (flag & tf_misc_test3) {
        cnt++;
        do_color_test();
    }
    if (flag & tf_misc_test4) {
        cnt++;
        iret |= test_hgt_file_name();
    }
    if (flag & tf_misc_test5) {
        cnt++;
        iret |= do_mappy_tile_test();
    }
    if (flag & tf_misc_test6) {
        cnt++;
        iret |= get_elev_tests();
    }
    if (cnt) {
        SPRTF("%s: With bit flag %u, 0x%08X, did %d tests in %s\n", module,
            flag,flag,cnt,
            get_elapsed_stg(bgn));
    } else {
        SPRTF("%s: Warning: for value %u, 0x%08X, NO tests performed.\n", module,
            flag, flag );
    }
	return iret;
}

typedef struct tagTESTBITS {
    test_flags bit;
    const char *name;
    const char *info;
}TESTBITS, *PTESTBITS;

static TESTBITS testtable[] = {
        // 1 arc-secs
    { tf_hgt1_test1, "tf_hgt1_test1", "do_hgt1_test1(); Mont Blanc elev test, mult zoom imgs, html, plus 1024x1024" },
    { tf_hgt1_test2, "tf_hgt1_test2", "do_hgt1_test2(); Mont Blanc - scan of file - works well" },
    { tf_hgt1_test3, "tf_hgt1_test3", "do_hgt1_test3(); Mont Blanc, N45E006.hgt - write images zoom 15 - 10" },
    { tf_hgt1_test4, "tf_hgt1_test4", "do_hgt1_test4(); Mount Whitney, California 4421 m 36.5786,-118.2920" },
    // F:/data/dem1/srtm1/index.html - Region_definition.jpg
    { tf_hgt1_test5, "tf_hgt1_test5", "do_hgt1_test5(); Mount McKinley 6168m 63.07,-151.01 USA Region_definition.jpg" },
    { tf_hgt1_test6, "tf_hgt1_test6", "do_hgt1_test6(); Mount Whitney, CA 4421 m 36.5786,-118.2920  write all 9 images" },
    // for F:\\data\\dem1\\srtm1\\N36W119.hgt
    { tf_hgt1_test7, "tf_hgt1_test7", "do_hgt1_test7(hgt); whole file to image, like N36W119.hgt" },
    // ====================================================================
    // 3 arc-secs
    { tf_hgt3_test1, "tf_hgt3_test1", "do_hgt3_test1(); write ferr3 N31E035.hgt to image, and report on voids" },
    { tf_hgt3_test2, "tf_hgt3_test2", "do_hgt3_test2(); Mont Blanc N45E006.hgt full set elevtests" },
    { tf_hgt3_test3, "tf_hgt3_test3", "do_hgt3_test3(); Mt Lamlam, Guam, elev, N13E144 to img, Tristan, elev, S38W013 to img" },
    { tf_hgt3_test4, "tf_hgt3_test4", "do_hgt3_test4(); Mt Everest N27E086.hgt write 256x256 image of BBOX" },
    { tf_hgt3_test5, "tf_hgt3_test5", "do_hgt3_test5(); KSFO z=10 get elev, show 9 boxes, write 10_163_396 img use gen.color" },
    { tf_hgt3_test6, "tf_hgt3_test6", "do_hgt3_test6(); Mont Blanc img at various zooms" },
    { tf_hgt3_test7, "tf_hgt3_test7", "do_hgt3_test7(); elevation tests Ferranti ferr3_dir, and USGS usgs3_dir" },
    { tf_hgt3_test8, "tf_hgt3_test8", "do_hgt3_test8(); elevation, for various zooms" },
    // =====================================================================
    // 30 arc-secs
    //tf_sr30_test1 = 0x00002000, // none
    { tf_sr30_test2, "tf_sr30_test2", "do_srtm30_test2();  Mt Everest file e060n40 write 256x256 img bbox at zooms" },
    { tf_sr30_test3, "tf_sr30_test3", "do_srtm30_test3(); Ferranti, ferr30_dir, bathymetry 30-arcsec files" },
    { tf_sr30_test4, "tf_sr30_test4", "do_netcdf_test();" },
    // =====================================================================
    // single large whole world 30 arc-sec file - SRTM land, bathemetric oceans
    // ************************************************************************
    { tf_st30_test1, "tf_st30_test1", "do_stopo30_test1(); try limits, elevtests and group on single 2 GB file" },
    { tf_st30_test2, "tf_st30_test2", "do_stopo30_test2(); build a 1024x512 image of whole file" },
    { tf_st30_test3, "tf_st30_test3", "do_stopo30_test3(); write Mt Everest slippy maps for zooms 4-19" },
    //tf_st30_test4 = 0x00100000, // none 
    { tf_st30_test5, "tf_st30_test5", "do_stopo30_test5(); write slippy maps for elevtests locs, zooms 0 - 14" },

#ifdef INCLUDE_GLOBE_DATA
    // ***********************************************************************
    // original OLD GLOBE 30 arc-sec set - 16 files - full of voids (-500)
    // ********************************************************************
    // seems so BAD, maybe NOT WORTH THE EFFORT!!!
    // gtopo30: NO DATA cnt 46,155,637, too high cnt 0, on 64,800,000 points.
    // =====================================================================
    { tf_glob_test0, "tf_glob_test0", "do_globe_test(); scan h10g get min,max... then run elevtests" },
    { tf_glob_test1, "tf_glob_test1", "do_globe_test1()" },
    { tf_glob_test2, "tf_glob_test2", "do_globe_test2()" },
    { tf_glob_test3, "tf_glob_test3", "do_globe_test3()" },
#else
    { tf_usgs_test1, "tf_usgs_test1", "do_usgs30_test1(); Mount Everest, and run elevtests set" },
    { tf_usgs_test2, "tf_usgs_test2", "do_usgs30_test2(); scan all usgs30_dir/*.DEM files for max/min" },
    { tf_usgs_test3, "tf_usgs_test3", "do_usgs30_test3(); write all usgs30_dir/*.DEM files to png/bmp images" },
    { tf_usgs_test4, "tf_usds_test4", "do_usgs30_test4(); slippy map tile using usgs30_dir/*.DEM files" },
#endif // #ifdef INCLUDE_GLOBE_DATA

    // MISC
    // =====================================================================
    { tf_misc_test1, "tf_misc_test1", "do_tile_test()" },
    { tf_misc_test2, "tf_misc_test2", "do_dir_tests() check the services works" },
    { tf_misc_test3, "tf_misc_test3", "do_color_test()" },
    { tf_misc_test4, "tf_misc_test4", "test_hgt_file_name()" },
    { tf_misc_test5, "tf_misc_test5", "do_mappy_tile_test()" },
    { tf_misc_test6, "tf_misc_test6", "get_elev_tests()" },
    // UGH - out of bits, but there are some, and could maybe remove GLOBE
    //iret |= do_dist_test();
    // TABLE TERMINATOR
    { tf_none, 0, 0 }
};

bool get_bit_from_name( char *sarg, unsigned int *pui)
{
    PTESTBITS ptb = testtable;
    while (ptb->name) {
        if (strcmp(sarg,ptb->name) == 0) {
            *pui = ptb->bit;
            return true;
        }
        ptb++;
    }
    return false;
}

void show_test_bits()
{
    PTESTBITS ptb = testtable;
    unsigned int tf = -1;
    SPRTF("Bit        Hex        -  Name          - Info\n");
    while (ptb->name) {
        SPRTF( "%10u 0x%08X - %14s - %s\n",
            ptb->bit, ptb->bit, ptb->name, ptb->info);
        tf &= ~ptb->bit;
        ptb++;
    }
    if (tf) {
        SPRTF("Unused ");
        unsigned int val = 1;
        int cnt = 32;
        while (tf && cnt) {
            if (tf & val) {
                SPRTF("%u 0x%08X ", val, val );
                tf &= ~val;
            }
            val = val << 1;
            cnt--;
        }
        SPRTF("\n");
    }
}

// all_tests.cxx
