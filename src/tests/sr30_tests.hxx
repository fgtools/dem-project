// sr30_tests.hxx
#ifndef _SR30_TESTS_HXX_
#define _SR30_TESTS_HXX_

extern int do_srtm30_test();
extern int do_srtm30_test2(); // Mt Everest file e060n40 write 256x256 img bbox at zooms
// run the full set of elevation tests - PELEVTESTS pet = elevtests;
// Using the de Ferranti ferr_dir, bathymetry 30-arcsec files
extern int do_srtm30_test3();

#endif // #ifndef _SR30_TESTS_HXX_
// eof - sr30_tests.hxx

