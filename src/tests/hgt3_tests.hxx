// hgt3_tests.hxx
#ifndef _HGT3_TESTS_HXX_
#define _HGT3_TESTS_HXX_
#include <string>

extern int do_hgt3_test1(); // write ferr3 N31E035.hgt to image, and report on voids

// run the full set of elevation tests - PELEVTESTS pet = elevtests;
// Using both the de Ferranti hgt_dir, and the previous USGS usgs_dir
extern int do_hgt3_test7(); // elevtests; de Ferranti hgt_dir, and USGS usgs_dir
extern int do_hgt3_test8(); // elevtests, for various zooms

extern int do_hgt3_test3(); // Mt Lamlam, Guam, elev, N13E144 to img, Tristan, elev, S38W013 to img 
extern int do_hgt3_test4(); // Mt Everest N27E086.hgt write 256x256 image of BBOX
extern int hgt3_write_image( int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1, const char *img_file );
extern int do_hgt3_test5(); // KSFO z=10 get elev, show 9 boxes, write 10_163_396 img use gen.color
extern int do_hgt3_test6(); // Mont Blanc at various zooms
// { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
extern int do_hgt3_test2();

#endif // #ifndef _HGT3_TESTS_HXX_
// eof - hgt3_tests.hxx

