// sr30_tests.cxx
/*\
 * ===========================================================================================
    BUT, this set downloaded from : http://topex.ucsd.edu/WWW_html/srtm30_plus.html 
    (Scripps Institute of Oceanography, University of California, San Diego, USA) - 33 files
    SO, they contain not only the land elevations, but also the bathynetry
    NEW!    FTP SRTM30_PLUS, entire grid  V9.0                December 19, 2013
    which offers a SINGLE large file - topo30 - see stopo30.hxx/cxx for that - 
    AND DWN: D:\SRTM\scripps\srtm30\data, 33 files of the form -
        57,600,000 e020n40.Bathymetry.srtm
        51,840,000 w060s60.Bathymetry.srtm
    This module is to load, view and understand these files...
    ALSO downloaded to F:\data\srtm30_plus - 33 files

 * =============================================================================================
\*/
#include <stdio.h>
#include <sstream>
#include <string>
#ifndef _MSC_VER
#include <stdlib.h> // for atoi(), ...
#include <string.h> // for strcmp(), ...
#endif
#ifdef USE_SIMGEAR_LIB
#include <simgear/misc/strutils.hxx>
#endif // USE_SIMGEAR_LIB
#include "sprtf.hxx"
#include "color.hxx"
#include "utils.hxx"
#include "dem_tests.hxx"
#include "dir_utils.hxx"
#include "srtm30.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "slippystuff.hxx"
#include "demFile.hxx"
#include "sr30_tests.hxx"

static const char *module = "sr30_tests";

// avoid, if poss
// using namespace std;
#ifdef USE_SIMGEAR_LIB
using namespace simgear;
#endif // USE_SIMGEAR_LIB

// get a slippy tile test
// expect /zoom/x/y.png 
    // expect /zoom/x/y.png 
    bool isSlippyPath( std::string path, int *px, int *py, int *pz )
    {
        bool bret = false;
        std::string::size_type pos = path.find('.');
        if ((pos == std::string::npos)||(pos < 5)) {
            return false;
        }
        std::string dirs = path.substr(1,pos-1);
        vSTG argv = string_split(dirs, "/");
        if (argv.size() == 3) {
            *pz = atoi(argv[0].c_str());
            *px = atoi(argv[1].c_str());
            *py = atoi(argv[2].c_str());
            bret = true;
        }
        return bret;
    }

    bool sendSlippyTile( int x, int y, int z )
    {
        bool bret = false;
        int iverb = 3+4+8;  // verbose + write BMP + write PPM
        char *cp = 0;
        size_t len = 0;
        if (get_slippy_tile(x,y,z,&cp,iverb) && cp && (is_file_or_directory(cp) == 1)) {
            len = get_last_file_size();
            char *buf = new char[len];
            if (buf) {
                FILE *fp = fopen(cp,"rb");
                if (fp) {
                    size_t res = fread(buf,1,len,fp);
                    if (res == len) {
                        //std::vector<unsigned char> vect;
                        std::stringstream d;
                        std::string c(buf,len);
                        //set_header( d, 200, ct_imgpng, c.size() );
                        d << c; // hope this works with binary data
                        SPRTF("%s: returning length %d\n", module, (int)d.str().size());
                        //push(d.str().c_str());
                        //closeAfterSending();
                        bret = true;
                    }
                    fclose(fp);
                }
                delete buf;
            }
        }
        return bret;
    }



// Mt Everest file - e060n40 - load and convert 1 file to a 256x256 image
int do_strm30_test()
{
    int iret = 0;
    int x,y,rows, cols, spanx, spany;
    double dspanx, dspany;
    int bgn_x,bgn_y;
    bool ok;
    short elev;
    int width = 256;
    int height = 256;
    unsigned int colr;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize+1];
    size_t doff;
    unsigned char r,g,b;
    SPRTF("\n");
    SPRTF("%s: === do_srtm30_test() =====================\n", module);

    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }
    srtm30 s;
    std::string f(ferr30_dir);
    f += PATH_SEP;
    f += "e060n40.Bathymetry.srtm";
    if (is_file_or_directory((char *)f.c_str()) != 1) {
        SPRTF("%s: Unable to 'stat' file %s\n", module, f.c_str());
        check_me();
        return 1;
    }
    s.verb = false;
    if (!s.begin_span_file(f)) {
        SPRTF("%s: Unable to 'strm30:begin_span_file' with file %s\n", module, f.c_str());
        check_me();
        return 1;
    }
    rows = s.rows;
    cols = s.cols;
    spany = rows / height;
    if (rows % height) 
        spany++;
    spanx = cols / width;
    if (cols % width) 
        spanx++;
    dspany = (double)rows / (double)height;
    dspanx = (double)cols / (double)width;
    SPRTF("%s: Processing file %s, x,y %d,%d, %d bytes, span %d,%d, incs %s,%s\n", module,
        f.c_str(),
        cols, rows,
        (int) s.file_size,
        spanx, spany,
        get_trim_double(dspanx),
        get_trim_double(dspany) );
    ok = true;
    s.verb = true;  // to see any error
    clear_used_colors();

    double bgn = get_seconds();
    for (y = 0; y < height; y++) {
        bgn_y = (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = (int)((double)x * dspanx);
            ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
            if (!ok) {
                SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                check_me();
                return 1;
            }
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    char *out_png = get_out_png();
    if (out_png) {
        if (writePNGImage24(out_png, width, height, bit_depth, color_type, buffer ))
            iret |= 8;
    }
#else // !USE_PNG_LIB
    char *out_bmp = get_out_bmp();
    if (out_bmp) {
        if (writeBMPImage24(out_bmp, width, height, buffer, bsize))
            iret |= 4;
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed %s bytes to %dx%d image in %s\n", module,
        get_NiceNumberStg(s.file_size),
        width, height, secs );

    delete buffer;
    out_used();
    return iret;
}

int srtm30_write_image( srtm30 &s, int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1, const char *img_file = 0 );


// Mt Everest file - e060n40 - write 256x256 image of bbox at each zoom
int do_srtm30_test2() // Mt Everest file e060n40 write 256x256 img bbox at zooms
{
    int iret = 0;
    // Mt Everest - 8,848 meters - 27.988, 86.9253 = N27E086
    double lat = 27.988;
    double lon = 86.9253;
    //int zoom = 5; // span 1350,-4917 points - SPANS different files - TODO:
    int zoom = 6; // span 675,557 points
    //int zoom = 7; // span 338,290 points
    //int zoom = 8; // span 168,148 points
    //int zoom = 9; // span 85,74 points
    int width = 256;
    int height = 256;
    const char *dir = ferr30_dir;
    short elev;
    char *cp;

    BNDBOX bb;
    PBNDBOX pbb = &bb;
    SPRTF("\n");
    SPRTF("%s: === do_srtm30_test2() =====================\n", module);

    if (is_file_or_directory((char *)dir) != 2) {
        SPRTF("%s: Directory '%s' NOT valid!\n", module, dir);
        return 1;
    }

    srtm30 s;
    // s.verb = false; // set to run silent
    if (!s.set_file_dir(dir)) {
        s.verb = true; // set verb
        s.set_file_dir(dir);
        return 1;
    }
    
    // get Mt Everest, just to CHECK
    if (!s.get_elevation( lat, lon, &elev )) {
        SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return 1;
    }
    SPRTF("%s: for lat,lon %s,%s srtm30 reported elevation %d, expected 8848 meters.\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev);

    for (zoom = 4; zoom < 20; zoom++) {
        SPRTF("\n");
        if ( !get_slippy_path( lat, lon, zoom, &cp ) ) {
            SPRTF("%s: WHAT! failed to get slippy map path for lat,lon,zoom %s,%s,%d!\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                zoom );

        }
        if (!get_map_bounding_box( lat, lon, zoom, pbb )) {
            SPRTF("%s: Failed to get bounding box for lat,lon,zoom %s,%s,%d!\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                zoom );
            return 1;
        }

        SPRTF("%s: For lat,lon,zoom %s,%s,%d %s\n", module,
             get_trim_double(lat),
             get_trim_double(lon),
             zoom,
             get_bounding_box_stg(pbb,true));

        // sanity check only
        if (( lat > pbb->bb_max_lat )||
            ( lat < pbb->bb_min_lat )||
            ( lon > pbb->bb_max_lon )||
            ( lon < pbb->bb_min_lon )) {
            SPRTF("%s: ZUTE: Given lat,lon NOT in BBOX!\n", module);
            SPRTF("%s: ",module);
            if (lat > pbb->bb_max_lat) 
                SPRTF("lat %s GTT max lat %s ", get_trim_double(lat), get_trim_double(pbb->bb_max_lat));
            if ( lat < pbb->bb_min_lat )
                SPRTF("lat %s LSS min lat %s ", get_trim_double(lat), get_trim_double(pbb->bb_min_lat));
            if ( lon > pbb->bb_max_lon )
                SPRTF("lon %s GTT max lon %s ", get_trim_double(lon), get_trim_double(pbb->bb_max_lon));
            if ( lon < pbb->bb_min_lon )
                SPRTF("lon %s LSS min lon %s ", get_trim_double(lon), get_trim_double(pbb->bb_min_lon));
            SPRTF("\n");
            check_me();
            return 1;
        }

        int x_bgn, y_bgn, x_end, y_end;
        std::string file1, file2;
        TILELIMITS tl1,tl2;
        // now get upper left elevation
        if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
            SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
                get_trim_double(pbb->bb_max_lat),
                get_trim_double(pbb->bb_min_lon) );
            return 1;
        }
        file1 = s.file;
        x_bgn = s.x_col;
        y_bgn = s.y_row;
        tl1 = s.tl;
        SPRTF("%s: for lat,lon %s,%s srtm30 reported elevation %d,\n from file %s at x,y %d,%d\n", module,
                get_trim_double(pbb->bb_max_lat),
                get_trim_double(pbb->bb_min_lon),
                elev,
                file1.c_str(),
                x_bgn, y_bgn);
        // now get lower right elevation
        if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
            SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
                get_trim_double(pbb->bb_min_lat),
                get_trim_double(pbb->bb_max_lon) );
            return 1;
        }
        file2 = s.file;
        x_end = s.x_col;
        y_end = s.y_row;
        tl2 = s.tl;
        SPRTF("%s: for lat,lon %s,%s srtm30 reported elevation %d,\n from file %s at x,y %d,%d\n", module,
                get_trim_double(pbb->bb_min_lat),
                get_trim_double(pbb->bb_max_lon),
                elev,
                file2.c_str(),
                x_end, y_end);

        int xcols = x_end - x_bgn;
        int yrows = y_end - y_bgn;
        SPRTF("%s: Z=%d %s covers x,y %d,%d to %d,%d, span %d,%d points\n", module,
            zoom,
            get_bounding_box_stg(pbb),
            x_bgn,y_bgn,x_end,y_end,
            xcols, yrows );

        // get image file name
        char *tmp = get_cache_file_name(cp,true);
        std::string img = get_image_out_dir();
        img += PATH_SEP;
        img += "srtm30";
        if (is_file_or_directory((char *)img.c_str()) != 2) {
            SPRTF("%s: Out path %s DOES NOT EXIST! *** FIX ME ***\n", module, img.c_str());
            check_me();
            return 1;
        }
        img += PATH_SEP;
        img += tmp;
#ifdef USE_PNG_LIB
        img += ".png";
#else // !USE_PNG_LIB
        img += ".bmp";
#endif // USE_PNG_LIB y/n

        if (file1 != file2) {
            // different files
            BNDBOX bb1, bb2;
            // we have the tile limits of 1 and 2, TL and BR
            // and the BBOX we want to render
            bb1 = bb;
            if (bb1.bb_min_lat < tl1.min_lat)
                bb1.bb_min_lat = tl1.min_lat;
            if (bb1.bb_max_lat > tl1.max_lat)
                bb1.bb_max_lat = tl1.max_lat;
            if (bb1.bb_min_lon < tl1.min_lon)
                bb1.bb_min_lon = tl1.min_lon;
            if (bb1.bb_max_lon > tl1.max_lon)
                bb1.bb_max_lon = tl1.max_lon;

            bb2 = bb;
            if (bb2.bb_min_lat < tl2.min_lat)
                bb2.bb_min_lat = tl2.min_lat;
            if (bb2.bb_max_lat > tl2.max_lat)
                bb2.bb_max_lat = tl2.max_lat;
            if (bb2.bb_min_lon < tl2.min_lon)
                bb2.bb_min_lon = tl2.min_lon;
            if (bb2.bb_max_lon > tl2.max_lon)
                bb2.bb_max_lon = tl2.max_lon;

            SPRTF("%s: File 1 covers lat,lon min %s,%s max %s,%s, %s\n", module,
                get_trim_double(tl1.min_lat),
                get_trim_double(tl1.min_lon),
                get_trim_double(tl1.max_lat),
                get_trim_double(tl1.max_lon),
                get_bounding_box_stg(&bb1,true));
            SPRTF("%s: File 2 covers lat,lon min %s,%s max %s,%s, %s\n", module,
                get_trim_double(tl2.min_lat),
                get_trim_double(tl2.min_lon),
                get_trim_double(tl2.max_lat),
                get_trim_double(tl2.max_lon),
                get_bounding_box_stg(&bb2,true));
            SPRTF("%s: NOT handling %s, in different files - %s %s\n", module,
                get_bounding_box_stg(pbb,true),
                get_file_name(file1).c_str(),
                get_file_name(file2).c_str());
        } else {
            if ( (xcols >= MIN_SPAN) && (yrows >= MIN_SPAN) ) {
                iret |= srtm30_write_image( s, width, height, yrows, xcols, y_bgn, x_bgn, file1, img.c_str() );
            } else {
                SPRTF("%s: At this ZOOM %d, on DEM30 data, the BBOX is LESS THAN MIN_SPAN %dx%d (ie %d,%d) points!\n", module, zoom, MIN_SPAN, MIN_SPAN,
                    xcols, yrows);
            }
        }
    }
    return iret;
}

int srtm30_write_image( srtm30 &s, int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1, const char *img_file  )
{
    int iret = 0;
    int x,y;
    short elev;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    size_t doff;
    unsigned char r,g,b;
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    unsigned int colr;

    bool ok = s.begin_span_file(file1);
    if (!ok) {
        SPRTF("%s: Failed to begin_span_file(%s)!\n", module, file1.c_str() );
        return 1;
    }

    s.verb = true;  // to see any error
    clear_used_colors();
    double bgn = get_seconds();
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
            if (!ok) {
                SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                check_me();
                return 1;
            }
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    char *out_png;
    if (img_file)
        out_png = (char *)img_file;
    else
        out_png = get_out_png();
    if (out_png) {
        if (writePNGImage24(out_png, width, height, bit_depth, color_type, buffer )) {
            iret |= 8;
        }
    }
#else // !USE_PNG_LIB
    char *out_bmp;
    if (img_file)
        out_bmp = (char *)img_file;
    else
        out_bmp = get_out_bmp();
    if (out_bmp) {
        if (writeBMPImage24(out_bmp, width, height, buffer, bsize)) {
            iret |= 4;
        }
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        xcols, yrows,
        get_NiceNumberStg(xcols * yrows),
        width, height, secs );
    delete buffer;
    out_used();
    return iret;

}

////////////////////////////////////////////////////////////////////////////
// run the full set of elevation tests - PELEVTESTS pet = elevtests;
// Using the de Ferranti ferr_dir, bathymetry 30-arcsec files
// ///////////////////////////////////////////////////////////////////////
int do_srtm30_test3_OLD()
{
    int iret = 0;
    vSTG files;
    SPRTF("\n");
    SPRTF("%s: === do_srtm30_test3() =====================\n", module);

    PELEVTESTS pet = elevtests;
    double lat,lon;
    short elev, diff;
    int count = 0;
    while (pet->name) {
        count++;
        pet++;
    }
    SPRTF("\n");
    SPRTF("%s: %d 'test' elevations using [%s].\n", module,
        count,
        ferr30_dir );

    srtm30 s;
    s.verb = false; // set to run silent
    if (!s.set_file_dir(ferr30_dir)) {
        s.verb = true;
        s.set_file_dir(ferr30_dir);
        return 1;
    }
    // s.verb = true; // show errors
    double bgn = get_seconds();
    pet = elevtests; // reset pointer
    while (pet->name) {
        lat = pet->lat;
        lon = pet->lon;
        if (s.verb)
            SPRTF("\n");
        if (s.get_elevation( lat, lon, &elev)) {
            diff = (elev < pet->expected) ? pet->expected - elev : elev - pet->expected;
            SPRTF("%s: For lat,lon %s,%s, %s, got elev %d, expected %d. (d=%d)\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                elev,
                pet->expected,
                diff);
            if (!string_in_vec( files, s.file )) {
                files.push_back(get_file_name(s.file));
            }

        } else {
            SPRTF("%s: For lat,lon %s,%s, %s, FAILED, expected %d.\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                pet->expected);
        }
        pet++;
    }
    size_t ii, max = files.size();
    if (max) {
        int wrap = 0;
        if (s.verb)
            SPRTF("\n");

        SPRTF("%s: Used %d %s files in %s.\n    ", module,
            (int)max, 
            (((int)max < count) ? "plus" : ""),
            get_elapsed_stg(bgn));
        for (ii = 0; ii < max; ii++) {
            SPRTF("%s ", files[ii].c_str());
            wrap++;
            if (wrap == 4) {
                SPRTF("\n    ");
                wrap = 0;
            }
        }
        if (wrap)
            SPRTF("\n");
    }
    return iret;
}

// -t 32768 0x00008000 - tf_sr30_test3 - Using de Ferranti ferr_dir, bathymetry 30-arcsec files do_srtm30_test3()
// get a slippy tile test
// tile /zoom/x/y.png 
int do_srtm30_test3()
{
    int iret = 0;
    SPRTF("\n");
    SPRTF("%s: === do_srtm30_test3() =====================\n", module);
    char *cp;
    double lat = 27.988;
    double lon = 86.9253;
    int x,y,z;
    double bgn;
    double tlon, tlat;
    BNDBOX bb;
    //std::string sm = "/10/759/429.png";  //     { "Mount Everest",  27.988, 86.9253, 8848   },
    SPRTF("%s: The tile is for Mount Everest,  27.988, 86.9253, at 8848 m\n", module);
#if 0 // ========================================
    z = 10;
    x = 759;
    y = 429;
    tlon = tilex2lon(x, z);
    tlat = tiley2lat(y, z);
    if (!get_map_bounding_box( tlat, tlon, z, &bb )) {
        SPRTF("%s: failed BBOX for lat,lon,zoom %s,%s,%d\n", module,
            get_trim_double(tlat),
            get_trim_double(tlon),
            z );
        return 1;
    }
    cp = GetNxtBuf();
    sprintf(cp,"/%d/%d/%d.png", z, x, y);
    SPRTF("\n");
    SPRTF("%s: Getting tile [%s] %s\n", module, cp,
       get_bounding_box_stg(&bb,true));
    bgn = get_seconds();
    if ( sendSlippyTile( x, y, z ) ) {
        SPRTF("%s: Got tile in %s\n", module,
            get_elapsed_stg(bgn) );
    } else {
        iret |= 1;
        SPRTF("%s: FAILED on tile in %s\n", module, 
            get_elapsed_stg(bgn));
    }
    // For lat,lon,zoom 27.988,86.9253,8, dir is [/8/189/107.png]
    z = 8;
    x = 189;
    y = 107;
    tlon = tilex2lon(x, z);
    tlat = tiley2lat(y, z);
    if (!get_map_bounding_box( tlat, tlon, z, &bb )) {
        SPRTF("%s: failed BBOX for lat,lon,zoom %s,%s,%d\n", module,
            get_trim_double(tlat),
            get_trim_double(tlon),
            z );
        return 1;
    }
    cp = GetNxtBuf();
    sprintf(cp,"/%d/%d/%d.png", z, x, y);
    SPRTF("\n");
    SPRTF("%s: Getting tile [%s] %s\n", module, cp,
       get_bounding_box_stg(&bb,true));
    bgn = get_seconds();
    if ( sendSlippyTile( x, y, z ) ) {
        SPRTF("%s: Got tile in %s\n", module,
            get_elapsed_stg(bgn) );
    } else {
        iret |= 1;
        SPRTF("%s: FAILED on tile in %s\n", module, 
            get_elapsed_stg(bgn));
    }
#endif // if 0 // ========================================
    // For lat,lon,zoom 27.988,86.9253,5, dir is [/5/23/13.png]
    z = 5;
    x = 23;
    y = 13;
    tlon = tilex2lon(x, z);
    tlat = tiley2lat(y, z);
    if (!get_map_bounding_box( tlat, tlon, z, &bb )) {
        SPRTF("%s: failed BBOX for lat,lon,zoom %s,%s,%d\n", module,
            get_trim_double(tlat),
            get_trim_double(tlon),
            z );
        return 1;
    }
    cp = GetNxtBuf();
    sprintf(cp,"/%d/%d/%d.png", z, x, y);
    SPRTF("\n");
    SPRTF("%s: Getting tile [%s] %s\n", module, cp,
       get_bounding_box_stg(&bb,true));
    bgn = get_seconds();
    if ( sendSlippyTile( x, y, z ) ) {
        SPRTF("%s: Got tile in %s\n", module,
            get_elapsed_stg(bgn) );
    } else {
        iret |= 1;
        SPRTF("%s: FAILED on tile in %s\n", module, 
            get_elapsed_stg(bgn));
    }
    // move bounding box
    for (int i = nbb_right; i < nbb_max; i++) {
        BNDBOX bb2 = bb;
        if (get_next_map_bbox( &bb2, (bb_dir)i, true )) {
            tlat = (bb2.bb_max_lat + bb2.bb_min_lat) / 2.0;
            tlon = (bb2.bb_max_lon + bb2.bb_min_lon) / 2.0;
            x = lon2tilex(tlon,z);
            y = lat2tiley(tlat,z);
            cp = GetNxtBuf();
            sprintf(cp,"/%d/%d/%d.png", z, x, y);
            SPRTF("\n");
            SPRTF("%s: Getting tile [%s] %s\n", module, cp,
               get_bounding_box_stg(&bb2,true));
            bgn = get_seconds();
            if ( sendSlippyTile( x, y, z ) ) {
                SPRTF("%s: Got tile in %s\n", module,
                    get_elapsed_stg(bgn) );
            } else {
                iret |= 1;
                SPRTF("%s: FAILED on tile in %s\n", module, 
                    get_elapsed_stg(bgn));
            }
        } else {
            SPRTF("%s: FAILED to get next BBOX!\n", module);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // For lat,lon,zoom 27.988,86.9253,3, dir is [/3/5/3.png] = NEEDS TWO FILES - TODO: NOT YET HANDLED
    // For lat,lon,zoom 27.988,86.9253,4, dir is [/4/11/6.png] = NEEDS TWO FILES - TODO: NOT YET HANDLED 
    // demFile: Got 2 files - e060n40.Bathymetry.srtm and e060n90.Bathymetry.srtm - NOT presently handled!
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // path /4/27/17.png failed??? BECAUSE THE x OR y IS OUT OF RANGE FOR ZOOM
    z = 4;
    x = 11;
    y = 6;
    cp = GetNxtBuf();
    //sprintf(cp,"/%d/%d/%d.png", z, x, y);
    strcpy(cp,"/4/27/17.png");
    SPRTF("\n");
    SPRTF("%s: Testing tile [%s]\n", module, cp);
    if (isSlippyPath(cp,&x,&y,&z)) {
        if (xy_in_range_for_zoom(x,y,z,true)) {
            SPRTF("%s: Getting tile [%s], x,y,z\n", module, cp, x, y, z);
            bgn = get_seconds();
            if ( sendSlippyTile( x, y, z ) ) {
                SPRTF("%s: Got tile in %s\n", module,
                    get_elapsed_stg(bgn) );
            } else {
                iret |= 1;
                SPRTF("%s: FAILED on tile in %s\n", module, 
                    get_elapsed_stg(bgn));
            }
        }
    } else {
        SPRTF("%s: HUH! [%s] not valid path\n", module, cp);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    return iret;

    for (z = 0; z < 20; z++) {
        x = lon2tilex(lon,z);
        y = lat2tiley(lat,z);
        cp = GetNxtBuf();
        sprintf(cp,"/%d/%d/%d.png", z, x, y);
        SPRTF("\n");
        SPRTF("%s: Getting tile [%s]\n", module, cp);
        bgn = get_seconds();
        int ix,iy,iz;
        if ( isSlippyPath( cp, &ix, &iy, &iz )) {
            SPRTF("%s: IsSlippyPath x,y,z %d,%d,%d (%d,%d,%d)\n", module, ix, iy, iz, x, y, z);
            if ( sendSlippyTile( x, y, z ) ) {
                SPRTF("%s: Got tile in %s\n", module,
                    get_elapsed_stg(bgn) );
            } else {
                iret |= 1;
                SPRTF("%s: FAILED on tile in %s\n", module, 
                    get_elapsed_stg(bgn));
            }
        } else {
            iret |= 1;
            SPRTF("%s: FAILED on tile in %s\n", module, 
                get_elapsed_stg(bgn));
        }
    }
    // 
    return iret;
}


// eof - sr30_tests.cxx
