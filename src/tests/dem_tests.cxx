/*\
    dem_tests.cxx

\*/


#include <stdio.h>
#ifndef _MSC_VER
#include <stdlib.h> // for stroul(),...
#endif
#include <string>
#include "sprtf.hxx"
#include "utils.hxx"
#include "cache_utils.hxx"
#include "all_tests.hxx"
#include "dem_utils.hxx"
#include "dir_utils.hxx"
#include "dem_tests.hxx"

static const char *module = "dem_tests";
#ifndef DEM_TEST_VERS
#define DEM_TEST_VERS "0.0.2"
// #define DEM_TEST_VERS "0.0.1"
#endif


void give_help( char *name )
{
    std::string exe = get_file_name(name);
    SPRTF("%s version " DEM_TEST_VERS "\n", exe.c_str());
    SPRTF("%s: Usage: options\n", module);
    SPRTF("Options:\n");
    SPRTF(" --help   (-h or -?)  = This help, and exit(2)\n");
    SPRTF(" --test <flag>  (-t) = Run a test, or tests per the bit flag.\n");
    SPRTF(" --show         (-s) = Show the test list, and their bit flag, and exit(2).\n");
    SPRTF(" --Test <name>  (-T) = Run a test per the name as shown by the -s test list.\n");
    dem_path_help();
    show_cache_stats();
}

int parse_args(int argc, char **argv)
{
    int i, i2, c, iret = 0;
    char *arg, *sarg;
    unsigned int ui;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 's':
                show_test_bits();
                return 2;
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    //long long ll = atoll(argv[i]);
                    if (is_hex_value(sarg)) {
                        ui = UlongFromHexString( &sarg[2] );
                    } else {
#ifdef _MSC_VER
                        ui = std::strtoul(sarg,0,10);
#else
                        ui = strtoul(argv[i],0,10);
#endif
                    }
                    //return do_tests( (test_flags)atoi(argv[i]) );
                    iret = do_tests( (test_flags)ui );
                    if (iret)
                        return iret;
                } else {
                    SPRTF("%s: Error: Expected test flag value to follow!\n", module );
                    goto Bad_Arg;
                }
                break;
            case 'T':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    if (get_bit_from_name(sarg, &ui)) {
                        iret = do_tests( (test_flags)ui );
                        if (iret)
                            return iret;
                    } else {
                        SPRTF("%s: Error: Unable to find name '%s' in table!\n", module, sarg );
                        return 1;
                    }
                } else {
                    SPRTF("%s: Error: Expected test 'name' to follow!\n", module );
                    SPRTF("Use -s to view current test names.\n");
                    goto Bad_Arg;
                }
                break;
            case 'd':
                if (i2 < argc) {
                    i++;
                    iret = set_new_directory(argv[i]);
                    if (iret)
                        return iret;
                } else {
                    SPRTF("%s: Error: Expected name:directory to follow!\n", module );
                    goto Bad_Arg;
                }
                break;
            default:
                goto Bad_Arg;
                break;
            }

        } else {
Bad_Arg:
           SPRTF("%s: Unknown command [%s]! Try -? for help\n", module, arg);
           return 1;
        }
    }
    return iret;
}


int main( int argc, char **argv )
{
    int iret = 0;

    set_log_file((char *)"temptest.log", false);

    set_endian();

    iret = parse_args( argc, argv );

    return iret;

}

// eof - dem_tests.cxx
