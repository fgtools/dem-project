/*\
 * hgt1-dump.cxx
 *
 * Copyright (c) 2015 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <stdlib.h> // for atof(), ...
#include <string.h> // for strdup(), ...
#include "sprtf.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif // USE_PNG_LIB
#include "hgt1File.hxx"
// other includes
/*\ 
 *  Notes on BBOX bounding box conventions
 * KML = [West,South,East,North] = Left,Bottom,Right,Top = min_lon,min_lat,max_lon,max_lat = BL to TR
 * WMS = same min_x,min_y,max_x,max_y
 * GDAL = seems different ulx uly lrx, lry
 * Tiles
 * FG/SG/TG = origin in bottom left
 * Slippy = origin is upper, left
 * 
 * Here I will use KML/WMS for bbox, and FG/SG/TG for tiles
 * ========================================================
\*/

static const char *module = "hgt1-dump";

static const char *def_log = "temphgt1d.txt";

//static const char *test_sample = "F:\\data\\dem1\\srtm1\\N37W123.hgt";
static const char *test_sample = "D:\\SRTM\\theo\\N37W123.hgt";
//static const char *test_bbox = "-122.5079,37.4888,-122.491,37.5";
static const char *test_bbox = "-122.5,37.48,-122.491,37.5";

static const char *usr_input = 0;

#ifdef NDEBUG
static bool add_test = false;
#else
static bool add_test = true;
#endif
#ifdef USE_PNG_LIB
static bool write_png_image = false;
#endif // #ifdef USE_PNG_LIB

typedef struct tagSBBOX {   // simple bounding box in wgs84 lat,lon degrees
    double min_lon, min_lat, max_lon, max_lat;
}SBBOX, *PSBBOX;

typedef struct tagMPOS2 {
    int x,y;
    MPOS pos;
}MPOS2, *PMPOS2;

typedef std::vector<MPOS2> vMPOS2;
typedef vMPOS2::iterator iMPOS2;

static bool got_bbox = false;
static SBBOX sbbox;

bool ll_in_sbbox( double lat, double lon, PSBBOX pbb )
{
    if ( (lat <= pbb->max_lat) && (lat >= pbb->min_lat) &&
         (lon <= pbb->max_lon) && (lon >= pbb->min_lon) ) {
        return true;
    }
    return false;
}
bool valid_sbbox( PSBBOX pbb ) 
{
    if (in_world_range( pbb->max_lat, pbb->max_lon ) &&
        in_world_range( pbb->min_lat, pbb->min_lon ) &&
        ( pbb->min_lat < pbb->max_lat ) &&
        ( pbb->min_lon < pbb->max_lon ) ) {
        return true;
    }
    return false;
}
char *get_bounding_sbox_stg(PSBBOX pbb)
{
    char *cp = GetNxtBuf();
    sprintf(cp, "%s,%s,%s,%s", 
        get_trim_double(pbb->min_lon),
        get_trim_double(pbb->min_lat),
        get_trim_double(pbb->max_lon),
        get_trim_double(pbb->max_lat));
    return cp;
}

void give_help( char *name )
{
    printf("%s: usage: [options] usr_input\n", module);
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(2)\n");
    printf(" --bbox lon,lat,lon,lat (-b) = Define a bounding box in order min lon,lat max lon,lat\n");

    // TODO: More help
}

int parse_args( int argc, char **argv )
{
    int i,i2,c,cnt;
    char *arg, *sarg;
    size_t ii, len;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'b':
                if (i2 < argc) {
                    i++;
                    sarg = strdup(argv[i]); // expect 4 double, comma separated
                    len = strlen(sarg);
                    char *bgn = sarg;
                    cnt = 0;
                    for (ii = 0; ii < len; ii++) {
                        if (sarg[ii] == ',') {
                            sarg[ii] = 0;
                            switch (cnt) {
                            case 0:
                                sbbox.min_lon = atof(bgn);
                                break;
                            case 1:
                                sbbox.min_lat = atof(bgn);
                                break;
                            case 2:
                                sbbox.max_lon = atof(bgn);
                                break;
                            case 3:
                                sbbox.max_lat = atof(bgn);
                                break;
                            default:
                                printf("%s: Already have 4 coords! What is this %s\n", module, argv[i] );
                                return 1;
                            }
                            bgn = &sarg[ii+1];
                            cnt++;
                        }
                    }
                    if (cnt == 3) {
                        sbbox.max_lat = atof(bgn);
                        cnt++;
                    }
                    if ((cnt == 4) && valid_sbbox( &sbbox )) {
                        got_bbox = true;
                    } else {
                        printf("%s: Failed to get valid BBOX from %s\n", module, argv[i] );
                        return 1;
                    }
                } else {
                    printf("%s: Expected BBOX (4 doubles) to follow '%s'\n", module,
                        arg);
                    return 1;
                }
                break;
            // TODO: Other arguments
            default:
                printf("%s: Unknown argument '%s'. Tyr -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                printf("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(arg);
        }
    }

    if (add_test && !usr_input) {
        //usr_input = strdup("D:\\SRTM\\theo\\N37W123.hgt");
        usr_input = strdup(test_sample);
    }
    if (!usr_input) {
        printf("%s: No user input found in command!\n", module);
        return 1;
    }
    return 0;
}

int check_me2()
{
    int i;
    i = 10;
    return i;
}

const char *hm = "hmn37w123.png";
static BNDBOX bbox1, bbox2;
void process_data( hgt1File *p1 )
{
    int x, y;
    short *sp;
    short emin = 10000;
    short emax = -10000;
    //int maxx,maxy,minx,miny;
    //double maxxlon,maxylat,minxlon,minylat;
    int vcnt = 0;
    int okcnt = 0;
    short elev, elev2, elev3;
    //bool little_endian = !test_endian();
    int offset;
    unsigned char *pbuf = (unsigned char *)&p1->hgt_data[0][0];
    unsigned char *pb, *pb2;
    int minus1 = 0;
    int minus2 = 0;
    int minus3 = 0;
    int zero = 0;
    int plus1 = 0;
    int plus2 = 0;
    int plus3 = 0;
    int min_x,min_y,max_x,max_y;
    double min_dx,min_dy,max_dx,max_dy;
    double lat, lon;
    elev = HGT_VOID;
    size_t bsize = HGT1_SIZE_1 * HGT1_SIZE_1 * 3;
    unsigned char *png = new unsigned char[bsize+1];
    int doff, j = 0;
    short last_elev = 0;
    bool got_lat_lon = p1->got_lat_lon;
    double file_lat =  p1->file_lat;
    double file_lon =  p1->file_lon;
    double top_lat  = p1->file_lat + 1.0;
    PBNDBOX pbb = &bbox1;
    vMPOS2 *pvp = 0;
    iMPOS2 ip2;
    MPOS2 mp2, mp22;
    size_t cnt, ii, max;
    if (got_lat_lon) {
#if 0 // 00000000000000000000000000000000000000000000000000000
        // this calculation seems FAULTY!!!
        pbb = &bbox1;
        SPRTF("%s: lat,lon bounds ", module);
        x = 0;
        y = 0;
        lon = file_lon + ((double)x / (double)HGT1_SIZE_1); // of 1 degree NO
        lat = top_lat  - ((double)(y+1) / (double)HGT1_SIZE_1);
        SPRTF("TL(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        seed_bounding_box( lat, lon, pbb );
        x = HGT1_SIZE_1 - 1;
        lon = file_lon + ((double)x / (double)HGT1_SIZE_1); // of 1 degree NO
        lat = top_lat  - ((double)(y+1) / (double)HGT1_SIZE_1);
        SPRTF("TR(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        expand_bounding_box_by_ll( lat, lon, pbb );
        y = HGT1_SIZE_1 - 1;
        lon = file_lon + ((double)x / (double)HGT1_SIZE_1); // of 1 degree NO
        lat = top_lat  - ((double)(y+1) / (double)HGT1_SIZE_1);
        SPRTF("BR(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        expand_bounding_box_by_ll( lat, lon, pbb );
        x = 0;
        lon = file_lon + ((double)x / (double)HGT1_SIZE_1); // of 1 degree NO
        lat = top_lat  - ((double)(y+1) / (double)HGT1_SIZE_1);
        SPRTF("BL(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        expand_bounding_box_by_ll( lat, lon, pbb );
        SPRTF("\n");
#endif
        pbb = &bbox2;
        SPRTF("%s: lat,lon bounds ", module);
        x = 0;
        y = 0;
        lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
        lon = file_lon + (x * HGT1_XDIM);
        SPRTF("TL(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        seed_bounding_box( lat, lon, pbb );
        x = HGT1_SIZE_1 - 1;
        lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
        lon = file_lon + (x * HGT1_XDIM);
        SPRTF("TR(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        expand_bounding_box_by_ll( lat, lon, pbb );
        y = HGT1_SIZE_1 - 1;
        lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
        lon = file_lon + (x * HGT1_XDIM);
        SPRTF("BR(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        expand_bounding_box_by_ll( lat, lon, pbb );
        x = 0;
        lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
        lon = file_lon + (x * HGT1_XDIM);
        SPRTF("BL(%d,%d) %s,%s ", y, x, get_trim_double(lat), get_trim_double(lon));
        expand_bounding_box_by_ll( lat, lon, pbb );
        SPRTF("\n");
        //SPRTF("%s: bbox1 %s\n", module, get_bounding_box_stg( &bbox1 ));
        SPRTF("%s: bbox2 %s\n", module, get_bounding_box_stg( &bbox2 ));
    }
    if (got_bbox) {
        pvp = new vMPOS2;
    }
    // start top row to bottom
    for (y = 0; y < HGT1_SIZE_1; y++) {
        // sart left column to right
        for (x = 0; x < HGT1_SIZE_1; x++) {
            offset = ((y * HGT1_SIZE_1) + x) * 2;
            //if (offset >= 0x1280) {
            //    check_me2();
            //}
            lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
            lon = file_lon + (x * HGT1_XDIM);

            //sp = &hgt_data[x][y];
            sp = &p1->hgt_data[y][x];
            // buffer already swapped if needed
            //if (little_endian)
            //    SWAP16(sp); // is little endian - do byte swap now
            elev = *sp;
            if (got_bbox) {
                if (ll_in_sbbox( lat, lon, &sbbox )) {
                    cnt = 1;
                    for (ip2 = pvp->begin(); ip2 != pvp->end(); ip2++) {
                        mp2 = *ip2;
                        if (mp2.x == x) {
                            mp2.x = x;
                            mp2.y = y;
                            mp2.pos.elev = elev;
                            mp2.pos.lat = lat;
                            mp2.pos.lon = lon;
                            pvp->insert(ip2,mp2);
                            cnt = 0;
                            break;
                        }
                    }
                    if (cnt) {
                        mp2.x = x;
                        mp2.y = y;
                        mp2.pos.elev = elev;
                        mp2.pos.lat = lat;
                        mp2.pos.lon = lon;
                        pvp->push_back(mp2);
                    }
                }
            }

            pb = &pbuf[offset];
            elev2 = pb[0] + (256 * pb[1]);
            pb2 = (unsigned char *)&p1->hgt_data[y][x];
            elev3 = pb2[0] + (256 * pb2[1]);
            if ((elev != elev2) || (elev != elev3)) {
                SPRTF("Error extracting data %d %d %d\n", (int)elev, (int)elev2, (int)elev3);
            }
            if (elev < -11000) {
            //if (elev <= HGT_VOID) {
                vcnt++;
                elev = last_elev;
            } else {
                okcnt++;
                if (elev < emin) {
                    emin = elev;
                    min_x = x;
                    min_y = y;
                    min_dy = lat;
                    min_dx = lon;
                }
                if (elev > emax) {
                    emax = elev;
                    max_x = x;
                    max_y = y;
                    max_dy = lat;
                    max_dx = lon;
                }
                if (elev == 0) {
                    zero++;
                } else if (elev == 1) {
                    plus1++;
                } else if (elev == 2) {
                    plus2++;
                } else if (elev == 3) {
                    plus3++;
                } else if (elev == -1) {
                    minus1++;
                } else if (elev == -2) {
                    minus2++;
                } else if (elev == -3) {
                    minus3++;
                }
            }
            // inverting the order
            doff = ((HGT1_SIZE_1 - 1 - y) * HGT1_SIZE_1 * 3) + (x * 3);
            //png[ j++ ] = elev & 0x0000ff;
			//png[ j++ ] = ( elev & 0x00ff00 ) >> 8;
			//png[ j++ ] = 0; //( elevation & 0xff0000 ) >> 16;
            // AND inverting the color inot this buffer
            png[doff+2] = elev & 0x0000ff;
            png[doff+1] = ( elev & 0x00ff00 ) >> 8;
            png[doff+0] = 0;
            last_elev = elev;
        }
    }
    SPRTF("%s: max %d, min %d, %d of -1\n", module, emax, emin, minus1 );
    if (vcnt) {
        SPRTF("%s: Have %d VOIDS\n", module, vcnt );
    } else {
        SPRTF("%s: NO 'voids' found\n", module, vcnt );
    }
    SPRTF("%s: Elev 3 %d, 2 %d, 1 %d, 0 %d, -1 %d, -2 %d, -3 %d\n", module,
        plus3, plus2, plus1, zero, minus1, minus2, minus3 );
    x = min_x;
    y = min_y;
    offset = ((y * HGT1_SIZE_1) + x) * 2;
    sp = &p1->hgt_data[y][x];
    elev = *sp;
    pb = &pbuf[offset];
    elev2 = pb[0] + (256 * pb[1]);
    pb2 = (unsigned char *)&p1->hgt_data[y][x];
    elev3 = pb2[0] + (256 * pb2[1]);
    //lon = file_lon + ((double)x / (double)HGT1_SIZE_1); // of 1 degree NO
    //lat = top_lat  - ((double)(y+1) / (double)HGT1_SIZE_1);
    lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
    lon = file_lon + (x * HGT1_XDIM);
    if ((lon != min_dx)||(lat != min_dy)) {
        SPRTF("%s: Check lat,lon - %lf vs %lf - %lf vs %lf\n", module,
            lat, min_dy, lon, min_dx );
    }
    SPRTF("%s: Off min %d, %d m (%04X), at %lf,%lf\n", module, offset, elev, (elev & 0xffff), lat, lon);

    x = max_x;
    y = max_y;
    offset = ((y * HGT1_SIZE_1) + x) * 2;
    sp = &p1->hgt_data[y][x];
    elev = *sp;
    pb = &pbuf[offset];
    elev2 = pb[0] + (256 * pb[1]);
    pb2 = (unsigned char *)&p1->hgt_data[y][x];
    elev3 = pb2[0] + (256 * pb2[1]);
    //lon = file_lon + ((double)x / (double)HGT1_SIZE_1); // of 1 degree NO
    //lat = top_lat  - ((double)(y+1) / (double)HGT1_SIZE_1);
    lat = file_lat + ((HGT1_SIZE - y) * HGT1_YDIM);
    lon = file_lon + (x * HGT1_XDIM);
    SPRTF("%s: File offset max %d, %d m (%04X), at %lf,%lf\n", module, offset, elev, (elev & 0xffff), lat, lon);
    // p1->hgt_to_image();
#ifdef USE_PNG_LIB
    if (write_png_image) {
        if (writePNGImage24((char *)hm, HGT1_SIZE_1, HGT1_SIZE_1, 8, 6, png )) {
            SPRTF("%s: heighmap to %s FAILED!\n", module, hm );
        } else {
            SPRTF("%s: Written simple heighmap to %s\n", module, hm );
        }
    }
#endif // #ifdef USE_PNG_LIB
    if (got_bbox) {
        max = pvp->size();
        if (max) {
            SPRTF("%s: Found %d points in BBOX %s\n", module, (int)max, get_bounding_sbox_stg( &sbbox ));
            mp2 = pvp->at(0);
            x = mp2.x;
            cnt = 0;
            SPRTF("%lf,%lf ", mp2.pos.lat, mp2.pos.lon);
            mp22 = mp2;
            for (ii = 0; ii < max; ii++) {
                mp2 = pvp->at(ii);
                if (mp2.x != x) {
                    if (cnt)
                        SPRTF(" %lf,%lf\n%lf,%lf ", mp22.pos.lat, mp22.pos.lon, mp2.pos.lat, mp2.pos.lon);
                    cnt = 0;
                    x = mp2.x;
                }
                SPRTF("%3d ", (mp2.pos.elev & 0xffff));
                // SPRTF("%3d ", (int)mp2.pos.elev);
                cnt++;
                mp22 = mp2;
            }
            if (cnt)
                SPRTF(" %lf,%lf\n", mp22.pos.lat, mp22.pos.lon);
            SPRTF("%s: Shown %d points in BBOX %s\n", module, (int)max, get_bounding_sbox_stg( &sbbox ));
        } else {
            SPRTF("%s: NO points in BBOX %s\n", module, get_bounding_sbox_stg( &sbbox ));
        }
        pvp->clear();
        delete pvp;
    }
    delete png;
    SPRTF("\n");
}

int dump_user_input()
{
    int iret = 0;
    std::string file = usr_input;
    hgt1File *p1 = new hgt1File;
    p1->verb = 1;
    p1->report = 1;

    if (!p1->load_file(file)) {
        SPRTF("%s: Failed to load '%s'!\n", module, file.c_str() );
        iret = 1;
        goto exit;
    }
    SPRTF("%s: Loaded file '%s'\n",module,file.c_str());
    process_data(p1);

exit:
    delete p1;
    return 0;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    iret = parse_args(argc,argv);
    if (iret)
        return iret;
    set_log_file((char *)def_log,false);

    iret = dump_user_input();   // actions of app


    close_log_file();
    return iret;
}


// eof = hgt1-dump.cxx
