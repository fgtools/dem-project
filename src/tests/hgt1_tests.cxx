// hgt1_tests.cxx

#include <stdio.h>
#include <string>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#endif
#ifdef USE_SIMGEAR_LIB
#include <simgear/constants.h>
#include <simgear/math/SGMath.hxx>
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "hgt1File.hxx"
#include "color.hxx"
#include "dem_utils.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "dem_tests.hxx"
#include "dir_utils.hxx"
#include "slippystuff.hxx"
#include "all_tests.hxx"
#include "hgt1_tests.hxx"

static const char *module = "hgt1_tests";

int hgt1_out_image( int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1,  hgt1File & s, const char *img_file = 0);
int dem1_bbox_2_image( PBNDBOX pbb, int width, int height, hgt1File & s, const char *img_file = 0 );


int hgt1_write_image( int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1 )
{
    int iret = 0;
    int x,y,cnt;
    short elev;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    size_t doff;
    unsigned char r,g,b;
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    unsigned int colr;
    SPRTF("%s: Extract w,h %d,%d from x,y bgn %d,%d, span %d,%d (%s,%s) from %s.\n", module,
        width, height,
        x_bgn, y_bgn,
        spanx, spany,
        get_trim_double(dspanx),
        get_trim_double(dspany),
        get_file_name(file1).c_str());

    clear_used_colors();
    double bgn = get_seconds();

    hgt1File s;
    s.verb = false; // set to run silent
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;  // to 'see' any errors
    bool ok = s.begin_span_file(file1);
    if (!ok) {
        SPRTF("%s: Failed to begin_span_file(%s)!\n", module, file1.c_str() );
        return 1;
    }
    cnt = 0;
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
            if (!ok) {
                SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                check_me();
                return 1;
            }
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
            cnt++;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
    SPRTF("%s: Did %s av. calls, in %s.\n", module,
        get_NiceNumberStg(cnt),
        get_elapsed_stg(bgn));

#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    char *out_png = get_out_png();
    if (out_png) {
        if (writePNGImage24(out_png, width, height, bit_depth, color_type, buffer ))
            iret |= 8;
    }
#else // !USE_PNG_LIB
    char *out_bmp = get_out_bmp();
    if (out_bmp) {
        if (writeBMPImage24(out_bmp, width, height, buffer, bsize))
            iret |= 4;
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        xcols, yrows,
        get_NiceNumberStg(xcols * yrows),
        width, height, secs );
    delete buffer;
    out_used();
    return iret;

}

int hgt1_out_image( int width, int height, int yrows, int xcols, int y_bgn, int x_bgn,
    std::string file1,  hgt1File & s, const char *img_file)
{
    int iret = 0;
    int x,y,cnt,ncnt;
    short elev;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    size_t doff;
    unsigned char r,g,b;
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    int last_bgn_x, last_bgn_y;
    unsigned int colr;
    SPRTF("%s: Extract w,h %d,%d from x,y bgn %d,%d, span %d,%d (%s,%s) from %s.\n", module,
        width, height,
        x_bgn, y_bgn,
        spanx, spany,
        get_trim_double(dspanx),
        get_trim_double(dspany),
        get_file_name(file1).c_str());

    clear_used_colors();
    double bgn = get_seconds();

    //hgt1File s;
    //s.verb = false; // set to run silent
    //s.set_hgt_dir(srtm1);
    //s.verb = true;  // to 'see' any errors
    bool ok = s.begin_span_file(file1);
    if (!ok) {
        SPRTF("%s: Failed to begin_span_file(%s)!\n", module, file1.c_str() );
        return 1;
    }
    ncnt = cnt = 0;
    last_bgn_x = last_bgn_y = -1;
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            if ((last_bgn_x != bgn_x)||(last_bgn_y != bgn_y)) {
                ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
                if (!ok) {
                    SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                    check_me();
                    return 1;
                }
                //SPRTF("%d ",elev);
                colr = get_BGR_color(elev); // convert to a color
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                ncnt++;
            }
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
            cnt++;
            last_bgn_x = bgn_x;
            last_bgn_y = bgn_y;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
    SPRTF("%s: Did %s of %s av. calls, in %s.\n", module,
        get_NiceNumberStg(ncnt),
        get_NiceNumberStg(cnt),
        get_elapsed_stg(bgn));

    const char *out_img = img_file;
#ifdef USE_PNG_LIB
    //int bit_depth = 8;
    //int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    if (!out_img)
        out_img = get_out_png();
    if (out_img) {
        if (writePNGImage24((char *)out_img, width, height, 8, 6, buffer )) {
            iret |= 8;
        }
    }
#else // !USE_PNG_LIB
    if (!out_img)
        out_img = get_out_bmp();
    if (out_img) {
        if (writeBMPImage24(out_img, width, height, buffer, bsize)) {
            iret |= 4;
        }
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        xcols, yrows,
        get_NiceNumberStg(xcols * yrows),
        width, height, secs );
    delete buffer;
    out_used();
    return iret;

}

bool try_new_util_service = true;
int dem1_bbox_2_image( PBNDBOX pbb, int width, int height, hgt1File & s, const char *img_file )
{
    int iret = 0;
    int res;
    short elev;
    std::string file1, file2;
    int x_bgn,y_bgn,x_end,y_end;
    int zoom = pbb->llz.zoom;

    // Hmmm, only doing this to check -
    // (a) Get the file name, and
    // (b) Is it all in one file
    // but COULD do that test here using 
    //extern int stat_hgt1_file( double dlat, double dlon, std::string file_dir, std::string &hgt, 
    //int *px_off, int *py_off, size_t *poff, bool verb = false );
    if (try_new_util_service) {
        res = stat_hgt1_file( pbb->bb_max_lat, pbb->bb_min_lon, s.file_dir, file1,
            &x_bgn, &y_bgn, 0);

        if (res) {
            if (res == 2) {
                SPRTF("%s: Failed to 'stat' file %s for lat,lon %s,%s!\n", module,
                    file1.c_str(),
                    get_trim_double(pbb->bb_max_lat),
                    get_trim_double(pbb->bb_min_lon) );
            } else {
                SPRTF("%s: Failed to get file for lat,lon %s,%s!\n", module,
                    get_trim_double(pbb->bb_max_lat),
                    get_trim_double(pbb->bb_min_lon) );
            }
            return 1;
        }
        SPRTF("%s: for lat,lon %s,%s is file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon),
            file1.c_str(),
            x_bgn, y_bgn);
    } else {
        // now get upper left elevation
        if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
            SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
                get_trim_double(pbb->bb_max_lat),
                get_trim_double(pbb->bb_min_lon) );
            return 1;
        }
        file1 = s.file;
        x_bgn = s.x_off;
        y_bgn = s.y_off;
        SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_max_lat),
            get_trim_double(pbb->bb_min_lon),
            elev,
            file1.c_str(),
            x_bgn, y_bgn);
    } 

    if (try_new_util_service) {
        res = stat_hgt1_file( pbb->bb_min_lat, pbb->bb_max_lon, s.file_dir, file2,
            &x_end, &y_end, 0);
        if (res) {
            if (res == 2) {
                SPRTF("%s: Failed to 'stat' file %s for lat,lon %s,%s!\n", module,
                    file2.c_str(),
                    get_trim_double(pbb->bb_min_lat),
                    get_trim_double(pbb->bb_max_lon) );
            } else {
                SPRTF("%s: Failed to get file for lat,lon %s,%s!\n", module,
                    get_trim_double(pbb->bb_min_lat),
                    get_trim_double(pbb->bb_max_lon) );
            }
            return 1;
        }
        SPRTF("%s: for lat,lon %s,%s is file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon),
            file2.c_str(),
            x_end, y_end);
    } else {
        // now get lower right elevation
        if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
            SPRTF("%s: Failed to get elevation for lat,lon %s,%s!\n", module,
                get_trim_double(pbb->bb_min_lat),
                get_trim_double(pbb->bb_max_lon) );
            return 1;
        }
        file2 = s.file;
        x_end = s.x_off;
        y_end = s.y_off;
        SPRTF("%s: for lat,lon %s,%s reported elevation %d,\n from file %s at x,y %d,%d\n", module,
            get_trim_double(pbb->bb_min_lat),
            get_trim_double(pbb->bb_max_lon),
            elev,
            file2.c_str(),
            x_end, y_end);
    }

    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;

    if (strcmp(file1.c_str(),file2.c_str())) {
        double f1_lat,f1_lon,f2_lat,f2_lon;
        if (get_hgt_lat_lon( file1, &f1_lat, &f1_lon )&&
            get_hgt_lat_lon( file2, &f2_lat, &f2_lon )) {
            int f1_ilat = (int)f1_lat;
            int f1_ilon = (int)f1_lon;
            int f2_ilat = (int)f2_lat;
            int f2_ilon = (int)f2_lon;
            if ((f1_ilat >= f2_ilat)&&
                (f1_ilon <= f2_ilon)) {
                int ilat,ilon;
                if (f2_ilat < f1_ilat) {
                    yrows = 0;
                    for (ilat = f2_ilat; ilat <= f1_ilat; ilat++) {
                        if (ilat == f2_ilat)
                            yrows += HGT1_SIZE_1 - y_bgn;
                        else if (ilat == f1_ilat)
                            yrows += y_end;
                        else
                            yrows += HGT1_SIZE_1;
                    }
                }
                if (f1_ilon < f2_ilon) {
                    xcols = 0;
                    for (ilon = f1_ilon; ilon <= f2_ilon; ilon++) {
                        if (ilon == f1_ilon)
                            xcols += HGT1_SIZE_1 - x_bgn;
                        else if (ilon == f2_ilon)
                            xcols += x_end;
                        else
                            xcols += HGT1_SIZE_1;
                    }
                }
            }
        }

        SPRTF("%s: Z=%d %s covers x,y %d,%d to %d,%d, span %d,%d points\n", module,
            zoom,
            get_bounding_box_stg(pbb,true),
            x_bgn,y_bgn,x_end,y_end,
            xcols, yrows );
        SPRTF("%s: Z=%d Can NOT handling 2 files, %s and %s, but will...\n", module,
            zoom,
            get_file_name(file1).c_str(),
            get_file_name(file2).c_str());
        return 1;
    }

    SPRTF("%s: Z=%d %s\n", module,
        zoom,
        get_bounding_box_stg(pbb,true));
    SPRTF("%s: Z=%d Covers x,y %d,%d to %d,%d, span %d,%d\n", module,
        zoom,
        x_bgn,y_bgn,x_end,y_end,
        xcols, yrows );

    iret = hgt1_out_image( width, height, yrows, xcols, y_bgn, x_bgn, file1, s, img_file );

    return iret;
}

//  1    { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
//  GPS: 32T E 0334 115 N 5077 665 - 45.832704, 6.864797 (Dec Deg)
int do_hgt1_test1() // Mont Blanc elev test, mult zoom imgs, html, plus 1024x1024
{
    int iret = 0;
    short elev;
    int zoom = 15;
    double lat = 45.832704;
    double lon = 6.864797;
    int width = 256;
    int height = 256;
    int min_zoom = 8;  // below zoom 12 requires MULTIPLE files - TODO!
    int max_zoom = MAX_SLIPPY_ZOOM;
    BNDBOX bb;
    PBNDBOX pbb = &bb;

    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test1() - %d =====================\n", module, tf_hgt1_test1);

    hgt1File s;
    //s.verb = false;
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;

    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }
    SPRTF("%s: For lat,lon %s,%s, Mont Blanc got elev %d, expected 4810\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev);

    vSTG files;
    LOCZOOM lz;
    vLZ vlz;
    vlz.clear();
    lz.exp = 4810;
    lz.files.clear();
    lz.lat = lat;
    lz.lon = lon;
    lz.loc = "Mount Blanc";
    size_t ii,max;
    for (zoom = min_zoom; zoom < max_zoom; zoom++) {
        char *cp;
        if (!get_map_bounding_box( lat, lon, zoom, pbb, true )) {
            SPRTF("%s: FAILED to get slippy map bounding box!\n", module);
            return 1;
        }
        if (!get_slippy_path( lat, lon, zoom, &cp )) {
            SPRTF("%s: FAILED to get slippy map path!\n", module);
            return 1;
        }
        std::string img = get_image_file_path(cp);

        files.clear();
        if (!get_hgt3_file_names( pbb, files, true, false )) {
            SPRTF("%s: FAILED to get file names for this bbox!\n", module);
            return 1;
        }
        max = files.size();
        SPRTF("%s: Z=%d Need %d file(s): ", module, zoom, (int)max );
        for (ii = 0; ii < max; ii++) {
            std::string f = files[ii];
            std::string ff = srtm1_dir;
            ff += PATH_SEP;
            ff += f;
            SPRTF("%s ",f.c_str());
            if (is_file_or_directory((char *)ff.c_str()) == 1) {
                SPRTF("ok ");
            } else {
                SPRTF("NF ");
            }
        }
        SPRTF("\n");
        if (max == 1) {
            iret |= dem1_bbox_2_image( pbb, width, height, s, img.c_str() );
        } else {
            SPRTF("%s: Presently do NOT handle multiple files!\n", module);
        }
        lz.files.push_back(img);
    }
    vlz.push_back(lz);
    char *tmp = GetNxtBuf();
    sprintf(tmp,"Test %u View", tf_hgt1_test1);
    write_html_view(vlz, min_zoom, max_zoom, tmp);

    width = 1024;
    height = 1024;
    pbb->bb_min_lat = 45.0;
    pbb->bb_max_lat = 45.9999;
    pbb->bb_min_lon = 6.0;
    pbb->bb_max_lon = 6.9999;
    pbb->llz.zoom = 10; //zoom;   // not really
    pbb->llz.lat  = lat;
    pbb->llz.lon  = lon;

    SPRTF("\n");
    SPRTF("%s: Write whole file N45E006.hgt to %dx%d image\n", module, width, height);
    iret |= dem1_bbox_2_image( pbb, width, height, s );

    return iret;
}

// { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
int do_hgt1_test2()
{
    int iret = 0;
    //int zoom = 15;
    double lat = 45.832704;
    double lon = 6.864797;
    short elev = 4810;

    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test2() =====================\n", module);

    hgt1File s;
    s.verb = false;
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }

    SPRTF("%s: For lat,lon %s,%s, Mont Blanc got elev %d, expected 4810\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev);
    std::string file = srtm1_dir;
    file += PATH_SEP;
    file += "N45E006.hgt";
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        SPRTF("%s: Can NOT locate file %s\n", module,
            file.c_str());
    }
    size_t file_size = get_last_file_size();
    SPRTF("\n");
    SPRTF("%s: Doing full scan of file %s, %s bytes...\n", module,
        file.c_str(),
        get_NiceNumberStg(file_size));

    double bgn = get_seconds();
    if (!s.scan_hgt_file(file, 45.0, 6.0)) {
        SPRTF("%s: hgt scan failed!\n", module);
        return 1;
    }
    SPRTF("%s: Scan of %s byte file took %s.\n", module,
        get_NiceNumberStg(file_size),
        get_elapsed_stg(bgn));

    SPRTF("%s: Expected highest 4810 at lat,lon %s,%s, got %d at %s,%s.\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        s.max_elev,
        get_trim_double(s.max_lat),
        get_trim_double(s.max_lon) );
#ifdef USE_SIMGEAR_LIB
    SGGeod p1,p2;
    double az1,az2,dist;
    p1.setLatitudeDeg(lat);
    p1.setLongitudeDeg(lon);
    p1.setElevationM(elev);
    p2.setLatitudeDeg(s.max_lat);
    p2.setLongitudeDeg(s.max_lon);
    p2.setElevationM(s.max_elev);
    SGGeodesy::inverse(p1,p2,az1,az2,dist);
    SPRTF("%s: SG Geodesy reports these are %s appart, on course %s\n", module,
        get_meters_stg( SG_FEET_TO_METER * dist ),
        get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
    //SPRTF("%s: SG Geodesy reports these are %s Km appart, on course %s\n", module,
    //    get_trim_double( (double)((int)(((SG_FEET_TO_METER * dist) / 1000.0) * 10.0)) / 10.0 ),
    //    get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
#endif // USE_SIMGEAR_LIB

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(s.max_lat),
            get_trim_double(s.max_lon));
    if (!s.get_elevation(s.max_lat,s.max_lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(s.max_lat),
            get_trim_double(s.max_lon));
        return 1;
    }
    SPRTF("%s: For lat,lon %s,%s, got elev %d, expected %d.\n", module,
        get_trim_double(s.max_lat),
        get_trim_double(s.max_lon),
        elev,
        s.max_elev);

    // another file load and scan on load
    SPRTF("\n");
    SPRTF("%s: Doing load of file %s...\n", module,
            file.c_str());
    bgn = get_seconds();
    if (!s.load_file(file)) {
        SPRTF("%s: Load of file %s FAILED!\n", module,
            file.c_str());
        return 1;
    }
    SPRTF("%s: Done load and scan of %s byte file took %s.\n", module,
        get_NiceNumberStg(file_size),
        get_elapsed_stg(bgn));
#ifdef USE_SIMGEAR_LIB
    p2.setLatitudeDeg(s.emax_lat);
    p2.setLongitudeDeg(s.emax_lon);
    p2.setElevationM(s.emax);
    SGGeodesy::inverse(p1,p2,az1,az2,dist);
    SPRTF("%s: SG Geodesy reports these are %s appart, on course %s\n", module,
        get_meters_stg( SG_FEET_TO_METER * dist ),
        get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
#endif // USE_SIMGEAR_LIB

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(s.emax_lat),
            get_trim_double(s.emax_lon));
    if (!s.get_elevation(s.emax_lat,s.emax_lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(s.emax_lat),
            get_trim_double(s.emax_lon));
        return 1;
    }
    SPRTF("%s: For lat,lon %s,%s, got elev %d, expected %d.\n", module,
        get_trim_double(s.emax_lat),
        get_trim_double(s.emax_lon),
        elev,
        s.emax);
    return iret;
}

// { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt
int do_hgt1_test3()
{
    int iret = 0;
    // hgt1_tests: Z=15 BBOX lat,lon min 45.828799,6.855469 max 45.836454,6.866455 span 0.007655,0.010986 covers x,y 3080,588 to 3120,616, span 40,28 points
    int zoom = 15; // work fine - slippy tile 15\17008\11679.png - 
    //int zoom = 12;
    double lat = 45.832704;
    double lon = 6.864797;
    short elev = 4810;
    BNDBOX bb;
    PBNDBOX pbb = &bb;
    char *cp = 0;
    bool got_sp = false;
    int width = 256;
    int height = 256;

    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test3() =====================\n", module);

    hgt1File s;
    s.verb = false;
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;

    SPRTF("\n");
    SPRTF("%s: Doing test of elevation at lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }

    SPRTF("%s: For lat,lon %s,%s, Mont Blanc got elev %d, expected 4810\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        elev);

    zoom = 11;
    while (zoom >= 0) {
        SPRTF("\n");

        got_sp = get_slippy_path( lat, lon, zoom, &cp);

        if (!get_map_bounding_box( lat, lon, zoom, pbb, false )) {
            SPRTF("%s: Failed to get boundbox for lat,lon %s,%s\n", module,
                get_trim_double(lat),
                get_trim_double(lon));
            return 1;
        }

        SPRTF("%s: for lat,lon %s,%s got %s\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            get_bounding_box_stg(pbb,true));

        SPRTF("%s: do_hgt1_test3: Write w,h %d,%d image for BBOX, slippy tile %s\n", module,
            width,
            height,
            ((got_sp && cp) ? cp : "NO SLIPPY PATH!") );
        if (got_sp && cp) {
            set_slippy_file_name(cp);
        }

        iret = dem1_bbox_2_image( pbb, width, height, s );

        if (iret == 0) {
            SPRTF("%s: MAX lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
                get_trim_double(s.sp_max_lat),
                get_trim_double(s.sp_max_lon),
                s.sp_maxe,
                s.sp_maxy, s.sp_maxx );
            SPRTF("%s: MIN lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
                get_trim_double(s.sp_min_lat),
                get_trim_double(s.sp_min_lon),
                s.sp_mine,
                s.sp_miny, s.sp_minx );

        } else {
            // break;
        }
        zoom--;
    }
   
    return iret;
}

// Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
int do_hgt1_test4()
{
    int iret = 0;
    int zoom = 15;
    //int zoom = 12;
    double lat = 36.5786;
    double lon = -118.2920;
    short elev;
    short expected = 4421;
    BNDBOX bb;
    PBNDBOX pbb = &bb;
    const char *loc = "Mount Whitney, CA";
    char *cp = 0;
    bool got_sp = false;
    int width = 256;
    int height = 256;
    size_t file_size;
    int org_zoom = zoom;

    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test4() =====================\n", module);

    hgt1File s;
    s.verb = false;
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;

    SPRTF("%s: Doing test of elevation at lat,lon %s,%s, %s.\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            loc);
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }
    file_size = s.file_size;
    SPRTF("%s: For lat,lon %s,%s, %s, got elev ", module,
        get_trim_double(lat),
        get_trim_double(lon),
        loc );
    if (elev == HGT_VOID)
        SPRTF("VOID");
    else
        SPRTF("%d",elev);
    SPRTF(", expected %d.\n", expected);

    std::string file = s.file;
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        SPRTF("%s: Unable to 'stat' file %s!\n", module,
            file.c_str());
        return 1;
    }
    file_size = get_last_file_size();

    double flat,flon;
    if (!get_hgt_lat_lon( file, &flat, &flon )) {
        SPRTF("%s: Unable to get lat,lon from file %s!\n", module,
            file.c_str());
        return 1;
    }
    SPRTF("%s: Scan of file %s, lat,lon %s,%s\n", module,
        file.c_str(),
        get_trim_double(flat),
        get_trim_double(flon));

    double bgn = get_seconds();
    if (!s.scan_hgt_file(file,flat,flon)) {
        SPRTF("%s: Scan of file %s FAILED!\n", module,
            file.c_str());
        return 1;
    }
    SPRTF("%s: Scan of %s byte file took %s.\n", module,
        get_NiceNumberStg(file_size),
        get_elapsed_stg(bgn));

    SPRTF("%s: Expected highest %d at lat,lon %s,%s, got %d at %s,%s.\n", module,
        expected,
        get_trim_double(lat),
        get_trim_double(lon),
        s.max_elev,
        get_trim_double(s.max_lat),
        get_trim_double(s.max_lon) );

#ifdef USE_SIMGEAR_LIB
    SGGeod p1,p2;
    double az1,az2,dist;
    p1.setLatitudeDeg(lat);
    p1.setLongitudeDeg(lon);
    p1.setElevationM(elev);
    p2.setLatitudeDeg(s.max_lat);
    p2.setLongitudeDeg(s.max_lon);
    p2.setElevationM(s.max_elev);
    SGGeodesy::inverse(p1,p2,az1,az2,dist);
    SPRTF("%s: SG Geodesy reports these are %s appart, on course %s\n", module,
        get_meters_stg( SG_FEET_TO_METER * dist ),
        get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
#endif // USE_SIMGEAR_LIB

    // set the highest location
    lat = s.max_lat;
    lon = s.max_lon;
    double min_lat = s.min_lat;
    double min_lon = s.min_lon;

    while (zoom > 0) {

        SPRTF("\n");
        got_sp = get_slippy_path( lat, lon, zoom, &cp);

        if (!get_map_bounding_box( lat, lon, zoom, pbb, false )) {
            SPRTF("%s: Failed to get boundbox for lat,lon %s,%s\n", module,
                get_trim_double(lat),
                get_trim_double(lon));
            return 1;
        }

        SPRTF("%s: for lat,lon %s,%s got %s\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            get_bounding_box_stg(pbb,true));

        SPRTF("%s: do_hgt1_test4: Write w,h %d,%d image for BBOX, slippy tile %s\n", module,
            width,
            height,
            ((got_sp && cp) ? cp : "NO SLIPPY PATH!") );

        if (got_sp && cp) {
            set_slippy_file_name(cp);
        }

        iret = dem1_bbox_2_image( pbb, width, height, s );

        if (iret == 0) {
            SPRTF("%s: MAX lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
                get_trim_double(s.sp_max_lat),
                get_trim_double(s.sp_max_lon),
                s.sp_maxe,
                s.sp_maxy, s.sp_maxx );
            SPRTF("%s: MIN lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
                get_trim_double(s.sp_min_lat),
                get_trim_double(s.sp_min_lon),
                s.sp_mine,
                s.sp_miny, s.sp_minx );
        } else {
            break;
        }
        zoom--;
    }

    zoom = org_zoom;
    // set the lowest location
    lat = min_lat;
    lon = min_lon;

    while (zoom > 0) {

        SPRTF("\n");
        got_sp = get_slippy_path( lat, lon, zoom, &cp);

        if (!get_map_bounding_box( lat, lon, zoom, pbb, false )) {
            SPRTF("%s: Failed to get boundbox for lat,lon %s,%s\n", module,
                get_trim_double(lat),
                get_trim_double(lon));
            return 1;
        }

        SPRTF("%s: for lat,lon %s,%s got %s\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            get_bounding_box_stg(pbb,true));

        SPRTF("%s: Z=%d Write w,h %d,%d image for BBOX, slippy tile %s\n", module,
            zoom,
            width,
            height,
            ((got_sp && cp) ? cp : "NO SLIPPY PATH!") );
        if (got_sp && cp) {
            set_slippy_file_name(cp);
        }

        iret = dem1_bbox_2_image( pbb, width, height, s );

        if (iret == 0) {
            SPRTF("%s: MAX lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
                get_trim_double(s.sp_max_lat),
                get_trim_double(s.sp_max_lon),
                s.sp_maxe,
                s.sp_maxy, s.sp_maxx );
            SPRTF("%s: MIN lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
                get_trim_double(s.sp_min_lat),
                get_trim_double(s.sp_min_lon),
                s.sp_mine,
                s.sp_miny, s.sp_minx );
        } else {
            break;
        }
        zoom--;
    }

    return iret;

}

// ARG! USGS DEM1 ONLY GO TO 60 degrees N - Region_07.txt - top is N59W165.hgt.zip
// see : F:/data/dem1/srtm1/index.html - Region_definition.jpg
// Mount McKinley (Denali)	20,236 ft 6168 m 63.0690, -151.0063
int do_hgt1_test5()
{
    int iret = 0;
    //int zoom = 15;
    //int zoom = 12;
    double lat = 63.0690;
    double lon = -151.0063;
    short elev;
    short expected = 6168;
    //BNDBOX bb;
    //PBNDBOX pbb = &bb;
    const char *loc = "Mount McKinley, AL";
    //char *cp = 0;
    //bool got_sp = false;
    //int width = 256;
    //int height = 256;
    size_t file_size;

    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test5() =====================\n", module);

    hgt1File s;
    s.verb = false;
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;

    SPRTF("%s: Doing test of elevation at lat,lon %s,%s, %s.\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            loc);
    if (!s.get_elevation(lat,lon,&elev)) {
        SPRTF("%s: Failed to get elevation of lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }
    file_size = s.file_size;
    SPRTF("%s: For lat,lon %s,%s, %s, got elev ", module,
        get_trim_double(lat),
        get_trim_double(lon),
        loc );
    if (elev == HGT_VOID)
        SPRTF("VOID");
    else
        SPRTF("%d",elev);
    SPRTF(", expected %d.\n", expected);

    std::string file = s.file;
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        SPRTF("%s: Unable to 'stat' file %s!\n", module,
            file.c_str());
        return 1;
    }
    file_size = get_last_file_size();

    double flat,flon;
    if (!get_hgt_lat_lon( file, &flat, &flon )) {
        SPRTF("%s: Unable to get lat,lon from file %s!\n", module,
            file.c_str());
        return 1;
    }
    SPRTF("%s: Scan of file %s, lat,lon %s,%s\n", module,
        file.c_str(),
        get_trim_double(flat),
        get_trim_double(flon));

    double bgn = get_seconds();
    if (!s.scan_hgt_file(file,flat,flon)) {
        SPRTF("%s: Scan of file %s FAILED!\n", module,
            file.c_str());
        return 1;
    }
    SPRTF("%s: Scan of %s byte file took %s.\n", module,
        get_NiceNumberStg(file_size),
        get_elapsed_stg(bgn));

    SPRTF("%s: Expected highest %d at lat,lon %s,%s, got %d at %s,%s.\n", module,
        expected,
        get_trim_double(lat),
        get_trim_double(lon),
        s.max_elev,
        get_trim_double(s.max_lat),
        get_trim_double(s.max_lon) );

#ifdef USE_SIMGEAR_LIB
    SGGeod p1,p2;
    double az1,az2,dist;
    p1.setLatitudeDeg(lat);
    p1.setLongitudeDeg(lon);
    p1.setElevationM(elev);
    p2.setLatitudeDeg(s.max_lat);
    p2.setLongitudeDeg(s.max_lon);
    p2.setElevationM(s.max_elev);
    SGGeodesy::inverse(p1,p2,az1,az2,dist);
    SPRTF("%s: SG Geodesy reports these are %s appart, on course %s\n", module,
        get_meters_stg( SG_FEET_TO_METER * dist ),
        get_trim_double((double)((int)((az1 + 0.5) * 10.0) / 10.0)));
#endif // USE_SIMGEAR_LIB


    return iret;

}

// Mount Whitney, California 14,505 ft 4421 m	36.5786, -118.2920
// write the series of 9 images around this area
int do_hgt1_test6()
{
    int iret = 0;
    int zoom = 15;
    double lat = 36.5786;
    double lon = -118.2920;
    //short elev;
    //short expected = 4421;
    BNDBOX bb;
    PBNDBOX pbb = &bb;
    BNDBOX bb2;
    PBNDBOX pbb2 = &bb2;
    bool got_sp;
    char *cp = 0;
    int width = 256;
    int height = 256;
    double lat2,lon2;

    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test6() =====================\n", module);

    hgt1File s;
    s.verb = false;
    if (!s.set_hgt_dir(srtm1_dir)) {
        s.verb = true;  // to 'see' any errors
        s.set_hgt_dir(srtm1_dir);
        return 1;
    }
    s.verb = true;

while (zoom > 0) {

    SPRTF("\n");
    SPRTF("%s: Doing ZOOM %d\n", module, zoom );

    if (!get_map_bounding_box( lat, lon, zoom, pbb, false )) {
        SPRTF("%s: Failed to get boundbox for lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
        return 1;
    }

    SPRTF("%s: for lat,lon,zoom %s,%s,%d got %s\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        zoom,
        get_bounding_box_stg(pbb,true));

    got_sp = get_slippy_path( lat, lon, zoom, &cp);
    if (!got_sp && !cp) {
        SPRTF("%s: Failed to get slippy path for lat,lon,zoom %s,%s,%d\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return 1;
    }

    set_slippy_file_name(cp);
    SPRTF("%s: Z=%d Write w,h %d,%d image for BBOX, slippy tile %s\n", module,
        zoom, width, height, cp );

    iret = dem1_bbox_2_image( pbb, width, height, s );

    if (iret == 0) {
        SPRTF("%s: MAX lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
            get_trim_double(s.sp_max_lat),
            get_trim_double(s.sp_max_lon),
            s.sp_maxe,
            s.sp_maxy, s.sp_maxx );
        SPRTF("%s: MIN lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
            get_trim_double(s.sp_min_lat),
            get_trim_double(s.sp_min_lon),
            s.sp_mine,
            s.sp_miny, s.sp_minx );
    } else {
        SPRTF("%s: Z=%d Write image 1 failed!\n", module, zoom );
        break;
    }

    for (int i = nbb_right; i < nbb_max; i++) {

        bb2 = bb;   // ESET bbox
        if (!get_next_map_bbox( pbb2, (bb_dir)i, true )) {
            SPRTF("%s: Failed to get next BBOX!\n", module );
            return 1;
        }

        // get the center lat,lon
        lat2 = (pbb2->bb_max_lat + pbb2->bb_min_lat) / 2.0;
        lon2 = (pbb2->bb_max_lon + pbb2->bb_min_lon) / 2.0;
        SPRTF("%s: for lat,lon,zoom %s,%s,%d got %s\n", module,
            get_trim_double(lat2),
            get_trim_double(lon2),
            zoom,
            get_bounding_box_stg(pbb2,true));

        got_sp = get_slippy_path( lat2, lon2, zoom, &cp);
        if (!got_sp && !cp) {
            SPRTF("%s: Failed to get slippy path for lat,lon,zoom %s,%s,%d\n", module,
                get_trim_double(lat2),
                get_trim_double(lon2),
                zoom );
            return 1;
        }

        set_slippy_file_name(cp);
        SPRTF("%s: Z=%d Write w,h %d,%d image for BBOX, slippy tile %s\n", module,
            zoom, width, height, cp );

        iret = dem1_bbox_2_image( pbb2, width, height, s );
        if (iret) {
            SPRTF("%s: Write of image failed!\n", module );
            break;
        }

        SPRTF("%s: MAX lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
            get_trim_double(s.sp_max_lat),
            get_trim_double(s.sp_max_lon),
            s.sp_maxe,
            s.sp_maxy, s.sp_maxx );
        SPRTF("%s: MIN lat,lon %s,%s, elev %d, at y,x %d,%d\n", module,
            get_trim_double(s.sp_min_lat),
            get_trim_double(s.sp_min_lon),
            s.sp_mine,
            s.sp_miny, s.sp_minx );
    }
    if (iret)
        break;
    zoom--;
}

    return iret;
}

// whole file to image, like for
// const char *hgt = "F:\\data\\dem1\\srtm1\\N36W119.hgt";
int do_hgt1_test7(const char *hgt)
{
    int iret = 0;
    SPRTF("\n");
    SPRTF("%s: === do_hgt1_test7() =====================\n", module);

    hgt1File s;
    s.verb = 1;
    s.report = 1;

    if (!s.load_file(hgt)) {
        SPRTF("%s: file %s FAILED!\n", module, hgt);
        return 1;
    }

    iret = s.hgt_to_image();
    if (iret) {
        SPRTF("%s: Image for file %s FAILED! %d\n", module, hgt, iret);
        return 1;
    }

    SPRTF("%s: Done image for file %s\n", module, hgt );

    return iret;

}

// eof 
