/*\
 * gs30_tests.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _GS30_TESTS_HXX_
#define _GS30_TESTS_HXX_

//     tf_usgs_test1 = 0x00400000, //iret |= do_usgs30_test1(); // Mount Everest, and run elevtests set
extern int do_usgs30_test1();
extern int do_usgs30_test2(); // tf_usgs_test2 = 0x00800000, // scan all usgs30_dir/*.DEM files for max/min
extern int do_usgs30_test3(); // tf_usgs_test3 = 0x01000000,  // write all usgs30_dir/*.DEM files to png/bmp images
extern int do_usgs30_test4(); // slippy map tile using usgs30_dir/*.DEM files

#endif // #ifndef _GS30_TESTS_HXX_
// eof - gs30_tests.hxx
