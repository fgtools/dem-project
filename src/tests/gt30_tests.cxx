// gt30_tests.cxx

#include <stdio.h>
#ifndef _MSC_VER
#include <stdlib.h> // for exit()
#include <limits.h> // for SHRT_MIN, ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "gtopo30.hxx"
#include "dem_tests.hxx"
#include "color.hxx"
#include "stopo30.hxx"
#include "srtm30.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "gt30_tests.hxx"

#ifdef INCLUDE_GLOBE_DATA

static const char *module = "gt30_tests";

// scan of file <gtopo_dir>\h10g get min,max... then run the elevtests
int do_globe_test()
{
    SPRTF("\n");
    SPRTF("%s: === do_globe_test() =====================\n", module);
    gtopo30 g;
    double lat,lon;
    short elev, telev;
    std::string file(globe30_dir);
    file += PATH_SEP;
    file += "h10g";
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        SPRTF("%s: Can NOT 'stat' file %s! *** FIX ME ***\n", module, file.c_str());
        check_me();
        return 1;
    }
    SPRTF("%s: Doing a min/max scan of file %s\n",module, file.c_str());

    if (!g.set_file(file)) {
        SPRTF("%s: HUH! Can NOT set_file(%s)! *** FIX ME ***\n", module, file.c_str());
        check_me();
        return 1;
    }
    if (!g.load_file()) {
        SPRTF("%s: YEEK! Can NOT load %s! *** FIX ME ***\n", module, file.c_str());
        check_me();
        return 1;

    }
    if (!g.scan_min_max()) {
        SPRTF("%s: OOPS! Can NOT scan %s! *** FIX ME ***\n", module, file.c_str());
        check_me();
        return 1;
    }
    bool globe30_ok = g.set_globe_dir(globe30_dir);
    PELEVTESTS pet = elevtests;
    stopo30 s1;
    srtm30  s2;
    bool stopo30_ok = s1.set_file(stopo30_fil);   // set the single LARGE file
    bool ferr30_ok = s2.set_file_dir(ferr30_dir); // set Ferranti Bathymetric .srtm dir

    while (pet->name) {
        lat = pet->lat;
        lon = pet->lon;
        SPRTF("\n");
        if (g.get_elevation( lat, lon, &elev)) {
            SPRTF("%s: For lat,lon %s,%s, %s, got elev %d, expected %d.\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                elev,
                pet->expected);
            if (stopo30_ok && s1.get_elevation(lat,lon,&telev)) {
                SPRTF("%s: stopo30 returned %d\n", module, telev);
            }
            if (ferr30_ok && s2.get_elevation(lat,lon,&telev)) {
                SPRTF("%s: srtm30 returned %d\n", module, telev);
            }
        }
        pet++;
    }
    return 0;
}

// This does a scan of all 16 files, and reports min/max
int do_globe_test1()
{
    int iret = 0;
    gtopo30 g;
    std::string path(globe30_dir);
    std::string mask("*10g");
    SPRTF("\n");
    SPRTF("%s: === do_globe_test1() =====================\n", module);
    vSTG files = read_directory(path,mask,dm_FILE);
    size_t ii,max = files.size();
    if (max) {
        SPRTF("%s: On path %s, mask %s, have %d files to process...\n", module,
            path.c_str(), mask.c_str(), (int)max);
    } else {
        SPRTF("%s: On path %s, found no files with mask %s!\n", module,
            path.c_str(), mask.c_str());
        return 1;
    }
    short max_elev = SHRT_MIN;
    short min_elev = SHRT_MAX;
    size_t nodata_cnt = 0;
    size_t total_pts = 0;
    double min_lat, min_lon, max_lat, max_lon;
    int min_x, min_y, max_x, max_y;
    size_t min_off, max_off;

    for (ii = 0; ii < max; ii++) {
        path = files[ii];
        if (g.set_file(path)) {
            if (g.scan_min_max()) {
                if (g.max_elev > max_elev) {
                    max_elev = g.max_elev;
                    max_lat = g.max_lat;
                    max_lon = g.max_lon;
                    max_x   = g.max_x;
                    max_y   = g.max_y;
                    max_off = g.max_off;
                }
                if (g.min_elev < min_elev) {
                    min_elev = g.min_elev;
                    min_lat = g.min_lat;
                    min_lon = g.min_lon;
                    min_x   = g.min_x;
                    min_y   = g.min_y;
                    min_off = g.min_off;

                }
                nodata_cnt += g.nodata_cnt;
                total_pts += (g.cols * g.rows);
            }
        }
    }
    double pcent = ((double)nodata_cnt / (double)total_pts) * 100.0;
    pcent = ((int)((pcent + 0.05) * 10)) / 10.0;

    SPRTF("%s: Elevation range min %d to max %d, with %s of %s pts, %s%%, as no-data.\n", module,
        min_elev, max_elev,
        get_NiceNumberStg(nodata_cnt), get_NiceNumberStg(total_pts),
        get_trim_double(pcent));

    /////////////////////////////////////////////////////////////////
    // Now test the elevation look-up function
    short elev;
    // first set where to find the files
    if (!g.set_globe_dir(globe30_dir)) {
        SPRTF("%s: GLOBE 30 directory %s INVALID!\n", module, globe30_dir);
        return 1;
    }
    // now look-up a location
    if (g.get_elevation( max_lat, max_lon, &elev )) {
        SPRTF("%s: Elevation at lat,lon %s,%s is reported as %d, expected %d (x,y %d,%d off %d)\n", module,
            get_trim_double(max_lat),
            get_trim_double(max_lon),
            elev, max_elev,
            max_x, max_y,
            (int)max_off );
    }
    if (g.get_elevation( min_lat, min_lon, &elev )) {
        SPRTF("%s: Elevation at lat,lon %s,%s is reported as %d, expected %d (x,y %d,%d off %d)\n", module,
            get_trim_double(min_lat),
            get_trim_double(min_lon),
            elev, min_elev,
            min_x, min_y,
            (int)min_off );
    }
    ////////////////////////////////////////////////////////////////////
    return iret; // exit(1);
}


// =====================================================
// given a lat,lon, load the appropriate GLOBE topo file,
// and output a full size image
// =====================================================
int do_globe_test2()
{
    SPRTF("\n");
    SPRTF("%s: === do_globe_test2() =====================\n", module);
    gtopo30 topo;
    int iret = 0;
    std::string path(gtopo_a);
    char c = get_GTOPO_filechar(48.726969293,2.369992317);
    if ( c && (is_file_or_directory((char *)globe30_dir) == 2) ) {
        std::string tmp(globe30_dir);
        tmp += "\\";
        tmp += c;
        tmp += "10g";
        if (is_file_or_directory((char *)tmp.c_str()) == 1) {
            path = tmp;
        }
    } else {
        SPRTF("%s: GLOBE 30 directory %s INVALID!\n", module, globe30_dir);
        return 1;
    }

    //topo.set_file_range(2.369992317,48.726969293);
    //topo.set_file_range( 89.99991, -50.00001 );
    short curr_void = set_void_elevation(GTOPO_NODATA); 

    if (!topo.set_file(path))
        return 1;
    if (!topo.load_file())
        return 1;
    iret = topo.gtopo_to_images();

    set_void_elevation(curr_void); 

    return iret;
}

int do_globe_test3()
{
    int iret = 0;
    int x,y,rows, cols, spanx, spany;
    double dspanx, dspany;
    int bgn_x,bgn_y;
    bool ok;
    short elev;
    //int width = 512;
    //int height = 256;
    int width = 64;
    int height = 64;
    unsigned int colr;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize+1];
    size_t doff;
    unsigned char r,g,b;
    SPRTF("\n");
    SPRTF("%s: === do_globe_test3() =====================\n", module);

    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }
    gtopo30 gt;
    std::string path(globe30_dir);
    std::string mask("*10g");
    vSTG files = read_directory(path,mask,dm_FILE);
    size_t ii,max = files.size();
    if (max) {
        SPRTF("%s: On path %s, mask %s, have %d files to process...\n", module,
            path.c_str(), mask.c_str(), (int)max);
    } else {
        SPRTF("%s: On path %s, found no files with mask %s!\n", module,
            path.c_str(), mask.c_str());
        return 1;
    }
    // establish the void value and color
    short cvoid = set_void_elevation(GTOPO_NODATA);
    // unsigned int cvcolr = set_void_elev_color(0);
    clear_used_colors();
    double bgn = get_seconds();
    size_t total_bytes = 0;
    for( ii = 0; ii < max; ii++) {
        std::string f(files[ii]);
        if (is_file_or_directory((char *)f.c_str()) != 1) {
            SPRTF("%s: Unable to 'stat' file %s\n", module, f.c_str());
            check_me();
            return 1;
        }
        //g.verb = false;

        if (!gt.begin_span_file(f)) {
            SPRTF("%s: Unable to 'gtopo30:begin_span_file' with file %s\n", module, f.c_str());
            check_me();
            return 1;
        }
        rows = gt.rows;
        cols = gt.cols;
        spany = rows / height;
        if (rows % height) 
            spany++;
        spanx = cols / width;
        if (cols % width) 
            spanx++;
        dspany = (double)rows / (double)height;
        dspanx = (double)cols / (double)width;
        SPRTF("%s: Processing file %s, x,y %d,%d, %d bytes, span %d,%d, incs %s,%s\n", module,
            f.c_str(),
            cols, rows,
            (int) gt.file_size,
            spanx, spany,
            get_trim_double(dspanx),
            get_trim_double(dspany) );
        ok = true;
        gt.verb = true;  // to see any error
        total_bytes += gt.file_size;
        for (y = 0; y < height; y++) {
            bgn_y = (int)((double)y * dspany);
            for (x = 0; x < width; x++) {
                bgn_x = (int)((double)x * dspanx);
                ok = gt.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
                if (!ok) {
                    SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                    check_me();
                    return 1;
                }
                //SPRTF("%d ",elev);
                //if (elev <= GTOPO_NODATA)
                //    colr = 0;
                //else
                    colr = get_BGR_color(elev); // convert to a color
                doff = ((height - 1 - y) * width * 3) + (x * 3);
                if ((doff + 3) > bsize) {
                    SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                        x, y, (int)doff, (int)bsize );
                    check_me();
                    return 1;
                }
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = r;
                buffer[doff+1] = g;
                buffer[doff+2] = b;
            }
            //SPRTF("\n");
            if (!ok)
                break;
        }
        gt.end_span_file();
        // restore the void value and color
#ifdef USE_PNG_LIB
        int bit_depth = 8;
        int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
        char *out_png = get_out_png();
        if (out_png) {
            if (writePNGImage24(out_png, width, height, bit_depth, color_type, buffer ))
                iret |= 8;
        }
#else // !USE_PNG_LIB
        char *out_bmp = get_out_bmp();
        if (out_bmp) {
            if (writeBMPImage24(out_bmp, width, height, buffer, bsize))
                iret |= 4;
        }
#endif // USE_PNG_LIB y/n
    }
    char *secs = get_seconds_stg(get_seconds() - bgn);
    SPRTF("%s: Processed %d files, %s bytes, to images in %s\n", module,
        (int)max,
        get_NiceNumberStg(total_bytes),
        secs);

    delete buffer;
    out_used();
    set_void_elevation(cvoid);
    //set_void_elev_color(cvcolr);
    return iret;
}

#endif // #ifdef INCLUDE_GLOBE_DATA

// eof
