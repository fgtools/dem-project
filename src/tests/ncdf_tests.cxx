// ncdf_tests.cxx
#include <stdio.h>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "dem_tests.hxx"
#include "netcdf.hxx"
#include "stopo30.hxx"
#include "srtm30.hxx"
#include "ncdf_tests.hxx"

static const char *module = "ncdf_test";

bool use_stopo_file = false; // use the single LARGE file

int do_netcdf_test()
{
    bool tok = true;
    double lat,lon;
    short elev;
    bool add_show = false;
    bool res, cv;
    size_t cnt;
    bool do_file_1 = false;
    PELEVTESTS pet = elevtests;
    short telev;

    SPRTF("\n");
    SPRTF("%s: === do_netcdf__test() =====================\n", module);

    stopo30 s1;
    srtm30  s2;
    netcdf c;
    s1.verb = false;
    s2.verb = true;
    // set Ferranti Bathymetric .srtm dir
    tok = s2.set_file_dir(ferr30_dir);
    if (do_file_1)
        res = c.set_file(netcdf1);
    else
        res = c.set_file(netcdf2); // "D:\\SRTM\\scripps\\srtm30\\grd\\w060s60.nc";

    if (res && add_show) {
        c.show_data = true; // output ALL the data - can be BIG
        cnt = c.show_netcdf();
        SPRTF("%s: Shown %d header items\n", module, (int)cnt);
    }
    c.verb = false;
    s1.verb = false;
    s2.verb = false;
    if (res) {
        SPRTF("\n");
        //     Cerro Aconcagua : Elevation: 22,841 feet (6,962 meters), 
        // Coordinates: 32 39 20 S 70 00 57 W (-32.6555555555556, -70.0158333333333)
        lat = -32.6555555555556;
        lon = -70.0158333333333;
        // c.debug = true; // add more output
        if (c.get_elevation( lat, lon, &elev )) {
            SPRTF("%s: For lat,lon %s,%s, Cerro Aconcagua, got elev %d, expected 6962\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                elev);
        }
    }
    res = c.set_netcdf_dir(ncdf30_dir);
    if (res) {
        SPRTF("\n");
        if (c.get_elevation( 27.983333, 86.925000, &elev )) {
            SPRTF("%s: For lat,lon 27.983333,86.925000, Mt Everest, got elev %d, expected %d\n", module,
                elev,8685);
        }
    }
    c.clear();
    res = c.set_netcdf_dir(ncdf30_dir);
    if (use_stopo_file) {
        tok = s1.set_file(stopo30_fil);
    }

    //c.verb = true;
    //s1.verb = false;
    //s2.verb = true;
    while (res && pet->name) {
        SPRTF("\n");
        lat = pet->lat;
        lon = pet->lon;
        if (c.get_elevation( lat, lon, &elev )) {
            SPRTF("%s: For lat,lon %s,%s, %s, got elev %d, expected %d ", module,
                get_trim_double(lat),
                get_trim_double(lon),
                pet->name,
                elev,pet->expected);
            if (use_stopo_file) {
                if (s1.get_elevation(lat,lon,&telev))
                    SPRTF("(T30:%d)", telev);
            } else {
                if (s2.verb) SPRTF("\n");
                if (s2.get_elevation(lat,lon,&telev)) {
                    if(s2.verb) {
                        SPRTF("%s: de Ferranti Bathymetric %s returned %d\n", module,
                            s2.file.c_str(), telev);
                        if (strcmp(pet->name,"Marianas Trench") == 0) {
                            cv = s2.verb;
                            s2.verb = false;
                            if (s2.scan(s2.file)) {
                                SPRTF("%s: Lowest %d found at x,y %d,%d, lon,lat %s,%s\n", module,
                                    s2.min_elev,
                                    s2.min_x, s2.min_y,
                                    get_trim_double(s2.min_lon),
                                    get_trim_double(s2.min_lat));

                            }
                            s2.verb = cv;
                        } else if (strcmp(pet->name,"Mount Everest") == 0) {
                            cv = s2.verb;
                            s2.verb = false;
                            if (s2.scan(s2.file)) {
                                SPRTF("%s: Highest %d found at x,y %d,%d, lon,lat %s,%s\n", module,
                                    s2.max_elev,
                                    s2.max_x, s2.max_y,
                                    get_trim_double(s2.max_lon),
                                    get_trim_double(s2.max_lat));
                            }
                            s2.verb = cv;
                        }
                    } else {
                        SPRTF("(F30:%d)", telev);
                    }
                }
            }
            if (!s2.verb)
                SPRTF("\n");
        }
        pet++;
    }
    c.clear();
    if (!tok)
        return 1;
    return 0;
}


// eof
