// misc_tests.hxx
#ifndef _MISC_TESTS_HXX_
#define _MISC_TESTS_HXX_

extern void do_dir_tests();
extern int do_tile_test();
extern int do_mappy_tile_test();
extern int test_hgt_file_name();
extern int do_dist_test();
extern int get_elev_tests();

#endif // #ifndef _MISC_TESTS_HXX_
// eof - misc_tests.hxx

