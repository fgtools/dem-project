// gt30_tests.hxx
#ifndef _GT30_TESTS_HXX_
#define _GT30_TESTS_HXX_

// scan of file <gtopo_dir>\h10g get min,max... then run the elevtests
extern int do_globe_test();

extern int do_globe_test1();
extern int do_globe_test2();
extern int do_globe_test3();


#endif // #ifndef _GT30_TESTS_HXX_
// eof

