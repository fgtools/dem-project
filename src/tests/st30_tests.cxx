// st30_tests.cxx

#include <stdio.h>
#include <sstream>
#include "sprtf.hxx"
#include "utils.hxx"
#include "stopo30.hxx"
#include "color.hxx"
#include "dem_utils.hxx"
#include "dem_tests.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "dir_utils.hxx"
#include "slippystuff.hxx"
#include "demFile.hxx"
#include "all_tests.hxx"
#include "st30_tests.hxx"

static const char *module = "st30_tests";

/* ============================================================
    ftp://topex.ucsd.edu/pub/srtm30_plus/topo30 file topo30
    ONE(1) large file - too big to load into memory
    D:\SRTM\scripps\topo30\topo30 -
    31/03/2014  13:28     1,866,240,000 topo30
    So only uses file seek
   ============================================================ */
//# convert from permalink OSM format like:
//# http://www.openstreetmap.org/?lat=43.731049999999996&lon=15.79375&zoom=13&layers=M
//# to OSM "Export" iframe embedded bbox format like:   minlat  minlon  maxlat maxlon - ie BL to TR
//# http://www.openstreetmap.org/export/embed.html?bbox=15.7444,43.708,15.8431,43.7541&layer=mapnik

int do_stopo30_test1() // try limits and elevtests and group on single 2 GB file
{
    SPRTF("\n");
    SPRTF("%s: === do_stopo30_test1 =====================\n", module);
    stopo30 s;
    double xdim = 360.0 / (double)STOPO30_COLS;
    double ydim = 180.0 / (double)STOPO30_ROWS;
    double lat,lon;
    bool full = true; // with a full scan SHOW
    if (s.set_file(stopo30_fil)) {
        if(s.test_file(full)) {
            // 27.983333,86.925000 = 8685 - y,x 7442,10431
            // 11.375000,142.425000 = -10910 - y,x 9435,17091
            short elev;
            PELEVTESTS pet = elevtests;

            // test limits
            SPRTF("\n");
            lat = 90.0;
            lon = 0;
            if (s.get_elevation( lat, lon, &elev )) { // top left cell
                SPRTF("%s: For lat,lon %s,%s, got elev %d\n",module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    elev);
            }

            SPRTF("\n");
            lat = 90.0;
            lon = -xdim;
            if (s.get_elevation( lat, lon, &elev )) { // top left cell
                SPRTF("%s: For lat,lon %s,%s, got elev %d\n",module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    elev);
            }

            SPRTF("\n");
            lat = -90.0 + ydim;
            lon = -180.0 + xdim;
            if (s.get_elevation( lat, lon, &elev )) { // top left cell
                SPRTF("%s: For lat,lon %s,%s, got elev %d\n",module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    elev);
            }

            //SPRTF("\n");
            //lat = -90.0;
            //lon = -180.0;
            //if (s.get_elevation( lat, lon, &elev )) { // top left cell
            //    SPRTF("For lat,lon %lf,%lf, got elev %d\n",lat,lon,elev);
            //}

            SPRTF("\n");
            lat = -90.0 + ydim;
            lon = -xdim;
            if (s.get_elevation( lat, lon, &elev )) { // top left cell
                SPRTF("%s: For lat,lon %s,%s, got elev %d\n",module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    elev);
            }

            while (pet->name) {
                SPRTF("\n");
                lat = pet->lat;
                lon = pet->lon;
                if (s.get_elevation( lat, lon, &elev )) {
                    SPRTF("%s: For lat,lon %s,%s, %s, got elev %d, expected %d.\n", module,
                        get_trim_double(lat),
                        get_trim_double(lon),
                        pet->name,
                        elev,
                        pet->expected);
                }
                pet++;
            }

            // group of points
            vSHRT *pvs = 0;
            lat = -37.09444444;
            lon = -12.288611111;
            int bracket = 10;
            int colcnt, count = 0;
            int width = (bracket * 2) + 1;
            int height = (bracket * 2) + 1;
            size_t bsize = width * height * 3;
            unsigned int colr;
            byte r,g,b;
            int x,y;
            size_t doff;
            SPRTF("\n");
            if (s.get_elevation_group( lat, lon, bracket, &pvs, &colcnt ) && pvs) {
                size_t ii, max = pvs->size();
                SPRTF("%s: For lat,lon %s,%s, bracket %d got %d points, cols %d\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    bracket,
                    (int)max,colcnt);
                x = y = 0;
                unsigned char *buffer = new unsigned char[bsize];
                clear_used_colors();
                for (ii = 0; ii < max; ii++) {
                    elev = pvs->at(ii);
                    SPRTF(" %5d ", elev);
                    count++;
                    if (count == colcnt) {
                        SPRTF("\n");
                        count = 0;
                    }
                    if (buffer) {
                        colr = get_BGR_color(elev); // convert to a color
                        doff = ((height - 1 - y) * width * 3) + (x * 3);
                        if ((doff + 3) > bsize) {
                            SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                                x, y, (int)doff, (int)bsize );
                            check_me();
                            return 1;
                        }
                        r = getRVal(colr);
                        g = getGVal(colr);
                        b = getBVal(colr);
                        // is this the correct order????????
                        buffer[doff+0] = r;
                        buffer[doff+1] = g;
                        buffer[doff+2] = b;
                    }
                    x++;
                    if (x == width) {
                        y++;
                        x = 0;
                    }
                }
                if (count) {
                    SPRTF("\n");
                }
                if (buffer) {
#ifdef USE_PNG_LIB
                    int bit_depth = 8;
                    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
                    char *out_png = get_out_png();
                    if (out_png)
                        writePNGImage24(out_png, width, height, bit_depth, color_type, buffer );
#else // !USE_PNG_LIB
                    char *out_bmp = get_out_bmp();
                    if (out_bmp)
                        writeBMPImage24(out_bmp, width, height, buffer, bsize);
#endif // USE_PNG_LIB y/n
                    delete buffer;
                    out_used();
                }
                SPRTF("%s: See the island of Tristan da Cunha, lat,lon %s,%s?\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon) );
            }
        }
    } else {
        SPRTF("%s: single stopo30_fil %s INVALID\n", module, stopo30_fil);
        return 1;
    }
    return 0;
}


int do_stopo30_test2() // build a 1024x512 image of whole file
{
    int iret = 0;
    bool verb = true;
    int width = 1024;
    int height = 512;
    char *img = GetNxtBuf();
#ifdef USE_PNG_LIB
    const char *ext = "png";
#else
    const char *ext = "bmp";
#endif
    SPRTF("\n");
    SPRTF("%s: === do_stopo30_test2() =====================\n", module);

    sprintf(img,"%s" PATH_SEP "topo30-%dx%d.%s", get_image_out_dir(), width, height, ext );
    stopo30 s;
    s.verb = verb;
    iret = s.write_stopo30_image( width, height, img );  // build a width x height image of whole file

    return iret;

}


// build an 256x256 image of a part of the file
// this is a slippy map tile, so is related to zoom
// Mount Everest - 8,848 meters - 27.988, 86.9253 = N27E086
// extern int lon2tilex(double lon, int z);
// extern int lat2tiley(double lat, int z);
// extern double tilex2lon(int x, int z);
// extern double tiley2lat(int y, int z);
// ============================================================================================

int do_stopo30_test3()  // write Mt Everest slippy maps for zooms 4-19
{
    BNDBOX bb;
    //PBNDBOX pbb = &bb;
    double minlat = -85.0511;
    double maxlon = 180.0;
    double lonmax = 360.0;
    double latmax = 85.0511 * 2;
    int iret = 0;
    int zoom;
    // Mt Everest - 8,848 meters - 27.988, 86.9253 = N27E086
    double lat = 27.988;
    double lon = 86.9253;
    int min_zoom = 4;
    int max_zoom = 20;
    char *cp;
    char *tmp;
    LOCZOOM lz;
    vLZ vlz;
    std::string img;
    //int iverb = 3 | 0x8000; // be verbal and add test span
    int iverb = 3 | 0x4000; // be verbal and add test points, but NO test span

    //double tile_size = 256.0;
    SPRTF("\n");
    SPRTF("%s: === do_stopo30_test3() =====================\n", module);
    SPRTF("%s: multiple zoom tests using the topo30 file...\n", module);

    lz.exp = 8848;
    lz.files.clear();
    lz.lat = lat;
    lz.lon = lon;
    lz.loc = "Mt Everest";

    for (zoom = min_zoom; zoom < max_zoom; zoom++ ) {
        int maxtx = lon2tilex(maxlon,zoom);
        int maxty = lat2tiley(minlat,zoom);
        maxty++;
        // each tile at this zoom spans x,y degrees
        double lonsp = lonmax / (double)maxtx;
        double latsp = latmax / (double)maxty;
        int tilex = lon2tilex(lon,zoom);
        int tiley = lat2tiley(lat,zoom);

        double tlon = tilex2lon(tilex,zoom);
        double tlat = tiley2lat(tiley,zoom);
        SPRTF("\n");
        SPRTF("%s: Z=%d lon,lat %s,%s, tile x,y %d,%d, tile lon,lat %s,%s\n", module,
            zoom,
            get_trim_double(lon),
            get_trim_double(lat),
            tilex,tiley,
            get_trim_double(tlon),
            get_trim_double(tlat) );

        SPRTF("%s: Z=%d max x,y %d,%d, span lon,lat %s,%s\n", module,
            zoom,
            maxtx,maxty,
            get_trim_double(lonsp),
            get_trim_double(latsp) );

        if (!get_map_bounding_box( lat, lon, zoom, &bb, false )) {
            SPRTF("%s: Failed to get bounding box for lat,lon,zoom %s,%s,%d!\n", module,
                get_trim_double(lat),
                get_trim_double(lon),
                zoom );
            check_me();
            return 1;
        }
        SPRTF("%s: Z=%d %s\n", module,
            zoom,
            get_bounding_box_stg(&bb,true) );
        if (!get_slippy_path( lat, lon, zoom, &cp )) {
            SPRTF("%s: Failed to get slippy path!\n", module);
            SPRTF("%s\n", last_slippy_error.c_str());
            check_me();
            return 1;
        }
        // get image file name
        tmp = get_cache_file_name(cp,true);
        img = get_image_out_dir();
        if (is_file_or_directory((char *)img.c_str()) != 2) {
            SPRTF("%s: Out path %s DOES NOT EXIST! *** FIX ME ***\n", module, img.c_str());
            check_me();
            return 1;
        }
        img += PATH_SEP;
        //img += "srtm30";
        //img += PATH_SEP;
        img += tmp;
    #ifdef USE_PNG_LIB
        img += ".png";
    #else // !USE_PNG_LIB
        img += ".bmp";
    #endif // USE_PNG_LIB y/n
        //iret = st30_bb_to_image_depreciated( &bb, img.c_str() );
        if (!stopo30_bb_to_image( &bb, img.c_str(), iverb )) {
            iret |= 8;
        }
        lz.files.push_back(img);
    }
    vlz.push_back(lz);
    tmp = GetNxtBuf();
    sprintf(tmp,"Test %u View", tf_st30_test3);
    write_html_view(vlz, min_zoom, max_zoom, tmp);

    return iret;
}

#if 0 // 000000000000000000000000000000000000
void show_block( PLLZ pllz )
{
    int tilex = pllz->tilex;
    int tiley = pllz->tiley;
    int zoom  = pllz->zoom;
    int maxx  = lon2tilex(MAX_SLIPPY_LON,zoom);
    int maxy  = lat2tiley(MIN_SLIPPY_LAT,zoom);
    maxy++; // not sure about this CHECK ME TODO:
    int bgn_x, bgn_y, end_x, end_y;
    if (tilex > 0)
        bgn_x = tilex - 1;
    else
        bgn_x = 0;
    end_x = bgn_x + 2;
    if (end_x > maxx)
        end_x = maxx;
    if (tiley > 0)
        bgn_y = tiley - 1;
    else
        bgn_y = 0;
    end_y = bgn_y + 2;
    if (end_y > maxy)
        end_y = maxy;
    stopo30 s;
    s.verb = false; // set to run silent
    bool ok = s.set_file(stopo30_fil);

    int x,y;
    double lat,lon;
    short elev;
    SPRTF("%s: Display of 9 tiles x,y from %d,%d to %d,%d, on max %d,%d...\n", module,
        bgn_x,bgn_y,
        end_x,end_y,
        maxx,maxy);

    for (y = bgn_y; y <= end_y; y++) {
        lat = tiley2lat(y,zoom);
        SPRTF("lat: %s lons: ", get_trim_double(lat));
        for (x = bgn_x; x <= end_x; x++) {
            lon = tilex2lon(x,zoom);
            SPRTF("%s ",get_trim_double(lon));
            if (ok) {
                if (s.get_elevation( lat, lon, &elev ))
                    SPRTF("%d ",elev);
            }
        }
        SPRTF("\n");
    }
}


// need to use top/left
int st30_bb_to_image_depreciated( PBNDBOX pbb, const char *img_file )
{
    int iret = 0;
    short elev;
    int height = 256;
    int width = 256;
    int zoom = pbb->llz.zoom;

    stopo30 s;
    s.verb = false; // set to run silent
    bool ok = s.set_file(stopo30_fil);
    if (!ok) {
        SPRTF("%s: Failed to set_file(%s)!\n", module, stopo30_fil );
        return 1;
    }
    double blon = pbb->bb_min_lon;
    double blat = 90.0 - pbb->bb_max_lat;
    if (blon < 0) {
        blon += 360;
    }
    double elon = pbb->bb_max_lon;
    double elat = 90.0 - pbb->bb_min_lat;
    if (elon < 0) {
        elon += 360;
    }

    int x_bgn = (int)(blon / s.xdim);
    int y_bgn = (int)(blat / s.ydim);
    int x_end = (int)(elon / s.xdim);
    int y_end = (int)(elat / s.ydim);
    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;

    // check calculations
    if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
        SPRTF("%s: Failed to get first elevation from file %s!\n", module, stopo30_fil );
        return 1;
    }
    if ((x_bgn != s.x_col)||(y_bgn != s.y_row)) {
        SPRTF("%s: Failed to get same x,y %d,%d vs %d,%d!\n", module,
            x_bgn,y_bgn, s.x_col,s.y_row );
        return 1;
    }

    if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
        SPRTF("%s: Failed to get second elevation from file %s!\n", module, stopo30_fil );
        return 1;
    }
    if ((x_end != s.x_col)||(y_end != s.y_row)) {
        SPRTF("%s: Failed to get same x,y %d,%d vs %d,%d!\n", module,
            x_end,y_end, s.x_col,s.y_row );
        return 1;
    }

    SPRTF("%s: Z=%d %s gives x,y bgn %d,%d end %d,%d, span %d,%d\n", module,
        pbb->llz.zoom,
        get_bounding_box_stg(pbb),
        x_bgn,y_bgn,x_end,y_end,
        xcols, yrows );
    if ((xcols < MIN_SPAN) || (yrows < MIN_SPAN)) {
        SPRTF("%s: At this ZOOM %d, on DEM30 data, the BBOX is LESS THAN MIN_SPAN %dx%d (ie %d,%d) points!\n", module,
            zoom, MIN_SPAN, MIN_SPAN, xcols, yrows);
        return 2;
    }

    int x,y;
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    size_t doff;
    unsigned char r,g,b;
    if (!buffer) {
        SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
        check_me();
        return 1;
    }

    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    unsigned int colr;
    ok = s.begin_span_file(stopo30_fil);
    if (!ok) {
        SPRTF("%s: Failed to begin_span_file(%s)!\n", module, stopo30_fil );
        return 1;
    }

    s.verb = true;  // to see any error
    clear_used_colors();
    double bgn = get_seconds();
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
            if (!ok) {
                SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                check_me();
                return 1;
            }
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
    char *out_img = (char *)img_file;
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    if (!out_img)
        out_img = get_out_png();
    if (out_img) {
        if (writePNGImage24(out_img, width, height, bit_depth, color_type, buffer ))
            iret |= 8;
    }
#else // !USE_PNG_LIB
    if (!out_img)
        out_img = get_out_bmp();
    if (out_img) {
        if (writeBMPImage24(out_img, width, height, buffer, bsize))
            iret |= 4;
    }
#endif // USE_PNG_LIB y/n
    char *secs = get_seconds_stg( get_seconds() - bgn );
    SPRTF("%s: Processed span x,y %d,%d, %s bytes to %dx%d image in %s\n", module,
        xcols, yrows,
        get_NiceNumberStg(xcols * yrows),
        width, height, secs );
    delete buffer;
    out_used();

    return iret;
}

#endif // 0000000000000000000000000000


// Location              zoom level
// 
//typedef struct tagLOCZOOM {
//    std::string loc;
//    double lat,lon;
//    vSTG files;
//}LOCZOOM, *PLOCZOOM;
//typedef std::vector<LOCZOOM> vLZ;
//#ifndef EOL
//#define EOL << std::endl
//#endif


int do_stopo30_test5() // write slippy maps for elevtests locs, zooms 0 - 14
{
    int iret = 0;
    SPRTF("\n");
    SPRTF("%s: === do_stopo30_test5() =====================\n", module);
    PELEVTESTS pet = elevtests;
    double lat,lon;
    int zoom;
    int zoom_min = MIN_SLIPPY_ZOOM;
    int zoom_max = MAX_SLIPPY_ZOOM;
    //int iverb = 3 | 0x4000 | 0x8000;
    int iverb = 3;  // no chkcalc and no chkspan
    BNDBOX bb;
    char *cp;
    LOCZOOM lz;
    vLZ vlz;
    int loccnt = 0;
    char *tmp;

    while (pet->name) {
        SPRTF("\n");
        lat = pet->lat;
        lon = pet->lon;
        lz.loc = pet->name;
        lz.lat = lat;
        lz.lon = lon;
        lz.exp = pet->expected;
        lz.files.clear();
        SPRTF("%s: For %s, lat,lon %s,%s, elevation %d\n", module,
            pet->name,
            get_trim_double(lat),
            get_trim_double(lon),
            pet->expected );
        for (zoom = zoom_min; zoom < zoom_max; zoom++) {
            if (!get_map_bounding_box(lat,lon,zoom,&bb)) {
                SPRTF("%s: YEEK: No bounding box for lat,lon,zoom %s,%s,%d!\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    zoom );
                iret |= 2;
                continue;
            }
            if (!get_slippy_path( lat, lon, zoom, &cp )) {
                SPRTF("%s: YOW: No slippy path for lat,lon,zoom %s,%s,%d!\n", module,
                    get_trim_double(lat),
                    get_trim_double(lon),
                    zoom );
                iret |= 4;
                continue;
            }
            std::string img = get_image_file_path(cp);
            SPRTF("%s: Z=%d %s, for '%s' writing '%s'\n", module,
                zoom,
                get_bounding_box_stg(&bb,true),
                pet->name,
                img.c_str());
            if (is_file_or_directory((char *)img.c_str()) == 1) {
                SPRTF("%s: This file already EXISTS! Rename, delete, move to re-do!\n", module);
            } else {
                // service default to 256 x 256
                if (stopo30_bb_to_image( &bb, img.c_str(), iverb )) {
                    SPRTF("%s: Z=%d Written image %s\n", module, zoom, img.c_str());
                } else {
                    SPRTF("%s: Z=%d FAILED image %s\n", module, zoom, img.c_str());
                }
            }
            tmp = get_web_path(img.c_str());
            lz.files.push_back(tmp);
        }
        vlz.push_back(lz);
        loccnt++;
        pet++;
    }
    tmp = GetNxtBuf();
    sprintf(tmp,"Test %u View", tf_st30_test5);
    write_html_view(vlz, zoom_min, zoom_max,tmp);
    return iret;
}

// eof
