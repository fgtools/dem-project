// png_utils.cxx

#include <stdio.h>
#include <stdlib.h> // malloc()...
#include "utils.hxx"
#include "png_utils.hxx"

static const char *module = "png_utils";

#ifdef USE_PNG_LIB
#include <png.h>
#include "sprtf.hxx"

/*\
 * int writePNGImage(char *file, int width, int height, int bit_depth, int color_type,
 *   unsigned char *buf ) 
 *
 * NOTE: The buffer passed is a simple char array, of width * height
 * The char is either ON (255 - white) or OFF (0 - black)
 *
\*/
int writePNGImage(char *file, int width, int height, int bit_depth, int color_type,
    unsigned char *buf ) 
{
    double bgn = get_seconds();
    int y, x, z;
    unsigned char ch;
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!png_ptr) {
        SPRTF("%s: writePNGImage: failed to create write structure!\n", module);
        return 1;
    }
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        SPRTF("%s: writePNGImage: failed to create info structure!\n", module);
        return 1;
    }
    FILE *fp = fopen(file, "wb");
    if (!fp) {
        SPRTF("%s: writePNGImage: Can NOT create file %s!\n", module, file);
        return 1;
    }

    SPRTF("\n");
    SPRTF("%s: writePNGImage: Writing PNG to %s, w=%d, h=%d, b=%d, c=%d\n", module,
        file, width, height, bit_depth, color_type);

    png_init_io(png_ptr, fp);

    /* write header */
    png_set_IHDR(png_ptr, info_ptr, width, height,
        bit_depth, color_type, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);

    /* write bytes */
    png_bytep *row_pointers;
    // <allocate row_pointers and store each row of your image in it>
    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    if (!row_pointers) {
        SPRTF("%s: writePNGImage: Memory allocation failed!\n", module);
        return 1;
    }

    // start first, work upwards
    for (y = 0; y < height; y++) {
        row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));
        if (!row_pointers[y]) {
            SPRTF("%s: writePNGImage: Memory allocation failed!\n", module);
            return 1;
        }
        // copy the data into the row pointer
        png_byte* row = row_pointers[y];
        // start first column, work upwards
        for (x = 0; x < width; x++) {
            png_byte* dst = &row[x*4];
            // this is upside down
            //png_byte* src = &buf[(y*width)+x];
            // try inverting the source pointer
            // png_byte* src = &buf[((height - 1 - y) * width) + (width - 1 - x)];
            png_byte* src = &buf[((height - 1 - y) * width) + x];
            // Set the RGBA values - but sets lines to soft gray
            ch = *src;
            //for (z = 0; z < 4; z++)
            // Maybe only set RGB
            for (z = 0; z < 3; z++)
                dst[z] = ch;
            // now lines a black, but background is soft gray
            // try if (ch) dst[3] = 0; // NO sent the whole image gray UGLY ;=((
        }
    }

    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, NULL);

    /* cleanup heap allocation */
    for (y = 0; y < height; y++)
        free(row_pointers[y]);
    free(row_pointers);
    fclose(fp); // close file
    // should also add png free and destroy
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    SPRTF("%s: writePNGImage: Written PNG to %s in %s\n", module,
        file, 
        get_seconds_stg( get_seconds() - bgn ));

    return 0;
}

/*\
 * int writePNGImage(char *file, int width, int height, int bit_depth, int color_type,
 *   unsigned char *buf ) 
 *
 * NOTE: The buffer passed is a simple char array, of 3 * width * height
 * The 3 chars are the color for that pixel (0 - 255)
 *
 * Code for adding a 'title'
 * see : http://www.labbookpages.co.uk/software/imgProc/libPNG.html
 * This is done after png_set_IHDR(...) and before png_write_info(png_ptr, info_ptr);
    // Set title
    if (title != NULL) {
       png_text title_text;
       title_text.compression = PNG_TEXT_COMPRESSION_NONE;
       title_text.key = "Title";
       title_text.text = title;
       png_set_text(png_ptr, info_ptr, &title_text, 1);
    }
 *
 *
\*/
int writePNGImage24(char *file, int width, int height, int bit_depth, int color_type,
    unsigned char *buf3 ) 
{
    double bgn = get_seconds();
    int y, x;
    //int z, s;
    char r,g,b; // notional - may be in reverse!
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!png_ptr) {
        SPRTF("%s: writePNGImage24: failed to create write structure!\n", module);
        return 1;
    }
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        SPRTF("%s: writePNGImage24: failed to create info structure!\n", module);
        return 1;
    }

    FILE *fp = fopen(file, "wb");
    if (!fp) {
        SPRTF("%s: writePNGImage24: Can NOT create file %s!\n", module, file);
        return 1;
    }

    // SPRTF("\n");
    SPRTF("%s: writePNGImage24: Writing PNG to %s, w=%d, h=%d, b=%d, c=%d\n", module,
        file, width, height, bit_depth, color_type);

    /* ============================================
        could setup a C exception handler, like -
        // Setup Exception handling
        if (setjmp(png_jmpbuf(png_ptr))) {
            fprintf(stderr, "Error during png creation\n");
            code = 1;
            goto finalise;
        }
      ============================================== */

    png_init_io(png_ptr, fp);

    /* write header */
    png_set_IHDR(png_ptr, info_ptr, width, height,
        bit_depth, color_type, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    /* ======================================
       could insert 'title' here - see above 
       ====================================== */

    /* write the 'info' */
    png_write_info(png_ptr, info_ptr);


    /* write bytes */

    /* ====================================================================
        this could be done row by row, where only ONE row need be allocated
      // Allocate memory for one row (3 bytes per pixel - RGB)
      // need to change color_type to PNG_COLOR_TYPE_RGB
      png_bytep row = (png_bytep) malloc(3 * width * sizeof(png_byte));
      // Write image data
      int x, y;
      for (y=0 ; y<height ; y++) {
         for (x=0 ; x<width ; x++) {
           setRGB(&(row[x*3]), buffer[y*width + x]);
         }
         png_write_row(png_ptr, row);
       }

       OR
       as here setup a set of row pointers
       copy the data - here using RGBA - 4-bytes per pixel
       ---------------------------------------------------
       ==================================================================== */
    png_bytep *row_pointers;
    // <allocate row_pointers and store each row of your image in it>
    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    if (!row_pointers) {
        SPRTF("%s: writePNGImage24: Memory allocation failed!\n", module);
        return 1;
    }

    // start first, work upwards
    for (y = 0; y < height; y++) {
        row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));
        if (!row_pointers[y]) {
            SPRTF("%s: writePNGImage24: Memory allocation failed!\n", module);
            return 1;
        }
        // copy the data into the row pointer
        png_byte* row = row_pointers[y];
        // start first column, work upwards
        for (x = 0; x < width; x++) {
            png_byte* dst = &row[x*4];
            // this is upside down
            //png_byte* src = &buf[(y*width)+x];
            // try inverting the source pointer
            // png_byte* src = &buf[((height - 1 - y) * width) + (width - 1 - x)];
            png_byte* src3 = &buf3[((height - 1 - y) * width * 3) + (x*3)];
            // Set the RGBA values - but sets lines to soft gray
            // set RGB
            r = src3[0];
            g = src3[1];
            b = src3[2];
            // is this the right order?
            dst[0] = b;
            dst[1] = g;
            dst[2] = r;
            dst[3] = 255;
        }
    }

    png_write_image(png_ptr, row_pointers);

    /* end write */
    png_write_end(png_ptr, NULL);

    /* cleanup heap allocation */
    for (y = 0; y < height; y++)
        free(row_pointers[y]);
    free(row_pointers);
    fclose(fp); // close file
    // should also add png free and destroy
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    SPRTF("%s: writePNGImage24: Written PNG to %s, in %s\n", module,
        file,
        get_seconds_stg( get_seconds() - bgn ));

    return 0;
}

#endif //#ifdef USE_PNG_LIB
// eof
