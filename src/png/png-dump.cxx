/*\
 * png-dump.cxx
 *
 * Copyright (c) 2015 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _MSC_VER
#include <unistd.h>
#endif // !_MSC_VER
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h> // for strdup(), ...
#include <png.h>
#include "sprtf.hxx"
#include "png-dump.hxx"

static const char *module = "png-dump";
static const char *def_log = "temppngd.txt";
static const char *usr_input = 0;

void give_help( char *name )
{
    SPRTF("%s: usage: [options] usr_input\n", module);
    SPRTF("Options:\n");
    SPRTF(" --help  (-h or -?) = This help and exit(2)\n");
    // TODO: More help
}

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            // TODO: Other arguments
            default:
                SPRTF("%s: Unknown argument '%s'. Tyr -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                SPRTF("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(arg);
        }
    }
    if (!usr_input) {
        SPRTF("%s: No user input found in command!\n", module);
        return 1;
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////////
// abort application on error

void abort_(const char * s, ...)
{
        va_list args;
        va_start(args, s);
        vfprintf(stderr, s, args);
        fprintf(stderr, "\n");
        va_end(args);
        abort();
}

//   Color    Allowed    Interpretation
//   Type    Bit Depths
typedef struct tagPNGCT {
    int color_type;
    const char *bit_depths;
    const char *desc;
}PNGCT, *PPNGCT;

static PNGCT pngct[] = {
    { 0, "1,2,4,8,16",  "Each pixel is a grayscale sample." },
    { 2, "8,16",        "Each pixel is an R,G,B triple."    },
    { 3, "1,2,4,8",     "Each pixel is a palette index. "
                        "a PLTE chunk must appear."         },
    { 4, "8,16",        "Each pixel is a grayscale sample, "
                        "followed by an alpha sample."      },
    { 6, "8,16",        "Each pixel is an R,G,B triple, "
                        "followed by an alpha sample."      },

    // table terminator - must be last
    { 0, 0, 0 }
};
#ifndef ISDIGIT
#define ISDIGIT(a) (( a >= '0' ) && ( a <= '9' ))
#endif

void show_color_type( int ct, int dep )
{
    PPNGCT pct = pngct;
    while (pct->desc) {
        if (pct->color_type == ct) {
            size_t i, len = strlen(pct->bit_depths);
            bool is_ok = false;
            for (i = 0; i < len; i++) {
                if (ISDIGIT(pct->bit_depths[i])) {
                    int chk = atoi(&pct->bit_depths[i]);
                    while (ISDIGIT(pct->bit_depths[i+1]))
                        i++;
                    if (chk == dep) {
                        is_ok = true;
                        break;
                    }
                }
            }
            if (!is_ok)
                SPRTF("Warning: color_type NOT matched '%s'\n", pct->bit_depths);
            SPRTF("%s\n", pct->desc );
            return;
        }
        pct++;
    }
    abort_("[read_png_file] color type or bit depth error!");

}

int read_png_file(const char* file_name)
{
    int x, y;

    int width, height, row_bytes, png_row_bytes, row_steps;
    png_byte color_type;
    png_byte bit_depth;
    int wrap, col_wrap = 10;
    png_structp png_ptr;
    png_infop info_ptr;
    int number_of_passes;
    png_bytep *row_pointers;
    char header[8];    // 8 is the maximum size that can be checked

    /* open file and test for it being a png */
    FILE *fp = fopen(file_name, "rb");
    if (!fp)
            abort_("[read_png_file] File %s could not be opened for reading", file_name);

    fread(header, 1, 8, fp);
    if (png_sig_cmp((png_bytep)header, 0, 8))
            abort_("[read_png_file] File %s is not recognized as a PNG file", file_name);

    /* initialize stuff */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr)
            abort_("[read_png_file] png_create_read_struct failed");

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
            abort_("[read_png_file] png_create_info_struct failed");

    if (setjmp(png_jmpbuf(png_ptr)))
            abort_("[read_png_file] Error during init_io");

    SPRTF("%s: Reading: %s\n", module, file_name);
    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);

    png_read_info(png_ptr, info_ptr);

    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    color_type = png_get_color_type(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);

    number_of_passes = png_set_interlace_handling(png_ptr);

    SPRTF("%s: width %d, height %d, color_type %d, bit_depth %d, passes %d\n", module,
        width, height, color_type, bit_depth, number_of_passes);
    show_color_type( color_type, bit_depth );
    row_steps = bit_depth / 8;  // is it 1 or 2 byte steps

    if (color_type == 3) {
        // TODO: dump palette type
        SPRTF("%s: color type 3 NOT IMPLEMENTED YET!\n", module);
        exit(1);
    } else if (color_type == 0) {
        row_bytes = 2 * width;
    } else if (color_type == 4) {
        row_bytes = 3 * width;
    } else if (color_type == 2) {
        if (!row_steps || (row_steps > 2)) {
            SPRTF("%s: color type %d should have 1 or 2 row steps! NOT %d\n", module, color_type, row_steps);
            exit(1);
        }
        row_bytes = 3 * row_steps * width;  // RGB (3 * row_steps)
    } else if (color_type == 6) {
        if (!row_steps || (row_steps > 2)) {
            SPRTF("%s: color type %d should have 1 or 2 row steps! NOT %d!\n", module, color_type, row_steps);
            exit(1);
        }
        row_bytes = 4 * row_steps * width;  // RGBA (4 * 2)
    } else {
        SPRTF("%s: color_type = %d NOT IMPLEMENTED! *** FIX ME ***\n", module, color_type );
        exit(1);
    }

    png_read_update_info(png_ptr, info_ptr);

    /* read file */
    if (setjmp(png_jmpbuf(png_ptr)))
            abort_("[read_png_file] Error during read_image");

    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    if (!row_pointers)
            abort_("[read_png_file] Error allocation of pointer memory");
    png_row_bytes = png_get_rowbytes(png_ptr,info_ptr);

    for (y=0; y<height; y++) {
            row_pointers[y] = (png_byte*) malloc(png_row_bytes);
            if (!row_pointers[y])
                abort_("[read_png_file] Error allocation of row memory");
    }
    SPRTF("%s: Allocated %d row buffers, of %d (%d) bytes each (bpp=%d)\n", module, height, png_row_bytes, row_bytes, (png_row_bytes / width));

    png_read_image(png_ptr, row_pointers);

    fclose(fp);
    // dump it
    // The sample depth is the same as the bit depth except in the case of color type 3, in which the sample depth is always 8 bits.
    // For color types 0 and 4 (grayscale, with or without alpha), bKGD contains:
    //    Gray:  2 bytes, range 0 .. (2^bitdepth)-1
    // (For consistency, 2 bytes are used regardless of the image bit depth.) The value is the gray level to be used as background.
    //
    // For color types 2 and 6 (truecolor, with or without alpha), bKGD contains:
    //   Red:   2 bytes, range 0 .. (2^bitdepth)-1
    //   Green: 2 bytes, range 0 .. (2^bitdepth)-1
    //   Blue:  2 bytes, range 0 .. (2^bitdepth)-1
    // (For consistency, 2 bytes per sample are used regardless of the image bit depth.) This is the RGB color to be used as background.
    //
    // Or in SIMPLE terms
    // 32-bit RGBA, each row is (width * 4 bytes) long, for 24-bit RGB it's (width * 3 bytes) long
    if (color_type == 3) {
        // TODO: dump palette type
        SPRTF("%s: color type 3 NOT IMPLEMENTED YET!\n", module);
        exit(1);
    } else {
        int off, k;
        for (y = 0; y < height; y++) {
            png_byte *pb = row_pointers[y];
            unsigned short *ps;
            png_byte colr;
            png_byte *pc;
            SPRTF("Row %d: ", (y + 1));
            wrap = 0;
            for (x = 0; x < width; x++ ) {
                if ((color_type == 0) || (color_type == 4)) {
                    SPRTF("%s: color types 0 and 4 NOT IMPLEMENTED YET!\n", module);
                    exit(1);
                } else { // if ((color_type == 2) || (color_type == 6)) {
                    off = x * 3 * row_steps;
                    if (color_type == 6) {
                        off += x * row_steps;
                        ps = (unsigned short *)&pb[off];
                        pc = &pb[off];
                        for (k = 0; k < 4; k++) {
                            if (row_steps == 1)
                                colr = (pc[k] & 0xff);
                            else
                                colr = (ps[k] & 0xff);
                            if (k) {
                                if ((k+1) == 4) {
                                    SPRTF("%03u)", colr );
                                } else {
                                    SPRTF("%03u,", colr );
                                }
                            } else {
                                SPRTF("(%03u,", colr );
                            }
                        }
                    } else {
                        ps = (unsigned short *)&pb[off];
                        pc = &pb[off];
                        for (k = 0; k < 3; k++) {
                            if (row_steps == 1)
                                colr = (pc[k] & 0xff);
                            else
                                colr = (ps[k] & 0xff);
                            if (k) {
                                if ((k+1) == 3) {
                                    SPRTF("%03u)", colr );
                                } else {
                                    SPRTF("%03u,", colr );
                                }
                            } else {
                                SPRTF("(%03u,", colr );
                            }
                        }
                    }
                }
                wrap++;
                if (wrap >= col_wrap) {
                    SPRTF("\n     ");
                    wrap = 0;
                }
            }
            if (wrap)
                SPRTF("\n");
        }
    }
    // clean up
    for (y=0; y<height; y++) {
            free(row_pointers[y]);
    }
    free(row_pointers);
    return 0;
}

// SAMPLE: F:\FGx\jaanga\terrain-srtm30-plus-data-tms-1-7\0\0-512x256.png
// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)def_log,false);
    iret = parse_args(argc,argv);
    if (iret)
        return iret;

    iret = read_png_file(usr_input);

    // clean up
    free((void *)usr_input);
    return iret;
}


// eof = png-dump.cxx
