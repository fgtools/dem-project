// pbm_utils.hxx
#ifndef _PBM_UTILS_HXX_
#define _PBM_UTILS_HXX_

// return 0 = SUCCESS, 1 = bad params, 2 = create failed, 3 = write failed
extern int writePBMImage24( const char *file, int width, int height, unsigned char *buffer, size_t bsize, 
    bool ascii = false, bool verb = false);

#endif // #ifndef _PBM_UTILS_HXX_
// eof - pbm_utils.hxx
