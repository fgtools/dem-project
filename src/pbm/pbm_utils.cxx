/*\
    pbm_utils.cxx

    Arguably the simplest graphic format of them all. Seems all forms are handled by utilities
    like XnView (http://www.xnview.com/en/)

    see : http://en.wikipedia.org/wiki/Netpbm_format

    Here will ONLY output -
    Portable PixMap - P3 ASCII and P6 binary .ppm 0�255 (RGB) 

\*/

#include <stdio.h>
#include <string>
#ifndef _MSC_VER
#include <stdlib.h> // for exit(), ...
#include <string.h> // for strlen(), ...
#endif
#include "sprtf.hxx"
#include "pbm_utils.hxx"

static const char *module = "pbm_utils";

static int check_me()
{
    int i;
    SPRTF("%s: Any key to continue!\n", module);
    getchar();
    i = 0;
    return i;
}

// return 0 = SUCCESS, 1 = bad params, 2 = create failed, 3 = write failed
int writePBMImage24( const char *file, int width, int height, unsigned char *buffer, size_t bsize, 
    bool ascii, bool verb )
{
    const char *magic;
    size_t len,res,total = 0;
    char *cp;
    // a parameter check
    if ( !file || (*file == 0) || !buffer || ((size_t)(width * height * 3) != bsize) ) {
        if (verb) SPRTF("%s: Bad parameter!\n", module);
        return 1;
    }
    std::string f(file);
    res = f.rfind('.');
    if (res > 0) {
        f = f.substr(0,res);
    }
    f += ".ppm";

    FILE *fp = fopen(f.c_str(),"wb");
    if (!fp) {
        if (verb) SPRTF("%s: FAILED to create file [%s]!\n", module, f.c_str() );
        return 2;
    }
    if (ascii) {
        magic = "P3\n";
    } else {
        magic = "P6\n";
    }
    len = strlen(magic);
    res = fwrite(magic,1,len,fp);
    if (res != len) {
        fclose(fp);
        if (verb) SPRTF("%s: File write failed! req %d, got %d\n", module, (int)len, (int)res);
        return 3;
    }
    total += res;

    cp = GetNxtBuf();
    len = sprintf(cp,"%d %d\n", width, height );
    res = fwrite(cp,1,len,fp);
    if (res != len) {
        fclose(fp);
        if (verb) SPRTF("%s: File write failed! req %d, got %d\n", module, (int)len, (int)res);
        return 3;
    }
    total += res;

    if (ascii) {
        int h,w;
        size_t off;
        len = sprintf(cp,"%d\n", 255 );
        res = fwrite(cp,1,len,fp);
        if (res != len) {
            fclose(fp);
            if (verb) SPRTF("%s: File write failed! req %d, got %d\n", module, (int)len, (int)res);
            return 3;
        }
        total += res;
        //for (h = 0; h < height; h++) {  // TODO: Check if this needs to be INVERTED
        for (h = height - 1; h >= 0; h--) {  // TODO: Check if this needs to be INVERTED
            for (w = 0; w < width; w++) {
                off = (h * width * 3) + (w * 3);
                if ((off + 3) > bsize) {
                    SPRTF("%s: ARGH! A BAD calculation h=%d, w=%d, off %d on %d\n", module,
                        h, w, (int)off, (int)bsize);
                    check_me();
                    exit(1);
                }
                len = sprintf(cp,"%d %d %d ",
                    buffer[off] & 0xff,
                    buffer[off+1] & 0xff,
                    buffer[off+2] & 0xff);
                if ((w + 1) == width)
                    len--;
                res = fwrite(cp,1,len,fp);
                if (res != len) {
                    fclose(fp);
                    if (verb) SPRTF("%s: File write failed! req %d, got %d\n", module, (int)len, (int)res);
                    return 3;
                }
                total += res;
            }
            len = sprintf(cp,"\n");
            res = fwrite(cp,1,len,fp);
            if (res != len) {
                fclose(fp);
                if (verb) SPRTF("%s: File write failed! req %d, got %d\n", module, (int)len, (int)res);
                return 3;
            }
            total += res;
        }
    } else {
        // TODO: Check if this needs to be rwo/col INVERTED!
        len = bsize;
        res = fwrite(buffer,1,len,fp);
        if (res != len) {
            fclose(fp);
            if (verb) SPRTF("%s: File write failed! req %d, got %d\n", module, (int)len, (int)res);
            return 3;
        }
        total += res;
    }
    fclose(fp);

    if (verb) SPRTF("%s: Written file [%s], %d bytes.\n", module, f.c_str(), (int)total);

    return 0;   // SUCCESS

}

// eof - pbm_utils.cxx
