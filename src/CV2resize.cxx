// CV2resize.cxx
// Using OpenCV library, IF available

#include <string>
#include <iostream>
#include <stdio.h>

#ifdef USE_CV2_LIB
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "CV2resize.hxx"
#include "sprtf.hxx"

using namespace cv;

/// Global variables
Mat src, dst;
Mat map_x, map_y;

void update_map( float min, float max )
{
    for( int j = 0; j < src.rows; j++ ) { 
        for( int i = 0; i < src.cols; i++ ) {
            if(( i > src.cols*min ) && ( i < src.cols*max ) &&
               ( j > src.rows*min ) && ( j < src.rows*max )) {
                map_x.at<float>(j,i) = 2*( i - src.cols*min ) + 0.5f ;
                map_y.at<float>(j,i) = 2*( j - src.rows*min ) + 0.5f ;
            } else {
                map_x.at<float>(j,i) = 0 ;
                map_y.at<float>(j,i) = 0 ;
            }
        }
    }
}

bool resize_to_scale( std::string file, float scale, std::string out )
{
    int y, x;
    // Load the image
    src = imread( file, 1 );
    if (src.empty()) {
        SPRTF("Failed to load file %s\n", file.c_str());
        return false;
    }
    y = cvRound(src.rows/scale);
    x = cvRound(src.cols/scale);
    // specify fx and fy and let the function compute the destination image size.
    // resize(src, dst, Size(), 0.5, 0.5, interpolation);
    // smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
    // resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    // To shrink an image, it will generally look best with CV_INTER_AREA interpolation,
    dst.create(y, x, src.type());
    resize(src, dst, dst.size(), 0, 0, INTER_AREA );
    if (!imwrite( out, dst )) {
        SPRTF("Failed to write file %s\n", out.c_str());
        return false;
    }

    SPRTF("Loaded file %s, scaled by %f, written %s\n", file.c_str(), scale, out.c_str());
    return true;

}

// 0.25 min 0.75 max
bool resize_file( std::string file, float min, float max, std::string out )
{
    // Load the image
    src = imread( file, 1 );
    if (src.empty()) {
        SPRTF("Failed to load file %s\n", file.c_str());
        return false;
    }

    // Create dst, map_x and map_y with the same size as src:
    dst.create( src.size(), src.type() );
    map_x.create( src.size(), CV_32FC1 );
    map_y.create( src.size(), CV_32FC1 );
    update_map( min, max );
    remap( src, dst, map_x, map_y, INTER_LINEAR, BORDER_CONSTANT, Scalar(0, 0, 0) );

    // now write the dst ???
    if (!imwrite( out, dst )) {
        SPRTF("Failed to write file %s\n", out.c_str());
        return false;
    }

    SPRTF("Loaded file %s, resized %f,%f, written %s\n", file.c_str(), min, max, out.c_str());
    return true;
}

#else // NO OpenCV library
bool resize_to_scale( std::string file, float scale, std::string out )
{
    SPRTF("resize_to_scale: NO OpenCVv LIBRARY FOUND!\n");
    return false;
}
// 0.25 min 0.75 max
bool resize_file( std::string file, float min, float max, std::string out )
{
    SPRTF("resize_file: NO OpenCV LIBRARY FOUND!\n");
    return false;
}
#endif // USE_CV2_LIB
// eof
