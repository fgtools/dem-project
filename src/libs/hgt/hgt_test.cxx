/*\
 * hgt_test.cxx
 *
 * Copyright (c) 2017 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
 * load an show a HGT file
 *
\*/

#include <stdio.h>
#include <string.h> // for strdup(), ...
#include <sstream> // for #include <iostream>
#include <algorithm> // tranform()
#include "sprtf.hxx"

#include "hgt.hxx"

// other includes

static const char *module = "hgt_test";

static const char *usr_input = 0;
static int hgt_res = 3;
static int verbosity = 0;
static const char *def_log = "temphgt.txt";

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)


void give_help( char *name )
{
    printf("%s: usage: [options] usr_input\n", module);
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(0)\n");
    printf(" --res 1       (-r) = Resolution - 1 or 3 (def=%d)\n", hgt_res);
    printf(" --verb[num]   (-v) = Bump or set verbosity. (def=%d)\n", verbosity);
    // TODO: More help
}

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'r':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    if ((strlen(sarg) == 1) && ((*sarg == '1') || (*sarg == '3'))) {
                        hgt_res = atoi(sarg);
                        if ((hgt_res == 1) || (hgt_res == 3)) {
                            if (VERB2)
                                printf("[v2] Set hgt resolution to %d\n", hgt_res);
                        }
                        else {
                            printf("Error: Expected resolution 1 or 3 to follow %s! Not %d\n", arg, hgt_res);
                            return 1;
                        }
                    }
                    else {
                        printf("Error: Expected resolution 1 or 3 to follow %s! Not %s\n", arg, sarg);
                        return 1;
                    }

                }
                else {
                    printf("Error: Expected resolution 1 or 3 to follow %s!\n", arg);
                    return 1;
                }
                break;
            // TODO: Other arguments
            default:
                printf("%s: Unknown argument '%s'. Try -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                printf("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(arg);
        }
    }
    if (!usr_input) {
        printf("%s: No user input found in command!\n", module);
        return 1;
    }
    return 0;
}

int show_hgt(const char *file)  // actions of app
{
    TGHgt hgt(hgt_res);
    if (!hgt.open(SGPath(file))) {
        SPRTF("%s: Failed to open file '%s'!\n", module, file);
        return 1;
    }
    if (!hgt.load()) {
        SPRTF("%s: Failed to load file '%s'!\n", module, file);
        return 1;
    }
    SPRTF("%s: Loaded file '%s'!\n", module, file);

    double ox = hgt.get_originx();
    double oy = hgt.get_originy();
    int cols = hgt.get_cols();
    int rows = hgt.get_rows();
    double col_step = hgt.get_col_step();
    double row_step = hgt.get_row_step();
    double lon = ox / 3600.0;
    double lat = oy / 3600.0;
    std::stringstream out;

    out << "lat " << lat << ", lon " << lon << ", rows " << rows << ", cols " << cols <<
        ", row_step " << row_step << ", col_step " << col_step;
    SPRTF("%s\n", out.str().c_str());
    out.str(""); // stringstream clear the stream

    int row, col;
    int elev;
    int elements = 0;
    int max_elev = -32768;
    int min_elev = 32768;
    int void_elev = -32768;
    int void_count = 0;
    int zero_count = 0;
    for (row = 0; row < rows; row++) {
        out << "Row " << (row + 1) << ": ";
        for (col = 0; col < cols; col++) {
            elements++;
            elev = hgt.height(row, col);
            //out << elev << " ";
            if (elev <= void_elev) {
                out << "VOID ";
                void_count++;
            }
            else {
                out << elev << " ";
                if (elev > max_elev)
                    max_elev = elev;
                if (elev < min_elev)
                    min_elev = elev;
                if (elev == 0)
                    zero_count++;
            }

        }
        SPRTF("%s\n", out.str().c_str());
        out.str(""); // stringstream clear the stream
    }
    //out << "Shown " << elements << " elements";
    out << "Scanned " << elements << ", void " << void_count << ", zero " << zero_count <<
        ", max " << max_elev << ", min " << min_elev;

    SPRTF("%s\n", out.str().c_str());
    out.str(""); // stringstream clear the stream
    // hgt.close();
    return 0;
}


// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)def_log, false);
    iret = parse_args(argc,argv);
    if (iret) {
        if (iret == 2)
            iret = 0;
        return iret;
    }

    iret = show_hgt(usr_input);  // actions of app

    return iret;
}


// eof = hgt_test.cxx
