// load-hgt.hxx
// Just an experimental load of a HGT file, and write it to image files
// But this will eventually be replaced by the hgt3File class
#ifndef _LOAD_HGT_HXX_
#define _LOAD_HGT_HXX_


extern int load_hgt( std::string file );
extern int hgt_to_images();
extern void delete_hgt_data();


#endif // #ifndef _LOAD_HGT_HXX_
// eof

