// load-hgt.cxx

// Just an experimental load of a HGT file, and write it to image files

// But this will eventually be replaced by the hgt3File class

#include <string>
#ifndef _MSC_VER
#include <stdlib.h> // for exit(), ...
#include <string.h> // for memset(), ...
#include <stdio.h> // for fopen(), ...
#include <climits> // for SHRT_MAX, ...
#endif
#include "utils.hxx"
#include "sprtf.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "hgt3File.hxx"
#include "color.hxx"
#include "dir_utils.hxx"
#include "dem_utils.hxx"
#include "load-hgt.hxx"

static short (*hgt_data)[HGT3_SIZE_1];
static int got_lat_lon = 0;
static double file_lat,file_lon;
// voids indicated by -32768
static short a_void = -13813;
static bool use_get_color = false;

void delete_hgt_data() 
{
    if (hgt_data)
        delete [] hgt_data;
    hgt_data = 0;
}

int hgt_to_images()
{
    int iret = 0;
    int x,y;
    short elev;
    short min_elev = SHRT_MAX;
    short max_elev = SHRT_MIN;
    short *sp;
    int void_cnt = 0;
    int point_cnt = 0;
    //int max_steps = 10;
    //std::string tmp;
    for (x = 0; x < HGT3_SIZE_1; x++) {
        for (y = 0; y < HGT3_SIZE_1; y++) {
            point_cnt++;
            sp = &hgt_data[x][y];
            if (do_byte_swap) {
                SWAP16(sp);
            }
            //tmp = get_hex_str((unsigned char *)sp, 2);
            elev = *sp;
            if (elev <= a_void) {
                void_cnt++;
            } else {
                if (elev > max_elev)
                {
                    max_elev = elev;
                }
                if (elev < min_elev) 
                {
                    min_elev = elev;
                }
            }
        }
    }
    //SPRTF("Max short %d, Min short %d\n", SHRT_MAX, SHRT_MIN );
    SPRTF("Maximum %d, Minimum %d metres", max_elev, min_elev);
    if (void_cnt)
        SPRTF(", with %d of %d as voids", void_cnt, point_cnt);
    SPRTF(".\n");
    // set color range ZERO_COLOR = 20
    unsigned int steps = max_elev / ZERO_COLOR;
    if (max_elev % ZERO_COLOR)
        steps++;
    size_t bsize = HGT3_SIZE_1 * HGT3_SIZE_1 * 3;
    unsigned char *buffer = new unsigned char[bsize+1];
    size_t doff, coff;
    unsigned int colr;
    unsigned char r,g,b;
    clear_used_colors();
    for (y = HGT3_SIZE_1 - 1; y >= 0; y--) {
    //    for (x = HGT3_SIZE_1 - 1; x >= 0; x--) {
    //for (y = 0; y < HGT3_SIZE_1; y++) {
        for (x = 0; x < HGT3_SIZE_1; x++) {
            //sp = &hgt_data[x][y];
            sp = &hgt_data[y][x];
            //if (do_byte_swap) { SWAP16(sp); } // already been swapped
            elev = *sp;
            doff = ((HGT3_SIZE_1 - 1 - y) * HGT3_SIZE_1 * 3) + (x * 3);
            //doff = ((HGT3_SIZE_1 - 1 - y) * HGT3_SIZE_1 * 3) + ((HGT3_SIZE_1 - 1 - x) * 3);
            //doff = (y * HGT3_SIZE_1 * 3) + ((HGT3_SIZE_1 - 1 - x) * 3);
            //doff = (y * HGT3_SIZE_1 * 3) + (x * 3); // temp2.bmp
            //doff = (x * HGT3_SIZE_1 * 3) + (y * 3);
            if ((doff + 3) > bsize) {
                SPRTF("UGH: At x %d, y %d, exceeding buffer %d on %d\n", x, y, (int)doff, (int)bsize );
                check_me();
                exit(1);
            }
            if (elev > 0) {
                coff = elev / steps;
                if (coff >= MAX_COLORS) {
                    SPRTF("YEEK: At x %d, y %d, elev %d, max %d, exceeding color %d on %d\n", x, y, elev, max_elev, (int)coff, MAX_COLORS );
                    check_me();
                    exit(1);
                }
            } else {
                coff = ZERO_COLOR;
            }

            if (use_get_color) {

                colr = get_color(elev,coff);
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = b;
                buffer[doff+1] = g;
                buffer[doff+2] = r;

            } else {

                colr = get_BGR_color(elev);
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = r;
                buffer[doff+1] = g;
                buffer[doff+2] = b;
            }
        }
    }
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    char *out_png = get_out_png();
    if (out_png) {
        iret |= writePNGImage24(out_png, HGT3_SIZE_1, HGT3_SIZE_1, bit_depth, color_type, buffer ); 
    }
#else // !USE_PNG_LIB
    char *out_bmp = get_out_bmp();
    if (out_bmp) {
        iret = writeBMPImage24(out_bmp, HGT3_SIZE_1, HGT3_SIZE_1, buffer, bsize);
    }
#endif // USE_PNG_LIB y/n

    delete buffer;
    out_used();
    return iret;
}


int load_hgt( std::string file )
{
    int iret = 0;
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        SPRTF("Unable to 'stat' file %s\n", file.c_str());
        return 1;
    }
    size_t len = get_last_file_size();
    if (len != (HGT3_SIZE_1 * HGT3_SIZE_1 * sizeof(short))) {
        SPRTF("File %s is wrong size %d! Should be %d (size of short %d)\n", file.c_str(), (int)len,
            (int)(HGT3_SIZE_1 * HGT3_SIZE_1 * sizeof(short)), (int)(sizeof(short)));
        return 1;

    }
    if (!got_lat_lon) {
        if (!get_hgt_lat_lon(file, &file_lat, &file_lon)) {
            SPRTF("Faild to get lat,lon from name %s!\n", file.c_str());
            SPRTF("Needs to be in form [N|S]nn[E|W]nnn.hgt\n");
            return 1;
        }
        got_lat_lon = 1;

    }

    set_out_files( file );

    FILE *fp = fopen(file.c_str(), "rb");
    if (!fp) {
        SPRTF("Unable to 'open' file %s\n", file.c_str());
        return 1;
    }
    hgt_data = new short[HGT3_SIZE_1][HGT3_SIZE_1];
    size_t res = fread(hgt_data,1,len,fp);
    if (res != len) {
        SPRTF("Read of file %s FAILED! Requested %d, got %d\n", file.c_str(), (int)len, (int)res);
        iret = 1;
        goto exit;
    }
    SPRTF( "Loaded file %s, len %d, for lat %d to %d, lon %d to %d\n",
        file.c_str(), (int)len,
        (int)file_lat, (int)(file_lat + 1),
        (int)file_lon, (int)(file_lon + 1) );
exit:
    fclose(fp);
    return iret;
}

// eof
