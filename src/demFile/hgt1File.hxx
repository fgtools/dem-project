// hgt1File.hxx
#ifndef _HGT1FILE_HXX_
#define _HGT1FILE_HXX_
#include <string>
#include "dem_utils.hxx"    // for MPOS

#define HGT1_SIZE 3600
#define HGT1_SIZE_1 3601
#define HGT_1DEM_SIZE (HGT1_SIZE_1 * HGT1_SIZE_1 * 2)
#define HGT_VOID -32768
#define BAD_LAT_LON 200.0
#define HGT1_XDIM  (1.0 / (double)HGT1_SIZE)
#define HGT1_YDIM  (1.0 / (double)HGT1_SIZE)

#define MX_VOIDS    16

class hgt1File {
public:
    hgt1File();
    hgt1File(std::string file);
    ~hgt1File();

    // functions
    bool load();
    bool load_file(std::string in_file);
    bool get_elevation_m( double lat, double lon, short *pelev );
    int hgt_to_image(); // 0=success, bit 1=no file, bit 2=no data, bit 4=write bmp failed, bit 8=write png failed
    void gen_void_report();
    bool set_file(std::string in_file) { file = in_file; return true; }
    bool set_lat_lon( double lat, double lon );
    bool get_min_max_voids( int *pmax = 0, int *pmin = 0, int *pvoids = 0 );
    bool set_hgt_dir( std::string dir );
    bool get_elevation( double lat, double lon, short *ps );
    bool scan_hgt_file( std::string hgt, double flat, double flon );
    bool search_hgt( void *vp ); // TODO: Not yet implemented - just a thought...

    ////////////////////////////////////////////////////////////////////////////
    bool begin_span_file( std::string srtm );
    bool end_span_file();
    // this could lead to more sofisticated 'averaging' of elevations
    bool get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps );
    // vars - valid after a span has been done
    bool sp_got_ll;
    short sp_maxe,sp_mine;
    int sp_maxx, sp_maxy, sp_minx, sp_miny;
    double sp_max_lat,sp_max_lon,sp_min_lat,sp_min_lon;
    double sp_file_lat, sp_file_lon;
    int sp_count, sp_bgn_x, sp_bgn_y, sp_span_x, sp_span_y;
    ////////////////////////////////////////////////////////////////////////////
    // variables
    std::string file;
    std::string file_dir;   // source directory for hgt files
    size_t file_size;
    int cols, rows;
    double file_lat, file_lon;
    bool verb, report;

    // only if file load() done
    // ========================================
    bool got_lat_lon;
    short emax,emin;
    double emax_lat,emax_lon,emin_lat,emin_lon;
    // =========================================

    int x_off, y_off;
    size_t offset;
    // specials
    std::string alt_file;   // set to open and check another HGT1 database - ONLY for verb + report
    std::string gtopo_dir;  // set if also have GTOPO30 'all' files
    short last_elev;

    // ONLY valid after a full scan
    // ============================
    bool valid;
    short min_elev, max_elev;
    double min_lat, max_lat;
    double min_lon, max_lon;
    int min_x,min_y,max_x,max_y;
    int void_cnt;
    MPOS voids[MX_VOIDS];
    std::string file_scanned;
    // ============================

//private:
    void init();
    short (*hgt_data)[HGT1_SIZE_1];
    bool usr_lat_lon;
    bool done_mmv;
    int little_endian;
    FILE *curr_fp;
    // =============
    // span variables
    FILE *spanfp;
    unsigned char *row_buf;
    size_t row_buf_size;


};


#endif //#ifndef _HGT1FILE_HXX_
// eof

