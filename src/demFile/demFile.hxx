// demFile.hxx
#ifndef _DEMFILE_HXX_
#define _DEMFILE_HXX_

#include "gtopo30.hxx"
#include "hgt1File.hxx"
#include "hgt3File.hxx"
#include "netcdf.hxx"
#include "srtm30.hxx"
#include "stopo30.hxx"

extern bool get_best_elevation( double lat, double lon, short *pe, int verb = 3 );
extern const char *get_evevation_file(); // { return elev_file.c_str(); }
extern bool get_slippy_tile( int x, int y, int zoom, char **ptile, int iverb = 0 );
// need to use top/left
extern bool stopo30_bb_to_image( PBNDBOX pbb, const char *img_file, int iverb = 0,
        int width = 256, int height = 256 );


#endif // _DEMFILE_HXX_
// eof - demFile.hxx

