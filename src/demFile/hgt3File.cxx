/*\ =======================================================================
    hgt3File - 
    3 arc-sec HGT files from 'http://www.viewfinderpanoramas.org' site
    Got 26151 .hgt N|S file to process...
    Got 26151 lat,lon pairs, on a maximum of 64800 pairs, missing 38649 (38649)
    Missing HGT written to file C:\GTools\perl\tempmiss.txt

    Data developed and uploaded in Scotland by Jonathan de Ferranti.

    An HGT file covers an area of 1x1 degree.
    Its south western corner can be deduced from its file name: for example, n51e002.hgt 
    covers the area between N 51 E 2 and N 52 E 3, and s14w077.hgt covers S 14 W 77 to S 13 W 76.

    There are 1201 rows of 1201 cells each. 
    The rows are laid out like text on a page, starting with the northernmost row, 
    with each row reading from west to east. Each cell has two bytes, 
    and the elevation at that cell is 256*(1st byte) + (2nd byte).
    Note, this is BIG-ENDIAN format, so needs to be swapped on an Intel machine if read
    as a short.

    This makes each file 2,884,802 bytes.

\*/

#include <sys/types.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <stdlib.h> // for exit()
#include <string.h> // for strcmp(), ...
#include <limits.h> // for SHRT_MIN, ...
#endif
#include <string>
#include "utils.hxx"
#include "sprtf.hxx"
#include "color.hxx"
#include "dem_utils.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "gtopo30.hxx"
#include "hgt3File.hxx"
#include "dir_utils.hxx"

#ifndef SPRTF
#define SPRTF printf
#endif

static const char *module = "hgt3File";

void hgt3File::init() {
    file = "";
    hgt_data = 0;
    file_size = 0;
    file_lat = BAD_LAT_LON;
    file_lon = BAD_LAT_LON;
    cols = HGT3_SIZE_1;
    rows = HGT3_SIZE_1;
    got_lat_lon = false;
    usr_lat_lon = false;
    verb = true;    // default to verbose
    report = false; // generate a full report is false
    emax = -10000;
    emin = 10000;
    void_cnt = 0;
    done_mmv = false;
    little_endian = !test_endian();
    curr_fp = 0;
    valid = false;
    // span variables
    spanfp = 0;
    row_buf = 0;
    row_buf_size = 0;
}

hgt3File::hgt3File() {
    init();
}
hgt3File::hgt3File(std::string file) {
    init();
    file = file;
}
hgt3File::~hgt3File() {
    if (hgt_data)
        delete [] hgt_data;
    hgt_data = 0;
    file_size = 0;
    file_lat = BAD_LAT_LON;
    file_lon = BAD_LAT_LON;
    file = "";
    if (curr_fp)
        fclose(curr_fp);
    curr_fp = 0;

    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    row_buf_size = 0;
}
bool hgt3File::load_file(std::string in_file)
{
    set_file(in_file);
    return load();
}


bool hgt3File::load() {
    if (file.size() == 0) {
        if (verb) SPRTF("%s: No file name to load!\n", module);
        return false;
    }
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file %s\n", module, file.c_str());
        return false;
    }
    file_size = get_last_file_size();
    if (file_size != HGT_3DEM_SIZE) {
        if (verb) SPRTF("%s: Incorrect file size! got %d, should be %d\n", module,
            (int)file_size, HGT_3DEM_SIZE);
        return false;
    }
    FILE *fp = fopen(file.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s!\n", module,
            file.c_str());
        return false;
    }
    if (hgt_data)
        delete [] hgt_data;
    hgt_data = new short[HGT3_SIZE_1][HGT3_SIZE_1];
    if (!hgt_data) {
        if (verb) SPRTF("%s: Memory allocation of %s bytes, FAILED!\n", module,
            get_NiceNumberStg(HGT_3DEM_SIZE));
        return false;
    }
    size_t res = fread(hgt_data,1,file_size,fp);
    fclose(fp);
    if (res != file_size) {
        if (verb) SPRTF("%s: Read of file %s FAILED! Rested %d, got %d\n", module,
            file.c_str(), (int)file_size, (int)res);
        delete [] hgt_data;
        hgt_data = 0;
        return false;
    }
    if (verb) SPRTF( "%s: Loaded file %s, len %s\n", module,
        file.c_str(),
        get_NiceNumberStg(file_size) );
    if (!got_lat_lon) {
        if ( !get_hgt_lat_lon( file, &file_lat, &file_lon ) ) {
            if (verb) SPRTF( "%s: Do NOT know the lat, lon of the data!\n", module);
            if (verb) SPRTF( "%s: Use set_lat_lon(dlat,dlon) before any other hgt3File services!\n", module);
        } else {
            got_lat_lon = true;
        }
    }

    int x, y;
    short *sp;
    short min = 10000;
    short max = -10000;
    int maxx,maxy,minx,miny;
    double maxxlon,maxylat,minxlon,minylat;
    int vcnt = 0;
    int okcnt = 0;
    short elev;
    double top_lat = file_lat + 1.0;
    // start top row to bottom
    for (y = 0; y < HGT3_SIZE_1; y++) {
        // sart left column to right
        for (x = 0; x < HGT3_SIZE_1; x++) {
            sp = &hgt_data[y][x];
            if (little_endian) SWAP16(sp); // is little endian - do byte swap now
            elev = *sp;
            if (elev <= HGT_VOID) {
                vcnt++;
            } else {
                if (elev > max) {
                    max = elev;
                    maxx = x;
                    maxy = y;
                    //maxxlon = file_lon + ((double)x / (double)HGT3_SIZE_1); // of 1 degree NO
                    //maxylat = top_lat  - ((double)(y+1) / (double)HGT3_SIZE_1);
                    maxxlon = file_lon + (x * HGT3_XDIM);
                    maxylat = file_lat + ((HGT3_SIZE - y) * HGT3_YDIM);
                }
                if (elev < min) {
                    min = elev;
                    minx = x;
                    miny = y;
                    //minxlon = file_lon + ((double)x / (double)HGT3_SIZE_1);
                    //minylat = top_lat  - ((double)(y+1) / (double)HGT3_SIZE_1);
                    minxlon = file_lon + (x * HGT3_XDIM);
                    minylat = file_lat + ((HGT3_SIZE - y) * HGT3_YDIM);
                }
                okcnt++;
            }
        }
    }
    done_mmv = true;
    emax = max;
    emin = min;
    emin_lat = minylat;
    emin_lon = minxlon;
    emax_lat = maxylat;
    emax_lon = maxxlon;
    void_cnt = vcnt;
    if (verb && little_endian) SPRTF("%s: Done little-endian bytes swapping\n", module);
    if (got_lat_lon) {
        if (verb) SPRTF( "%s: Data assumed to be lat %d to %d, lon %d to %d degrees\n", module,
            (int)file_lat, (int)(file_lat + 1),
            (int)file_lon, (int)(file_lon + 1) );
    }
    if (verb) {
        if (got_lat_lon || usr_lat_lon) {
            SPRTF("%s: Elevation max. %d, at lat,lon %s,%s, at y,x %d,%d\n", module,
                emax,
                get_trim_double(maxylat),
                get_trim_double(maxxlon),
                maxy, maxx);
            SPRTF("%s: Elevation min. %d, at lat,lon %s,%s, at y,x %d,%d\n", module,
                emin,
                get_trim_double(minylat),
                get_trim_double(minxlon),
                miny, minx);
            if (vcnt) {
                SPRTF("%s: File %s has %s voids.\n", module,
                    get_file_name(file).c_str(),
                    get_NiceNumberStg(vcnt) );
            }

        } else {
            SPRTF("%s: Elevation min %d, max %d, void count %d\n", module,
                (int)min, (int)max, vcnt);
        }
    }

    if (void_cnt && verb && report) {
        gen_void_report();
    }
    return true;
}

bool hgt3File::set_lat_lon( double dlat, double dlon )
{
    if (!in_world_range(dlat,dlon)) {
        if (verb) SPRTF("%s: lat %lf, lon %lf NOT in world range\n", module,
            dlat, dlon );
        return false;
    }
    file_lat = dlat;
    file_lon = dlon;
    got_lat_lon = true;
    usr_lat_lon = true;
    return true;
}

bool hgt3File::get_min_max_voids( int *pmax, int *pmin, int *pvoids )
{
    if (!hgt_data) {
        if (verb) SPRTF("%s: No hgt data loaded!\n", module);
        return false;
    }
    if (pmax) *pmax = emax;
    if (pmin) *pmin = emin;
    if (pvoids) *pvoids = void_cnt;
    if (verb) SPRTF("%s: Elevation min %d, max 5d, void count %d\n", module,
        (int)emin, (int)emax, void_cnt);
    return true;
}

bool hgt3File::get_elevation_m( double dlat, double dlon, short *pelev )
{
    if (!hgt_data) {
        if (verb) SPRTF("%s: No hgt data file loaded!\n", module);
        return false;
    }
    if (!in_world_range(dlat,dlon)) {
        if (verb) SPRTF("%s: Requested lat %lf, lon %lf NOT in world range!\n", module,
            dlat, dlon );
        return false;
    }
    if (!got_lat_lon) {
        if (verb) SPRTF("%s: Do not know the lat, lon of the data loaded!\n", module,
            dlat, dlon );
        return false;

    }
    if (( dlat > (file_lat + 1.0) ) || (dlat < file_lat) ||
        ( dlon > (file_lon + 1.0) ) || (dlon < file_lon) )
    {
        if (verb) SPRTF("%s: Requested lat %lf, lon %lf out of data range!\n", module,
            dlat, dlon );
        if (verb) SPRTF("%s: Data assumed to be lat %d to %d, lon %d to %d\n", module,
            (int)file_lat, (int)(file_lat + 1),
            (int)file_lon, (int)(file_lon + 1) );
        return false;
    }


    // TODO: Convert user lat, lon to y x index into data
    // 1 degree == HGT3_SIZE_1
    double latd = dlat - file_lat;
    double lond = dlon - file_lon;
    int x, y;
    if (latd > 0.0 ) {
        y = (int) (( 1 / latd * HGT3_SIZE_1 ) + 0.5 );
    } else {
        y = 0;
    }
    if (lond > 0.0) {
        x = (int) ((( 1 / lond ) * HGT3_SIZE_1 ) + 0.5 );
    } else {
        x = 0;
    }
    *pelev = hgt_data[x][y];
    return true;
}

int hgt3File::hgt_to_image()
{
    if (file.size() == 0) {
        if (verb) SPRTF("%s: No file loaded\n", module);
        return 1;
    }
    if (!hgt_data) {
        if (verb) SPRTF("%s: No hgt data loaded\n", module);
        return 2;
    }
    int iret = 0;
    int x,y;
    short elev;
    short *sp;
    // int max_steps = 10;
    if (verb) {
        SPRTF("%s: Maximum %d, Minimum %d metres", module,
            emax, emin);
        if (void_cnt)
            SPRTF(", with %d voids", void_cnt);
        SPRTF(".\n");
    }
    // set color range ZERO_COLOR = 20
    unsigned int steps = emax / ZERO_COLOR;
    if (emax % ZERO_COLOR)
        steps++;
    size_t bsize = HGT3_SIZE_1 * HGT3_SIZE_1 * 3;
    unsigned char *buffer = new unsigned char[bsize+1];
    size_t doff, coff;
    unsigned int colr;
    unsigned char r,g,b;
    clear_used_colors();
    for (y = HGT3_SIZE_1 - 1; y >= 0; y--) {
    //    for (x = HGT3_SIZE_1 - 1; x >= 0; x--) {
    //for (y = 0; y < HGT3_SIZE_1; y++) {
        for (x = 0; x < HGT3_SIZE_1; x++) {
            //sp = &hgt_data[x][y];
            sp = &hgt_data[y][x];
            elev = *sp;
            doff = ((HGT3_SIZE_1 - 1 - y) * HGT3_SIZE_1 * 3) + (x * 3);
            //doff = ((HGT3_SIZE_1 - 1 - y) * HGT3_SIZE_1 * 3) + ((HGT3_SIZE_1 - 1 - x) * 3);
            //doff = (y * HGT3_SIZE_1 * 3) + ((HGT3_SIZE_1 - 1 - x) * 3);
            //doff = (y * HGT3_SIZE_1 * 3) + (x * 3); // temp2.bmp
            //doff = (x * HGT3_SIZE_1 * 3) + (y * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                exit(1);
            }
            if (elev > 0) {
                coff = elev / steps;
                if (coff >= MAX_COLORS) {
                    SPRTF("%s: YEEK: At x %d, y %d, elev %d, max %d, exceeding color %d on %d\n", module,
                        x, y, elev, emax, (int)coff, MAX_COLORS );
                    check_me();
                    exit(1);
                }
            } else {
                coff = ZERO_COLOR;
            }
#if 0
            if (use_get_color) {

                colr = get_color(elev,coff);
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = b;
                buffer[doff+1] = g;
                buffer[doff+2] = r;

            } else {
#endif // 0
                colr = get_BGR_color(elev);
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = r;
                buffer[doff+1] = g;
                buffer[doff+2] = b;
//            }
        }
    }
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    char *out_png = get_out_png();
    if (out_png) {
        if (writePNGImage24(out_png, HGT3_SIZE_1, HGT3_SIZE_1, bit_depth, color_type, buffer ))
            iret |= 8;
    }
#else // !USE_PNG_LIB
    char *out_bmp = get_out_bmp();
    if (out_bmp) {
        if (writeBMPImage24(out_bmp, HGT3_SIZE_1, HGT3_SIZE_1, buffer, bsize))
            iret |= 4;
    }

#endif // USE_PNG_LIB y/n
    delete buffer;
    out_used();
    return iret;
}

void hgt3File::gen_void_report()
{
    int x,y;
    short *sp;
    short elev;
    double clat,clon;
    short velev, aelev;
    bool shown_valid = false;
    int vrow, vcol, xx, yy, maxx, miny, bx, by;
    int vcnt = 0;
    int okcnt = 0;
    int bracket = 4;
    hgt3File *alt = 0;
    gtopo30 *alt2 = 0;
    SPRTF("%s: Re-processing file %s showing VOID report for %d locations.\n", module,
        file.c_str(), void_cnt);
    if ((alt_file.size() > 0) && (is_file_or_directory((char *)alt_file.c_str())) &&
        (get_last_file_size() == HGT_3DEM_SIZE)) {
        alt = new hgt3File();
        alt->verb = false;  // silent load
        alt->set_file(alt_file);
        if (!alt->load()) {
            delete alt;
            alt = 0;
        }
    }
    if (alt_file.size() > 0) {
        if (alt) {
            SPRTF("%s: Will compare VOIDS with file %s\n", module, 
                alt_file.c_str());
        } else {
            SPRTF("%s: Note: Failed to load alternate file %s\n", module,
                alt_file.c_str());
        }
    }
    // have we been give a GTOPO source
    if ((gtopo_dir.size() > 0) && got_lat_lon && (is_file_or_directory((char *)gtopo_dir.c_str()) == 2)) {
        std::string gfile = get_GTOPO_file( gtopo_dir, file_lat, file_lon );
        if (gfile.size() > 0) {
            alt2 = new gtopo30();
            alt2->verb = false;
            short curr_void = set_void_elevation(GTOPO_NODATA, false); 
            alt2->set_file(gfile);
            if (!alt2->load_file()) {
                delete alt2;
                alt2 = 0;
            }
            set_void_elevation(curr_void,false); 
        }
    }
    if (gtopo_dir.size() > 0) {
        if (alt2) {
            SPRTF("%s: Will compare VOIDS with file %s\n", module,
                alt2->file.c_str());
        } else {
            SPRTF("%s: Note: Failed to load GTOPO file from %s\n", module,
                gtopo_dir.c_str());
        }
    }
    // start bottom row to top
    for (y = HGT3_SIZE_1-1; y >= 0; y--) {
        // start left column to right
        for (x = 0; x < HGT3_SIZE_1; x++) {
            sp = &hgt_data[x][y];
            elev = *sp;
            if (elev <= HGT_VOID) {
                vcnt++;
                SPRTF("\n%s: %d of %d: VOID at row %d, col %d ", module,
                    vcnt, void_cnt, y, x );
                if (got_lat_lon) {
                    clat = file_lat + ((HGT3_SIZE - y) * HGT3_YDIM);
                    clon = file_lon + (x * HGT3_XDIM);
                    SPRTF("IE lat %s, lon %s",
                        get_trim_double(clat),
                        get_trim_double(clon) );
                }
                SPRTF("\n");
                if (okcnt && !shown_valid) {
                    SPRTF("%s: Valid at row %d, col %d ", module, 
                        vrow, vcol );
                    if (got_lat_lon) {
                        clat = file_lat + ((HGT3_SIZE - vrow) * HGT3_YDIM);
                        clon = file_lon + (vcol * HGT3_XDIM);
                        SPRTF("IE lat %s, lon %s",
                            get_trim_double(clat),
                            get_trim_double(clon) );
                    }
                    SPRTF(", with elev %d\n",velev);
                    shown_valid = true;
                }
                yy   = y + bracket;
                miny = y - bracket;
                xx   = x - bracket;
                maxx = x + bracket;
                // check limits
                if (yy > HGT3_SIZE)
                    yy = HGT3_SIZE;
                if (miny < 0)
                    miny = 0;
                if (xx < 0)
                    xx = 0;
                if (maxx > HGT3_SIZE)
                    maxx = HGT3_SIZE;
                by = yy;
                bx = xx;
                // output bracketed area
                for (yy = by; yy >= miny; yy--) {
                    for (xx = bx; xx <= maxx; xx++) {
                        elev = hgt_data[xx][yy];
                        if (elev <= HGT_VOID) {
                            SPRTF(" void ");
                        } else {
                            SPRTF(" %4d ",elev);
                        }
                    }
                    SPRTF("\n");
                }
                if (alt) {
                    SPRTF("%s: Values from alternate source\n", module);
                    for (yy = by; yy >= miny; yy--) {
                        for (xx = bx; xx <= maxx; xx++) {
                            elev = alt->hgt_data[xx][yy];
                            if (elev <= HGT_VOID) {
                                SPRTF(" void ");
                            } else {
                                SPRTF(" %4d ",elev);
                            }
                            if ((x == xx) && (y == yy)) {
                                aelev = elev;
                            }
                        }
                        SPRTF("\n");
                    }
                    if (aelev <= HGT_VOID) {
                        SPRTF("%s: Alternate source likewise a VOID\n", module);
                    } else {
                        SPRTF("%s: Alternate source suggests elevation %d\n", module,
                            aelev);
                    }
                }
                if (alt2) {
                    // note already checked that we have lat,lon, so clat,clon have been set


                }
            } else {
                okcnt++;
                vrow = y;
                vcol = x;
                velev = elev;
                shown_valid = false;
            }
        }
    }
    if (alt) {
        delete alt;
    }
    if (alt2) {
        delete alt2;
    }
}

bool hgt3File::set_hgt_dir( std::string dir )
{
    if (is_file_or_directory((char *)dir.c_str()) != 2) {
        if (verb) SPRTF("%s: Can NOT 'stat' directory %s!\n", module,
            dir.c_str());
        return false;
    }
    // could check if it contains any valid HGT files - maybe later - TODO
    file_dir = dir;
    if (verb) SPRTF("%s: Set HGT source directory to %s.\n", module,
        file_dir.c_str());
    return true;
}

// de Ferranti DEM 3 file
// Its south western corner can be deduced from its file name: for example, N51E002.hgt 
// covers the area between N 51 E 2 and N 52 E 3, and S14W077.hgt covers S 14 W 77 to S 13 W 76.
bool hgt3File::set_offsets( double dlat, double dlon )
{
    int ilat,ilon;

    if (!in_world_range(dlat,dlon)) {
        if (verb) SPRTF("%s: Given lat %s, and/or lon %s NOT in world range!\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return false;
    }
    char *cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon, verb );
    if (!cp) {
        if (verb) SPRTF("%s: Failed to get file name for lat,lon %s,%s!\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return false;
    }
    double dx,dy;
    // TODO: Check this OFFSET calculation
    if (dlon < 0.0) {
        dx = dlon + (double)ilon;
    } else {
        dx = dlon - (double)ilon;
    }
    if (dlat < 0.0) {
        dy = 1.0 - dlat - (double)ilat;
    } else {
        dy = 1.0 - (dlat - (double)ilat);
    }
    x_off = (int)(dx * (double)HGT3_SIZE_1);
    y_off = (int)(dy * (double)HGT3_SIZE_1);
    if (x_off >= HGT3_SIZE_1)
        x_off = HGT3_SIZE_1 - 1;
    if (y_off >= HGT3_SIZE_1)
        y_off = HGT3_SIZE_1 - 1;

    offset = ((y_off * HGT3_SIZE_1) + x_off) * 2;
    return true;
}

bool hgt3File::get_elevation( double dlat, double dlon, short *ps )
{
    int ilat,ilon;

    if (!in_world_range(dlat,dlon)) {
        if (verb) SPRTF("%s: Given lat %s, and/or lon %s NOT in world range!\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return false;
    }
    char *cp = get_hgt_file_name( dlat, dlon, &ilat, &ilon, verb );
    if (!cp) {
        if (verb) SPRTF("%s: Failed to get file name for lat,lon %s,%s!\n", module,
            get_trim_double(dlat),
            get_trim_double(dlon) );
        return false;
    }
    std::string hgt = file_dir;
    if (hgt.size() > 0)
        hgt += PATH_SEP;
    hgt += cp;

    if (is_file_or_directory((char *)hgt.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file %s!\n", module, hgt.c_str());
        if (verb && (file_dir.size() == 0)) {
            SPRTF("%s: Since no file directory given can only search cwd [%s]\n", module,
                get_current_dir().c_str() );
            SPRTF("%s: Use hgt2File::set_hgt_dir(dir) if they ae in a different source.\n", module);
        }
        return false;
    }

    file_size = get_last_file_size();
    if (file_size != HGT_3DEM_SIZE) {
        if (verb) SPRTF("%s: Incorrect file size! got %d, should be %d\n", module,
            (int)file_size, HGT_3DEM_SIZE);
        return false;
    }

    if (strcmp(hgt.c_str(),file.c_str())) {
        // different file to last
        if (curr_fp)
            fclose(curr_fp);
        curr_fp = fopen(hgt.c_str(),"rb");
        if (!curr_fp) {
            if (verb) SPRTF("%s: Unable to 'open' file %s!\n", module,
                hgt.c_str());
            return false;
        }
        set_file(hgt);
        if (verb) SPRTF("%s: Opened file [%s], %s bytes.\n", module,
            hgt.c_str(),
            get_NiceNumberStg(file_size));
    } else {
        // same file as last time
        if (!curr_fp) {
            curr_fp = fopen(hgt.c_str(),"rb");
            if (!curr_fp) {
                if (verb) SPRTF("%s: Unable to 'open' file %s!\n", module,
                    hgt.c_str());
                return false;
            }
            if (verb) SPRTF("%s: Openned file [%s], %s bytes.\n", module,
                hgt.c_str(),
                get_NiceNumberStg(file_size));
        }
    }

    // we have the file - compute offset
    // take file S90W180.hgt - name by bottom left
    // so if given -89.5, -179.5 how to get an index???
    //  lon = flon + ((double)x / (double)HGT3_SIZE_1);
    //  lon - flon = ((double)x / (double)HGT3_SIZE_1);
    //  x = (lon - flon) * (double)HGT3_SIZE_1);

    //  lat = flat - ((double)y / (double)HGT3_SIZE_1);
    //  y = (flat - lat) * (double)HGT3_SIZE_1);
    // Tristan da Cunha, Peak -  2,062 metres (6,765 ft) - -37.09444444,-12.288611111
    // is in S38W013.hgt so this tile covers -37 to -38 - -13 to -12
    // in the 1201x1201 image, its center is about x,y 856,130 with top/left being 0,0
    // 856 / 1201 = 0.712739 == (-12.2886 - -13) * 1201 = 854
    // 130 / 1201 = 0.108243 == 1 - (-37.0944 - -38) * 1201 = 113
    // Mount Lamlam - 406 meters (1,332 ft) - 13.33861,144.662778 13 20 19 N 144 39 46 E
    // on image is x,y 798,795 =
    // y = 1 - (dlat - ilat) * 1201 = 794
    // x = dlon - ilon * 1201 = 796

    double dx,dy;
    //double dx = lon - (double)ilon; // index
    //double dy = lat - (double)ilat;
    if (dlon < 0.0) {
        dx = dlon + (double)ilon;
    } else {
        dx = dlon - (double)ilon;
    }
    if (dlat < 0.0) {
        dy = 1.0 - dlat - (double)ilat;
    } else {
        dy = 1.0 - (dlat - (double)ilat);
    }
    x_off = (int)(dx * (double)HGT3_SIZE_1);
    y_off = (int)(dy * (double)HGT3_SIZE_1);
    if (x_off >= cols)
        x_off = cols - 1;
    if (y_off >= rows)
        y_off = rows - 1;

    offset = ((y_off * HGT3_SIZE_1) + x_off) * 2;

    size_t res = fseek(curr_fp,offset,SEEK_SET);
    if (res != 0) {
        if (verb) SPRTF("%s: Failed seeking to offet %s on %s!\n", module,
            get_NiceNumberStg(offset), get_NiceNumberStg(file_size));
        return false;
    }
    short elev;
    res = fread(&elev,1,2,curr_fp);
    if (res != 2) {
        fclose(curr_fp);
        curr_fp = 0;
        if (verb) SPRTF("%s: Read failed! requested 2, got %d! At offset %d on %d\n", module,
            (int)res,
            get_NiceNumberStg(offset),
            get_NiceNumberStg(file_size));
        return false;
    }
    if (little_endian) {
        SWAP16(&elev); // if little endian - swap em
    }
    last_elev = elev;
    *ps = elev;
    if (verb) SPRTF("%s: Elevation of lat,lon %s,%s is %d, at y,x %d,%d\n", module,
        get_trim_double(dlat),
        get_trim_double(dlon),
        last_elev,
        y_off, x_off );
    return true;
}

//     Mount Everest -  8,848 meters - 27.988, 86.9253 = N27E086
bool hgt3File::scan_hgt_file( std::string hgt, double flat, double flon )
{
    if (is_file_or_directory((char *)hgt.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file [%s]!\n", module,
            hgt.c_str());
        return false;
    }
    size_t fs = get_last_file_size();
    if (fs != HGT_3DEM_SIZE) {
        if (verb) SPRTF("%s: Bad file size! Got %s, expected %s\n", module,
            get_NiceNumberStg(fs),
            get_NiceNumberStg(HGT_3DEM_SIZE));
        return false;
    }
    FILE *fp = fopen(hgt.c_str(), "rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file [%s]!\n", module,
            hgt.c_str());
        return false;
    }
    int x,y;
    short elev;
    size_t res;
    int cnt_void = 0;
    short elev_max = SHRT_MIN;
    short elev_min = SHRT_MAX;
    int x_min,x_max,y_min,y_max;
    double lat_max, lon_max, lat_min, lon_min;
    x_min = x_max = y_min = y_max = -1;
    lat_max = lon_max = lat_min = lon_min = 200.0;
    double top_lat = flat + 1.0;
    // start top to bottom row
    for (y = 0; y < HGT3_SIZE_1; y++) {
        // sart left column to right
        for (x = 0; x < HGT3_SIZE_1; x++) {
            res = fread(&elev,1,2,fp);
            if (res != 2) {
                if (verb) SPRTF("%s: Read failed! requested 2 got %d\n", module,
                    (int)res );
                fclose(fp);
                return false;
            }
            if (little_endian) {
                SWAP16(&elev); // if little endian - swap em
            }
            if (elev == HGT_VOID) {
                cnt_void++;
            } else {
                if (elev > elev_max) {
                    elev_max = elev;
                    x_max = x;
                    y_max = y;
                    lon_max = flon + ((double)x / (double)HGT3_SIZE_1);
                    lat_max = top_lat - ((double)(y+1) / (double)HGT3_SIZE_1);
                }
                if (elev < elev_min) {
                    elev_min = elev;
                    x_min = x;
                    y_min = y;
                    lon_min = flon + ((double)x / (double)HGT3_SIZE_1);
                    lat_min = top_lat - ((double)(y+1) / (double)HGT3_SIZE_1);
                }
            }
        }
    }

    fclose(fp);
    min_elev = elev_min;
    max_elev = elev_max;
    min_lat  = lat_min;
    max_lat  = lat_max;
    min_lon  = lon_min;
    max_lon  = lon_max;
    min_x    = x_min;
    min_y    = y_min;
    max_x    = x_max;
    max_y    = y_max;
    void_cnt = cnt_void;
    file_scanned = hgt;
    valid = true;
    if (verb) {
        SPRTF("%s: Scan of %s, of %s bytes, %s voids.\n", module,
            hgt.c_str(),
            get_NiceNumberStg(fs),
            get_NiceNumberStg(void_cnt));
        SPRTF("%s: Elevation Max %d meters, at lat,lon %s,%s, y,x %d,%d\n",module,
            max_elev,
            get_trim_double(max_lat),
            get_trim_double(max_lon),
            max_y, max_x );
        SPRTF("%s: Elevation Min %d meters, at lat,lon %s,%s, y,x %d,%d\n",module,
            min_elev,
            get_trim_double(min_lat),
            get_trim_double(min_lon),
            min_y, min_x );

    }
    return true;
}

bool hgt3File::begin_span_file( std::string srtm )
{
    bool cv = verb;
    verb = 0;
    // =========================================================================
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: begin_span_file: failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================
    verb = cv;
    spanfp = fopen(srtm.c_str(),"rb");
    if (!spanfp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    last_elev = 0;
    return true;
}

bool hgt3File::end_span_file()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    return true;
}

// this could lead to more sofisticated 'averaging' of elevations
bool hgt3File::get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps )
{
    int y,cnt,elev;
    size_t off,res,xlen,x,xspan;
    short s1,s2;
    int end_x = bgn_x + span_x;
    int end_y = bgn_y + span_y;
    if (end_x > cols)
        end_x = cols;
    if (end_y > rows)
        end_y = rows;
    if ((bgn_x < 0) || (bgn_y < 0) || (span_x < 1) || (span_y < 1) || !ps || !spanfp ) {
        if (verb) SPRTF("%s: Bad parameter! x=%d y=%d spx=%d spy=%d %s %s\n", module,
            bgn_x, bgn_y, span_x, span_y,
            (ps ? "" : "ps=<null> pointer"),
            (spanfp ? "" : "file closed"));
        return false;
    }
    cnt = 0;
    elev = 0;
    xspan = end_x - bgn_x;
    xlen = (xspan * 2);
// try again - #if 0 // ok, seems faster
    if (row_buf == 0) {
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }
    } else if (xlen > row_buf_size) {
        delete row_buf;
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }

    }
    if (xlen) {
        for (y = bgn_y; y < end_y; y++) {
            off = ( y * cols * 2 ) + ( bgn_x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)off, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(row_buf,1,xlen,spanfp);
            if (res != xlen) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            for (x = 0; x < xspan; x++) {
                short *sp = (short *)( &row_buf[x * 2] );
                s1 = *sp;
                get_ix_short( &s1, &s2 );
                elev += s2;
                cnt++;
            }
        }
    }
// === #endif // 0

#if 0 // ok, this work but is slow, maybe due to the many seeks???
    for (y = bgn_y; y < end_y; y++) {
        for (x = bgn_x; x < end_x; x++) {
            off = ( y * cols * 2 ) + ( x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)offset, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(&s1,1,2,spanfp);
            if (res != 2) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            get_ix_short( &s1, &s2 );
            elev += s2;
            cnt++;
        }
    }
#endif // 0
    if (cnt) {
        // this is a simple AVERAGE - could be much more 'complex'
        last_elev = (elev / cnt);
    } else {
        if (verb) SPRTF("%s: WARNING: Returning previous elev %d for x,y %d,%d span %d,%d on %d,%d\n", module,
            last_elev, x, y, span_x, span_y, cols, rows );
    }
    *ps = last_elev;
    return true;
}


// eof - hgt3File.cxx
