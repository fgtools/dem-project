/*\
 * usgs30.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <string>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#include <limits.h> // for SHRT_MIN, ...
#include <unistd.h> // for getcwd(), ...
#define stricmp strcasecmp
#define _getcwd getcwd
#endif
#include "utils.hxx"
#include "sprtf.hxx"
#include "color.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "dir_utils.hxx"
#include "usgs30.hxx"


static const char *module = "usgs30";

///////////////////////////////////////////////////////////////////////////////////////
// helper functions


// convert lat,lon to file name - ie find file name for a specific lat,lon
// =======================================================================
typedef struct tagUSGS30 {
    const char *tile;
    int lat_min;
    int lat_max;
    int lon_min;
    int lon_max;
    int elev_min;
    int elev_max;
    int mean;
    int std_dev;
} USGS30, *PUSGS30;

//              lat lat  lat  lon 
USGS30 usgs30_tile_table[] = {
    { "w180n90", 40, 90, -180, -140, -6, 6098, 67, 246 },
    { "w140n90", 40, 90, -140, -100, -71, 4635, 378, 563 },
    { "w100n90", 40, 90, -100, -60, -18, 2416, 185, 267 },
    { "w060n90", 40, 90, -60, -20, -14, 3940, 520, 924 },
    { "w020n90", 40, 90, -20, 20, -179, 4536, 93, 266 },
    { "e020n90", 40, 90, 20, 60, -188, 5472, 116, 254 },
    { "e060n90", 40, 90, 60, 100, -156, 7169, 340, 618 },
    { "e100n90", 40, 90, 100, 140, -110, 3901, 391, 464 },
    { "e140n90", 40, 90, 140, 180, -26, 4578, 415, 401 },
    { "w180n40", -10, 40, -180, -140, -2, 4120, 1, 34 },
    { "w140n40", -10, 40, -140, -100, -83, 4228, 198, 554 },
    { "w100n40", -10, 40, -100, -60, -42, 6543, 139, 414 },
    { "w060n40", -10, 40, -60, -20, -10, 2503, 29, 94 },
    { "w020n40", -10, 40, -20, 20, -139, 3958, 256, 314 },
    { "e020n40", -10, 40, 20, 60, -415, 5778, 516, 573 },
    { "e060n40", -10, 40, 60, 100, -46, 8685, 784, 1534 },
    { "e100n40", -10, 40, 100, 140, -71, 7213, 236, 625 },
    { "e140n40", -10, 40, 140, 180, -6, 4650, 14, 144 },
    { "w180s10", -60, -10, -180, -140, 0, 1784, 0, 7 },
    { "w140s10", -60, -10, -140, -100, 0, 910, 0, 1 },
    { "w100s10", -60, -10, -100, -60, -206, 6813, 262, 814 },
    { "w060s10", -60, -10, -60, -20, -61, 2823, 83, 211 },
    { "w020s10", -60, -10, -20, 20, -12, 2498, 73, 291 },
    { "e020s10", -60, -10, 20, 60, -1, 3408, 186, 417 },
    { "e060s10", -60, -10, 60, 100, -4, 2555, 0, 8 },
    { "e100s10", -60, -10, 100, 140, -20, 1360, 64, 145 },
    { "e140s10", -60, -10, 140, 180, -43, 3119, 40, 140 },
    { 0 }
};

const char *get_usgs30_tile_name(double lat, double lon, int *pilat, int *pilon, bool verb)
{
    if (!in_world_range(lat,lon)) {
        if (verb) SPRTF("NOTE: lat %s, and/or lon %s NOT in world range!\n",
            get_trim_double(lat),
            get_trim_double(lon) );
        return 0;
    }
    PUSGS30 pt = usgs30_tile_table;
    while (pt->tile) {
        if ((lat <= (double)pt->lat_max) &&
            (lat >  (double)pt->lat_min) &&
            (lon <  (double)pt->lon_max) &&
            (lon >= (double)pt->lon_min)) {
            int ilat = pt->lat_max;
            int ilon = pt->lon_max;
            if (pilat)
                *pilat = ilat;
            if (pilon)
                *pilon = ilon;
            return pt->tile;
        }
        pt++;
    }
    if (verb) {
        if (lat <= -60.0) {
            SPRTF("%s: lat,lon  %s,%s NO USGS DEM 30 below -60.0 degrees!\n", module,
                get_trim_double(lat),
                get_trim_double(lon) );
        } else {
            SPRTF("%s: lat,lon  %s,%s NOT found in table! *** FIX ME ***\n", module,
                get_trim_double(lat),
                get_trim_double(lon) );
        }
    }
    return 0;

}

bool get_usgs30_tile_limits(const char *tile, PTILELIMITS ptl)
{
    PUSGS30 pt = usgs30_tile_table;
    while (pt->tile) {
        if (stricmp(tile,pt->tile) == 0) {
            ptl->max_lat = (double)pt->lat_max;
            ptl->min_lat = (double)pt->lat_min;
            ptl->max_lon = (double)pt->lon_max;
            ptl->min_lon = (double)pt->lon_min;
            return true;
        }
        pt++;
    }
    return false;   // tile NOT found
}

// get valid DEM files from a given directory
// ==========================================
vSTG get_USGS30_DEM_Files( std::string dir, bool verb )
{
    vSTG dir_files = read_directory( dir, "*" USGS_EXT,  dm_FILE );
    size_t ii, max = dir_files.size();
    if (!max) {
        if (verb) {
            if (is_file_or_directory((char *)dir.c_str()) != 2) {
                SPRTF("%s: '%s' is NOT is NOT a valid directory!\n", module, dir.c_str());
            } else {
                SPRTF("%s: Directory '%s' has no valid files!\n", module, dir.c_str());
            }
        }
        return dir_files;
    }
    std::string f,ext;
    vSTG files;
    size_t size;
    for (ii = 0; ii < max; ii++) {
        f = dir_files[ii];
        if (is_file_or_directory((char *)f.c_str()) != 1) {
            if (verb)
                SPRTF("%s: Failed to 'stat' file %s!\n", module, f.c_str());
            continue;
        }
        size = get_last_file_size();
        if ( size != USGS30_FILE_SIZE) {
            if (verb) {
                SPRTF("%s: File %s NOT correct size. got %s, expected %s bytes!\n", module, f.c_str(),
                    get_NiceNumberStg(size),
                    get_NiceNumberStg(USGS30_FILE_SIZE));
            }
            continue;
        }
//#if !(defined(_MSC_VER) || defined(UNIX_READ_DIR_FIX))
        int pos = f.rfind('.');
        if (pos > 0) {
            ext = f.substr(pos);
            if (strcmp(ext.c_str(),".DEM")) {
                // not correct extend
                if (verb) {
                    SPRTF("%s: File %s NOT correct extension. Got '%s', expected '.DEM'!\n", module, f.c_str(),
                        ext.c_str() );
                }
                continue;
            }
        } else {
            // no extent
            continue;
        }
//#endif
        files.push_back(f);
    }
    if (verb) {
        max = files.size();
        SPRTF("%s: Directory %s yielded %d USGS DEM files.\n", module, dir.c_str(), (int)max);
    }
    return files;
}



///////////////////////////////////////////////////////////////////////////////////////
// implementation
void usgs30::init()
{
    file = "";
    verb = true;
    valid = false;
    min_elev = SHRT_MAX;
    max_elev = SHRT_MIN;
    usr_ll = false;
    // for span workings
    spanfp = 0;
    last_elev = 0;
    row_buf = 0;
    row_buf_size = 0;
    // ================
    file_lat = BAD_LATLON;
    file_lon = BAD_LATLON;
}

usgs30::usgs30()
{
    init();
}

usgs30::~usgs30()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
}

bool usgs30::set_file(std::string in_file)
{
    if (is_file_or_directory((char *)in_file.c_str()) != 1) {
        if (verb) SPRTF("Unable to 'stat' file %s\n", in_file.c_str());
        return false;
    }
    file_size = get_last_file_size();
    if (file_size == USGS30_FILE_SIZE) {
        rows = USGS30_ROWS;
        cols = USGS30_COLS;
    } else {
        if (verb) SPRTF("File size is %d, instead of %d\n", (int)file_size,
            USGS30_FILE_SIZE );
        return false;
    }
    file = in_file;
    if (verb) SPRTF("%s: Set %s as input file, %s bytes.\n", module, file.c_str(), get_NiceNumberStg(file_size));
    return true;
}

bool usgs30::set_file_dir( std::string dir )
{
    if (is_file_or_directory((char *)dir.c_str()) != 2) {
        if (verb) SPRTF("%s: Input %s NOT a valid directory!\n", module, dir.c_str());
        return false;
    }
    usgs30_files = get_USGS30_DEM_Files( dir, verb );
    size_t max = usgs30_files.size();
    if (!max) {
        if (verb) SPRTF("%s: In %s found no *" USGS_EXT " files!\n", module, dir.c_str());
        return false;
    }
    file_dir = dir;
    return true;
}

////////////////////////////////////////////////////////////////////////
// get elevation at a specific location, auto selecting the file
///////////////////////////////////////////////////////////////////////
bool usgs30::get_elevation( double lat, double lon, short *ps )
{
    size_t res;
    std::string srtm;
    int ilat,ilon;
    short s1;
    if (!in_world_range(lat,lon)) {
        if (verb) SPRTF("%s: Given lat %s, and/or lon %s OUT OF WORLD RANGE!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }

    tile = get_usgs30_tile_name(lat,lon,&ilat,&ilon,false);
    if (!tile) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Can NOT get tile for given lat %s, lon %s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }
    // USGS uses UPPER CASE names
    char *tmp = GetNxtBuf();
    strcpy(tmp,tile);
    tmp[0] = toupper(tmp[0]);
    tmp[4] = toupper(tmp[4]);

    if (!get_usgs30_tile_limits(tile, &tl)) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Can NOT get tile limits of %s!\n", module, tile);
        return false;
    }
    if ( !((lat <= tl.max_lat) &&
           (lat >= tl.min_lat) &&
           (lon <= tl.max_lon) &&
           (lon >= tl.min_lon)) ) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Given lat,lon NOT in limits of tile %s!\n",module, tile);
        return false;
    }

    if (verb) {
        SPRTF("%s: Given lon,lat %s,%s, is tile %s, min %s,%s, max %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            tmp,
            get_trim_double(tl.min_lat),
            get_trim_double(tl.min_lon),
            get_trim_double(tl.max_lat),
            get_trim_double(tl.max_lon) );
    }

    // build up PATH to file
    srtm = file_dir;
    if (file_dir.size() > 0) {
        srtm += PATH_SEP;
    }
    srtm += tmp;    // tile;
    srtm += USGS_EXT;   // ".DEM";
    // hgt += ".hgt";
    if (is_file_or_directory((char *)srtm.c_str()) != 1) {
        if (verb) {
            SPRTF("%s: Unable to 'stat' file '%s'\n", module, srtm.c_str());
            if (file_dir.size() == 0) {
                char cwd[MAX_PATH];
                cwd[0] = 0;
                _getcwd(cwd, MAX_PATH);
                SPRTF("SRTM (Ferranti/Bathymetric) file directory NOT set, so can only search CWD %s\n", cwd);
            }
        }
        return false;
    }

    // #####################################################################################
    if (!set_file(srtm)) {   // IMPORTANT: This SETS the rows/cols, depending on file size
        return false;
    }

    // #####################################################################################
    // This is the calculation if the origin of the file is TOP/LEFT
    //x_col = (int)((lon - tl.min_lon) / (tl.max_lon - tl.min_lon) * cols);
    //y_row = (int)((lat - tl.min_lat) / (tl.max_lat - tl.min_lat) * rows);
    double dtlon = (lon - tl.min_lon);
    double lonsp = (tl.max_lon - tl.min_lon);
    double dtlat = (lat - tl.min_lat);
    double latsp = (tl.max_lat - tl.min_lat);
    x_col = (int)(dtlon / lonsp * cols);
    //y_row = (int)(dlat / latsp * rows);   // always get this LAT mixed up
    y_row = (int)((latsp - dtlat) / latsp * rows);   // this is what I meant ;=))
    offset = (y_row * cols * 2) + (x_col * 2);
    if (verb) {
        //SPRTF("%s: Calculated offset %d on %d, col %d on %d, row %d on %d\n", module,
        //    (int)offset, (int)file_size, x_col, cols, y_row, rows );
        SPRTF("%s: Calculated x,y %d,%d on %d,%d, offset %s on %s\n", module,
            x_col, y_row, cols, rows,
            get_NiceNumberStg(offset),
            get_NiceNumberStg(file_size) );
        SPRTF("%s: delta lon %s on %s of %d, delta lat %s on %s of %d\n", module,
            get_trim_double(dtlon),
            get_trim_double(lonsp),
            cols,
            get_trim_double(latsp - dtlat),  // note this!!!
            get_trim_double(latsp),
            rows );

    }

    FILE *fp = fopen(srtm.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    res = fseek(fp,offset,SEEK_SET);
    if (res != 0) {
        fclose(fp);
        if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
            (int)offset, srtm.c_str(), (int)file_size);
        return false;
    }
    res = fread(&s1,1,2,fp);
    if (res != 2) {
        fclose(fp);
        if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
            srtm.c_str(), (int)res);
        return false;
    }
    get_ix_short( &s1, &elev );
    *ps = elev;
    fclose(fp);
    if (verb) SPRTF("%s: For lat,lon %s,%s, got elevation %d meters, at y,x %d,%d\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev,
            y_row, x_col );
    return true;
}

bool usgs30::set_lat_lon( double in_lat, double in_lon )
{
    if (!in_world_range(in_lat,in_lon)) {
        if (verb) SPRTF("%s: Given lat,lon %s,%s NOT in world range!\n", module,
            get_trim_double(in_lat),
            get_trim_double(in_lon));
        return false;
    }
    file_lat = in_lat;
    file_lon = in_lon;

    usr_ll = true;
    if (verb) SPRTF("%s: Set user lat,lon %s,%s.\n", module,
            get_trim_double(in_lat),
            get_trim_double(in_lon));
    return true;
}


bool usgs30::scan( std::string srtm )
{
    size_t ii,res;
    short s1,s2,max,min;
    int x,y;
    double dim = 50.0 / 6000.0;
    double clat,clon,dlat,dlon;
    int minx,miny,maxx,maxy;
    const char *ctile;

    valid = false;
    min_elev = SHRT_MAX;
    max_elev = SHRT_MIN;

    // =========================================================================
    // set file to get rows,cols
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: scan failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================

    if (!usr_ll) {
        if ( !get_srtm_lat_lon( srtm, &clat, &clon ) ) {
            if (verb) {
                SPRTF("%s: Unable to get lat,lon from file %s\n", module, srtm.c_str());
                SPRTF("%s: Use set_lat_lon(lat,lon) to give location\n", module );
            }
        }
        file_lat = clat;
        file_lon = clon;

        ctile = get_usgs30_tile_name(clat,clon,0,0,false);
        // if (verb)
        SPRTF("%s: Set file lat,lon %s,%s, tile %s.\n", module,
            get_trim_double(clat),
            get_trim_double(clon),
            (ctile ? ctile : "TILE NOT FOUND") );
        if (ctile) {
            TILELIMITS ctl;
            if (get_usgs30_tile_limits(ctile,&ctl)) {
                //if (verb) 
                SPRTF("%s: That set the limits lat,lon to min %s,%s max %s,%s\n", module,
                    get_trim_double( ctl.min_lat ),
                    get_trim_double( ctl.min_lon ),
                    get_trim_double( ctl.max_lat ),
                    get_trim_double( ctl.max_lon ) );
            }
        }
    }

    FILE *fp = fopen(srtm.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    max = SHRT_MIN;
    min = SHRT_MAX;
    x = 0;
    y = 0;
    for (ii = 0; ii < file_size; ii += 2) {
        res = fread(&s1,1,2,fp);
        if (res != 2) {
            fclose(fp);
            if (verb) SPRTF("FAILED 'read' of file %s, request 2, got %d bytes\n",
                srtm.c_str(), (int)res);
            return false;
        }
        get_ix_short( &s1, &s2 );
        if (s2 > max) {
            max = s2;
            clat = file_lat - ((double)y * dim);
            clon = file_lon + ((double)x * dim);
            maxx = x;
            maxy = y;
        }
        if (s2 < min) {
            min = s2;
            dlat = file_lat - ((double)y * dim);
            dlon = file_lon + ((double)x * dim);
            minx = x;
            miny = y;
        }
        x++;
        if (x == cols) {
            y++;
            x = 0;
        }
    }

    max_elev = max;
    max_lat  = clat;
    max_lon  = clon;
    max_x    = maxx;
    max_y    = maxy;

    min_elev = min;
    min_lat  = dlat;
    min_lon  = dlon;
    min_x    = minx;
    min_y    = miny;

    valid = true;
    if (verb) {
        SPRTF("%s: MAX %d, at lat,lon %s,%s, offset y,x %d,%d\n", module,
            max_elev,
            get_trim_double(max_lat),
            get_trim_double(max_lon),
            max_y, max_x );
        SPRTF("%s: MIN %d, at lat,lon %s,%s, offset y,x %d,%d\n", module,
            min_elev,
            get_trim_double(min_lat),
            get_trim_double(min_lon),
            min_y, min_x );
    }
    return true;
}

bool usgs30::begin_span_file( std::string srtm )
{
    bool cv = verb;
    //double clat,clon;
    verb = 0;
    // =========================================================================
    // set file to get rows,cols
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: begin_span_file: failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================
    verb = cv;
#if 0 // hmmm, maybe NOT needed
    if (!usr_ll) {
        if ( !get_srtm_lat_lon( srtm, &clat, &clon ) ) {
            if (verb) {
                SPRTF("%s: Unable to get lat,lon from file %s\n", module, srtm.c_str());
                SPRTF("%s: Use set_lat_lon(lat,lon) to give location\n", module );
            }
        }
        file_lat = clat;
        file_lon = clon;
    }
#endif // 0
    spanfp = fopen(srtm.c_str(),"rb");
    if (!spanfp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    last_elev = 0;
    return true;
}

bool usgs30::end_span_file()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    return true;
}

// this could lead to more sofisticated 'averaging' of elevations
bool usgs30::get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps )
{
    int y,cnt,elev;
    size_t off,res,xlen,x,xspan;
    short s1,s2;
    int end_x = bgn_x + span_x;
    int end_y = bgn_y + span_y;
    if (end_x > cols)
        end_x = cols;
    if (end_y > rows)
        end_y = rows;
    if ((bgn_x < 0) || (bgn_y < 0) || (span_x < 1) || (span_y < 1) || !ps || !spanfp ) {
        if (verb) SPRTF("%s: Bad parameter! x=%d y=%d spx=%d spy=%d %s %s\n", module,
            bgn_x, bgn_y, span_x, span_y,
            (ps ? "" : "ps=<null> pointer"),
            (spanfp ? "" : "file closed"));
        return false;
    }
    cnt = 0;
    elev = 0;
    xspan = end_x - bgn_x;
    xlen = (xspan * 2);
// try again - #if 0 // ok, but this did not work???
    if (row_buf == 0) {
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }
    } else if (xlen > row_buf_size) {
        delete row_buf;
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }

    }
    if (xlen) {
        for (y = bgn_y; y < end_y; y++) {
            off = ( y * cols * 2 ) + ( bgn_x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)off, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(row_buf,1,xlen,spanfp);
            if (res != xlen) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            for (x = 0; x < xspan; x++) {
                short *sp = (short *)( &row_buf[x * 2] );
                s1 = *sp;
                get_ix_short( &s1, &s2 );
                elev += s2;
                cnt++;
            }
        }
    }
// === #endif // 0

#if 0 // ok, this work but is slow, maybe due to the many seeks???
    for (y = bgn_y; y < end_y; y++) {
        for (x = bgn_x; x < end_x; x++) {
            off = ( y * cols * 2 ) + ( x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)off, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(&s1,1,2,spanfp);
            if (res != 2) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            get_ix_short( &s1, &s2 );
            elev += s2;
            cnt++;
        }
    }
#endif // 0
    if (cnt) {
        // this is a simple AVERAGE - could be much more 'complex'
        last_elev = (elev / cnt);
    } else {
        if (verb) SPRTF("%s: WARNING: Returning previous elev %d for x,y %d,%d span %d,%d on %d,%d\n", module,
            last_elev, x, y, span_x, span_y, cols, rows );
    }
    *ps = last_elev;
    return true;
}

bool usgs30::usgs30_file_to_image( std::string in_file, const char *img_file )
{
    bool bret = true;
    if (!set_file(in_file)) {
        if (verb) SPRTF("%s: Failed to set file '%s'!\n", module, in_file.c_str());
        return false;
    }
    if (!usr_ll || (file_lat == BAD_LATLON) || (file_lon == BAD_LATLON)) {
        if (!get_srtm_lat_lon( file, &file_lat, &file_lon )) {
            if (verb) {
                SPRTF("%s: Unable to get lat,lon from file '%s'!\n", module, file.c_str());
                SPRTF("%s: Use set_lat_lon(lat,lon) to establish.\n", module);
            }
        }
        if (verb) {
            SPRTF("%s: Set file lat,lon %s,%s from name.\n", module,
                get_trim_double(file_lat),
                get_trim_double(file_lon));
        }
    } else if (verb) {
        SPRTF("%s: Using given file lat,lon %s,%s.\n", module,
            get_trim_double(file_lat),
            get_trim_double(file_lon));
    }
    size_t bsize = rows * cols * 3;
    unsigned char *buffer = new unsigned char[bsize];   // get color buffer
    if (!buffer) {
        if (verb) {
            SPRTF("%s: Memory FAILED on %u bytes!\n", module, (int)bsize);
        }
        return false;
    }
    FILE *fp = fopen(file.c_str(), "rb");
    if (!fp) {
        delete buffer;
        if (verb) {
            SPRTF("%s: FAILED to 'open' file %s!\n", module, file.c_str());
        }
        return false;
    }
    short s1,s2;
    size_t res,doff;
    int x,y;
    unsigned int colr;
    byte r,g,b;
    int width = cols;
    int height = rows;

    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            res = fread( &s1, 1, 2, fp );
            if (res != 2) {
                if (verb) {
                    SPRTF("%s: Read of 2 bytes FAILED! got %d\n", module, (int)res);
                }
                bret = false;
                goto clean_up;
            }
            get_ix_short( &s1, &s2 );
            colr = get_BGR_color(s2);
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // doff = (y * width * 3) + (x * 3);
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                return 1;
            }
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
        }
    }

clean_up:
    fclose(fp);
    if (bret) {
#ifdef USE_PNG_LIB
        // int bit_depth = 8;
        // int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
        if (writePNGImage24((char *)img_file, width, height, 8, 6, buffer )) {
            if (verb) {
                SPRTF("%s: FAILED to write image file '%s'\n", module, img_file);
            }
            bret = false;
        }
#else // !USE_PNG_LIB
        if (writeBMPImage24((char *)img_file, width, height, buffer, bsize)) {
            if (verb) {
                SPRTF("%s: FAILED to write image file '%s'\n", module, img_file);
            }
            bret = false;
        }
#endif // USE_PNG_LIB y/n
    }
    delete buffer;
    return bret;
}

// eof = usgs30.cxx
