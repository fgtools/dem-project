// srtm30.hxx
#ifndef _SRTM30_HXX_
#define _SRTM30_HXX_
#include "dem_utils.hxx"

// Original 33 files
#define SRTM30_COLS 4800    // each 40 degrees
#define SRTM30_ROWS 6000    // each 50 degrees
#define SRTM30_COL2 7200    // each 60 degrees
#define SRTM30_ROW2 3600    // each 30 degrees

// this seems arse up
//#define SRTM30_COLS 6000    // each 50 degrees
//#define SRTM30_ROWS 4800    // each 40 degrees
//#define SRTM30_COL2 7200    // each 60 degrees
//#define SRTM30_ROW2 3600    // each 30 degrees

#define SRTM30_FILE_SIZE1 (SRTM30_ROWS * SRTM30_COLS * 2)
#define SRTM30_FILE_SIZE2 (SRTM30_ROW2 * SRTM30_COL2 * 2)


class srtm30 
{
public:
    srtm30();
    ~srtm30();
    void init();
    bool verb;
    bool set_file( std::string in_file );
    bool set_file_dir( std::string dir );
    bool set_lat_lon( double lat, double lon );
    bool get_elevation( double lat, double lon, short *ps );
    bool scan( std::string in_file );    // scan to get max,min

    // span service
    bool begin_span_file( std::string srtm );
    bool end_span_file();
    bool get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps );

    // variables
    std::string file;
    std::string file_dir;
    int rows, cols;
    size_t file_size;
    int x_col, y_row; // same as int x_off, y_off;
    size_t offset;
    const char *tile;
    TILELIMITS tl;
    short elev;

    // ONLY valid after a full scan
    // ============================
    bool valid;
    short min_elev, max_elev;
    double min_lat, max_lat;
    double min_lon, max_lon;
    int min_x,min_y,max_x,max_y;
    // ============================

private:
    vSTG srtm30_files;
    bool usr_ll;
    double file_lat,file_lon;
    // for span workings
    FILE *spanfp;
    short last_elev;
    unsigned char *row_buf;
    size_t row_buf_size;
    // ==================
};

extern const char *get_srmt30_tile_name(double lat, double lon,
    int *pilat = 0, int *pilon = 0, bool verb = true);
extern bool get_srmt30_tile_limits(const char *tile, PTILELIMITS ptl);

#endif // #ifndef _SRTM30_HXX_
// eof -srtm30.hxx
