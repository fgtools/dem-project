/*\
 * =============================================================================
 * gtopo30.hxx - GLOBE topographical tiles
 * see : http://www.ngdc.noaa.gov/mgg/topo/gltiles.html
 *
 * A full GLOBE set consists of
 * Tile A: 50-90N, 90-180W	 Tile B: 50-90N, 0-90W	 Tile C: 50-90N, 0-90E	 Tile D: 50-90N, 90-180E
 * Tile E: 0-50N, 90-180W	 Tile F: 0-50N, 0-90W	 Tile G: 0-50N, 0-90E	 Tile H: 0-50N, 90-180E
 * Tile I: 0-50S, 90-180W	 Tile J: 0-50S, 0-90W	 Tile K: 0-50S, 0-90E	 Tile L: 0-50S, 90-180E
 * Tile M: 50-90S, 90-180W	 Tile N: 50-90S, 0-90W	 Tile O: 50-90S, 0-90E	 Tile P: 50-90S, 90-180E
 *
 * These are 30 arc-secs DEM files. (dim=0.0083333 degs). 
 * They are an array of 2-byte integers (a short), beginning at the lower left corner of the tile. 
 * As can be seen, there are TWO tile 'sizes'.
 * All tiles cover 90 degrees in width. That is 10800 columns. 
 * 1: polar tiles ABCDMNOP are 40 degrees in height. That is 4800 rows. So 10800 x 4800 x 2 = 103,680,000 bytes 
 * 2: equitorial tiles EFGHIJKL are 50 degreees in height. That is 6000 rows. So 10800 x 6000 x 2 = 129,600,000 bytes.
 *
 * Normally a 'void' value is represented by -500
 *
 * =============================================================================
\*/

#ifndef _GTOPO30_HXX_
#define _GTOPO30_HXX_
#include <string>

#define GTOPO_COLS 10800
#define GTOPO_PROWS 4800
#define GTOPO_EROWS 6000

#define GTOPO_XDIM (90.0 / (double)GTOPO_COLS)  // = 0.00833333... etc
#define GTOPO_YDIM GTOPO_XDIM

#define GTOPO_NODATA -500
#define GTOPO_TOOHIGH 9000

#define GTOPO_PSIZE (GTOPO_COLS * GTOPO_PROWS * 2)
#define GTOPO_ESIZE (GTOPO_COLS * GTOPO_EROWS * 2)

class gtopo30 
{
public:
    gtopo30();
    ~gtopo30();

    // functions
    bool set_file(std::string in_file);
    bool set_file_range( double min_x_lon, double min_y_lat ); // give lower left corner of file range
    int gtopo_to_images();
    bool get_elevation( double lat, double lon, short *pelev );
    bool load_file();
    bool set_globe_dir( std::string dir );
    bool scan_min_max();
    // set of functions to get data by blocks
    bool begin_span_file( std::string srtm );
    bool end_span_file();
    bool get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps );

    // variables
    std::string file;
    std::string globe_dir;
    bool verb;
    size_t file_size, offset;
    int rows, cols, x_col, y_row;
    bool is_polar;
    double min_x_lon, max_x_lon, min_y_lat, max_y_lat;
    bool usr_set_rng, range_set;

    // ONLY valid after a full scan
    // ============================
    bool valid;
    short max_elev, min_elev, elev;
    double min_lat, max_lat;
    double min_lon, max_lon;
    int min_x,min_y,max_x,max_y;
    int nodata_cnt, toohigh_cnt;
    size_t max_off, min_off;    // offset into file
    // ============================

private:

    void init();
    void clear();
    short (*gtopo_data)[GTOPO_COLS];

    FILE *spanfp;
    short last_elev;
    unsigned char *row_buf;
    size_t row_buf_size;
};

typedef struct tagGTOPORNG {
    char letter;
    double min_x, max_x;
    double min_y, max_y;
    bool is_polar;
} GTOPORNG, *PGTOPORNG;

extern PGTOPORNG get_GTOPO_Range( char c );
extern char get_GTOPO_filechar( double lat, double lon );
extern std::string get_GTOPO_file( std::string dir, double lat, double lon );

#endif // #ifndef _GTOPO30_HXX_
// eof

