/*\
 * usgs30.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _USGS30_HXX_
#define _USGS30_HXX_
#include "dem_utils.hxx"

// Original 27 files
#define USGS30_COLS 4800    // each 40 degrees
#define USGS30_ROWS 6000    // each 50 degrees

#define USGS30_FILE_SIZE (USGS30_ROWS * USGS30_COLS * 2)

// This is the DEM30 extension - other files are .DIF .DMW .GIF .HDR .JPG .NUM .PRJ .SCH .SRC .STD... 
#define USGS_EXT ".DEM"
#define USGS_VOID -32768 // TODO: CHECK ME! tests seem to indicate perhaps ZERO (0) is where there is NO DATA

class usgs30 
{
public:
    usgs30();
    ~usgs30();
    void init();
    bool verb;
    bool set_file( std::string in_file );
    bool set_file_dir( std::string dir );
    bool set_lat_lon( double lat, double lon );
    bool get_elevation( double lat, double lon, short *ps );
    bool scan( std::string in_file );    // scan to get max,min
    bool usgs30_file_to_image( std::string file, const char *imgfile );

    // span service
    bool begin_span_file( std::string srtm );
    bool end_span_file();
    bool get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps );

    // variables
    std::string file;
    std::string file_dir;
    int rows, cols;
    size_t file_size;
    int x_col, y_row; // same as int x_off, y_off;
    size_t offset;
    const char *tile;
    TILELIMITS tl;
    short elev;

    // ONLY valid after a full scan
    // ============================
    bool valid;
    short min_elev, max_elev;
    double min_lat, max_lat;
    double min_lon, max_lon;
    int min_x,min_y,max_x,max_y;
    // ============================

private:
    vSTG usgs30_files;
    bool usr_ll;
    double file_lat,file_lon;
    // for span workings
    FILE *spanfp;
    short last_elev;
    unsigned char *row_buf;
    size_t row_buf_size;
    // ==================
};

// helper functions
extern const char *get_usgs30_tile_name(double lat, double lon, int *pilat, int *pilon, bool verb);
extern bool get_usgs30_tile_limits(const char *tile, PTILELIMITS ptl);
extern vSTG get_USGS30_DEM_Files( std::string dir, bool verb = false );


#endif // #ifndef _USGS30_HXX_
// eof - usgs30.hxx
