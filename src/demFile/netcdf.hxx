// netcdf.hxx

#ifndef _NETCDF_HXX_
#define _NETCDF_HXX_
#include <string>
#include <vector>
#include "dem_utils.hxx" // for TILELIMITS
#include "utils.hxx" // vSTG

enum cdftype {
    n_none,
    n_dim,
    n_attr,
    n_var,
    n_varatt
};

union NETDTYPE    // Declare union type
{
    char   ch;
    short  s;
    int    i;
    long   l;
    float  f;
    double d;
    int *int_ptr;
};          // Optional declaration of union variable

typedef struct tagNETCDF {
    cdftype type;
    std::string name;
    std::string txt;
    size_t typ, val, len, off;
    NETDTYPE ndtyp[2];
    size_t tcnt;
}NETCDF, *PNETCDF;

#define NULLCDF(x) { x.type = n_none; x.name = ""; x.txt = ""; x.typ = 0; x.val = 0; x.len = 0; x.off = 0; x.tcnt = 0; }

typedef std::vector<NETCDF> vNCDF;

#define RD_BLK_SIZE 4096

/*
 *  The internal data types
 */
typedef enum {
	NC_UNSPECIFIED = 0,
/* future	NC_BITFIELD = 7, */
/*	NC_STRING =	8,	*/
	NC_DIMENSION =	10,
	NC_VARIABLE =	11,
	NC_ATTRIBUTE =	12
} NCtype;

#define X_ALIGN			4	/* a.k.a. BYTES_PER_XDR_UNIT */

/*
 *  The netcdf external data types
 */
#define	NC_NAT 	    0	/**< Not A Type */
#define	NC_BYTE     1	/**< signed 1 byte integer */
#define	NC_CHAR 	2	/**< ISO/ASCII character */
#define	NC_SHORT 	3	/**< signed 2 byte integer */
#define	NC_INT 	    4	/**< signed 4 byte integer */
#define NC_LONG     NC_INT  /**< deprecated, but required for backward compatibility. */
#define	NC_FLOAT 	5	/**< single precision floating point number */
#define	NC_DOUBLE 	6	/**< double precision floating point number */
#define	NC_UBYTE 	7	/**< unsigned 1 byte int */
#define	NC_USHORT 	8	/**< unsigned 2-byte int */
#define	NC_UINT 	9	/**< unsigned 4-byte int */
#define	NC_INT64 	10	/**< signed 8-byte int */
#define	NC_UINT64 	11	/**< unsigned 8-byte int */
#define	NC_STRING 	12	/**< string */

#define X_SIZEOF_CHAR		1
#define X_SIZEOF_SHORT		2
#define X_SIZEOF_INT		4	/* xdr_int */
#if 0
#define X_SIZEOF_LONG		8 */	/* xdr_long_long */
#endif
#define X_SIZEOF_FLOAT		4
#define X_SIZEOF_DOUBLE		8

/* useful for aligning memory */
#define	_RNDUP(x, unit)  ((((x) + (unit) - 1) / (unit)) * (unit))

#define ncx_len_char(nelems) \
	_RNDUP((nelems), X_ALIGN)

#define ncx_len_short(nelems) \
	(((nelems) + (nelems)%2)  * X_SIZEOF_SHORT)

#define ncx_len_int(nelems) \
	((nelems) * X_SIZEOF_INT)

#define ncx_len_long(nelems) \
	((nelems) * X_SIZEOF_LONG)

#define ncx_len_float(nelems) \
	((nelems) * X_SIZEOF_FLOAT)

#define ncx_len_double(nelems) \
	((nelems) * X_SIZEOF_DOUBLE)

// flag bits
#define bf_row      0x0001
#define bf_col      0x0002
//#define bf_off      0x0004
#define bf_fil      0x0008
#define bf_xoff     0x0010
#define bf_yoff     0x0020
#define bf_zoff     0x0040
#define bf_fmmx     0x0080
#define bf_fmmy     0x0100
#define bf_fmmz     0x0200

#define bf_min (bf_row | bf_col | bf_zoff)
#define bf_max (bf_min | bf_fil | bf_xoff | bf_yoff | bf_fmmx | bf_fmmy | bf_fmmz)

class netcdf
{
public:
    netcdf();
    ~netcdf();

    // functions
    void init();
    void clear();
    bool set_file(std::string in_file);
    bool get_size_t( size_t *pvar );
    size_t len_nc_attr(size_t type, size_t nelems);
    char *get_att_type(size_t type);
    size_t show_netcdf();
    size_t show_short_array( NETCDF &ncdf );
    size_t show_double_array( NETCDF &ncdf );
    bool get_elevation( double lat, double lon, short *ps );
    std::string get_flag_stg();
    bool set_netcdf_dir( std::string dir );
    bool load_and_get_elevation( std::string in_file, double lat, double lon, short *ps );

    // variables
    std::string file;
    std::string ncdf_dir;
    bool verb, debug;
    bool show_data;
    size_t file_size;
    short fill_value;
    int rows, cols, flag;
    vNCDF vNetCDF;
    TILELIMITS fmm;
    double fmin_elev,fmax_elev;
    size_t x_off, y_off, z_off;
    size_t x_cnt, y_cnt, z_cnt;

    // current - after get_elevation()
    // ===============================
    bool valid;
    TILELIMITS tl;
    int xcol, yrow;
    size_t offset;
    short elev;
    const char *tile;
    double rlat, rlon;
    // ===============================

private:

    // helper functions
    bool next_read();
    bool get_nc_dim();
    bool get_nc_string();
    bool get_nc_attrarray();
    bool get_nc_attr();
    bool get_nc_vararray();
    bool get_nc_var();

    // mainly DEBUG function
    bool find_xy_for_ll(double lat, double lon, int *px, int *py);

    // helper variables
    unsigned char *cp; // moving pointer into the buffer
    FILE *netfp;
    unsigned char *end; // end of buffer
    std::string last_str;
    std::string act_var;    // set to variable name, before processing attrarray for it, and each var-attr
    unsigned char *att_buff;
    size_t attbuf_size;
    char *attr_type;
    bool in_var;
    size_t remaining;
    unsigned char *block;
    size_t buff_size;
    vSTG ncdf_files;
    bool no_ncdf_dir;

};

#endif // _NETCDF_HXX_
// eof
