// stopo30.hxx
/* ===============================================================================
    topo30 format
    The  subdirectory called topo30 has the data 
    stored in a single large file of 2-byte integers
    in MSB format (i.e. big-endian).  The grid spans 
    0 to 360 in longitude and -90 to 90 in latitude. 
    The upper left corner of the upper left grid cell
    has latitude 90 and longitude 0.  There are 
    43200 columns and 21600 rows.

   =============================================================================== */
#ifndef _STOPO30_HXX_
#define _STOPO30_HXX_

#define STOPO30_COLS 43200
#define STOPO30_ROWS 21600

typedef struct tagMAXMIN {
    double lat, lon;
    short elev;
    int x, y;
}MAXMIN, *PMAXMIN;

#define MAX_MM 20

class stopo30 
{
public:
    stopo30();
    ~stopo30();
    bool set_file( std::string file );
    bool test_file(bool scan = false);
    bool get_elevation( double lat, double lon, short *pelev );
    bool get_elevation_group( double lat, double lon, int bracket, vSHRT **pv, int *colcnt = 0 );
    bool get_elevation_block( int bx, int by, int ex, int ey, short *buffer, size_t bsize );
    bool check_file();
    // span group
    bool begin_span_file( std::string srtm );
    bool end_span_file();
    // this could lead to more sofisticated 'averaging' of elevations
    bool get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps );
    int write_stopo30_image(int width, int height, const char *img_file); // build a width x height image of whole file

    // variable
    bool verb;
    std::string file;
    size_t file_size;
    int rows, cols;
    int x_col, y_row;
    double xdim, ydim;
    short max_elev, min_elev, last_elev;
    size_t offset;

    // after a full scan
    // turns out the highest and lowest both in North/East +/+ - quad 1
    MAXMIN minm[MAX_MM];
    MAXMIN maxm[MAX_MM];
    MAXMIN semin;
    MAXMIN semax;
    MAXMIN swmin;
    MAXMIN swmax;
    MAXMIN nwmax;
    MAXMIN nwmin;
    bool done_full_scan;

private:
    void init();
    // short (*stopo_data)[STOPO30_COLS];
    FILE *stopo_fp;
    bool little_endian;
    bool done_test;

    // span variables
    FILE *spanfp;
    unsigned char *row_buf;
    size_t row_buf_size;

};


#endif //#ifndef _STOPO30_HXX_
// eof

