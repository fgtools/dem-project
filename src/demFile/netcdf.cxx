// netcdf.cxx

#include <stdio.h>
#include <string>
#include <vector>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#endif
#include "utils.hxx"
#include "sprtf.hxx"
#include "dir_utils.hxx"
#include "srtm30.hxx"
#include "netcdf.hxx"


static const char *module = "netcdf";
static const char ncmagic1[] = {'C', 'D', 'F', 0x01};


//////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
typedef struct tagFLG2STR {
    int bit;
    const char *name;
}FLG2STR, *PFLG2STR;

FLG2STR flg2str[] = {
    { bf_row, "ROWS" },
    { bf_col, "COLS" },
//    { bf_off, "OFFSET" },
    { bf_fil, "FILL" },
    { bf_xoff, "XOFF" },
    { bf_yoff, "YOFF" },
    { bf_zoff, "ZOFF" },
    { bf_fmmx, "XMM" },
    { bf_fmmy, "YMM" },
    { bf_fmmz, "ZMM" },
    // terminate table
    { 0,       0     }
};

std::string netcdf::get_flag_stg()
{
    std::string s;
    int f = flag;
    PFLG2STR pfs = flg2str;
    while (f && pfs->name) {
        if (f & pfs->bit) {
            if (s.size() > 0)
                s += " ";
            s += pfs->name;
            f &= ~pfs->bit;
        }
        pfs++;
    }
    return s;
}

void netcdf::init()
{
    file = "";
    verb = true;
    debug = false;
    block = 0;
    netfp = 0;
    attr_type = 0;
    att_buff = 0;
    attbuf_size = 256;
    vNetCDF.clear();
    in_var = false;
    show_data = false;
    fill_value = -1;
    rows = 0;
    cols = 0;
    flag = 0;
    x_off = y_off = z_off = 0;
    x_cnt = y_cnt = z_cnt = 0;
    no_ncdf_dir = false;
    valid = false;
}

void netcdf::clear()
{
    if (att_buff)
        delete att_buff;
    att_buff = 0;
    if (block)
        delete block;
    block = 0;
    if (netfp)
        fclose(netfp);
    netfp = 0;
    vNetCDF.clear();
    ncdf_files.clear();
}


netcdf::netcdf()
{
    init();
}


netcdf::~netcdf()
{
    clear();
    vNetCDF.clear();
}

bool netcdf::next_read()
{
    if (!remaining)
        return false;
    if (!netfp)
        return false;
    size_t rd = remaining;
    if (rd > buff_size)
        rd = buff_size;
    remaining -= rd;
    size_t res = fread(block,1,rd,netfp);
    if (res != rd)
        return false;
    end = block + rd;
    cp = block;
    return true;
}

bool netcdf::get_size_t( size_t *pvar )
{
    if (cp >= end) {
        if ( !next_read()) // time to read more
            return false;
    }
	*pvar = (unsigned)(*cp++ << 24);
    if (cp >= end) {
        if ( !next_read()) // time to read more
            return false;
    }
	*pvar |= (*cp++ << 16);
    if (cp >= end) {
        if ( !next_read()) // time to read more
            return false;
    }
	*pvar |= (*cp++ << 8);
    if (cp >= end) {
        if ( !next_read()) // time to read more
            return false;
    }
	*pvar |= *cp++; 

    return true;

}
bool netcdf::get_nc_string()
{
    size_t res, cnt;
    char c[2];
    char *sp = c;
    std::string str;
    c[1] = 0;

    // get string SIZE
    if (!get_size_t(&res)) {
        if (verb) SPRTF("Failed on getting a header string!\n");
        return false;
    }

    cnt = res;
    // collect the string
    while (cnt--) {
        if (cp >= end) {
            if ( !next_read()) { // time to read more
                if (verb) SPRTF("ran out of data in string of %d bytes\n", (int)res );
                return false;
            }
        }
	    c[0] = *cp++; 
        str += sp;
    }
    size_t rndup = res % X_ALIGN;
	if(rndup)
		rndup = X_ALIGN - rndup;
    while (rndup--) {
        if (cp >= end) {
            if ( !next_read()) { // time to read more
                if (verb) SPRTF("ran out of data in string of %d bytes\n", (int)res );
                return false;
            }
        }
        cp++;   // skip zero termination
    }
    last_str = str;
    return true;
}

bool netcdf::get_nc_dim()
{
    size_t res;
    if (!get_nc_string())
        return false;

    if (!get_size_t( &res )) {
        if (verb) SPRTF("Failed get DIM after string %s\n", last_str.c_str());
        return false;
    }
    NETCDF ncdf;
    NULLCDF(ncdf);
    ncdf.type = n_dim;
    ncdf.name = last_str;
    ncdf.val = res;
    vNetCDF.push_back(ncdf);
    std::string msg("");
    if (strcmp(last_str.c_str(),"x") == 0) {
        cols = (int)res;
        flag |= bf_col;
        msg = "set columns";
    } else if (strcmp(last_str.c_str(),"y") == 0) {
        rows = (int)res;
        flag |= bf_row;
        msg = "set rows";
    }

    if (verb) SPRTF("DIM %s = %d %s\n", last_str.c_str(), (int)res, msg.c_str());
    return true;
}

bool netcdf::get_nc_attrarray()
{
    size_t type,nelem,cnt;
    if (!get_size_t( &type )) {
        if (verb) SPRTF("ran out of data getting attribute type!\n" );
        return false;
    }
    if (!get_size_t( &nelem )) {
        if (verb) SPRTF("ran out of data getting attribute length!\n" );
        return false;
    }
	if (type != NC_ATTRIBUTE) {
        if (verb) SPRTF("Not an NC_ATTRIBUTE!\n" );
        return false;
    }
    if( nelem == 0 ) {
        if (verb) SPRTF("attribute has no length!\n" );
        return false;
    }

    if (verb) SPRTF("Get attrarray: count %d\n", (int)nelem);

    for (cnt = 0; cnt < nelem; cnt++) {
        if (!get_nc_attr()) {
            return false;
        }
    }
    return true;

}
char *netcdf::get_att_type(size_t type)
{
	switch(type) 
    {
	case NC_BYTE:
	case NC_CHAR:
        return (char *)"NC_BYTE";
	case NC_SHORT:
        return (char *)"NC_SHORT";
	case NC_INT:
        return (char *)"NC_INT";
	case NC_FLOAT:
        return (char *)"NC_FLOAT";
	case NC_DOUBLE:
        return (char *)"NC_DOUBLE";
    }
    return (char *)"NC_UNKNOWN";
}

size_t netcdf::len_nc_attr(size_t type, size_t nelems)
{
    attr_type = get_att_type(type);
	switch(type) 
    {
	case NC_BYTE:
	case NC_CHAR:
		return ncx_len_char(nelems);
	case NC_SHORT:
		return ncx_len_short(nelems);
	case NC_INT:
		return ncx_len_int(nelems);
	case NC_FLOAT:
		return ncx_len_float(nelems);
	case NC_DOUBLE:
		return ncx_len_double(nelems);
	default:
	    if (verb) SPRTF("Bad attr type %d, len %d\n", (int)type, (int)nelems);    // assert("ncx_len_NC_attr bad type" == 0);
        break;
	}
	return 0;
}

bool netcdf::get_nc_attr()
{
    size_t type,nelem,cnt,res,off,clen;
    std::string hex;
    int c;
    bool isprt = true;
    bool isstg = false;

    if (!get_nc_string()) {
        if (verb) SPRTF("ran out of data getting an attr string!\n" );
        return false;
    }

    if (!get_size_t( &type )) {
        if (verb) SPRTF("ran out of data getting an attr type!\n" );
        return false;
    }

    if (!get_size_t( &nelem )) {
        if (verb) SPRTF("ran out of data getting an attr len!\n" );
        return false;
    }

    res = cnt = len_nc_attr( type, nelem ); // length depends on type
    if ((type == NC_BYTE) || (type == NC_CHAR)) {
        isstg = true;
        clen = nelem;
    } else {
        clen = 0;
        isstg = false;
        isprt = false;
    }

    if (!att_buff) {
        if ((cnt + 4) > attbuf_size)
            attbuf_size = cnt + 4;
        att_buff = new unsigned char[attbuf_size];
        if (!att_buff) {
            if (verb) SPRTF("Memory allocation failed on %d bytes!\n", (int)attbuf_size);
            return false;
        }
    } else if ((cnt + 4) > attbuf_size) {
        attbuf_size = cnt + 4;
        delete att_buff;
        att_buff = new unsigned char[attbuf_size];
    }

    off = 0;
    while (cnt--) {
        if (cp >= end) {
            if ( !next_read()) { // time to read more
                if (verb) SPRTF("ran out of data in get attr len %d\n", (int)res );
                return false;
            }
        }
        c = *cp++;
        att_buff[off++] = (unsigned char)c;   // skip data

        if (isprt) {
            if (clen && (off <= clen)) {
                if (!isprint(c))
                    isprt = false;
            }
        }
    }
    att_buff[off] = 0;

    char *tmp = 0;
    NETCDF ncdf;
    NULLCDF(ncdf);

    ncdf.type = n_attr;

    switch(type) 
    {
	case NC_BYTE:
	case NC_CHAR:
        break;
	case NC_SHORT:
        if (cnt >= 2) {
            short s;
            tmp = GetNxtBuf();
            *tmp = 0;
            get_ix_short( att_buff, &s );
            sprintf(EndBuf(tmp),"%d", (int)s );
            ncdf.ndtyp[0].s = s;    // store short
            ncdf.tcnt = 1;
        }
        break;
	case NC_INT:
        if (cnt >= 4) {
            int i;
            tmp = GetNxtBuf();
            *tmp = 0;
            get_ix_int( att_buff, &i );
            sprintf(EndBuf(tmp),"%d", i );
            ncdf.ndtyp[0].i = i;
            ncdf.tcnt = 1;
        }
        break;
	case NC_FLOAT:
        if (cnt >= 4) {
            float f;
            tmp = GetNxtBuf();
            *tmp = 0;
            get_ix_float( att_buff, &f );
            sprintf(EndBuf(tmp),"%f", f );
            ncdf.ndtyp[0].f = f;
            ncdf.tcnt = 1;
        }
        break;
	case NC_DOUBLE:
        if (cnt >= 8) {
            tmp = GetNxtBuf();
            *tmp = 0;
            double d;
            get_ix_double( att_buff, &d );
            sprintf(EndBuf(tmp),"%lf", d );
            ncdf.ndtyp[0].d = d;
            ncdf.tcnt = 1;
            if (cnt >= 16) {
                strcat(tmp,", ");
                get_ix_double( &att_buff[8], &d );
                sprintf(EndBuf(tmp),"%lf", d );
                ncdf.ndtyp[1].d = d;
                ncdf.tcnt++;
            }
        }
        break;
	default:
        break;
	}

    cnt = res;
    if (cnt > 16)
        cnt = 16;
    if (isprt && isstg) {
        hex = (char *)att_buff;
    } else {
        hex = get_hex_str( att_buff, (int)cnt );
    }

    char *att = (char *)"attr";
    std::string msg("");

    if (in_var) {
        att = (char *)"var-attr";
        ncdf.type = n_varatt;
        if ((strcmp(last_str.c_str(),"actual_range") == 0) &&
            (type == NC_DOUBLE) &&
            (ncdf.tcnt == 2) )
        {
            if (strcmp(act_var.c_str(),"x") == 0) {
                // Done var-attr actual_range, type NC_DOUBLE, of 16 bytes -60.000000, 0.000000
                fmm.min_lon = ncdf.ndtyp[0].d;
                fmm.max_lon = ncdf.ndtyp[1].d;
                flag |= bf_fmmx;
                msg = "set x/lon min/max";
            } else if (strcmp(act_var.c_str(),"y") == 0) {
                // Done var-attr actual_range, type NC_DOUBLE, of 16 bytes -90.000000, -60.000000
                fmm.min_lat = ncdf.ndtyp[0].d;
                fmm.max_lat = ncdf.ndtyp[1].d;
                flag |= bf_fmmy;
                msg = "set y/lat min/max";
            } else if (strcmp(act_var.c_str(),"z") == 0) {
                // Done var-attr actual_range, type NC_DOUBLE, of 16 bytes -7329.000000, 3095.000000
                fmin_elev = ncdf.ndtyp[0].d;
                fmax_elev = ncdf.ndtyp[1].d;
                flag |= bf_fmmz;
                msg = "set z/alt min/max";
            } 
        } else if ((strcmp(last_str.c_str(),"_FillValue") == 0) &&
            (type == NC_SHORT) &&
            (ncdf.tcnt >= 1) ) 
        {
            if (strcmp(act_var.c_str(),"z") == 0) {
                fill_value = ncdf.ndtyp[0].s;
                flag |= bf_fil;
                msg = "set fill value";
            }
        }
    }

    ncdf.typ = type;
    ncdf.name = last_str;
    ncdf.txt = hex;
    vNetCDF.push_back(ncdf);

    if (verb) {
        SPRTF("Done %s %s, type %s, of %d bytes [%s] %s %s\n", att, last_str.c_str(), 
        (attr_type ? attr_type : "NOT SET!"), (int)res,
        hex.c_str(),
        (tmp ? tmp : ""),
        msg.c_str() );
    }
    return true;
}

bool netcdf::get_nc_vararray()
{
    size_t type,nelem,cnt;

    if (!get_size_t( &type )) {
        if (verb) SPRTF("ran out of data getting vararray type!\n" );
        return false;
    }

    if (!get_size_t( &nelem )) {
        if (verb) SPRTF("ran out of data getting vararray len!\n" );
        return false;
    }
	if(type != NC_VARIABLE) {
        if (verb) SPRTF("Not an NC_VARIABLE!\n" );
        return false;
    }
    if( nelem == 0 ) {
        if (verb) SPRTF("vararray has no length!\n" );
        return false;
    }

    if (verb) SPRTF("Get vararray: count %d\n", (int)nelem);
    in_var = true;
    for (cnt = 0; cnt < nelem; cnt++) {
        if (!get_nc_var()) {
            if (verb) SPRTF("Failed on %d of %d vararray elements!\n", (int)cnt, (int)nelem);
            return false;
        }
    }
    in_var = false;

    if (verb) SPRTF("Done vararray of %d elements\n", (int)nelem);

    return true;
}

bool netcdf::get_nc_var()
{
    size_t type, nelem, cnt, off;
    if (!get_nc_string()) {
        return false;
    }
    if (!get_size_t( &nelem )) {
        if (verb) SPRTF("ran out of data getting a var len!\n" );
        return false;
    }
    cnt = nelem * X_SIZEOF_INT;
    while (cnt--) {
        if (cp >= end) {
            if ( !next_read()) { // time to read more
                if (verb) SPRTF("ran out of data in get var len %d\n", (int)nelem );
                return false;
            }
        }
        cp++;   // skip data
    }
    act_var = last_str; // set to know which variable being processed, before getting attrarray for it
    if (verb) SPRTF("Done var %s, of %d bytes\n", last_str.c_str(), (int)nelem);

	if (!get_nc_attrarray()) {
        return false;
    }

    if (!get_size_t( &type )) {
        if (verb) SPRTF("ran out of data getting a var attr type!\n" );
        return false;
    }

    if (!get_size_t( &cnt ) ) {
        if (verb) SPRTF("ran out of data getting a var attr type!\n" );
        return false;
    }

    if (!get_size_t( &off ) ) {
        if (verb) SPRTF("ran out of data getting a var attr off!\n" );
        return false;
    }
    char *curr = attr_type;
    attr_type = 0;
    bool cv = verb;
    verb = false;
    size_t tcnt = cnt;
    if (type == NC_SHORT)
        tcnt /= sizeof(short);
    else if (type == NC_INT)
        tcnt /= sizeof(int);
    else if (type == NC_FLOAT)
        tcnt /= sizeof(float);
    else if (type == NC_DOUBLE)
        tcnt /= sizeof(double);
    size_t res = len_nc_attr( type, tcnt ); // length depends on type
    verb = cv;

    NETCDF ncdf;
    NULLCDF(ncdf);
    ncdf.type = n_var;
    ncdf.typ = type;
    ncdf.off = off;
    ncdf.len = tcnt;
    vNetCDF.push_back(ncdf);
    // set vars
    // size_t x_off, y_off, z_off;
    // size_t x_cnt, y_cnt, z_cnt;
    std::string msg("");
    if (strcmp(act_var.c_str(),"x") == 0) {
        x_off = off;
        x_cnt = tcnt;
        flag |= bf_xoff;
        msg = "set x off/cnt";
    } else if (strcmp(act_var.c_str(),"y") == 0) {
        y_off = off;
        y_cnt = tcnt;
        flag |= bf_yoff;
        msg = "set y off/cnt";
    } else if (strcmp(act_var.c_str(),"z") == 0) {
        z_off = off;
        z_cnt = tcnt;
        flag |= bf_zoff;
        msg = "set z off/cnt";
    }
    if(verb) SPRTF("Done var-attarray, type %d (%s), tcnt %s, off %d, len %s %s\n", (int)type,
        (attr_type ? attr_type : "NS"),
        get_NiceNumberStg(tcnt), (int)off, get_NiceNumberStg(res),
        msg.c_str());
    attr_type = curr;

    return true;

}

size_t netcdf::show_netcdf()
{
    size_t max = vNetCDF.size();
    size_t ii, jj;
    NETCDF ncdf;
    char *atyp, *tmp;
    for (ii = 0; ii < max; ii++) {
        ncdf = vNetCDF[ii];
        atyp = get_att_type(ncdf.typ);
        tmp = 0;
        switch (ncdf.type) {
        case n_dim:
            SPRTF("dim %s %d\n", ncdf.name.c_str(), (int)ncdf.val);
            break;
        case n_attr:
            if (ncdf.tcnt) {
                switch (ncdf.typ) {
                case NC_INT:
                    tmp = GetNxtBuf();
                    *tmp = 0;
                    for (jj = 0; jj < ncdf.tcnt; jj++) {
                        if (jj) strcat(tmp,", ");
                        sprintf(EndBuf(tmp),"%d", ncdf.ndtyp[jj].i);
                    }
                    break;
                case NC_FLOAT:
                    tmp = GetNxtBuf();
                    *tmp = 0;
                    for (jj = 0; jj < ncdf.tcnt; jj++) {
                        if (jj) strcat(tmp,", ");
                        sprintf(EndBuf(tmp),"%f", ncdf.ndtyp[jj].f);
                    }
                    break;
                case NC_DOUBLE:
                    tmp = GetNxtBuf();
                    *tmp = 0;
                    for (jj = 0; jj < ncdf.tcnt; jj++) {
                        if (jj) strcat(tmp,", ");
                        sprintf(EndBuf(tmp),"%lf", ncdf.ndtyp[jj].d);
                    }
                    break;
                }
            }
            SPRTF("attr %s (%s) %s\n", ncdf.name.c_str(), atyp, 
                (tmp ? tmp : ncdf.txt.c_str()) );
            break;
        case n_varatt:
            if (ncdf.tcnt) {
                switch (ncdf.typ) {
                case NC_INT:
                    tmp = GetNxtBuf();
                    *tmp = 0;
                    for (jj = 0; jj < ncdf.tcnt; jj++) {
                        if (jj) strcat(tmp,", ");
                        sprintf(EndBuf(tmp),"%d", ncdf.ndtyp[jj].i);
                    }
                    break;
                case NC_FLOAT:
                    tmp = GetNxtBuf();
                    *tmp = 0;
                    for (jj = 0; jj < ncdf.tcnt; jj++) {
                        if (jj) strcat(tmp,", ");
                        sprintf(EndBuf(tmp),"%f", ncdf.ndtyp[jj].f);
                    }
                    break;
                case NC_DOUBLE:
                    tmp = GetNxtBuf();
                    *tmp = 0;
                    for (jj = 0; jj < ncdf.tcnt; jj++) {
                        if (jj) strcat(tmp,", ");
                        sprintf(EndBuf(tmp),"%lf", ncdf.ndtyp[jj].d);
                    }
                    break;
                }
            }
            SPRTF("var-attr %s (%s) %s\n", ncdf.name.c_str(), atyp,
                (tmp ? tmp : ncdf.txt.c_str()) );

            if (strcmp(ncdf.name.c_str(),"long_name") == 0) {
                last_str = ncdf.txt;
            }
            break;
        case n_var:
            SPRTF("vararray type %s, count %d, offset %d\n", atyp, (int)ncdf.len, (int)ncdf.off);
            if (show_data && netfp) {
                if ( ncdf.typ == NC_SHORT )
                    show_short_array(ncdf);
                else if ( ncdf.typ == NC_DOUBLE )
                    show_double_array(ncdf);
                else
                    SPRTF("No server to show array! *** FIX ME ***\n");
             }
            break;
        default:
            SPRTF("Uncased type %d - FIX ME!\n", (int)ncdf.type );
            break;
        }
    }
    return max;
}


bool netcdf::set_file(std::string in_file)
{
    size_t res, cnt;
    int len;
    std::string hex1;
    bool bret = true;

    if (is_file_or_directory((char *)in_file.c_str()) != 1) {
        if (verb) SPRTF("Unable to 'stat' file %s\n", in_file.c_str());
        return false;
    }
    file_size = remaining = get_last_file_size();

    size_t rdsize = file_size;
    if (rdsize > RD_BLK_SIZE)
        rdsize = RD_BLK_SIZE;
    file = in_file;
    netfp = fopen(file.c_str(),"rb");
    if (!netfp) {
        if (verb) SPRTF("Can NOT open file %s\n",file.c_str());
        return false;
    }
    block = new unsigned char[rdsize];
    if (!block) {
        if (verb) SPRTF("Memory FAILED %d bytes!\n", (int)rdsize);
        bret = false;
        goto clean_up;
    }
    end = block + rdsize;
    buff_size = rdsize;
    res = fread(block,1,rdsize,netfp);
    if (res != rdsize) {
        if (verb) SPRTF("Failed read %s, request %d, got %d!\n", file.c_str(),
            (int)rdsize, (int)res);
        bret = false;
        goto clean_up;
    }

    remaining -= rdsize; // reduce remaining file bytes

    cp = block;
    len = sizeof(ncmagic1);
    hex1 = get_hex_str(cp,len);
    if (memcmp(cp,ncmagic1,len)) {
        if (verb) {
            SPRTF("File %s NOT CDF Version 1!\n", file.c_str());
            std::string hex2 = get_hex_str((unsigned char *)ncmagic1,len);
            SPRTF("Expected %s, got %s\n", hex2.c_str(), hex1.c_str());
        }
        bret = false;
        goto clean_up;

    }

    if (verb) SPRTF("Processing CDF file %s, version 1, of %s bytes, magic %s.\n", file.c_str(),
        get_NiceNumberStg(file_size),
        hex1.c_str());

    cp += len;  // bump past magic header bytes;

    if (!get_size_t( &res )) {
        if (verb) SPRTF("File %s ran out of bytes reading header!\n", file.c_str());
        bret = false;
        goto clean_up;
    }

    // get 'type'
    if (!get_size_t( &res )) {
        if (verb) SPRTF("File %s ran out of bytes reading header!\n", file.c_str());
        bret = false;
        goto clean_up;
    }

	if(res != NC_DIMENSION) {
        if (verb) SPRTF("File %s is NOT an NC_DIMENSION type!\n", file.c_str());
        bret = false;
        goto clean_up;
    }

    // get 'len'
    if (!get_size_t( &res )) {
        if (verb) SPRTF("File %s ran out of bytes reading header!\n", file.c_str());
        bret = false;
        goto clean_up;
    }

	if(res == 0) {
        if (verb) SPRTF("File %s has NO DIMENSION elements!\n", file.c_str());
        bret = false;
        goto clean_up;
    }

    if (verb) SPRTF("Get dimensions: count %d\n", (int)res);
    for (cnt = 0; cnt < res; cnt++) {
        if (!get_nc_dim()) {
            if (verb) SPRTF("File %s failed to get %d DIMENSION elements!\n", file.c_str(), (int)res);
            bret = false;
            goto clean_up;
        }
    }

    if (!get_nc_attrarray()) {
            if (verb) SPRTF("File %s failed getting attribute array!\n", file.c_str());
            bret = false;
            goto clean_up;

    }

    if (!get_nc_vararray()) {
            if (verb) SPRTF("File %s failed getting var array!\n", file.c_str());
            bret = false;
            goto clean_up;

    }

    if(verb) {
        SPRTF("Set file %s, of %s bytes.\n", file.c_str(), get_NiceNumberStg(file_size));
        rdsize = remaining;
        rdsize += end - cp;
        SPRTF("Processed %s bytes... balance %s\n", get_NiceNumberStg(file_size - rdsize),
             get_NiceNumberStg(rdsize));
        hex1 = get_flag_stg();
        SPRTF("Decoded following flags %s - %s.\n", hex1.c_str(),
            ((flag & bf_max) == bf_max) ? "ok ALL" :
            ((flag & bf_min) == bf_min) ? "ok MIN" :
            "bad NOT SUFFICIENT"
            );
    }
    return bret;

clean_up:
    clear();
    return bret;
}


size_t netcdf::show_double_array( NETCDF &ncdf )
{
    size_t max = ncdf.len;
    SPRTF("Data %s, count %d\n", last_str.c_str(), (int)max);
    size_t ii, off;
    double d1,d2;
    size_t wrap = 8;
    size_t cnt = 0;
    if (max) SPRTF("{\n");
    for (ii = 0; ii < max; ii++) {
        off = ncdf.off + (ii * 8);
        size_t res = fseek(netfp,off,SEEK_SET);
        if (res != 0) {
            SPRTF("%d: Failed seek to offset %d\n", (int)(ii + 1), (int)off);
            return ii;
        }
        res = fread(&d1,1,8,netfp);
        if (res != 8) {
            SPRTF("%d: Failed read at offset %d! req 8, got %d\n", (int)(ii + 1), (int)off, (int)res );
            return ii;
        }
        get_ix_double( &d1, &d2 );
        if ((ii + 1) == max)
            SPRTF("%1f",d2);
        else
            SPRTF("%lf, ",d2);
        cnt++;
        if (cnt == wrap) {
            SPRTF("\n");
            cnt = 0;
        }
    }
    SPRTF("}\n");
    return max;
}

size_t netcdf::show_short_array( NETCDF &ncdf )
{
    size_t max = ncdf.len;
    SPRTF("Data %s, count %d\n", last_str.c_str(), (int)max);
    size_t ii, off;
    short s1,s2;
    size_t wrap = 10;
    size_t cnt = 0;
    if (max) SPRTF("{\n");
    for (ii = 0; ii < max; ii++) {
        off = ncdf.off + (ii * 2);
        size_t res = fseek(netfp,off,SEEK_SET);
        if (res != 0) {
            SPRTF("%d: Failed seek to offset %d\n", (int)(ii + 1), (int)off);
            return ii;
        }
        res = fread(&s1,1,2,netfp);
        if (res != 2) {
            SPRTF("%d: Failed read at offset %d! req 2, got %d\n", (int)(ii + 1), (int)off, (int)res );
            return ii;
        }
        get_ix_short( &s1, &s2 );
        if ((ii + 1) == max)
            SPRTF("%d",s2);
        else
            SPRTF("%d, ",s2);
        cnt++;
        if (cnt == wrap) {
            SPRTF("\n");
            cnt = 0;
        }
    }

    SPRTF("}\n");
    return max;
}

bool netcdf::get_elevation( double lat, double lon, short *ps )
{
    std::string f,fil;
    int res;
    short s1;
    bool inlims = false;
    size_t off = z_off;
    size_t cnt = z_cnt;
    valid = false;
    rlat = lat;
    rlon = lon;

    if (!in_world_range(lat,lon)) {
        if (verb) SPRTF("%s: Given lat %s, and/or lon %s OUT OF WORLD RANGE!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }

    tile = get_srmt30_tile_name(lat,lon,0,0,false);
    if (!tile) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Can NOT get tile for given lat %s, lon %s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }

    if (!get_srmt30_tile_limits(tile, &tl)) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Can NOT get tile limits of %s!\n",module, tile);
        return false;
    }

    if ((flag & (bf_fmmx|bf_fmmy)) == (bf_fmmx|bf_fmmy)) {
        if ((lat <= fmm.max_lat) &&
            (lat >= fmm.min_lat) &&
            (lon <= fmm.max_lon) &&
            (lon >= fmm.min_lon)) {
            inlims = true;
        }
    }


    //if ((lat <= tl.max_lat) &&
    //    (lat >= tl.min_lat) &&
    //    (lon <= tl.max_lon) &&
    //    (lon >= tl.min_lon)) {
    //    inlims = true;
    //}

    if (inlims &&
        (file.size() > 0) && 
        off && cnt && netfp && 
        ((flag & bf_min) == bf_min)) {
       //if (inlims) {
       //    res = 1;
       //} else {
       //    f = get_file_name_only(file);
       //    res = InStri( (char *)f.c_str(), (char *)tile );
       //}
       //if (res) {
           // we have this tile loaded - just seek, and fetch value
           // hmmm, first convert the lat,lon to an x, y offset
           xcol = (int)((lon - tl.min_lon) / (tl.max_lon - tl.min_lon) * cols);
           yrow = (int)((lat - tl.min_lat) / (tl.max_lat - tl.min_lat) * rows);
           offset = (yrow * cols * 2) + (xcol * 2);
           off += offset;
           if (debug) {
               SPRTF("%s: Converted lon,lat %s,%s, to x,y %d,%d, offset %d, actual %d on %d\n", module,
                   get_trim_double(lon),
                   get_trim_double(lat),
                   xcol, yrow, (int)offset, (int)off, (int)file_size);
               if ((flag & bf_xoff) && (flag & bf_yoff)) {
                   int xx,yy;
                   if (find_xy_for_ll(lat,lon,&xx,&yy)) {
                       if ((xcol != xx) || (yrow != yy)) {
                           SPRTF("Computed x,y %d,%d NOT EQUAL lookup x,y %d,%d\n"
                               "*** FIX ME ***, or at least CHECK WHY!!!\n",
                               xcol, yrow, xx, yy );
                           return false;
                       }
                   }
               }
           }
           res = fseek(netfp,off,SEEK_SET);
            if (res != 0) {
                if (verb) SPRTF("Failed seek to offset %d\n", (int)off);
                return false;
            }
            res = fread(&s1,1,2,netfp);
            if (res != 2) {
                if (verb) SPRTF("Failed read at offset %d! req 2, got %d\n", (int)off, (int)res );
                return false;
            }
            get_ix_short( &s1, &elev );
            *ps = elev;
            valid = true;
            if (verb && !debug) {
               SPRTF("%s: Conv. lon,lat %s,%s to x,y %d,%d, off %d, file %d on %d, elev %d (%s)\n", module,
                   get_trim_double(lon),
                   get_trim_double(lat),
                   xcol, yrow, (int)offset, (int)off, (int)file_size,
                   elev,
                   get_file_name(file).c_str() );
            }
       //}
        return true;
    }

    if (no_ncdf_dir)
        return false;


    if (ncdf_dir.size() == 0) {
        if (verb) SPRTF("Is NOT in currect file, and no netcdf.nc directory to search!\n");
        return false;
    }
    if (ncdf_files.size() == 0) {
        if (verb) SPRTF("Is NOT in currect file, and no netcdf.nc files to search!\n");
        return false;
    }
    size_t ii, max = ncdf_files.size();
    for (ii = 0; ii < max; ii++) {
        fil = ncdf_files[ii];
        f = get_file_name_only(fil);
        res = InStri( (char *)f.c_str(), (char *)tile );
        if (res)
            break;
    }
    if (ii == max) {
        if (verb) SPRTF("File matching tile %s, not found in netcdf direcotry given!\n", tile);
        return false;
    }

    no_ncdf_dir = true; // should NEVER happen, but protect anyway
    bool bret = load_and_get_elevation( fil, lat, lon, ps );
    no_ncdf_dir = false;
    return bret;
}


bool netcdf::find_xy_for_ll(double lat, double lon, int *px, int *py)
{
    if (!netfp)
        return false;
    if ( !((flag & bf_xoff) && (flag & bf_yoff)) )
        return false;
    int x, y;
    double lat1,lat2,lon1,lon2;
    double d1,d2;
    size_t ii, off, res;
    x = 0;
    y = 0;
    lat1 = 0.0;
    lon1 = 0.0;
    for (ii = 0; ii < x_cnt; ii++) {
        off = x_off + ( ii * 8 );
        res = fseek(netfp,off,SEEK_SET);
        if (res != 0) {
            SPRTF("%d: Failed seek to offset %d\n", (int)(ii + 1), (int)off);
            return false;
        }
        res = fread(&d1,1,8,netfp);
        if (res != 8) {
            SPRTF("%d: Failed read at offset %d! req 8, got %d\n", (int)(ii + 1), (int)off, (int)res );
            return false;
        }
        get_ix_double( &d1, &d2 );
        lon2 = lon1;
        lon1 = d2;
        if (ii) {
            if ((lon >= lon2) &&
                (lon <= lon1)) {
                break;
            }
        }
    }
    if (ii == x_cnt) {
        return false;
    }
    x = (int)ii;
    for (ii = 0; ii < y_cnt; ii++) {
        off = y_off + ( ii * 8 );
        res = fseek(netfp,off,SEEK_SET);
        if (res != 0) {
            SPRTF("%d: Failed seek to offset %d\n", (int)(ii + 1), (int)off);
            return false;
        }
        res = fread(&d1,1,8,netfp);
        if (res != 8) {
            SPRTF("%d: Failed read at offset %d! req 8, got %d\n", (int)(ii + 1), (int)off, (int)res );
            return false;
        }
        get_ix_double( &d1, &d2 );
        lat2 = lat1;
        lat1 = d2;
        if (ii) {
            if ((lat >= lat2) &&
                (lat <= lat1)) {
                break;
            }
        }
    }
    if (ii == y_cnt) {
        return false;
    }
    y = (int)ii;
    *px = x;
    *py = y;
    SPRTF("Found x,y %d,%d within range lon,lat %s,%s to %s,%s\n",
        x, y,
        get_trim_double(lon1),
        get_trim_double(lat1),
        get_trim_double(lon2),
        get_trim_double(lat2) );

    off = z_off + ( y * x_cnt * 2 ) + ( x * 2 );
    res = fseek(netfp,off,SEEK_SET);
    if (res == 0) {
        short elev,s2;
        res = fread(&s2,1,2,netfp);
        if (res == 2) {
            get_ix_short( &s2, &elev );
            SPRTF("lat/y,lon/x %s/%d,%s/%d is elevation %d\n",
                get_trim_double(lat), y,
                get_trim_double(lon), x,
                elev );
        }
    }
    return true;
}

bool netcdf::set_netcdf_dir( std::string dir )
{
    if (is_file_or_directory((char *)dir.c_str()) != 2) {
        if (verb) SPRTF("Input %s NOT a valid directory!\n", dir.c_str());
        return false;
    }
    ncdf_files = read_directory( dir, "*.nc",  dm_FILE );
    size_t max = ncdf_files.size();
    if (!max) {
        if (verb) SPRTF("In %s found no *.nc files!\n", dir.c_str());
        return false;
    }
    if (verb) SPRTF("Found %d *.nc files in %s.\n", (int)max, dir.c_str());
    ncdf_dir = dir;
    return true;
}

bool netcdf::load_and_get_elevation( std::string in_file, double lat, double lon, short *ps )
{
    bool bret;
    netcdf c;
    c.verb = false;
    if (!c.set_file(in_file)) {
        if (verb) SPRTF("%s: Failed to load %s\n", module, in_file.c_str());
        return false;
    }
    c.verb = verb;
    bret = c.get_elevation(lat,lon,ps);
    c.clear();
    if (!bret) {
        if (verb) SPRTF("%s: Failed to get elevation for lat,lon %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon));
    }
    return bret;
}


// eof
