// stopo30.cxx
/* ============================================================================

    **************************************************************************
    This module deals only with the topo30 single large file of 2-byte integers
    in MSB format (i.e. big-endian).  The grid spans 0 to 360 in longitude 
    and -90 to 90 in latitude. The upper left corner of the upper left grid cell
    has latitude 90 and longitude 0.  There are 43200 columns and 21600 rows.

    File is TOO LARGE to load into memory - 1,866,240,000 bytes, so all actions 
    use fseek(). In my directory system it is stored in D:\SRTM\scripps\topo30\topo30

    It is a combination of SRTM 'land' data, with all 'voids' filled in, and
    bathymetric data for the oceans. A loverly effort at 30 arc-secs.

    **************************************************************************

    Read more about all the data from the Scripps Institute of Oceanography,
    University of California, San Diego, Californai, USA.

    from : http://topex.ucsd.edu/WWW_html/srtm30_plus.html

    SRTM30_PLUS: DATA FUSION OF SRTM LAND TOPOGRAPHY WITH MEASURED AND ESTIMATED SEAFLOOR TOPOGRAPHY

    SRTM30_PLUS V9.0 - December 19, 2013
    David T. Sandwell dsandwell@ucsd.edu
    Chris Olson <cjolson@ucsd.edu>
    Amber Jackson <amberleajackson@gmail.com>
    Joseph J. Becker <joseph.becker.ctr@nrlssc.navy.mil>

    The SRTM30_PLUS is available in 2 formats.

    1) topo30 format
    The  subdirectory called topo30 has the data 
    stored in a single large file of 2-byte integers
    in MSB format (i.e. big-endian).  The grid spans 
    0 to 360 in longitude and -90 to 90 in latitude. 
    The upper left corner of the upper left grid cell
    has latitude 90 and longitude 0.  There are 
    43200 columns and 21600 rows. A matching source 
    identification file (SID) called topo30_sid is also 
    included.  The sid numbers are stored as unsigned 
    2-byte integers.

    2) srtm30 format
    The directory called srtm30 has the same data in
    original SRTM30 format consisting of 33 tiles
    is described below.

    DIRECTORIES
    data - 33 files of signed 2-byte integers for global elevation(> 0)
           and depth (<0). The global elevations are an exact copy of the
           SRTM30 grids provided at the following location.  Our contribution
           is to fill the ocean areas with some estimate of depth.
           ftp://e0srp01u.ecs.nasa.gov

   ============================================================================ */
#include <stdio.h>
#include <string>
#ifndef _MSC_VER
#include <limits.h> // for SHRT_MIN, ...
#endif
#include <limits>
#include "utils.hxx"
#include "sprtf.hxx"
#include "dir_utils.hxx"
#include "dem_utils.hxx"
#include "color.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "stopo30.hxx"

static const char *module = "stopo30";

void stopo30::init()
{
    file = "";
    // stopo_data = 0;
    verb = true;
    done_test = false;
    stopo_fp = 0;
    // setup the 30 second variables
    xdim = 360.0 / (double)STOPO30_COLS;
    ydim = 180.0 / (double)STOPO30_ROWS;
    little_endian = !test_endian();

    // constants
    cols = STOPO30_COLS;
    rows = STOPO30_ROWS;

    // span variables
    spanfp = 0;
    row_buf = 0;
    row_buf_size = 0;
    done_full_scan = false;
}

stopo30::stopo30()
{
    init();
}
stopo30::~stopo30()
{
    //if (stopo_data)
    //    delete [] stopo_data;
    //stopo_data = 0;
    if (stopo_fp)
        fclose(stopo_fp);
    stopo_fp = 0;

    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    row_buf_size = 0;
}

bool stopo30::set_file( std::string in_file )
{
    if (is_file_or_directory((char *)in_file.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file %s\n", module, in_file.c_str());
        return false;
    }
    file_size = get_last_file_size();
    if (file_size != (STOPO30_COLS * STOPO30_ROWS * 2)) {
        if (verb) SPRTF("%s: File size is %d, instead of %d\n", module, (int)file_size,
            (STOPO30_COLS * STOPO30_ROWS * 2) );
        return false;
    }
    file = in_file;
    if (verb) SPRTF("%s: Set %s as input file, %s bytes.\n", module,
        file.c_str(), get_NiceNumberStg(file_size));
    return true;

}


// should be an INLINE macro, for speed
void move_up( PMAXMIN pmm )
{
    int z, z2;
    short elev;
    double lat,lon;
    int x, y;
    PMAXMIN p1,p2;
    x = 0;
    for (z = (MAX_MM - 1); z > 0; z--) {
        z2 = z - 1;
        p1 = &pmm[z2]; // get from bottom/lower
        p2 = &pmm[z];  // stuff in top/upper
        elev = p1->elev;
        lat  = p1->lat;
        lon =  p1->lon;
        x    = p1->x;
        y    = p1->y;
        p2->elev = elev;
        p2->lat  = lat;
        p2->lon  = lon;
        p2->x    = x;
        p2->y    = y;
    }
}

//void move_min_up() { move_up(minm); }
//void move_max_up() { move_up(maxm); }

/* -----------------------------------------------------
    in MSB format (i.e. big-endian), so need swapping in Intel.
    
    The grid spans 0 to 360 in longitude and -90 to 90 in latitude. 

    The upper left corner of the upper left grid cell has latitude 90 and longitude 0.
    
    There are 43200 columns and 21600 rows.
  ------------------------------------------------------ */
bool stopo30::test_file( bool scan )
{
    if (is_file_or_directory((char *)file.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file %s\n", module, file.c_str());
        return false;
    }
    file_size = get_last_file_size();
    if (file_size != (STOPO30_COLS * STOPO30_ROWS * 2)) {
        if (verb) SPRTF("%s: File size is %d, instead of %d\n", module, (int)file_size,
            (STOPO30_COLS * STOPO30_ROWS * 2) );
        return false;
    }
    if (verb) {
        SPRTF("%s: Set '%s' as input file. %s bytes\n", module,
            file.c_str(),
            get_NiceNumberStg(file_size));
    }
    FILE *tfp = fopen( file.c_str(), "rb" );
    if (!tfp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, file.c_str());
        return false;
    }
    done_test = true;
    if (!scan) {
        fclose(tfp);
        if (verb) SPRTF("%s: Tested file %s, %s bytes...\n", module, file.c_str(),
            get_NiceNumberStg(file_size) );
        return true;
    }
    // wants a FULL scan
    short elev;
    max_elev = SHRT_MIN;
    min_elev = SHRT_MAX;
    semin.elev = SHRT_MAX;
    semax.elev = SHRT_MIN;
    swmin.elev = SHRT_MAX;
    swmax.elev = SHRT_MIN;
    nwmax.elev = SHRT_MIN;
    nwmin.elev = SHRT_MAX;
    //int cols = STOPO30_COLS;    // 43200;
    //int rows = STOPO30_ROWS;    // 21600;
    //int xspan = 360;
    //int yspan = 180;
    //double xdim = (double)xspan / (double)cols;
    //double ydim = (double)yspan / (double)rows;
    //double xsecs = xdim * 3600.0;
    //double ysecs = ydim * 3600.0;
    int x,y;
    x = 0;
    y = 0;
    double min_lat, min_lon, max_lat, max_lon;
    double lat,lon;
    if (verb) SPRTF("%s: Doing FULL scan... patience...\n", module );

    while (!feof(tfp)) {
        size_t res = fread(&elev,1,2,tfp);
        if (res == 0) 
            continue;
        if (res != 2) {
            if (verb) SPRTF("%s: Read %s FAILED! Reqested %d, got %d\n", module, 
                file.c_str(), 2, (int)res );
            fclose(tfp);
            return false;
        }

        if (little_endian) {
            SWAP16(&elev);
        }

        // upper left = latitude 90 and longitude 0 (or is that -180)
        // lon = 180.0 - (180.0 - (x * xdim));
        lon = (x * xdim); // of 360
        if (lon > 180.0)
            lon -= 360.0;
        lat = 90.0 - (y * ydim); // of 180
        if (elev < min_elev) {
            min_elev = elev;
            min_lon = lon;
            min_lat = lat; // of 180
            move_up(minm); // move_min_up();
            minm[0].elev = min_elev;
            minm[0].lat  = min_lat;
            minm[0].lon  = min_lon;
            minm[0].x    = x;
            minm[0].y    = y;
        }
        if (elev > max_elev) {
            max_elev = elev;
            max_lon = lon;
            max_lat = lat;
            move_up(maxm); // move_max_up();
            maxm[0].elev = max_elev;
            maxm[0].lat  = max_lat;
            maxm[0].lon  = max_lon;
            maxm[0].x    = x;
            maxm[0].y    = y;
        }
        if ((lat < 0.0) && (lon < 0.0)) {
            if (elev < swmin.elev) {
                swmin.elev = elev;
                swmin.lat  = lat;
                swmin.lon  = lon;
                swmin.x    = x;
                swmin.y    = y;
            }
            if (elev > swmax.elev) {
                swmax.elev = elev;
                swmax.lat  = lat;
                swmax.lon  = lon;
                swmax.x    = x;
                swmax.y    = y;
            }
        }
        if ((lat < 0.0) && (lon >= 0.0)) {
            if (elev < semin.elev) {
                semin.elev = elev;
                semin.lat  = lat;
                semin.lon  = lon;
                semin.x    = x;
                semin.y    = y;
            }
            if (elev > semax.elev) {
                semax.elev = elev;
                semax.lat  = lat;
                semax.lon  = lon;
                semax.x    = x;
                semax.y    = y;
            }
        }
        if ((lat >= 0.0) && (lon < 0.0)) {
            if (elev < nwmin.elev) {
                nwmin.elev = elev;
                nwmin.lat  = lat;
                nwmin.lon  = lon;
                nwmin.x    = x;
                nwmin.y    = y;
            }
            if (elev > nwmax.elev) {
                nwmax.elev = elev;
                nwmax.lat  = lat;
                nwmax.lon  = lon;
                nwmax.x    = x;
                nwmax.y    = y;
            }
        }

        // bump the x,y
        x++;
        if (x == STOPO30_COLS) {
            y++;
            x = 0;
        }
    }
    fclose(tfp);
    if (verb) {
        int z;
        SPRTF("%s: Found: List of %d maximums and minimums...\n", module, MAX_MM);
        for (z = 0; z < MAX_MM; z++) {
            SPRTF(" max %lf,%lf = %d - y,x %d,%d\n", maxm[z].lat, maxm[z].lon, maxm[z].elev, maxm[z].y, maxm[z].x );
        }
        for (z = 0; z < MAX_MM; z++) {
            SPRTF(" min %lf,%lf = %d - y,x %d,%d\n", minm[z].lat, minm[z].lon, minm[z]. elev,minm[z].y, minm[z].x );
        }
        SPRTF(" Known: NE min { \"Marianas Trench\", 11.317, 142.25, -10911 }, "
            "max { \"Mount Everest\",  27.988, 86.9253, 8848   }\n");
        SPRTF(" Found: SW min %s,%s = %d - y,x %d,%d max %s,%s = %d - y,x %d,%d \n",
            get_trim_double(swmin.lat),
            get_trim_double(swmin.lon),
            swmin.elev,
            swmin.y, swmin.x,
            get_trim_double(swmax.lat),
            get_trim_double(swmax.lon),
            swmax.elev,
            swmax.y, swmax.x );
        SPRTF(" Known: SW min { \"Honzon Deep\", -23.266667, -174.75, -10800 }, "
            "max { \"Cerro Aconcagua\", -32.6555555555556, -70.0158333333333, 6962 }\n");
        SPRTF(" Found: SE min %s,%s = %d - y,x %d,%d max %s,%s = %d - y,x %d,%d \n",
            get_trim_double(semin.lat),
            get_trim_double(semin.lon),
            semin.elev,
            semin.y, semin.x,
            get_trim_double(semax.lat),
            get_trim_double(semax.lon),
            semax.elev,
            semax.y, semax.x );
        SPRTF(" Known: SE min  { \"Amirante Basin\", -6.55, 154.016667, -9104 }, "
            "max { \"Kilimanjaro, Tanzania\", -3.0758333333, 37.353333333, 5895 }\n");
        SPRTF(" Found: NW min %s,%s = %d - y,x %d,%d max %s,%s = %d - y,x %d,%d \n",
            get_trim_double(nwmin.lat),
            get_trim_double(nwmin.lon),
            nwmin.elev,
            nwmin.y, nwmin.x,
            get_trim_double(nwmax.lat),
            get_trim_double(nwmax.lon),
            nwmax.elev,
            nwmax.y, nwmax.x );
        SPRTF(" Known: NW min { \"Puerto Ricao Trench, 19.866667,-66.358333, -8518 }, "
            "max { \"Mt McKinley, Alaska\", 63.0690, -151.0063, 6168 }\n");

        SPRTF("%s: Tested file %s, %s bytes, elevation max %d, min %d\n", module,
            file.c_str(), get_NiceNumberStg(file_size), max_elev, min_elev );
        SPRTF("%s: Positions: lat,lon max %s,%s min %s,%s\n", module,
            get_trim_double(max_lat),
            get_trim_double(max_lon),
            get_trim_double(min_lat),
            get_trim_double(min_lon) );
    }
    done_full_scan = true;
    return true;

}
bool stopo30::check_file()
{
    if (file.size() == 0) {
        if (verb) SPRTF("%s: No input file given! Use set_file(file).\n",module);
        return false;
    }
    if (!stopo_fp) {
        if (!done_test) {
            if (!test_file(false)) {
                return false;
            }
        }
        stopo_fp = fopen( file.c_str(), "rb" );
        if (!stopo_fp) {
            if (verb) SPRTF("%s: Unable to 'open' file %s!\n", module, file.c_str());
            return false;
        }
    }
    return true;
}

bool stopo30::get_elevation( double lat, double lon, short *pelev )
{
    double clon = lon;
    double clat = 90.0 - lat;
    if (clon < 0) {
        clon += 360;
    }
    x_col = (int)(clon / xdim);
    y_row = (int)(clat / ydim);

    if (!check_file())
        return false;

    if (!in_world_range( lat, lon )) {
        if (verb) SPRTF("%s: lat %s and/or lon %s NOT in world range!\n", module, 
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }
    if (x_col < 0) {
        if (verb) SPRTF("%s: Longitude %s NOT HANDLED YET!\n", module, get_trim_double(lon));
        return false;
    }
    if (y_row < 0) {
        if (verb) SPRTF("%s: Latitude %s NOT HANDLED YET!\n", module, get_trim_double(lat));
        return false;
    }
    if (x_col >= STOPO30_COLS) {
        if (verb) SPRTF("%s: Longitude %s gave %d - out of max range %d!\n", module,
            get_trim_double(lon), x_col, STOPO30_COLS);
        return false;
    }
    if (y_row >= STOPO30_ROWS) {
        if (verb) SPRTF("%s: Latitude %s gave %d - out of max range %d!\n", module,
            get_trim_double(lat), y_row, STOPO30_ROWS);
        return false;
    }

    // calculate offset into file
    offset = ((y_row * STOPO30_COLS) + x_col ) * sizeof(short);

    if (verb) SPRTF("%s: Seeking to row %d of %d, col %d of %d = offset %s on %s\n", module,
        y_row, STOPO30_ROWS, x_col, STOPO30_COLS,
        get_NiceNumberStg(offset),
        get_NiceNumberStg(file_size) );

    // position file for read
    size_t res = fseek(stopo_fp,offset,SEEK_SET);
    if (res) {
        if (verb)SPRTF("%s: File seek FAILED on request %s, got NON-ZERO %d\n", module,
            get_NiceNumberStg(offset), (int)res);
        return false;
    }
    short elev;
    // get two bytes
    res = fread(&elev,1,2,stopo_fp);
    if (res != 2) {
        if (verb) SPRTF("%s: Read at %s offset FAILED! Reqested %d, got %d\n", module,
            get_NiceNumberStg(offset), 2, (int)res );
        return false;
    }
    if (little_endian) {   // note, test done in ctro init()
        SWAP16(&elev);  // not requied on big-endian (Motrolla) CPU
    }
    last_elev = elev;

    if(verb) SPRTF("%s: For lat,lon %s,%s, returning elevation %d\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        last_elev);
    *pelev = last_elev;
    return true;

}

bool stopo30::get_elevation_block( int sx, int sy, int ex, int ey, short *buffer, size_t bsize )
{
    int x,y,ox,oy;
    size_t off;
    if (!check_file()) {
        return false;
    }
    int width = ex - sx;
    int height = ey - sy;
    size_t tsize = (width * height);
    short elev;
    if ((sx < 0)||(sx >= STOPO30_COLS)||
        (ex < 0)||(ex >= STOPO30_COLS)||
        (sy < 0)||(sy >= STOPO30_ROWS)||
        (ey < 0)||(ey >= STOPO30_ROWS)||
        (sx >= ex)||(sy >= ey)||
        ( tsize > bsize)) {
        if (verb) {
            SPRTF("%s: Invalid PARAMETER(S)! ",module);
            if (sx < 0) SPRTF("start x %d LSS 0! ",sx);
            if (sx >= STOPO30_COLS) SPRTF("start x %d GTE max cols %d ",sx,STOPO30_COLS);
            if (ex < 0) SPRTF("end x %d LSS 0! ",ex);
            if (ex >= STOPO30_COLS) SPRTF("end x %d GTE max cols %d! ",ex,STOPO30_COLS);
            if (sy < 0) SPRTF("start y LSS 0! ",sy);
            if (sy >= STOPO30_ROWS) SPRTF("start y %d GTE max rows %d! ",sy, STOPO30_ROWS);
            if (ey < 0) SPRTF("end y LSS 0! ",ey);
            if (ey >= STOPO30_ROWS) SPRTF("end y %d GTE max rows %d! ",ey,STOPO30_ROWS);
            if (sx >= ex) SPRTF("start x %d GTE end x %d! ",sx,ex);
            if (sy >= ey) SPRTF("start y %d GTE end y %d! ",sy,ey);
            if (tsize > bsize) SPRTF("calc size %d GTT given %d! ", (int)tsize, (int)bsize);
            SPRTF("\n");
        }
        return false;
    }
    ox = 0;
    oy = 0;
    for (y = sy; y < ey; y++) {
        ox = 0;
        for (x = sx; x < ex; x++) {
            size_t offset = ((y * STOPO30_COLS) + x ) * sizeof(short);
            // position file for read
            size_t res = fseek(stopo_fp,offset,SEEK_SET);
            if (res) {
                if (verb)SPRTF("%s: File seek FAILED on request %d, got NON-ZERO %d\n", module,
                    (int)offset, (int)res);
                return false;
            }
            res = fread(&elev,1,2,stopo_fp);
            if (res != 2) {
                if (verb) SPRTF("%s: Read at %d offset FAILED! Reqested %d, got %d\n", module,
                    (int)offset, 2, (int)res );
                return false;
            }
            if (little_endian) {
                SWAP16(&elev);
            }
            off = (oy * width) + ox;
            if (off >= bsize) {
                if (verb) {
                    SPRTF("%s: ZUTE: Offset %d GTT %d for x,y %d,%d!\n", module,
                        (int)off,
                        (int)bsize,
                        ox, oy );
                }
                return false;
            }
            buffer[off] = elev;
            ox++;
        }
        oy++;
    }
    return true;
}


bool stopo30::get_elevation_group( double lat, double lon, int bracket, vSHRT **pv, int *colcnt )
{
    double clon = lon;
    double clat = 90.0 - lat;
    if (clon < 0) {
        clon += 360;
    }
    int x = (int)(clon / xdim);
    int y = (int)(clat / ydim);
    
    if (!check_file())
        return false;

    if (!in_world_range( lat, lon )) {
        if (verb) SPRTF("%s: Bad parameter: lat %s, and/or lon %s, NOT in world range!\n", module,
            get_trim_double(lat), 
            get_trim_double(lon) );
        return false;
    }
    if (bracket <= 0) {
        if (verb) SPRTF("%s: Bad parameter: Bracket MUST be a positive integer!\n", module);
        return false;
    }
    if (!pv) {
        if (verb) SPRTF("%s: Bad parameter: No vSHRT ** given!\n", module);
        return false;
    }

    if (x < 0) {
        if (verb) SPRTF("%s: Longitude %s NOT HANDLED YET!\n", module,
            get_trim_double(lon));
        return false;
    }
    if (y < 0) {
        if (verb) SPRTF("%s: Latitude %s NOT HANDLED YET!\n", module,
            get_trim_double(lat));
        return false;
    }
    if (x >= STOPO30_COLS) {
        if (verb) SPRTF("%s: Longitude %s gave %d - out of max range %d!\n", module,
            get_trim_double(lon),
            x, STOPO30_COLS);
        return false;
    }
    if (y >= STOPO30_ROWS) {
        if (verb) SPRTF("%s: Latitude %lf gave %d - out of max range %d!\n", module,
            get_trim_double(lat),
            y, STOPO30_ROWS);
        return false;
    }

    int sx = x - bracket;
    int ex = x + bracket;
    int sy = y - bracket;
    int ey = y + bracket;
    // check limits
    if (sx < 0)
        sx = 0;
    if (ex >= STOPO30_COLS)
        ex = STOPO30_COLS - 1;
    if (sy < 0)
        sy = 0;
    if (ey >= STOPO30_ROWS)
        ey = STOPO30_ROWS - 1;
    int count = 0;
    if (colcnt)
        *colcnt = ex - sx + 1;
    for (y = sy; y <= ey; y++) {
        for (x = sx; x <= ex; x++) {
            count++;
        }
    }
    if (count == 0) {
        if (verb) SPRTF("%s: Failed to get a group to return!\n", module);
        return false;
    }

    // ready to party getting all the points
    vSHRT *vs = new vSHRT;
    short elev;
    double mlon = (x * xdim);
    if (mlon > 180.0)
        mlon -= 360.0;
    double mlat =  90.0 - (y * ydim);
    double max_lon = mlon;
    double min_lon = mlon;
    double max_lat = mlat;
    double min_lat = mlat;
    short maxelev = SHRT_MIN;
    short minelev = SHRT_MAX;
    for (y = sy; y <= ey; y++) {
        for (x = sx; x <= ex; x++) {
            // calculate the lat, lon of this point
            mlon = (x * xdim);
            if (mlon > 180.0)
                mlon -= 360.0;
            mlat =  90.0 - (y * ydim);
            if ( mlon > max_lon ) max_lon = mlon;
            if ( mlon < min_lon ) min_lon = mlon;
            if ( mlat > max_lat ) max_lat = mlat;
            if ( mlat < min_lat ) min_lat = mlat;
            // calculate offset into file
            size_t offset = ((y * STOPO30_COLS) + x ) * sizeof(short);
            // position file for read
            size_t res = fseek(stopo_fp,offset,SEEK_SET);
            if (res) {
                if (verb)SPRTF("%s: File seek FAILED on request %d, got NON-ZERO %d\n", module,
                    (int)offset, (int)res);
                delete vs;
                return false;
            }
            res = fread(&elev,1,2,stopo_fp);
            if (res != 2) {
                if (verb) SPRTF("%s: Read at %d offset FAILED! Reqested %d, got %d\n", module,
                    (int)offset, 2, (int)res );
                delete vs;
                return false;
            }
            if (little_endian) {
                SWAP16(&elev);
            }
            vs->push_back(elev);
            if (elev > maxelev) maxelev = elev;
            if (elev < minelev) minelev = elev;
        }
    }
    *pv = vs;
    if (verb) {
        SPRTF("%s: Range: lat,lon max %s,%s, min %s,%s, y-span %s, x-span %s degrees.\n", module,
            get_trim_double(max_lat), 
            get_trim_double(max_lon),
            get_trim_double(min_lat),
            get_trim_double(min_lon),
            get_trim_double((max_lat - min_lat)),
            get_trim_double((max_lon - min_lon)));
        SPRTF("%s: Got %d elevations, min %d, max %d meters.\n", module,
            (int) vs->size(),
            minelev, maxelev );
    }
    return true;
}

bool stopo30::begin_span_file( std::string srtm )
{
    bool cv = verb;
    verb = 0;
    // =========================================================================
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: begin_span_file: failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================
    verb = cv;
    spanfp = fopen(srtm.c_str(),"rb");
    if (!spanfp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    last_elev = 0;
    return true;
}

bool stopo30::end_span_file()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    row_buf_size = 0;
    return true;
}

// this could lead to more sofisticated 'averaging' of elevations
bool stopo30::get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps )
{
    int y,cnt,elev;
    size_t off,res,xlen,x,xspan;
    short s1,s2;
    int end_x = bgn_x + span_x;
    int end_y = bgn_y + span_y;
    if (end_x > cols)
        end_x = cols;
    if (end_y > rows)
        end_y = rows;
    if ((bgn_x < 0) || (bgn_y < 0) || (span_x < 1) || (span_y < 1) || !ps || !spanfp ) {
        if (verb) SPRTF("%s: Bad parameter! x=%d y=%d spx=%d spy=%d %s %s\n", module,
            bgn_x, bgn_y, span_x, span_y,
            (ps ? "" : "ps=<null> pointer"),
            (spanfp ? "" : "file closed"));
        return false;
    }
    cnt = 0;
    elev = 0;
    xspan = end_x - bgn_x;
    xlen = (xspan * 2);
// try again - #if 0 // ok, but this did not work???
    if (row_buf == 0) {
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }
    } else if (xlen > row_buf_size) {
        delete row_buf;
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }

    }
    if (xlen) {
        for (y = bgn_y; y < end_y; y++) {
            off = ( y * cols * 2 ) + ( bgn_x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)off, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(row_buf,1,xlen,spanfp);
            if (res != xlen) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            for (x = 0; x < xspan; x++) {
                short *sp = (short *)( &row_buf[x * 2] );
                s1 = *sp;
                get_ix_short( &s1, &s2 );
                elev += s2;
                cnt++;
            }
        }
    }
// === #endif // 0

#if 0 // ok, this work but is slow, maybe due to the many seeks???
    for (y = bgn_y; y < end_y; y++) {
        for (x = bgn_x; x < end_x; x++) {
            off = ( y * cols * 2 ) + ( x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)offset, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(&s1,1,2,spanfp);
            if (res != 2) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            get_ix_short( &s1, &s2 );
            elev += s2;
            cnt++;
        }
    }
#endif // 0
    if (cnt) {
        // this is a simple AVERAGE - could be much more 'complex'
        last_elev = (elev / cnt);
    } else {
        if (verb) SPRTF("%s: WARNING: Returning previous elev %d for x,y %d,%d span %d,%d on %d,%d\n", module,
            last_elev, x, y, span_x, span_y, cols, rows );
    }
    *ps = last_elev;
    return true;
}

int stopo30::write_stopo30_image(int width, int height, const char *img_file) // build a width x height image of whole file
{
    int iret = 0;
    int x,y,spanx, spany;
    double dspanx, dspany;
    int bgn_x,bgn_y;
    //bool ok = true;
    short elev;
    unsigned int colr;
    size_t bsize = width * height * 3;
    size_t doff;
    unsigned char r,g,b;
    double bgn = get_seconds();
    char *secs;
    char *out_img = (char *)img_file;
    unsigned char *buffer = new unsigned char[bsize];
    if (!buffer) {
        if (verb) {
            SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)bsize);
            //check_me();
        }
        iret = 1;
        goto exit;
    }
    if (!begin_span_file(stopo30_fil)) {
        if (verb) {
            SPRTF("%s: Failed in stopo30::begin_span_file(%s)!\n", module, stopo30_fil);
        }
        iret = 2;
        goto exit;
    }
    // ready to party
    spany = rows / height;
    if (rows % height) 
        spany++;
    spanx = cols / width;
    if (cols % width) 
        spanx++;
    dspany = (double)rows / (double)height;
    dspanx = (double)cols / (double)width;
    if (verb) {
        SPRTF("%s: Processing file %s, x,y %d,%d, %d bytes, span %d,%d, incs %s,%s\n", module,
            file.c_str(),
            cols, rows,
            (int) file_size,
            spanx, spany,
            get_trim_double(dspanx),
            get_trim_double(dspany) );
    }
    clear_used_colors();
    for (y = 0; y < height; y++) {
        bgn_y = (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = (int)((double)x * dspanx);
            if ( !get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev) ) {
                if (verb) {
                    SPRTF("%s: UGH: FAILED x %d, y %d!\n", module, x, y );
                    //check_me();
                }
                iret = 3;
                goto exit;
            }
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                if (verb) {
                    SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                        x, y, (int)doff, (int)bsize );
                    //check_me();
                }
                iret = 4;
                goto exit;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
        }
    }
    end_span_file();
#ifdef USE_PNG_LIB
    if (!out_img)
        out_img = get_out_png();
    if (out_img) {
        if (writePNGImage24(out_img, width, height, 8, 6, buffer )) {
            iret |= 8;
        }
    }
#else // !USE_PNG_LIB
    if (out_img)
        out_img = get_out_bmp();
    if (out_img) {
        if (writeBMPImage24(out_img, width, height, buffer, bsize)) {
            iret |= 4;
        }
    }
#endif // USE_PNG_LIB y/n
    if ((iret == 0) && verb) {
        secs = get_seconds_stg( get_seconds() - bgn );
        SPRTF("%s: Processed %s bytes to %dx%d image in %s\n", module,
            get_NiceNumberStg(file_size),
            width, height, secs );
        out_used();
    }

exit:
    if (buffer)
        delete buffer;

    return iret;

}


// eof
