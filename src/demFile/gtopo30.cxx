// gtopo30.cxx
// see : http://www.ngdc.noaa.gov/mgg/topo/gltiles.html

#include <stdio.h>
#include <limits.h>
#include <algorithm>
#include <string> 
#include "utils.hxx"
#include "sprtf.hxx"
#include "color.hxx"
#include "dem_utils.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "dir_utils.hxx"
#include "gtopo30.hxx"

static const char *module = "gtopo30";

// NOTE WELL: GLOBE topo files are in little-endian format, unlike MANY other DEM files,
// thus need NO 'swapping' on an Intel machine.

// A - min. X : -180.0, max. X : -90.0, min. Y : 50.0, max. Y : 90.0
// B - min. X :  -90.0, max. X :   0.0, min. Y : 50.0, max. Y : 90.0
// C - min. X :    0.0, max. X :  90.0, min. Y : 50.0, max. Y : 90.0
// D - min. X :   90.0, max. X : 180.0, min. Y : 50.0, max. Y : 90.0
// E - min. X : -180.0, max. X : -90.0, min. Y :  0.0, max. Y : 50.0
// F - min. X :  -90.0, max. X :   0.0, min. Y :  0.0, max. Y : 50.0
// G - min. X :    0.0, max. X :  90.0, min. Y :  0.0, max. Y : 50.0
// H - min. X :   90.0, max. X : 180.0, min. Y :  0.0, max. Y : 50.0
// I - min. X : -180.0, max. X : -90.0, min. Y :-50.0, max. Y :  0.0
// J - min. X :  -90.0, max. X :   0.0, min. Y :-50.0, max. Y :  0.0
// K - min. X :    0.0, max. X :  90.0, min. Y :-50.0, max. Y :  0.0
// L - min. X :   90.0, max. X : 180.0, min. Y :-50.0, max. Y :  0.0
// M - min. X : -180.0, max. X : -90.0, min. Y :-90.0, max. Y :-50.0
// N - min. X :  -90.0, max. X :   0.0, min. Y :-90.0, max. Y :-50.0
// O - min. X :    0.0, max. X :  90.0, min. Y :-90.0, max. Y :-50.0
// P - min. X :   90.0, max. X : 180.0, min. Y :-90.0, max. Y :-50.0

GTOPORNG gtoptRng[] = {
    { 'a', -180.0, -90.0,  50.0,  90.0, true },
    { 'b',  -90.0,   0.0,  50.0,  90.0, true },
    { 'c',    0.0,  90.0,  50.0,  90.0, true },
    { 'd',   90.0, 180.0,  50.0,  90.0, true },
    { 'e', -180.0, -90.0,   0.0,  50.0, false },
    { 'f',  -90.0,   0.0,   0.0,  50.0, false },
    { 'g',    0.0,  90.0,   0.0,  50.0, false },
    { 'h',   90.0, 180.0,   0.0,  50.0, false },
    { 'i', -180.0, -90.0, -50.0,   0.0, false },
    { 'j',  -90.0,   0.0, -50.0,   0.0, false },
    { 'k',    0.0,  90.0, -50.0,   0.0, false },
    { 'l',   90.0, 180.0, -50.0,   0.0, false },
    { 'm', -180.0, -90.0, -90.0, -50.0, true },
    { 'n',  -90.0,   0.0, -90.0, -50.0, true },
    { 'o',    0.0,  90.0, -90.0, -50.0, true },
    { 'p',   90.0, 180.0, -90.0, -50.0, true },
    // end of table
    {   0,    0.0,   0.0,   0.0,   0.0 }

};

#define IN_LCRNG(x) (( x >= 'a' ) && ( x <= 'p' ))
#define IN_UCRNG(x) (( x >= 'A' ) && ( x <= 'P' ))

PGTOPORNG get_GTOPO_Range( char c )
{
    PGTOPORNG ptr = &gtoptRng[0];
    while (ptr->letter) {
        if (ptr->letter == c)
            return ptr;
        ptr++;
    }
    return 0;
}

char get_GTOPO_filechar( double lat, double lon )
{
    PGTOPORNG ptr = &gtoptRng[0];
    while (ptr->letter) {
        if ((lat >= ptr->min_y) && (lat <= ptr->max_y) &&
            (lon >= ptr->min_x) && (lon <= ptr->max_x))
            return ptr->letter;

        ptr++;
    }
    return 0;
}

std::string get_GTOPO_file( std::string dir, double lat, double lon )
{
    std::string path("");
    char c = get_GTOPO_filechar(lat,lon);
    if ( c && (is_file_or_directory((char *)dir.c_str()) == 2) ) {
        std::string tmp(dir);
        tmp += "\\";
        tmp += c;
        tmp += "10g";
        if (is_file_or_directory((char *)tmp.c_str()) == 1) {
            path = tmp;
        }
    }
    return path;

}


#define MY_EPSILON1 0.0001

PGTOPORNG get_GTOPO_filerng( double lat, double lon )
{
    PGTOPORNG ptr = &gtoptRng[0];
    double dlat,dlon;
    while (ptr->letter) {
        dlat = abs(lat - ptr->min_y);
        dlon = abs(lon - ptr->min_x);
        if ((dlat < MY_EPSILON1) && 
            (dlon < MY_EPSILON1))
            return ptr;

        ptr++;
    }
    return 0;
}

std::string get_GTOPO_mins()
{
    char buf[128];
    std::string mins;
    PGTOPORNG ptr = &gtoptRng[0];
    while (ptr->letter) {
        sprintf(buf, "%.1lf,%.1lf ", ptr->min_y, ptr->min_x);
        mins += buf;
        ptr++;
    }
    return mins;
}



void gtopo30::init()
{
    file = "";
    verb = true;
    gtopo_data = 0;
    usr_set_rng = false;
    range_set = false;
    spanfp = 0;
    row_buf = 0;
    row_buf_size = 0;
}

void gtopo30::clear()
{
    if (gtopo_data)
        delete [] gtopo_data;
    gtopo_data = 0;
    usr_set_rng = false;
    range_set = false;
}

gtopo30::gtopo30()
{
    init();
}

gtopo30::~gtopo30()
{
    clear();
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
}

bool gtopo30::set_file( std::string in_file )
{
    if (is_file_or_directory((char *)in_file.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file %s\n", module,
            in_file.c_str());
        return false;
    }
    file_size = get_last_file_size();
    cols = GTOPO_COLS;
    if ( file_size == GTOPO_PSIZE ) {
        rows = GTOPO_PROWS;
        if (verb) SPRTF("%s: Have polar file cols %d x rows %d.\n", module,
            cols, rows ); 
        is_polar = true;

    } else if ( file_size == GTOPO_ESIZE ) {
        rows = GTOPO_EROWS;
        if (verb) SPRTF("%s: Have equitorial file cols %d x rows %d.\n", module,
            cols, rows ); 
        is_polar = false;

    } else {
        if (verb) SPRTF("%s: File %s, size %d bytes is NOT a GTOPO30 polar %d nor equitorial %d size!\n", module,
            in_file.c_str(), (int)file_size, GTOPO_PSIZE, GTOPO_ESIZE );
        return false;
    }

    file = in_file;
    if (verb) SPRTF("%s: Set input file %s, size %d bytes.\n", module, 
        in_file.c_str(), (int)file_size );
    if (!usr_set_rng) {
        std::string data = get_file_name_only(in_file);
        std::transform(data.begin(), data.end(), data.begin(), ::tolower);
        char c = data.c_str()[0];
        if (IN_LCRNG(c)) {
            PGTOPORNG ptr = get_GTOPO_Range(c);
            if (ptr) {
                min_x_lon = ptr->min_x;
                max_x_lon = ptr->max_x;
                min_y_lat = ptr->min_y;
                max_y_lat = ptr->max_y;
                if (verb) {
                    SPRTF("%s: By first letter of file '%c', set range lat,lon "
                        "max %s,%s, min %s,%s\n", module,
                        c, 
                        get_trim_double(max_y_lat), 
                        get_trim_double(max_x_lon), 
                        get_trim_double(min_y_lat), 
                        get_trim_double(min_x_lon) );
                    SPRTF("%s: If not correct, set range using set_file_range()\n", module);
                }
                range_set = true;
            }
        }
    }
    return true;
}

bool gtopo30::load_file()
{
    if (file.size() == 0) {
        if (verb) SPRTF("%s: No file to load! use set_file(file)\n", module);
        return false;
    }
    FILE *fp = fopen(file.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Error: Can NOT open file %s\n", module, file.c_str() );
        return false;
    }
    if (is_polar)
        gtopo_data = new short[GTOPO_PROWS][GTOPO_COLS];
    else
        gtopo_data = new short[GTOPO_EROWS][GTOPO_COLS];

    if (!gtopo_data) {
        if (verb) SPRTF("%s: Memory FAILED!\n", module);
        fclose(fp);
        return false;
    }


    size_t res = fread(gtopo_data,1,file_size,fp);
    fclose(fp);
    if (res != file_size) {
	    delete [] gtopo_data;
	    gtopo_data = 0;
	    if (verb) SPRTF("%s: Error: Reading file %s! Requested %d, got %d\n", module,
            file.c_str(), (int)file_size, (int)res );
	    return false;
    }
    int little_endian = test_endian();
    int x,y;
    short *sp;
    short elev;
    max_elev = SHRT_MIN;
    min_elev = SHRT_MAX;
    nodata_cnt = 0;
    double dim = 50.0 / 6000.0;
    double clat,clon,dlat,dlon;
    int minx,miny,maxx,maxy;

    for (y = 0; y < rows; y++) {
        for (x = 0; x < cols; x++) {
            sp = &gtopo_data[y][x];
            if (little_endian) {
                SWAP16(sp);
            }
            elev = *sp;
            if (elev <= GTOPO_NODATA) {
                nodata_cnt++;
            } else {
                if (elev > max_elev) {
                    max_elev = elev;
                    maxx = x;
                    maxy = y;
                    clat = min_y_lat + (y * dim);
                    clon = min_x_lon + (x * dim);
                }
                if (elev < min_elev) {
                    min_elev = elev;
                    minx = x;
                    miny = y;
                    dlat = min_y_lat + (y * dim);
                    dlon = min_x_lon + (x * dim);
                }
            }
        }
    }

    max_lat  = clat;
    max_lon  = clon;
    max_x    = maxx;
    max_y    = maxy;

    min_lat  = dlat;
    min_lon  = dlon;
    min_x    = minx;
    min_y    = miny;

    valid = true;

    //if (verb) SPRTF("Loaded GTOPO max %d, min %d, nodata %d\n", max_elev, min_elev, nodata_cnt );
    if (verb) {
        SPRTF("%s: loaded file %s lon,lat min %s,%s max %s,%s\n", module,
            file.c_str(),
            get_trim_double(min_x_lon),
            get_trim_double(min_y_lat),
            get_trim_double(max_x_lon),
            get_trim_double(max_y_lat) );
        SPRTF("%s: MIN x,y %d,%d, lon,lat %s,%s, elevation %d\n", module,
            min_x, min_y,
            get_trim_double(min_lon),
            get_trim_double(min_lat),
            min_elev );
        SPRTF("%s: MAX x,y %d,%d, lon,lat %s,%s, elevation %d\n", module,
            max_x, max_y,
            get_trim_double(max_lon),
            get_trim_double(max_lat),
            max_elev );
    }

    return true;

}

// but really should only accept
// A - min. X : -180.0, min. Y :  50.0, or
// B - min. X :  -90.0, min. Y :  50.0, or 
// C - min. X :    0.0, min. Y :  50.0, or
// D - min. X :   90.0, min. Y :  50.0, or
// E - min. X : -180.0, min. Y :   0.0, or
// F - min. X :  -90.0, min. Y :   0.0, or
// G - min. X :    0.0, min. Y :   0.0, or
// H - min. X :   90.0, min. Y :   0.0, or
// I - min. X : -180.0, min. Y : -50.0, or
// J - min. X :  -90.0, min. Y : -50.0, or
// K - min. X :    0.0, min. Y : -50.0, or
// L - min. X :   90.0, min. Y : -50.0, or
// M - min. X : -180.0, min. Y : -90.0, or
// N - min. X :  -90.0, min. Y : -90.0, or
// O - min. X :    0.0, min. Y : -90.0, or
// P - min. X :   90.0, min. Y : -90.0

bool gtopo30::set_file_range( double min_x_lon, double min_y_lat )
{
    if (!in_world_range(min_y_lat, min_x_lon)) {
        if (verb) SPRTF("%s: Min lat %s, and/or Min lon %s NOT in world range!\n", module,
            get_trim_double(min_y_lat), 
            get_trim_double(min_x_lon) );
        return false;
    }
    // columns is a constant
    //double calc_max_x_lon = min_x_lon + (GTOPO_COLS * GTOPO_YDIM);
    // rows depend on current min given - how to decide
    //double calc_max_y_lat1 = min_y_lat + (GTOPO_PROWS * GTOPO_YDIM);
    //double calc_max_y_lat2 = min_y_lat + (GTOPO_EROWS * GTOPO_YDIM);
    PGTOPORNG ptr = get_GTOPO_filerng( min_y_lat, min_x_lon );
    if (ptr) {
        min_x_lon = ptr->min_x;
        max_x_lon = ptr->max_x;
        min_y_lat = ptr->min_y;
        max_y_lat = ptr->max_y;
        usr_set_rng = true;
        is_polar = ptr->is_polar;
        if (verb) {
            SPRTF("%s: From usr min lon %s, min lat %s have set %s file range to\n", module,
                get_trim_double(min_x_lon), 
                get_trim_double(min_y_lat),
                (is_polar ? "polar" : "equitorial" ) );
            SPRTF("%s: lon,lat  min  %s,%s, max %s,%s\n", module,
                get_trim_double(min_x_lon), 
                get_trim_double(min_y_lat), 
                get_trim_double(max_x_lon), 
                get_trim_double(max_y_lat) );
        }
        range_set = true;
    } else {
        if (verb) {
            SPRTF("%s: Given min.lat %s, min.lon %s does NOT match allowed minimum pairs!\n",
                get_trim_double(min_y_lat),
                get_trim_double(min_x_lon));
            std::string tmp = get_GTOPO_mins();
            SPRTF("%s\n",tmp.c_str());
        }
        return false;
    }
    return true;
}


int gtopo30::gtopo_to_images()
{
    if (file.size() == 0) {
        if (verb) SPRTF("%s: No file loaded\n", module);
        return 1;
    }
    if (!gtopo_data) {
        if (verb) SPRTF("%s: No hgt data loaded\n", module);
        return 2;
    }
    int iret = 0;
    int x,y;
    short elev;
    short *sp;
    // int max_steps = 10;
    if (verb) {
        // short max_elev, min_elev; int nodata_cnt;
        SPRTF("%s: Maximum %d, Minimum %d metres", module, max_elev, min_elev);
        if (nodata_cnt)
            SPRTF(", with %d voids", nodata_cnt);
        SPRTF(".\n");
    }
    // set color range ZERO_COLOR = 20
    //unsigned int steps = emax / ZERO_COLOR;
    //if (emax % ZERO_COLOR)
    //    steps++;
    size_t bsize = rows * cols * 3;
    unsigned char *buffer = new unsigned char[bsize+1];
    size_t doff;
    // size_t coff;
    unsigned int colr;
    unsigned char r,g,b;
    clear_used_colors();
    for (y = rows - 1; y >= 0; y--) {
        for (x = 0; x < cols; x++) {
            sp = &gtopo_data[y][x];
            elev = *sp;
            doff = ((rows - 1 - y) * cols * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                check_me();
                exit(1);
            }
                colr = get_BGR_color(elev);
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                // is this the correct order????????
                buffer[doff+0] = r;
                buffer[doff+1] = g;
                buffer[doff+2] = b;
        }
    }

#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    char *out_png = get_out_png();
    if (out_png) {
        if (writePNGImage24(out_png, cols, rows, bit_depth, color_type, buffer ))
            iret |= 8;
    }
#else // !USE_PNG_LIB
    char *out_bmp = get_out_bmp();
    if (out_bmp) {
        if (writeBMPImage24(out_bmp, cols, rows, buffer, bsize))
            iret |= 4;
    }
#endif  // USE_PNG_LIB y/n
    delete buffer;
    out_used();
    return iret;
}

bool gtopo30::set_globe_dir( std::string dir )
{
    if (is_file_or_directory((char *)dir.c_str()) != 2) {
        if (verb) SPRTF("%s: Failed to 'stat' directory %s!\n", module, dir.c_str());
        return false;
    }
    globe_dir = dir;
    return true;
}


bool gtopo30::get_elevation( double lat, double lon, short *ps )
{
    if (!in_world_range(lat,lon)) {
        if (verb) SPRTF("%s: lat %s, and/or lon %s NOT in world range!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }
    char c = get_GTOPO_filechar(lat,lon);
    if (!c) {
        if (verb) SPRTF("%s: INTERNAL ERROR: for lat %s, lon %s FAILED to get file char!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }
    std::string tmp(globe_dir);
    if (tmp.size() > 0)
        tmp += PATH_SEP;
    tmp += c;
    tmp += "10g";
    if (is_file_or_directory((char *)tmp.c_str()) != 1) {
        if (verb) {
            SPRTF("%s: Unable to 'stat' file [%s]!\n", module, tmp.c_str());
            if (globe_dir.size() == 0) {
                SPRTF("%s: No GLOBE topo-directory set, so only searched cwd [%s].\n", module,
                    get_current_dir().c_str());
                SPRTF("%s: Use gtopo30::set_globe_dir(dir) if in another location.", module );
            }
        }
        return false;
    }

    clear();    // clear any previous load

    if (!set_file(tmp)) {
        if (verb) SPRTF("%s: Failed in set_file() due to the above!\n", module);
        return false;
    }

    // compute the offset into this file for this lat,lon
    // tile is lower left corner based, so
    // double min_x_lon, max_x_lon, min_y_lat, max_y_lat;
    // check we are within the file range
    if ((lat < min_y_lat) ||
        (lat > max_y_lat) ||
        (lon < min_x_lon) ||
        (lon > max_x_lon)) {
        if (verb) SPRTF("%s: lat,lon %s,%s NOT in range min %s,%s max %s,%s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            get_trim_double(min_y_lat),
            get_trim_double(min_x_lon),
            get_trim_double(max_y_lat),
            get_trim_double(max_x_lon)
            );
        return false;
    }

    double dlon = (lon - min_x_lon);
    double dlat = (lat - min_y_lat);
    double latsp = (max_y_lat - min_y_lat);
    double lonsp = (max_x_lon - min_x_lon);
    if ((dlon < 0.0) || (dlat < 0.0) || (latsp < 0.0) || (lonsp < 0.0)) {
        if (verb) SPRTF("%s: got BAD lon,lat delta %s,%s or span %s,%s!\n", module,
            get_trim_double(dlon),
            get_trim_double(dlat),
            get_trim_double(lonsp),
            get_trim_double(latsp));
        return false;
    }
    x_col = (int)((dlon / lonsp) * (double)cols);
    y_row = (int)((dlat / latsp) * (double)rows);
    offset = (y_row * cols * 2) + ( x_col * 2 );
    if (verb) SPRTF("%s: for lon,lat %s,%s, got x,y %d,%d on %d,%d, offset %s on %s\n", module,
            get_trim_double(lon),
            get_trim_double(lat),
            x_col, y_row,
            cols, rows,
            get_NiceNumberStg(offset),
            get_NiceNumberStg(file_size));

    // ready to party ;=))
    FILE *fp = fopen(file.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s!\n", module, file.c_str());
        return false;
    }
    size_t res = fseek(fp,offset,SEEK_SET);
    if (res != 0) {
        if (verb) SPRTF("%s: FAILED to 'seek' to offset %s in file %s, %s bytes\n", module,
            get_NiceNumberStg(offset), file.c_str(), get_NiceNumberStg(file_size));
        return false;
    }
    res = fread(&elev,1,2,fp);
    fclose(fp);
    if (res != 2) {
        if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
            file.c_str(), (int)res);
        return false;
    }
    if (!do_byte_swap) {
        SWAP16(&elev);
    }
    *ps = elev;
    if (verb) SPRTF("%s: for lat,lon %s,%s returning elevation %d %s\n", module,
        get_trim_double(lon),
        get_trim_double(lat),
        elev,
        ((elev == GTOPO_NODATA) ? "*NO DATA*" : ""));
    return true;
}


// NOTE WELL: GLOBE files are written in little-endian format, unlike a LOT of other DEM files,
// thus require NO SWAPPING on an Intel CPU!
bool gtopo30::scan_min_max()
{
    short min,max,s1;
    //short s2;
    int x,y;
    size_t ii,res,off,sz;
    double dim = 50.0 / 6000.0;
    double clat,clon,dlat,dlon;
    int minx,miny,maxx,maxy;
    size_t minoff, maxoff;
    double file_lat,file_lon;
    short nod = GTOPO_NODATA;
    short th  = GTOPO_TOOHIGH;

    if (is_file_or_directory((char *)file.c_str()) != 1) {
        if (verb) SPRTF("%s: Unable to 'stat' file %s!\n", module, file.c_str());
        return false;
    }
    sz = get_last_file_size();
    FILE *fp = fopen(file.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, file.c_str());
        return false;
    }

    nodata_cnt = 0;
    toohigh_cnt = 0;
    max = SHRT_MIN;
    min = SHRT_MAX;
    x = 0;
    y = rows - 1;
    // start bottom left
    file_lon = min_x_lon;
    file_lat = min_y_lat;
    off = 0;

    for (ii = 0; ii < file_size; ii += 2) {
        res = fread(&s1,1,2,fp);
        if (res != 2) {
            fclose(fp);
            if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                file.c_str(), (int)res);
            return false;
        }
        off += res;
        if (!do_byte_swap) {
            SWAP16(&s1);
        }
        if (s1 <= nod) {
            nodata_cnt++;
        } else if (s1 >= th) {
            toohigh_cnt++;
        } else {
            if (s1 > max) {
                max = s1;
                clat = file_lat + ((double)(rows - y - 1) * dim);
                clon = file_lon + ((double)x * dim);
                maxx = x;
                maxy = rows - y - 1;
                maxoff = off;
            }
            if (s1 < min) {
                min = s1;
                dlat = file_lat + ((double)(rows - y - 1) * dim);
                dlon = file_lon + ((double)x * dim);
                minx = x;
                miny = rows - y - 1;
                minoff = off;
            }
        }
        x++;
        if (x == cols) {
            y--;
            x = 0;
        }
    }

    max_elev = max;
    max_lat  = clat;
    max_lon  = clon;
    max_x    = maxx;
    max_y    = maxy;
    max_off  = maxoff;

    min_elev = min;
    min_lat  = dlat;
    min_lon  = dlon;
    min_x    = minx;
    min_y    = miny;
    min_off  = minoff;

    valid = true;
    if (verb) {
        SPRTF("%s: scanned file %s lon,lat min %s,%s max %s,%s\n", module,
            file.c_str(),
            get_trim_double(min_x_lon),
            get_trim_double(min_y_lat),
            get_trim_double(max_x_lon),
            get_trim_double(max_y_lat) );
        SPRTF("%s: min x,y %d,%d, lon,lat %s,%s, elevation %d\n", module,
            min_x, min_y,
            get_trim_double(min_lon),
            get_trim_double(min_lat),
            min_elev );
        SPRTF("%s: max x,y %d,%d, lon,lat %s,%s, elevation %d\n", module,
            max_x, max_y,
            get_trim_double(max_lon),
            get_trim_double(max_lat),
            max_elev );
        SPRTF("%s: NO DATA cnt %s, too high cnt %s, on %s points.\n", module,
            get_NiceNumberStg(nodata_cnt),
            get_NiceNumberStg(toohigh_cnt),
            get_NiceNumberStg(sz / 2));
    }
    return true;
}

bool gtopo30::begin_span_file( std::string srtm )
{
    bool cv = verb;
    //double clat,clon;
    verb = 0;
    // =========================================================================
    // set file to get rows,cols
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: begin_span_file: failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================
    verb = cv;
#if 0 // hmmm, maybe NOT needed
    if (!usr_ll) {
        if ( !get_srtm_lat_lon( srtm, &clat, &clon ) ) {
            if (verb) {
                SPRTF("%s: Unable to get lat,lon from file %s\n", module, srtm.c_str());
                SPRTF("%s: Use set_lat_lon(lat,lon) to give location\n", module );
            }
        }
        file_lat = clat;
        file_lon = clon;
    }
#endif // 0
    spanfp = fopen(srtm.c_str(),"rb");
    if (!spanfp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    last_elev = 0;
    return true;
}

bool gtopo30::end_span_file()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    return true;
}

// this could lead to more sofisticated 'averaging' of elevations
bool gtopo30::get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps )
{
    int y,cnt,elev,nod;
    size_t off,res,xlen,x,xspan;
    short s1;
    int end_x = bgn_x + span_x;
    int end_y = bgn_y + span_y;
    if (end_x > cols)
        end_x = cols;
    if (end_y > rows)
        end_y = rows;
    if ((bgn_x < 0) || (bgn_y < 0) || (span_x < 1) || (span_y < 1) || !ps || !spanfp ) {
        if (verb) SPRTF("%s: Bad parameter! x=%d y=%d spx=%d spy=%d %s %s\n", module,
            bgn_x, bgn_y, span_x, span_y,
            (ps ? "" : "ps=<null> pointer"),
            (spanfp ? "" : "file closed"));
        return false;
    }
    cnt = 0;
    elev = 0;
    nod = 0;
    xspan = end_x - bgn_x;
    xlen = (xspan * 2);
// try again - #if 0 // ok, but this did not work???
    if (row_buf == 0) {
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }
    } else if (xlen > row_buf_size) {
        delete row_buf;
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }

    }
    if (xlen) {
        for (y = bgn_y; y < end_y; y++) {
            off = ( y * cols * 2 ) + ( bgn_x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)offset, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(row_buf,1,xlen,spanfp);
            if (res != xlen) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request %d, got %d bytes\n", module,
                    file.c_str(), (int)xlen, (int)res);
                return false;
            }
            for (x = 0; x < xspan; x++) {
                short *sp = (short *)( &row_buf[x * 2] );
                if (!do_byte_swap) {
                    SWAP16(sp);
                }
                s1 = *sp;
                if (s1 <= GTOPO_NODATA) {
                    nod++;
                } else {
                    elev += s1;
                    cnt++;
                }
            }
        }
    }
// === #endif // 0

#if 0 // ok, this work but is slow, maybe due to the many seeks???
    for (y = bgn_y; y < end_y; y++) {
        for (x = bgn_x; x < end_x; x++) {
            off = ( y * cols * 2 ) + ( x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("FAILED to 'seek' to offset %d in file %s, %d bytes\n",
                    (int)offset, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(&s1,1,2,spanfp);
            if (res != 2) {
                end_span_file();
                if (verb) SPRTF("FAILED 'read' of file %s, request 2, got %d bytes\n",
                    file.c_str(), (int)res);
                return false;
            }
            if (!do_byte_swap) {
                SWAP16(&s1);
            }
            if (s1 <= GTOPO_NODATA) {
                nod++;
            } else {
                elev += s1;
                cnt++;
            }
        }
    }
#endif // 0
    if (cnt || nod) {
        if (nod > (cnt / 2)) {
            last_elev = GTOPO_NODATA;
        } else {
            // this is a simple AVERAGE - could be much more 'complex'
            last_elev = (elev / cnt);
        }
    } else {
        if (verb) SPRTF("WARNING: Returning previous elev %d for x,y %d,%d span %d,%d on %d,%d\n",
            last_elev, x, y, span_x, span_y, cols, rows );
    }
    *ps = last_elev;
    return true;
}

// eof
