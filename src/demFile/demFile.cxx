// demFile.cxx

#include <stdio.h>
#include <string>
#include <vector>
#include <math.h>
#ifndef _MSC_VER
#include <string.h> // for strdup(), ...
#endif
#include "dem_utils.hxx"
#include "sprtf.hxx"
#include "utils.hxx"
#include "color.hxx"
#include "slippystuff.hxx"
#include "bmp_utils.hxx"
#ifdef USE_PNG_LIB
#include "png_utils.hxx"
#endif
#include "pbm_utils.hxx"
#include "dir_utils.hxx"
#include "demFile.hxx"

static const char *module = "demFile";

static std::string elev_file;

const char *get_evevation_file() { return elev_file.c_str(); }

// 1 arc-sec
hgt1File hgt1;  // de Ferranti & USGS - only a sub-set of world, many voids. needs init set_hgt_dir(srtm1) to find files
// 3 arc-sec
hgt3File hgt3;  // de Ferranti & USGS - fairly full set of world, some voids. needs init set_hgt_dir(hgt_dir) to find files
// 30 arc-sec
srtm30 s30; // Scripps Institute of Oceanography, so contain bathymetrics - 33 files - set Ferranti Bathymetric .srtm dir set_file_dir(ferr_dir);
stopo30 st; // or single large file - D:\SRTM\scripps\topo30\topo30
netcdf cdf; // same 33 files, in CDF format - like "D:\\SRTM\\scripps\\srtm30\\grd\\w060s60.nc"

#ifdef INCLUDE_GLOBE_DATA
// seems so BAD, maybe NOT WORTH THE EFFORT!!!
// gtopo30: NO DATA cnt 46,155,637, too high cnt 0, on 64,800,000 points.
gtopo30 gt; // orig GLOBE - set_globe_dir(gtopo_dir);
#endif //

void set_verbosity( bool verb )
{
    hgt1.verb = verb;
    hgt3.verb = verb;
    s30.verb = verb;
    st.verb = verb;
    cdf.verb = verb;
#ifdef INCLUDE_GLOBE_DATA
    gt.verb = verb;
#endif

}

bool get_best_elevation( double lat, double lon, short *pe, int verbosity )
{
    short elev1;
    bool bhgt1 = true;
    short elev3;
    bool bhgt3 = true;
    short elev30;
    bool bs30 = true;
    bool done = false;
    short netcdfe;
    bool bnetcdf = true;
    short stope;
    bool bstop = true;
    int count = 0;
    short values[8];
#ifdef INCLUDE_GLOBE_DATA
    short globe;
    bool bglob = true;
#endif
    bool verb1 = (verbosity & 1 ) ? true : false;
    bool verb2 = (verbosity & 2 ) ? true : false;

    set_verbosity( verb1 );

    if (hgt1.file_dir.size() == 0)
        bhgt1 = hgt1.set_hgt_dir(srtm1_dir);
    if (bhgt1)
        bhgt1 = hgt1.get_elevation( lat, lon, &elev1 );

    if (hgt3.file_dir.size() == 0)
        bhgt3 = hgt3.set_hgt_dir(ferr3_dir);
    if (bhgt3)
        bhgt3 = hgt3.get_elevation( lat, lon, &elev3 );

    if (s30.file_dir.size() == 0)
        bs30 = s30.set_file_dir(ferr30_dir);
    if (bs30)
        bs30 = s30.get_elevation( lat, lon, &elev30 );

    if (cdf.ncdf_dir.size() == 0)
        bnetcdf = cdf.set_netcdf_dir(ncdf30_dir);
    if (bnetcdf)
        bnetcdf = cdf.get_elevation( lat, lon, &netcdfe);

#ifdef INCLUDE_GLOBE_DATA
    //gtopo30 gt; // orig GLOBE - set_globe_dir(gtopo_dir);
    // gt.verb = true; // WHY NO DATA ALL THE TIME
    // gtopo30: NO DATA cnt 46,155,637, too high cnt 0, on 64,800,000 points.
    if (gt.globe_dir.size() == 0)
        bglob = gt.set_globe_dir(gtopo30_dir);
    if (bglob)
        bglob = gt.get_elevation( lat, lon, &globe );
#endif

    if (st.file.size() == 0)
        bstop = st.set_file(stopo30_fil); // single large file
    if (bstop)
        bstop = st.get_elevation( lat, lon, &stope );

    //if (verb2) SPRTF("\n");
    if (verb2) SPRTF("%s: elev for lat,lon %s,%s - ", module,
        get_trim_double(lat),
        get_trim_double(lon));
    if (bhgt1) {
        if (verb2) SPRTF("hgt1: %d %s ", elev1,
            ((elev1 == HGT_VOID) ? "a VOID" : ""));
        if (elev1 != HGT_VOID) {
            values[count++] = elev1;
            if (!done) {
                *pe = elev1;
                done = true;
                elev_file = hgt1.file;
            }
        }
    } else {
        if (verb2) SPRTF("hgt1: FAILED ");
    }

    if (bhgt3) {
        if (verb2) SPRTF("hgt3: %d %s ", elev3,
            ((elev3 == HGT_VOID) ? "a VOID" : ""));
        if (elev3 != HGT_VOID) {
            values[count++] = elev3;
            if (!done) {
                *pe = elev3;
                done = true;
                elev_file = hgt3.file;
            }
        }
    } else {
        if (verb2) SPRTF("hgt3: FAILED ");
    }

    if (bs30) {
        if (verb2) SPRTF("bs30: %d %s ", elev3,
            ((elev30 == HGT_VOID) ? "a VOID" : ""));
        if (elev30 != HGT_VOID) {
            values[count++] = elev30;
            if (!done) {
                *pe = elev30;
                done = true;
                elev_file = s30.file;
            }
        }
    } else {
        if (verb2) SPRTF("bs30: FAILED ");
    }

    if (bnetcdf) {
        if (verb2) SPRTF("netcdf: %d %s ", elev3,
            ((netcdfe == HGT_VOID) ? "a VOID" : ""));
        if (netcdfe != HGT_VOID) {
            values[count++] = netcdfe;
            if (!done) {
                *pe = netcdfe;
                done = true;
                elev_file = cdf.file;
            }
        }
    } else {
        if (verb2) SPRTF("netcdf: FAILED ");
    }

    if (bstop) {
        if (verb2) SPRTF("stopo: %d %s ", stope,
            ((stope == HGT_VOID) ? "a VOID" : ""));
        if (stope != HGT_VOID) {
            values[count++] = stope;
            if (!done) {
                *pe = stope;
                done = true;
                // bstop = st.set_file(stopo30_fil); // single large file
                elev_file = st.file;
            }
        }
    } else {
        if (verb2) SPRTF("stopo: FAILED ");
    }

#ifdef INCLUDE_GLOBE_DATA
    if (bglob) {
        if (verb2) SPRTF("globe: %d %s ", globe,
            ((globe == GTOPO_NODATA) ? "NO DATA" : ""));
        if (globe != GTOPO_NODATA) {
            values[count++] = globe;
            if (!done) {
                *pe = globe;
                done = true;
                elev_file = gt.file;
            }
        }
    } else {
        if (verb2) SPRTF("globe: FAILED ");
    }
#endif

    if ((count > 2) && verb2) {
        // calculate standard deviation of samples
        short val;
        int i, av, tmp;
        long long sum, total;
        double sd, dav;
        total = 0;
        //char *tb = GetNxtBuf();
        //*tb = 0;
        for (i = 0; i < count; i++) {
            val = values[i];
            total += val;
            //sprintf(EndBuf(tb),"+ %d = %" W64 "u ", val, total );
        }
        dav = ((double)total / (double)count);
        av = (dav < 0.0) ? (int)(dav - 0.5) : (int)(dav + 0.5);
        sum = 0;
        for (i = 0; i < count; i++) {
            tmp = (values[i] - av);
            sum += (tmp * tmp);
        }
        //dav = ((double)sum / (double)(count - 1));
        dav = ((double)sum / (double)count);
        sd = sqrt(dav);
        //SPRTF("sd: %s ", get_trim_double(sd));
        SPRTF("av: %d sd: %d ", av, (int)(sd + 0.5));
    }

    if (verb2) SPRTF("\n");
    return done;
}

bool write_slippy_tile30( srtm30 &s, std::string file1, std::string imgf, 
    int x_bgn, int y_bgn, int xspan, int yspan, int iverb)
{
    bool verb1 = (iverb & 1) ? true : false;
    bool verb2 = (iverb & 2) ? true : false;
    // ok, a file selected
    int height = 256;
    int width = 256;
    size_t bsize = width * height * 3;
    size_t doff;
    unsigned char r,g,b;
    unsigned char *buffer = new unsigned char[bsize];
    if (!buffer) {
        if (iverb) SPRTF("%s: get_slippy_tile: memory FAILED on %d\n", module, (int)bsize );
        return false;
    }
    if (!s.begin_span_file(file1)) {
        if (iverb) SPRTF("%s: Unable to 'strm30:begin_span_file' with file %s\n", module, file1.c_str());
        delete buffer;
        return false;
    }
    int rows = yspan;   // s.rows;
    int cols = xspan;   // s.cols;
    int spany = rows / height;
    if (rows % height) 
        spany++;
    int spanx = cols / width;
    if (cols % width) 
        spanx++;
    double dspany = (double)rows / (double)height;
    double dspanx = (double)cols / (double)width;
    if (verb2) SPRTF("%s: Processing file %s, x,y %d,%d, %d bytes, span %d,%d, incs %s,%s\n", module,
        file1.c_str(),
        cols, rows,
        (int) s.file_size,
        spanx, spany,
        get_trim_double(dspanx),
        get_trim_double(dspany) );
    bool ok = true;
    s.verb = verb1;  // to see any error
    if (verb2) clear_used_colors();
    int bgn_y, bgn_x;
    unsigned int colr;
    double bgn = get_seconds();
    short elev;
    int x,y;
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn - (int)((double)y * dspany);  // TODO: This could be the REVERSE! CHECK ME
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
            if (!ok) {
                if (iverb) {
                    SPRTF("%s: UGH: Failed x,y %d,%d, bgn %d,%d, span %d,%d!\n", module,
                    x, y,
                    bgn_x, bgn_y,
                    spanx, spany);
                }
                delete buffer;
                return false;
            }
            //SPRTF("%d ",elev);
            colr = get_BGR_color(elev); // convert to a color
            // doff = ((height - 1 - y) * width * 3) + (x * 3);
            doff = (y * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                if (iverb) SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                    x, y, (int)doff, (int)bsize );
                delete buffer;
                return false;
            }
            r = getRVal(colr);
            g = getGVal(colr);
            b = getBVal(colr);
            // is this the correct order????????
            buffer[doff+0] = r;
            buffer[doff+1] = g;
            buffer[doff+2] = b;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();  // to close file
    if (verb2) out_used();
#ifdef USE_PNG_LIB
    int bit_depth = 8;
    int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    if (writePNGImage24((char *)imgf.c_str(), width, height, bit_depth, color_type, buffer )) {
        delete buffer;
        return false;
    }
#endif // USE_PNG_LIB 
    if (iverb & 4) {
        std::string file2 = imgf;
        int len = file2.rfind('.');
        if (len > 0) {
            file2 = file2.substr(0,len+1);
            file2 += "bmp";
        }
        if (writeBMPImage24((char *)file2.c_str(), width, height, buffer, bsize)) {
            delete buffer;
            return false;
        }
    }
    if (iverb & 8) {
        if (writePBMImage24(imgf.c_str(), width, height, buffer, bsize, true, true)) {
            delete buffer;
            return false;
        }
    }

    char *secs = get_seconds_stg( get_seconds() - bgn );
    if (iverb) SPRTF("%s: Processed %s bytes to %dx%d image in %s\n", module,
        get_NiceNumberStg(s.file_size),
        width, height, secs );

    delete buffer;

    return true;
}

// generic get slippy tile image - the /zoom/x/y.png 
// srtm30 ferr30_dir data
bool get_srtm30_tile( int x, int y, int zoom, char **ptile, int iverb )
{
    char *cp = GetNxtBuf();
    const char *od = get_image_out_dir();
    bool verb1 = (iverb & 1) ? true : false;
    bool verb2 = (iverb & 2) ? true : false;

    sprintf(cp, "%d_%d_%d.png", zoom, x, y);
    std::string imgf;
    if (od && *od) {
        imgf = od;
        imgf += PATH_SEP;
    }
    imgf += cp;
    if (is_file_or_directory((char *)imgf.c_str()) == 1) {
        if (iverb) SPRTF("%s: Returning file [%s], %d bytes\n", module,
            imgf.c_str(),
            (int)get_last_file_size());
        *ptile = strdup(imgf.c_str());
        return true;
    }

    // ok, must create the image
    const char *dir = ferr30_dir;
    double lon = tilex2lon(x, zoom);
    double lat = tiley2lat(y, zoom);
    BNDBOX bb;
    if (!get_map_bounding_box( lat, lon, zoom, &bb )) {
        if (iverb) SPRTF("%s: failed BBOX for lat,lon,zoom %s,%s,%d\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            zoom );
        return false;
    }

    if (verb2) SPRTF("%s: for lat,lon,zoom %s,%s,%d got %s\n", module,
        get_trim_double(lat),
        get_trim_double(lon),
        zoom,
        get_bounding_box_stg(&bb,true));
    // now how to choose the data set to use
    // for NOW just use ferr30_dir, so use srtm30 class
    srtm30 s;
    short elev;
    s.verb = verb1;
    if (!s.set_file_dir(dir)) {
        if (iverb) SPRTF("%s: failed setting srtm30 director [%s]\n", module, dir);
        return false;
    }

    // TODO: Would be nice to have a s.select_file(lat,lon) service - I do NOT actually need the elevation here
    // get file for lower left
    if (!s.get_elevation( bb.bb_min_lat, bb.bb_min_lon, &elev )) {
        if (iverb) SPRTF("%s: failed getting srtm30 elev fot lat,lon %s,%s\n", module, 
            get_trim_double(bb.bb_min_lat),
            get_trim_double(bb.bb_min_lon));
        return false;
    }
    int x_bgn, y_bgn;
    std::string file1 = s.file;
    // are these different
    //x_bgn = s.x_off;
    //y_bgn = s.y_off;
    x_bgn = s.x_col;
    y_bgn = s.y_row;

    // get file for upper right
    if (!s.get_elevation( bb.bb_max_lat, bb.bb_max_lon, &elev )) {
        if (iverb) SPRTF("%s: failed getting srtm30 elev fot lat,lon %s,%s\n", module, 
            get_trim_double(bb.bb_max_lat),
            get_trim_double(bb.bb_max_lon));
        return false;
    }
    std::string file2 = s.file;
    int x_end, y_end;
    // are these different
    //x_end = s.x_off;
    //y_end = s.y_off;
    x_end = s.x_col;
    y_end = s.y_row;

    if (file1 != file2) {
        if (iverb) SPRTF("%s: Got 2 files - %s and %s - NOT presently handled!\n", module,
            get_file_name(file1).c_str(),
            get_file_name(file2).c_str());
        return false;
    }
    int xspan = x_end - x_bgn;
    int yspan = y_bgn - y_end;  // NOTE this reversal for LAT
    if (iverb) SPRTF("%s: bgn x,y %d,%d, end x,y %d,%d, span %d,%d\n", module, x_bgn, y_bgn, x_end, y_end,
        xspan, yspan );
    if ((xspan == 0) || (yspan == 0)) {
        if (iverb) SPRTF("%s:FAILED! No span for this DEM 30!\n", module );
        return false;
    }

    bool bret = write_slippy_tile30( s, file1, imgf, x_bgn, y_bgn, xspan, yspan, iverb );

    if (bret && (is_file_or_directory((char *)imgf.c_str()) == 1) ) {
        if (iverb) SPRTF("%s: Returning file [%s], %s bytes\n", module,
            imgf.c_str(),
            get_NiceNumberStg(get_last_file_size()));
        *ptile = strdup(imgf.c_str());
    }
    return bret;
}

bool stopo30_expansion(PBNDBOX pbb, int width, int height, unsigned char *ubuff, size_t bsize, int iverb,
    int xcols, int yrows, int x_bgn, int y_bgn, int x_end, int y_end, double dspanx, double dspany,
    stopo30 &s)
{
    int zoom = pbb->llz.zoom;
    int bgn_x, bgn_y;
    int last_bgn_x, last_bgn_y;
    int nofetch, elevcnt;
    int x,y;
    short elev;
    size_t doff;
    unsigned int colr;
    byte r,g,b;
    // clear_used_colors();
    double bgn = get_seconds();

    size_t tsize = (xcols * yrows * sizeof(short));
    short *sbuf = new short[tsize];
    bool chg = false;
    size_t soff;
    short nelev,lelev;
    double dbgn_x,dbgn_y;
    int x2,y2;
    if (!sbuf) {
        if (iverb) {
            SPRTF("%s: memory failed on %d bytes\n", module, (int)tsize);
        }
        return false;
    }
    if (x_end != (x_bgn + xcols)) {
        x_end = x_bgn + xcols;
        chg = true;
    }
    if (y_end != (y_bgn + yrows)) {
        y_end = y_bgn + yrows;
        chg = true;
    }
    if (iverb && chg) {
        SPRTF("%s: Z=%d ADJUSTED x,y BGN %d,%d END %d,%d, SPAN %d,%d\n", module,
            zoom,
            x_bgn,y_bgn,x_end,y_end,
            xcols, yrows );
    }
    
    if (!s.get_elevation_block(x_bgn,y_bgn,x_end,y_end,sbuf,tsize)) {
        if (iverb) {
            SPRTF("%s: failed to get elevation block!\n", module);
        }
        return false;
    }
    last_bgn_x = last_bgn_y = -1;
    nofetch = elevcnt = 0;
    for (y = 0; y < height; y++) {
        dbgn_y = ((double)y * dspany);
        bgn_y = (int)dbgn_y;
        for (x = 0; x < width; x++) {
            dbgn_x = ((double)x * dspanx);
            bgn_x = (int)dbgn_x;
            soff = (bgn_y * xcols) + bgn_x;
            if (soff >= tsize) {
                SPRTF("%s: BAD offset %d on %d\n", module, (int)soff, (int)tsize);
                check_me();
                return false;
            }
            nelev = sbuf[soff];
            if ((last_bgn_x == bgn_x)&&(last_bgn_y == bgn_y)) {
                nofetch++;  // use last colr (r,g,b)
                chg = false;
                for (y2 = y; y2 < height; y2++) {
                    dbgn_y = ((double)y2 * dspany);
                    bgn_y = (int)dbgn_y;
                    for (x2 = x + 1; x2 < width; x2++) {
                        dbgn_x = ((double)x2 * dspanx);
                        bgn_x = (int)dbgn_x;
                        soff = (bgn_y * xcols) + bgn_x;
                        nelev = sbuf[soff];
                        if ((last_bgn_x != bgn_x)||(last_bgn_y != bgn_y)) {
                            chg = true;
                            break;
                        }
                    }
                    if (chg) break;
                }
                elev = (nelev + lelev) / 2;
                colr = get_BGR_color(elev); // convert to a color
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
            } else {
                elev = nelev;
                colr = get_BGR_color(elev); // convert to a color
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                elevcnt++;
                lelev = elev;
            }
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            ubuff[doff+0] = r;
            ubuff[doff+1] = g;
            ubuff[doff+2] = b;
            last_bgn_x = bgn_x;
            last_bgn_y = bgn_y;
        }
    }
    delete sbuf;
    if (iverb & 2) {
        SPRTF("%s: Extracted %d elevations points in %s\n", module,
            elevcnt, get_elapsed_stg(bgn));
    }
    return true;
}

bool get_elev_4_ll(double lat, double lon, short *pe, vLLE &vlle)
{
    bool bret = false;
    size_t ii, max = vlle.size();
    LLE lle1, lle2;
    short elev;
    for (ii = 0; ii < max; ii++) {
        lle1 = vlle[ii];
        if (ii) {
            if ((lat <= lle2.lat) &&
                (lat >= lle1.lat) &&
                (lon <= lle2.lon) &&
                (lon >= lle1.lon) ) {
                elev = (short)((lle2.elev + lle1.elev) / 2.0);
                *pe = elev;
                bret = true;
                break;
            }
        }
        lle2 = lle1;
    }
    return bret;

}

bool stopo30_expansion2(PBNDBOX pbb, int width, int height, unsigned char *ubuff, size_t bsize, int iverb,
    int xcols, int yrows, int x_bgn, int y_bgn, int x_end, int y_end, double dspanx, double dspany,
    stopo30 &s)
{
    size_t doff;
    short elev;
    unsigned int colr;
    byte r,g,b;
    int x,y;
            int mxcols = xcols + 2;
            int myrows = yrows + 2;
            int mx_bgn = x_bgn - 1;
            int my_bgn = y_bgn - 1;
            int mx_end = x_end + 1;
            int my_end = y_end + 1;
            size_t tsize = (mxcols * myrows);
            BNDBOX ebb;
            LLE lle;
            vLLE vlle;
            double dbgn_y, dbgn_x;
            double spandx = (pbb->bb_max_lon - pbb->bb_min_lon) / (double)width;
            double spandy = (pbb->bb_max_lat - pbb->bb_min_lat) / (double)height;
            short *sbuf = new short[tsize];
            if (!sbuf) {
                SPRTF("%s: Memory failed on %d bytes\n", module, (int)(tsize * sizeof(short)));
                return false;
            }
            if (!s.get_elevation_block(mx_bgn,my_bgn,mx_end,my_end,sbuf,tsize)) {
                if (iverb) {
                    SPRTF("%s: failed to get elevation block!\n", module);
                }
                //return false;
            }
            SPRTF("%s: Elevations cover -\n",module);
            vlle.clear();
            for (y = my_bgn; y < my_end; y++) {
                for (x = mx_bgn; x < mx_end; x++) {
                    lle.lat = (90.0 - ((double)y * s.ydim));
                    lle.lon = ((double)x * s.xdim);
                    lle.x   = x;
                    lle.y   = y;
                    doff = ((y - my_bgn) * mxcols) + (x - mx_bgn);
                    elev = sbuf[doff];
                    lle.elev = elev;
                    vlle.push_back(lle);
                    //SPRTF("%s,%s ",
                    //    get_trim_double(90.0 - ((double)y * s.ydim)),
                    //    get_trim_double((double)x * s.xdim));
                }
                //SPRTF("\n");
            }
            size_t ii, max = vlle.size();
            x = 0;
            for (ii = 0; ii < max; ii++) {
                lle = vlle[ii];
                SPRTF("%s,%s %d ",
                    get_trim_double(lle.lat),
                    get_trim_double(lle.lon),
                    lle.elev );
                x++;
                if (x == xcols) {
                    x = 0;
                    SPRTF("\n");
                }
            }
            ebb.bb_max_lat = (90.0 - ((double)my_bgn * s.ydim));
            ebb.bb_min_lat = (90.0 - ((double)my_end * s.ydim));
            ebb.bb_min_lon = ((double)mx_bgn * s.xdim);
            ebb.bb_max_lon = ((double)mx_end * s.xdim);
            if (iverb) {
                SPRTF("%s: Got elevation block of %s, bgn %s,%s\n", module,
                    get_bounding_box_stg(&ebb),
                    get_trim_double(90.0 - ((double)y_bgn * s.ydim)),
                    get_trim_double((double)x_bgn * s.xdim));
            }
            SPRTF("%s: Array of x,y %d,%d elevations\n", module, mxcols, myrows);
            for (y = 0; y < myrows; y++) {
                for (x = 0; x < mxcols; x++) {
                    doff = (y * mxcols) + x;
                    SPRTF("%4d ", sbuf[doff]);
                }
                SPRTF("\n");
            }
            for (y = 0; y < height; y++) {
                dbgn_y = pbb->bb_min_lat + ((double)y * spandy);
                for (x = 0; x < width; x++) {
                    dbgn_x = pbb->bb_min_lon + ((double)x * spandx);
                    if (get_elev_4_ll(dbgn_y, dbgn_x, &elev, vlle)) {
                        colr = get_BGR_color(elev); // convert to a color
                        r = getRVal(colr);
                        g = getGVal(colr);
                        b = getBVal(colr);
                        doff = ((height - 1 - y) * width * 3) + (x * 3);
                        // is this the correct order????????
                        ubuff[doff+0] = r;
                        ubuff[doff+1] = g;
                        ubuff[doff+2] = b;
                    }
                }
            }
            delete sbuf;
    return false;
}

bool stopo30_expansion3(PBNDBOX pbb, int width, int height, unsigned char *ubuff, size_t bsize, int iverb,
    int xcols, int yrows, int x_bgn, int y_bgn, int x_end, int y_end, double dspanx, double dspany,
    stopo30 &s, int *pcnt)
{
    size_t doff;
    int bgn_y,bgn_x,x,y;
    short elev;
    bool extra_debug = false;
    bool extra_dbg = false;
    //vLLE vlle;
    LLE lle;
    LLE lle2;
    int mxcols = xcols + 2;
    int myrows = yrows + 2;
    int mx_bgn = x_bgn - 1;
    int my_bgn = y_bgn - 1;
    int mx_end = x_end + 1;
    int my_end = y_end + 1;
    size_t tsize = (mxcols * myrows);
    short *sbuf = new short[tsize];
    if (!sbuf) {
        SPRTF("%s: Memory failed on %d bytes\n", module, (int)(tsize * sizeof(short)));
        return false;
    }
    if (!s.get_elevation_block(mx_bgn,my_bgn,mx_end,my_end,sbuf,tsize)) {
        if (iverb) {
            SPRTF("%s: failed to get elevation block!\n", module);
        }
        return false;
    }
    *pcnt = (int)tsize;
    //vlle.clear();
    if (extra_debug) {
        SPRTF("%s: Elevations cover -\n",module);
        for (y = my_bgn; y < my_end; y++) {
            for (x = mx_bgn; x < mx_end; x++) {
                lle.lat = (90.0 - ((double)y * s.ydim));
                lle.lon = ((double)x * s.xdim);
                doff = ((y - my_bgn) * mxcols) + (x - mx_bgn);
                elev = sbuf[doff];
                lle.elev = elev;
                SPRTF("%s,%s %d ",
                    get_trim_double(lle.lat),
                    get_trim_double(lle.lon),
                    lle.elev );
                //vlle.push_back(lle);
            }
            SPRTF("\n");
        }
        SPRTF("%s: Array of x,y %d,%d elevations\n", module, mxcols, myrows);
        for (y = 0; y < myrows; y++) {
            for (x = 0; x < mxcols; x++) {
                doff = (y * mxcols) + x;
                SPRTF("%4d ", sbuf[doff]);
            }
            SPRTF("\n");
        }
    }
    //size_t tsize2 = (width * height);
    //short *sbuf2 = new short[tsize2];
    //if (!sbuf2) {
    //    SPRTF("%s: Memory failed on %d bytes\n", module, (int)(tsize2 * sizeof(short)));
    //    return false;
    //}
    // now all I need to do is EXPAND the sbuf elevations, into sbuf2 elevations
    size_t soff;
    double dbgn_y,dbgn_x;
    LLE lles[10];
    short felev;
    int x2, y2, z2;
    bool fnd;
    short left_elev,right_elev;
    double left_lon, right_lon;
    short up_elev,down_elev;
    double up_lat, down_lat;
    int done = 0;
    double difflons;
    short difflone;
    double difflon;
    double difflats;
    short difflate;
    double difflat;
    unsigned int colr;
    byte r,g,b;

    for (y = 0; y < height; y++) {
        dbgn_y = ((double)y * dspany);
        bgn_y = (int)dbgn_y;
        dbgn_y += y_bgn;
        lle2.lat = 90.0 - (dbgn_y * s.ydim);
        for (x = 0; x < width; x++) {
            done++;
            dbgn_x = ((double)x * dspanx);
            bgn_x = (int)dbgn_x;
            dbgn_x += x_bgn;
            lle2.lon = dbgn_x * s.xdim; 
            if (extra_dbg) SPRTF("%s: %d: Need Elev for %s,%s ",module, done, get_trim_double(lle2.lat), get_trim_double(lle2.lon));
            z2 = 0;
            fnd = false;
            for (y2 = bgn_y; y2 < (bgn_y + 3); y2++) {
                for (x2 = bgn_x; x2 < (bgn_x + 3); x2++) {
                    soff = (y2 * mxcols) + x2;
                    elev = sbuf[soff];
                    lles[z2].elev = elev;
                    lles[z2].lat = 90.0 - (((y2 - bgn_y - 1) + y_bgn) * s.ydim);
                    lles[z2].lon = ((x2 - bgn_x - 1) + x_bgn) * s.xdim;
                    //if ((lle2.lat == lles[z2].lat) && (lle2.lon == lles[z2].lon)) {
                    //    felev = elev;
                    //    fnd = true;
                    //    SPRTF(" lat,lon match - found %d\n", felev );
                    //}
                    //SPRTF("%s,%s %d ",
                    //    get_trim_double(lles[z2].lat),
                    //    get_trim_double(lles[z2].lon),
                    //    lles[z2].elev );
                    z2++;
                }
                //SPRTF("\n");
            }
            left_elev = right_elev = -1;
            up_elev = down_elev = -1;
            if (!fnd) {
                // need to extrapolate an elevation
                for (z2 = 0; z2 < 9; z2++) {
                    elev = lles[z2].elev;
                    if (lle2.lat == lles[z2].lat) {
                        // same lat
                        if (lle2.lon < lles[z2].lon) {
                            // desire elev is to left of this
                            if (right_elev == -1) {
                                right_elev = elev;
                                right_lon = lles[z2].lon;
                            } else {
                                // decide which elevation to the right to keep
                                if (lles[z2].lon < right_lon) {
                                    right_elev = elev;
                                    right_lon = lles[z2].lon;
                                }
                            }
                        } else if (lle2.lon > lles[z2].lon) {
                            // desired elev is to right of this elev
                            if (left_elev == -1) {
                                left_elev = elev;
                                left_lon = lles[z2].lon;
                            } else {
                                // decide which elevation to left to keep
                                if (lles[z2].lon > left_lon) {
                                    left_elev = elev;
                                    left_lon = lles[z2].lon;
                                }
                            }
                        } else {
                            felev = lles[z2].elev;
                            fnd = true;
                            if (extra_dbg) SPRTF(" lat,lon match - found %d\n", felev );
                            break;
                        }
                    } else if (lle2.lon == lles[z2].lon) {
                        // same lon
                        if (lle2.lat < lles[z2].lat) {
                            if (up_elev == -1) {
                                up_elev = elev;
                                up_lat = lles[z2].lat;
                            } else {
                                // decide which to keep
                                if (lles[z2].lat < up_lat) {
                                    up_elev = elev;
                                    up_lat = lles[z2].lat;
                                }
                            }
                        } else if (lle2.lat > lles[z2].lat) {
                            if (down_elev == -1) {
                                down_elev = elev;
                                down_lat = lles[z2].lat;
                            } else {
                                // decide whihc to keep
                                if (lles[z2].lat > down_lat) {
                                    down_elev = elev;
                                    down_lat = lles[z2].lat;
                                }
                            }
                        }
                    } else {
                        // no matching lat or lon
                        if (lle2.lon < lles[z2].lon) {
                            // desire elev is to left of this
                            if (right_elev == -1) {
                                right_elev = elev;
                                right_lon = lles[z2].lon;
                            } else {
                                // decide which elevation to the right to keep
                                if (lles[z2].lon < right_lon) {
                                    right_elev = elev;
                                    right_lon = lles[z2].lon;
                                }
                            }
                        } else if (lle2.lon > lles[z2].lon) {
                            // desired elev is to right of this elev
                            if (left_elev == -1) {
                                left_elev = elev;
                                left_lon = lles[z2].lon;
                            } else {
                                // decide which elevation to left to keep
                                if (lles[z2].lon > left_lon) {
                                    left_elev = elev;
                                    left_lon = lles[z2].lon;
                                }
                            }
                        }
                        if (lle2.lat < lles[z2].lat) {
                            if (up_elev == -1) {
                                up_elev = elev;
                                up_lat = lles[z2].lat;
                            } else {
                                // decide which to keep
                                if (lles[z2].lat < up_lat) {
                                    up_elev = elev;
                                    up_lat = lles[z2].lat;
                                }
                            }
                        } else if (lle2.lat > lles[z2].lat) {
                            if (down_elev == -1) {
                                down_elev = elev;
                                down_lat = lles[z2].lat;
                            } else {
                                // decide whihc to keep
                                if (lles[z2].lat > down_lat) {
                                    down_elev = elev;
                                    down_lat = lles[z2].lat;
                                }
                            }
                        }

                    }
                }
                // did we find something to work with
                if ( !fnd && (left_elev != -1) && (right_elev != -1) &&
                        (up_elev != -1) && (down_elev != -1) ) {
                    if ((left_lon < right_lon)&&
                        (down_lat < up_lat) ) {
                        difflons = right_lon - left_lon;
                        difflone = right_elev - left_elev;
                        difflon = lle2.lon - left_lon;
                        short elev1 = (short)((difflon / difflons) * difflone);
                        difflats = up_lat - down_lat;
                        difflate = up_elev - down_elev;
                        difflat = lle2.lat - down_lat;
                        short elev2 = (short)((difflat / difflats) * difflate);
                        felev = ((elev1 + left_elev) + (elev2 + down_elev)) / 2;
                        if (extra_dbg) SPRTF(" by lat,lon extrapolation %d\n", felev);
                        fnd = true;

                    } else {
                        SPRTF("%s: Screwed up somewhere!", module);
                        check_me();
                        return false;
                    }

                }
                if ( !fnd && (left_elev != -1) && (right_elev != -1) ) {
                    // deisred elevation is left + fraction toward right
                    if (left_lon < right_lon) {
                        difflons = right_lon - left_lon;
                        difflone = right_elev - left_elev;
                        difflon = lle2.lon - left_lon;
                        elev = (short)((difflon / difflons) * difflone);
                        felev = elev + left_elev;
                        if (extra_dbg) SPRTF(" by lon extrapolation %d\n", felev);
                        fnd = true;
                    } else {
                        SPRTF("%s: What went wrong here!", module);
                        check_me();
                        return false;
                    }
                }
                if ( !fnd && (up_elev != -1) && (down_elev != -1) ) {
                    if (down_lat < up_lat) {
                        difflats = up_lat - down_lat;
                        difflate = up_elev - down_elev;
                        difflat = lle2.lat - down_lat;
                        elev = (short)((difflat / difflats) * difflate);
                        felev = elev + down_elev;
                        if (extra_dbg) SPRTF(" by lat extrapolation %d\n", felev);
                        fnd = true;
                    } else {
                        SPRTF("%s: What went wrong here!", module);
                        check_me();
                        return false;
                    }
                }
                if (!fnd) {
                    int elevcnt = 0;
                    int elevs[4];
                    if (left_elev != -1) {
                        //difflon = lle2.lon - left_lon;
                        elevs[elevcnt++] = left_elev;
                    }
                    if (right_elev != -1) {
                        elevs[elevcnt++] = right_elev;
                    }
                    if (up_elev != -1) {
                        elevs[elevcnt++] = up_elev;
                    }
                    if (down_elev != -1) {
                        elevs[elevcnt++] = down_elev;
                    }
                    if (elevcnt) {
                        int tot_elev = 0;
                        if (extra_dbg) SPRTF(" av of %d elev ", elevcnt);
                        for (z2 = 0; z2 < elevcnt; z2++) {
                            elev = elevs[z2];
                            tot_elev += elev;
                            if (extra_dbg) SPRTF("%d ",elev);
                        }
                        felev = (short)(tot_elev / elevcnt);
                        if (extra_dbg) SPRTF(" = %d\n", felev );
                        fnd = true;
                    }

                }
            }
            if (!fnd) {
                SPRTF(" none found!\n");
                SPRTF("%s: values ", module);
                fnd = false;
                if (left_elev != -1) {
                    SPRTF("left elev %d, at %s ",left_elev,get_trim_double(left_lon));
                    fnd = true;
                }
                if (right_elev != -1) {
                    SPRTF("right elev %d, at %s ",right_elev,get_trim_double(right_lon));
                    fnd = true;
                }
                if (up_elev != -1) {
                    SPRTF("up elev %d, at %s ",up_elev,get_trim_double(up_lat));
                    fnd = true;
                }
                if (down_elev != -1) {
                    SPRTF("down elev %d, at %s ",down_elev,get_trim_double(down_lat));
                    fnd = true;
                }
                if (fnd) {
                    SPRTF("maybe choose!\n");
                } else {
                    SPRTF("no match for lat,lon! WHY\n");
                    double min_dist = 999999.9;
                    int min_z;
                    for (z2 = 0; z2 < 9; z2++) {
                        double lat,lon;
                        lat = lles[z2].lat;
                        lon = lles[z2].lon;
                        double dist = sqrt( (lat * lat) + (lon * lon) );
                        if (dist < min_dist) {
                            min_z = z2;
                            min_dist = dist;
                        }
                    }
                    SPRTF("%s: The closest is %s,%s elev %d\n",module,
                        get_trim_double(lles[min_z].lat),
                        get_trim_double(lles[min_z].lon),
                        lles[min_z].elev );
                    for (z2 = 0; z2 < 9; z2++) {
                        SPRTF("%s,%s %d ",
                            get_trim_double(lles[z2].lat),
                            get_trim_double(lles[z2].lon),
                            lles[z2].elev );
                        if ((z2 + 1) % 3 == 0)
                            SPRTF("\n");
                    }

                }
                return false;
            } else {
                colr = get_BGR_color(felev); // convert to a color
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                doff = ((height - 1 - y) * width * 3) + (x * 3);
                // is this the correct order????????
                ubuff[doff+0] = r;
                ubuff[doff+1] = g;
                ubuff[doff+2] = b;
            }
        }
    }

    // delete sbuf2;
    delete sbuf;
    return true;
}

bool stopo30_bb_to_buffer(PBNDBOX pbb, int width, int height, unsigned char *ubuff, size_t bsize, int iverb )
{
    int zoom = pbb->llz.zoom;
    bool chkspan = (iverb & 0x8000) ? true : false;
    if (chkspan) iverb &= ~0x8000;
    bool chkcalc = (iverb & 0x4000) ? true : false;
    if (chkcalc) iverb &= ~0x4000;

    stopo30 s;
    double blon = pbb->bb_min_lon;
    double blat = 90.0 - pbb->bb_max_lat;
    if (blon < 0) {
        blon += 360;
    }
    double elon = pbb->bb_max_lon;
    double elat = 90.0 - pbb->bb_min_lat;
    if (elon < 0) {
        elon += 360;
    }

    int x_bgn = (int)(blon / s.xdim);
    int y_bgn = (int)(blat / s.ydim);
    int x_end = (int)(elon / s.xdim);
    int y_end = (int)(elat / s.ydim);
    if (zoom == 0) {
        // special case - fix stuff
        x_bgn = 0;
        x_end = STOPO30_COLS;
        y_bgn = 0;
        y_end = STOPO30_ROWS;
    }
    int xcols = x_end - x_bgn;
    int yrows = y_end - y_bgn;
    //    sprintf(EndBuf(cp), " span %s,%s",
    //        get_trim_double(pbb->bb_max_lat - pbb->bb_min_lat),
    //        get_trim_double(pbb->bb_max_lon - pbb->bb_min_lon));
    if (xcols == 0) {
        xcols = 1;
        x_end = x_bgn + xcols;
    } else if (xcols < 0) {
        int tmpx = (int)((pbb->bb_max_lon - pbb->bb_min_lon) / s.xdim);
        if (iverb) {
            SPRTF("%s: Neg. xcols %d %s! *** FIX ME *** maybe %d?\n", module, 
                xcols,
                get_bounding_box_stg(pbb,true),
                tmpx );
        }
        xcols = tmpx;
        x_end = x_bgn + xcols;
        if (x_end > STOPO30_COLS) {
            SPRTF("%s: DRAT x_end %d exceeeds max %d\n", module, x_end, STOPO30_COLS);
            check_me();
            x_end = STOPO30_COLS;
            return false;
        }
    }
    if (yrows == 0) {
        yrows = 1;
        y_end = y_bgn + yrows;
    } else if (yrows < 0) {
        int tmpy = (int)((pbb->bb_max_lat - pbb->bb_min_lat) / s.ydim);
        if (iverb) {
            SPRTF("%s: Neg. yrows %d %s! *** FIX ME *** maybe %d?\n", module, 
                yrows,
                get_bounding_box_stg(pbb,true),
                tmpy);
        }
        yrows = tmpy;
        y_end = y_bgn + yrows;
        if (y_end > STOPO30_ROWS) {
            SPRTF("%s: DRAT y_end %d exceeeds max %d\n", module, x_end, STOPO30_ROWS);
            check_me();
            y_end = STOPO30_ROWS;
            return false;
        }
    }

    bool ok = s.begin_span_file(stopo30_fil);
    if (!ok) {
        if (iverb) {
            SPRTF("%s: Failed to begin_span_file(%s)!\n", module, stopo30_fil );
        }
        return false;
    }

    if (chkcalc && (zoom > 0)) {
        // check calculations - does NOT work for zoom ZERO(0)
        short elev;
        if (!s.get_elevation( pbb->bb_max_lat, pbb->bb_min_lon, &elev )) {
            if (iverb) SPRTF("%s: Failed to get first elevation from file %s!\n", module, stopo30_fil );
            return false;
        }
        if ((x_bgn != s.x_col)||(y_bgn != s.y_row)) {
            if (iverb) SPRTF("%s: Failed to get same BGN x,y %d,%d vs %d,%d!\n", module,
                x_bgn,y_bgn, s.x_col,s.y_row );
            return false;
        }

        if (!s.get_elevation( pbb->bb_min_lat, pbb->bb_max_lon, &elev )) {
            if (iverb) SPRTF("%s: Failed to get second elevation from file %s!\n", module, stopo30_fil );
            return false;
        }
        if (zoom < 16) {
            // NOTE: End point may have been ADJUSTS if yields SAME POINT if ZOOM too large on this DEM 30
            if ((x_end != s.x_col)||(y_end != s.y_row)) {
                if (iverb) SPRTF("%s: Failed to get same END x,y %d,%d vs %d,%d!\n", module,
                    x_end,y_end, s.x_col,s.y_row );
                return false;
            }
        }
    }

    if (iverb) {
        SPRTF("%s: Z=%d %s gives x,y bgn %d,%d end %d,%d, span %d,%d\n", module,
            zoom,
            get_bounding_box_stg(pbb),
            x_bgn,y_bgn,x_end,y_end,
            xcols, yrows );
    }
    
    if ( chkspan && ((xcols < MIN_SPAN) || (yrows < MIN_SPAN))) {
        if (iverb) {
            SPRTF("%s: At this ZOOM %d, on DEM30 data, the BBOX is LESS THAN MIN_SPAN %dx%d (ie %d,%d) points!\n", module,
                zoom, MIN_SPAN, MIN_SPAN, xcols, yrows);
        }
        return false;
    }

    s.verb = (iverb & 2) ? true : false;  // to see any error

    int x,y;
    size_t doff;
    unsigned char r,g,b;
    unsigned int colr;
    short elev;
    int spany = yrows / height;
    if (yrows % height) 
        spany++;
    int spanx = xcols / width;
    if (xcols % width) 
        spanx++;
    double dspany = (double)yrows / (double)height;
    double dspanx = (double)xcols / (double)width;
    int bgn_x, bgn_y;
    int last_bgn_x, last_bgn_y;
    bool use_exp3 = true;
    int elevcnt = 0;
    int nofetch = 0;
    // clear_used_colors();
    double bgn = get_seconds();

    if (use_exp3 && ((xcols < (width/2))||(yrows < (height / 2)))) {
        if ((x_bgn > 0)&&(x_end < STOPO30_COLS)&&
            (y_bgn > 0)&&(y_end < STOPO30_ROWS)) {
            if (stopo30_expansion3(pbb, width, height, ubuff, bsize, iverb,
                xcols, yrows, x_bgn, y_bgn, x_end, y_end, dspanx, dspany, s, &elevcnt)) {
                if (iverb & 2) {
                    SPRTF("%s: Extracted %d elev points, expanded to %d, in %s\n", module,
                        elevcnt, (width * height), get_elapsed_stg(bgn));
                }
                return true;
            }
        } else {
            SPRTF("%s: TODO: Can not handle this EDGE CASE YET\n", module );
            check_me();
        }
        //return stopo30_expansion(pbb, width, height, ubuff, bsize, iverb,
        //    xcols, yrows, x_bgn, y_bgn, x_end, y_end, dspanx, dspany,
        //    s);
    }

    last_bgn_x = last_bgn_y = -1;
    for (y = 0; y < height; y++) {
        bgn_y = y_bgn + (int)((double)y * dspany);
        for (x = 0; x < width; x++) {
            bgn_x = x_bgn + (int)((double)x * dspanx);
            if ((last_bgn_x == bgn_x)&&(last_bgn_y == bgn_y)) {
                nofetch++;  // use last colr (r,g,b)
            } else {
                ok = s.get_av_of_span(bgn_x, bgn_y, spanx, spany, &elev);
                if (!ok) {
                    if (iverb) {
                        SPRTF("%s: To buffer failed x,y %d,%d, bgn %d,%d, span %d,%d!\n", module,
                        x, y,
                        bgn_x, bgn_y,
                        spanx, spany);
                    }
                    return false;
                }
                //SPRTF("%d ",elev);
                colr = get_BGR_color(elev); // convert to a color
                r = getRVal(colr);
                g = getGVal(colr);
                b = getBVal(colr);
                elevcnt++;
            }
            doff = ((height - 1 - y) * width * 3) + (x * 3);
            if ((doff + 3) > bsize) {
                if (iverb) {
                    SPRTF("%s: UGH: At x %d, y %d, exceeding buffer %d on %d\n", module,
                        x, y, (int)doff, (int)bsize );
                    //check_me();
                }
                return false;
            }
            // is this the correct order????????
            ubuff[doff+0] = r;
            ubuff[doff+1] = g;
            ubuff[doff+2] = b;
            last_bgn_x = bgn_x;
            last_bgn_y = bgn_y;
        }
        //SPRTF("\n");
        if (!ok)
            break;
    }
    s.end_span_file();
    if (iverb & 2) {
        SPRTF("%s: Extracted %d elevations points in %s\n", module,
            elevcnt, get_elapsed_stg(bgn));
    }
    return true;
}

// need to use top/left
bool stopo30_bb_to_image( PBNDBOX pbb, const char *img_file, int iverb,
    int width, int height )
{
    int zoom = pbb->llz.zoom;
    char *secs;
    char *out_img = (char *)img_file;

    if (!out_img) {
        if (iverb) {
            SPRTF("%s: WHAT! No image file!\n", module);
        }
        return false;
    }
    double bgn = get_seconds();
    size_t bsize = width * height * 3;
    unsigned char *buffer = new unsigned char[bsize];
    if (!stopo30_bb_to_buffer(pbb, width, height, buffer, bsize, iverb )) {
        if (iverb) {
            SPRTF("%s: Failed to set color buffer!\n", module);
        }
        delete buffer;
        return false;
    }

#ifdef USE_PNG_LIB
    //int bit_depth = 8;
    //int color_type = 6; // RGBA (PNG_COLOR_TYPE_RGBA) ( 2 | 4 )
    if (writePNGImage24(out_img, width, height, 8, 6, buffer )) {
        if (iverb) {
            SPRTF("%s: Failed to write '%s'!\n", module, out_img);
        }
        delete buffer;
        return false;
    }
#else // !USE_PNG_LIB
    if (writeBMPImage24(out_img, width, height, buffer, bsize)) {
        if (iverb) {
            SPRTF("%s: Failed to write '%s'!\n", module, out_img);
        }
        delete buffer;
        return false;
    }
#endif // USE_PNG_LIB y/n
    secs = get_seconds_stg( get_seconds() - bgn );
    if (iverb) {
        SPRTF("%s: Z=%d Written %dx%d image in %s\n", module,
            zoom, width, height, secs );
    }
    delete buffer;
    //out_used();
    return true;
}

bool get_slippy_tile( int x, int y, int zoom, char **ptile, int iverb )
{
    char *cp = GetNxtBuf();
    const char *od = get_image_out_dir();
    sprintf(cp, "%d_%d_%d.png", zoom, x, y);
    std::string imgf;
    if (od && *od) {
        imgf = od;
        imgf += PATH_SEP;
    }
    imgf += cp;
    if (is_file_or_directory((char *)imgf.c_str()) == 1) {
        if (iverb) SPRTF("%s: Returning file [%s], %d bytes\n", module,
            imgf.c_str(),
            (int)get_last_file_size());
        *ptile = strdup(imgf.c_str());
        return true;
    }

    double tlon = tilex2lon(x,zoom);
    double tlat = tiley2lat(y,zoom);
    BNDBOX bb;
    if (!get_map_bounding_box(tlat,tlon,zoom,&bb)) {
        if (iverb) SPRTF("%s: Failed to get BBOX for %s,%s,%d!\n", module,
            get_trim_double(tlat),
            get_trim_double(tlon),
            zoom );
        return false;
    }
    // service default to 256 x 256
    if (stopo30_bb_to_image( &bb, imgf.c_str(), iverb )) {
        if (is_file_or_directory((char *)imgf.c_str()) == 1) {
            if (iverb) SPRTF("%s: Returning file [%s], %d bytes\n", module,
                imgf.c_str(),
                (int)get_last_file_size());
            *ptile = strdup(imgf.c_str());
            return true;
        }
    }
    return false;

}


// eof
