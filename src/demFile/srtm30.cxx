// srtm30.cxx
/* -------------------------------------------------------------------------------------------
    from : http://www.webgis.com/srtm30.html
    SRTM30 - Shuttle Radar Topography Mission
    Global Coverage (~900m) 30 arc-sec
    SRTM30 is considered an upgrade to the original GTOPO30 data sets.
    This data was collected during the Shuttle Radar Topography Mission (SRTM) and contains global 
    coverage from 56 degrees south latitude to 60 degrees north latitude with an approximate 
    resolution of 900 by 900 meters.
    download: links to http://dds.cr.usgs.gov/srtm/version2_1/SRTM30/w180n90/ - for each tile
    files : w180n90.dem.zip w180n90.dif.zip w180n90.dmw.zip w180n90.gif.zip w180n90.hdr.zip
    w180n90.jpg.zip w180n90.num.zip w180n90.prj.zip w180n90.sch.zip w180n90.src.zip w180n90.std.zip w180n90.stx.zip

    More write-up and information from : http://vterrain.org/Elevation/SRTM/

    BUT, this set downloaded from : http://topex.ucsd.edu/WWW_html/srtm30_plus.html 
    (Scripps Institute of Oceanography, University of California, San Diego, USA) - 33 files
    SO, they contain not only the land elevations, but also the bathynetry
    NEW!    FTP SRTM30_PLUS, entire grid  V9.0                December 19, 2013
    which offers a SINGLE large file - topo30 - see stopo30.hxx/cxx for that - 
    AND DWN: D:\SRTM\scripps\srtm30\data, 33 files of the form -
        57,600,000 e020n40.Bathymetry.srtm
        51,840,000 w060s60.Bathymetry.srtm
    ALSO: DWN: D:\SRTM\scripps\srtm30\grd, 33 files of the form -
        57,687,044 e020n40.nc
        51,927,044 e060s60.nc
    which have a CDF header, then the data
    This module is to load, view and understand these files...
    ALSO downloaded to F:\data\srtm30_plus - 33 files

    Other software:
    from : http://vterrain.org/Doc/VTBuilder/overview.html - VTbuilder
    DWN: 01/04/2014  19:00         2,262,201 vtp-src-130421.zip
    SVN: svn checkout http://vtp.googlecode.com/svn/trunk/ vtp
   ------------------------------------------------------------------------------------------- */

#include <stdio.h>
#include <string>
#ifndef _MSC_VER
#include <string.h> // for strcmp(), ...
#include <limits.h> // for SHRT_MIN, ...
#include <unistd.h> // for getcwd(), ...
#define stricmp strcasecmp
#define _getcwd getcwd
#endif
#include "utils.hxx"
#include "dir_utils.hxx"
#include "sprtf.hxx"
#include "srtm30.hxx"

static const char *module = "srtm30";

#define FERR_EXT ".Bathymetry.srtm"

typedef struct tagSRTM30 {
    const char *tile;
    int lat_min;
    int lat_max;
    int lon_min;
    int lon_max;
    int elev_min;
    int elev_max;
    int mean;
    int std_dev;
} SRTM30, *PSRTM30;

//              lat lat  lon   lon 
SRTM30 srtm30_tile_table[] = {
    // first nine - 
    { "w180n90", 40, 90, -180, -140, -22, 6098, 448, 482 },
    { "w140n90", 40, 90, -140, -100, -108, 4635, 731, 596 },
    { "w100n90", 40, 90, -100, -60, -35, 2416, 337, 280 },
    { "w060n90", 40, 90, -60, -20, -13, 3940, 1626, 932 },
    { "w020n90", 40, 90, -20, 20, -179, 4536, 402, 426 },
    { "e020n90", 40, 90, 20, 60, -188, 5472, 213, 312 },
    { "e060n90", 40, 90, 60, 100, -156, 7169, 509, 697 },
    { "e100n90", 40, 90, 100, 140, -110, 3901, 596, 455 },
    { "e140n90", 40, 90, 140, 180, -26, 4578, 415, 401 },
    // second nine -
    { "w180n40", -10, 40, -180, -140, -3, 4120, 832, 860 },
    { "w140n40", -10, 40, -140, -100, -174, 4228, 1322, 745 },
    { "w100n40", -10, 40, -100, -60, -171, 6543, 367, 609 },
    { "w060n40", -10, 40, -60, -20, -22, 2504, 217, 160 },
    { "w020n40", -10, 40, -20, 20, -138, 3958, 438, 298 },
    { "e020n40", -10, 40, 20, 60, -422, 5778, 724, 557 },
    { "e060n40", -10, 40, 60, 100, -46, 8685, 1807, 1889 },
    { "e100n40", -10, 40, 100, 140, -147, 7213, 690, 911 },
    { "e140n40", -10, 40, 140, 180, -42, 4650, 530, 728 },
    // third nine
    { "w180s10", -60, -10, -180, -140, -41, 1784, 191, 294 },
    { "w140s10", -60, -10, -140, -100, -5, 910, 79, 133 },
    { "w100s10", -60, -10, -100, -60, -752, 6813, 1080, 1359 },
    { "w060s10", -60, -10, -60, -20, -127, 2823, 411, 294 },
    { "w020s10", -60, -10, -20, 20, -24, 2498, 1088, 404 },
    { "e020s10", -60, -10, 20, 60, -26, 3408, 889, 453 },
    { "e060s10", -60, -10, 60, 100, -3, 2557, 251, 262 },
    { "e100s10", -60, -10, 100, 140, -33, 1360, 290, 172 },
    { "e140s10", -60, -10, 140, 180, -43, 3119, 278, 265 },
    // last six
    { "w180s60", -90, -60, -180, -120 },
    { "w120s60", -90, -60, -120, -60 },
    { "e060s60", -90, -60, -60, 0 },
    { "w000s60", -90, -60, 0, 60 },
    { "w060s60", -90, -60, 60, 120 },
    { "e120s60", -90, -60, 120, 180 },
    // terminate table
    { 0,         0,    0,    0,   0,   0,   0,    0,   0 }
};

PSRTM30 get_tile_table() { return srtm30_tile_table; }
static int next_tile_table = 0;
PSRTM30 get_first_srtm30_tbl()
{
    next_tile_table = 0;
    return get_tile_table();
}
PSRTM30 get_next_srtm30_tbl()
{
    next_tile_table++;
    PSRTM30 pt = &get_tile_table()[next_tile_table];
    if (pt->tile)
        return pt;
    next_tile_table--;
    return 0;
}


const char *get_srmt30_tile_name(double lat, double lon, int *pilat, int *pilon, bool verb)
{
    if (!in_world_range(lat,lon)) {
        if (verb) SPRTF("NOTE: lat %s, and/or lon %s NOT in world range!\n",
            get_trim_double(lat),
            get_trim_double(lon) );
        return 0;
    }
    PSRTM30 pt = get_tile_table();
    while (pt->tile) {
#if 0
        if (lat <= (double)pt->lat_max) {
            if (lat >= (double)pt->lat_min) {
                if (lon <= (double)pt->lon_max) {
                    if (lon >= (double)pt->lon_min) {
                        return pt->tile;
                    }
                }
            }
        }
#endif // 0
        // 20140409: FIX: Change from >= to > min_lat, and <= to < lon_max
        if ((lat <= (double)pt->lat_max) &&
            (lat >  (double)pt->lat_min) &&
            (lon <  (double)pt->lon_max) &&
            (lon >= (double)pt->lon_min)) {
            double dlat = pt->lat_min;
            double dlon = pt->lon_min;
            int ilat = (int)dlat;
            int ilon = (int)dlon;
            if (dlat < 0) {
                dlat *= -1;
                ilat = (int)dlat;
            }
            if (dlon < 0.0) {
                dlon *= -1;
                ilon = (int)dlon;
            }
            if (pilat)
                *pilat = ilat;
            if (pilon)
                *pilon = ilon;
            return pt->tile;
        }
        pt++;
    }
    // 20140509: Now question the above, so try again
    pt = get_tile_table();
    while (pt->tile) {
        if ((lat <= (double)pt->lat_max) &&
            (lat >= (double)pt->lat_min) &&
            (lon <= (double)pt->lon_max) &&
            (lon >= (double)pt->lon_min)) {
            double dlat = pt->lat_min;
            double dlon = pt->lon_min;
            int ilat = (int)dlat;
            int ilon = (int)dlon;
            if (dlat < 0) {
                dlat *= -1;
                ilat = (int)dlat;
            }
            if (dlon < 0.0) {
                dlon *= -1;
                ilon = (int)dlon;
            }
            if (pilat)
                *pilat = ilat;
            if (pilon)
                *pilon = ilon;
            return pt->tile;
        }

        pt++;
    }
    if (verb) {
        SPRTF("%s: lat,lon  %s,%s NOT found in table! *** FIX ME ***\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
    }
    return 0;

}

bool get_srmt30_tile_limits(const char *tile, PTILELIMITS ptl)
{
    PSRTM30 pt = get_tile_table();
    while (pt->tile) {
        if (stricmp(tile,pt->tile) == 0) {
            ptl->max_lat = (double)pt->lat_max;
            ptl->min_lat = (double)pt->lat_min;
            ptl->max_lon = (double)pt->lon_max;
            ptl->min_lon = (double)pt->lon_min;
            return true;
        }
        pt++;
    }
    return false;   // tile NOT found
}


void srtm30::init()
{
    file = "";
    verb = true;
    valid = false;
    min_elev = SHRT_MAX;
    max_elev = SHRT_MIN;
    usr_ll = false;
    // for span workings
    spanfp = 0;
    last_elev = 0;
    row_buf = 0;
    row_buf_size = 0;
    // ================
}

srtm30::srtm30()
{
    init();
}

srtm30::~srtm30()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
}

bool srtm30::set_file(std::string in_file)
{
    if (is_file_or_directory((char *)in_file.c_str()) != 1) {
        if (verb) SPRTF("Unable to 'stat' file %s\n", in_file.c_str());
        return false;
    }
    file_size = get_last_file_size();
    if (file_size == SRTM30_FILE_SIZE1) {
        rows = SRTM30_ROWS;
        cols = SRTM30_COLS;
    } else if (file_size == SRTM30_FILE_SIZE2) {
        rows = SRTM30_ROW2;
        cols = SRTM30_COL2;
    } else {
        if (verb) SPRTF("File size is %d, instead of %d or %d\n", (int)file_size,
            SRTM30_FILE_SIZE1,
            SRTM30_FILE_SIZE2 );
        return false;
    }
    file = in_file;
    if (verb) SPRTF("%s: Set %s as input file, %s bytes.\n", module, file.c_str(), get_NiceNumberStg(file_size));
    return true;
}

bool srtm30::set_file_dir( std::string dir )
{
    if (is_file_or_directory((char *)dir.c_str()) != 2) {
        if (verb) SPRTF("%s: Input %s NOT a valid directory!\n", module, dir.c_str());
        return false;
    }
    srtm30_files = read_directory( dir, "*.srtm",  dm_FILE );
    size_t max = srtm30_files.size();
    if (!max) {
        if (verb) SPRTF("%s: In %s found no *.srtm files!\n", module, dir.c_str());
        return false;
    }
    if (verb) SPRTF("%s: Found %d *.srtm files in %s.\n", module, (int)max, dir.c_str());
    file_dir = dir;
    return true;

}

////////////////////////////////////////////////////////////////////////
// get elevation at a specific location, auto selecting the file
///////////////////////////////////////////////////////////////////////
bool srtm30::get_elevation( double lat, double lon, short *ps )
{
    size_t res;
    std::string srtm;
    int ilat,ilon;
    short s1;
    if (!in_world_range(lat,lon)) {
        if (verb) SPRTF("%s: Given lat %s, and/or lon %s OUT OF WORLD RANGE!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }


    tile = get_srmt30_tile_name(lat,lon,&ilat,&ilon,false);
    if (!tile) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Can NOT get tile for given lat %s, lon %s!\n", module,
            get_trim_double(lat),
            get_trim_double(lon) );
        return false;
    }

    if (!get_srmt30_tile_limits(tile, &tl)) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Can NOT get tile limits of %s!\n",module, tile);
        return false;
    }
    if ( !((lat <= tl.max_lat) &&
           (lat >= tl.min_lat) &&
           (lon <= tl.max_lon) &&
           (lon >= tl.min_lon)) ) {
        if (verb) SPRTF("%s: INTERNAL ERROR: Given lat,lon NOT in limits of tile %s!\n",module, tile);
        return false;
    }
    //double dlat = tl.min_lat;
    //double dlon = tl.min_lon;
    if (verb) {
        SPRTF("%s: Given lon,lat %s,%s, is tile %s, min %s,%s, max %s,%s\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            tile,
            get_trim_double(tl.min_lat),
            get_trim_double(tl.min_lon),
            get_trim_double(tl.max_lat),
            get_trim_double(tl.max_lon) );
    }

    // build up PATH to file
    srtm = file_dir;
    if (file_dir.size() > 0) {
        srtm += PATH_SEP;
    }
    srtm += tile;
    srtm += FERR_EXT;   // ".Bathymetry.srtm";
    // hgt += ".hgt";
    if (is_file_or_directory((char *)srtm.c_str()) != 1) {
        if (verb) {
            SPRTF("%s: Unable to 'stat' file [%s]\n", module, srtm.c_str());
            if (file_dir.size() == 0) {
                char cwd[MAX_PATH];
                cwd[0] = 0;
                _getcwd(cwd, MAX_PATH);
                SPRTF("%s: SRTM (Ferranti/Bathymetric) file directory NOT set, so can only search CWD %s\n",module, cwd);
            }
        }
        return false;
    }

    // #####################################################################################
    if (!set_file(srtm)) {   // IMPORTANT: This SETS the rows/cols, depending on file size
        return false;
    }

    // #####################################################################################
    // This is the calculation if the origin of the file is TOP/LEFT
    //x_col = (int)((lon - tl.min_lon) / (tl.max_lon - tl.min_lon) * cols);
    //y_row = (int)((lat - tl.min_lat) / (tl.max_lat - tl.min_lat) * rows);
    double dtlon = (lon - tl.min_lon);
    double lonsp = (tl.max_lon - tl.min_lon);
    double dtlat = (lat - tl.min_lat);
    double latsp = (tl.max_lat - tl.min_lat);
    x_col = (int)(dtlon / lonsp * cols);
    //y_row = (int)(dlat / latsp * rows);   // always get this LAT mixed up
    y_row = (int)((latsp - dtlat) / latsp * rows);   // this is what I meant ;=))
    offset = (y_row * cols * 2) + (x_col * 2);
    if (verb) {
        //SPRTF("%s: Calculated offset %d on %d, col %d on %d, row %d on %d\n", module,
        //    (int)offset, (int)file_size, x_col, cols, y_row, rows );
        SPRTF("%s: Calculated x,y %d,%d on %d,%d, offset %s on %s\n", module,
            x_col, y_row, cols, rows,
            get_NiceNumberStg(offset),
            get_NiceNumberStg(file_size) );
        SPRTF("%s: delta lon %s on %s of %d, delta lat %s on %s of %d\n", module,
            get_trim_double(dtlon),
            get_trim_double(lonsp),
            cols,
            get_trim_double(latsp - dtlat),  // note this!!!
            get_trim_double(latsp),
            rows );

    }

    FILE *fp = fopen(srtm.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("Unable to 'open' file %s\n", srtm.c_str());
        return false;
    }
    res = fseek(fp,offset,SEEK_SET);
    if (res != 0) {
        fclose(fp);
        if (verb) SPRTF("FAILED to 'seek' to offset %d in file %s, %d bytes\n",
            (int)offset, srtm.c_str(), (int)file_size);
        return false;
    }
    res = fread(&s1,1,2,fp);
    if (res != 2) {
        fclose(fp);
        if (verb) SPRTF("FAILED 'read' of file %s, request 2, got %d bytes\n",
            srtm.c_str(), (int)res);
        return false;
    }
    get_ix_short( &s1, &elev );
    *ps = elev;
    fclose(fp);
    if (verb) SPRTF("%s: For lat,lon %s,%s, got elevation %d meters, at y,x %d,%d\n", module,
            get_trim_double(lat),
            get_trim_double(lon),
            elev,
            y_row, x_col );
    return true;
}

bool srtm30::set_lat_lon( double in_lat, double in_lon )
{
    if (!in_world_range(in_lat,in_lon)) {
        if (verb) SPRTF("%s: Given lat,lon %s,%s NOT in world range!\n", module,
            get_trim_double(in_lat),
            get_trim_double(in_lon));
        return false;
    }
    file_lat = in_lat;
    file_lon = in_lon;

    usr_ll = true;
    if (verb) SPRTF("%s: Set user lat,lon %s,%s.\n", module,
            get_trim_double(in_lat),
            get_trim_double(in_lon));
    return true;
}


bool srtm30::scan( std::string srtm )
{
    size_t ii,res;
    short s1,s2,max,min;
    int x,y;
    double dim = 50.0 / 6000.0;
    double clat,clon,dlat,dlon;
    int minx,miny,maxx,maxy;
    const char *ctile;

    valid = false;
    min_elev = SHRT_MAX;
    max_elev = SHRT_MIN;

    // =========================================================================
    // set file to get rows,cols
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: scan failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================

    if (!usr_ll) {
        if ( !get_srtm_lat_lon( srtm, &clat, &clon ) ) {
            if (verb) {
                SPRTF("%s: Unable to get lat,lon from file %s\n", module, srtm.c_str());
                SPRTF("%s: Use set_lat_lon(lat,lon) to give location\n", module );
            }
        }
        file_lat = clat;
        file_lon = clon;
        ctile = get_srmt30_tile_name(clat,clon,0,0,false);
        // if (verb)
        SPRTF("%s: Set file lat,lon %s,%s, tile %s.\n", module,
            get_trim_double(clat),
            get_trim_double(clon),
            (ctile ? ctile : "TILE NOT FOUND") );
        if (ctile) {
            TILELIMITS ctl;
            if (get_srmt30_tile_limits(ctile,&ctl)) {
                //if (verb) 
                SPRTF("%s: That set the limits lat,lon to min %s,%s max %s,%s\n", module,
                    get_trim_double( ctl.min_lat ),
                    get_trim_double( ctl.min_lon ),
                    get_trim_double( ctl.max_lat ),
                    get_trim_double( ctl.max_lon ) );
            }
        }
    }

    FILE *fp = fopen(srtm.c_str(),"rb");
    if (!fp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    max = SHRT_MIN;
    min = SHRT_MAX;
    x = 0;
    y = 0;
    for (ii = 0; ii < file_size; ii += 2) {
        res = fread(&s1,1,2,fp);
        if (res != 2) {
            fclose(fp);
            if (verb) SPRTF("FAILED 'read' of file %s, request 2, got %d bytes\n",
                srtm.c_str(), (int)res);
            return false;
        }
        get_ix_short( &s1, &s2 );
        if (s2 > max) {
            max = s2;
            clat = file_lat - ((double)y * dim);
            clon = file_lon + ((double)x * dim);
            maxx = x;
            maxy = y;
        }
        if (s2 < min) {
            min = s2;
            dlat = file_lat - ((double)y * dim);
            dlon = file_lon + ((double)x * dim);
            minx = x;
            miny = y;
        }
        x++;
        if (x == cols) {
            y++;
            x = 0;
        }
    }

    max_elev = max;
    max_lat  = clat;
    max_lon  = clon;
    max_x    = maxx;
    max_y    = maxy;

    min_elev = min;
    min_lat  = dlat;
    min_lon  = dlon;
    min_x    = minx;
    min_y    = miny;

    valid = true;
    return true;
}

bool srtm30::begin_span_file( std::string srtm )
{
    bool cv = verb;
    //double clat,clon;
    verb = 0;
    // =========================================================================
    // set file to get rows,cols
    if ( !set_file( srtm ) ) {
        if (verb) SPRTF("%s: begin_span_file: failed in 'set_file'!\n", module );
        return false;
    }
    // =========================================================================
    verb = cv;
#if 0 // hmmm, maybe NOT needed
    if (!usr_ll) {
        if ( !get_srtm_lat_lon( srtm, &clat, &clon ) ) {
            if (verb) {
                SPRTF("%s: Unable to get lat,lon from file %s\n", module, srtm.c_str());
                SPRTF("%s: Use set_lat_lon(lat,lon) to give location\n", module );
            }
        }
        file_lat = clat;
        file_lon = clon;
    }
#endif // 0
    spanfp = fopen(srtm.c_str(),"rb");
    if (!spanfp) {
        if (verb) SPRTF("%s: Unable to 'open' file %s\n", module, srtm.c_str());
        return false;
    }
    last_elev = 0;
    return true;
}

bool srtm30::end_span_file()
{
    if (spanfp)
        fclose(spanfp);
    spanfp = 0;
    if (row_buf)
        delete row_buf;
    row_buf = 0;
    return true;
}

// this could lead to more sofisticated 'averaging' of elevations
bool srtm30::get_av_of_span( int bgn_x, int bgn_y, int span_x, int span_y, short *ps )
{
    int y,cnt,elev;
    size_t off,res,xlen,x,xspan;
    short s1,s2;
    int end_x = bgn_x + span_x;
    int end_y = bgn_y + span_y;
    if (end_x > cols)
        end_x = cols;
    if (end_y > rows)
        end_y = rows;
    if ((bgn_x < 0) || (bgn_y < 0) || (span_x < 1) || (span_y < 1) || !ps || !spanfp ) {
        if (verb) SPRTF("%s: Bad parameter! x=%d y=%d spx=%d spy=%d %s %s\n", module,
            bgn_x, bgn_y, span_x, span_y,
            (ps ? "" : "ps=<null> pointer"),
            (spanfp ? "" : "file closed"));
        return false;
    }
    cnt = 0;
    elev = 0;
    xspan = end_x - bgn_x;
    xlen = (xspan * 2);
// try again - #if 0 // ok, but this did not work???
    if (row_buf == 0) {
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }
    } else if (xlen > row_buf_size) {
        delete row_buf;
        row_buf_size = xlen;
        row_buf = new unsigned char[row_buf_size];
        if (!row_buf) {
            if (verb) SPRTF("%s: memory failed!\n", module);
            end_span_file();
            return false;
        }

    }
    if (xlen) {
        for (y = bgn_y; y < end_y; y++) {
            off = ( y * cols * 2 ) + ( bgn_x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)off, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(row_buf,1,xlen,spanfp);
            if (res != xlen) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            for (x = 0; x < xspan; x++) {
                short *sp = (short *)( &row_buf[x * 2] );
                s1 = *sp;
                get_ix_short( &s1, &s2 );
                elev += s2;
                cnt++;
            }
        }
    }
// === #endif // 0

#if 0 // ok, this work but is slow, maybe due to the many seeks???
    for (y = bgn_y; y < end_y; y++) {
        for (x = bgn_x; x < end_x; x++) {
            off = ( y * cols * 2 ) + ( x * 2 );
            res = fseek(spanfp,off,SEEK_SET);
            if (res != 0) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED to 'seek' to offset %d in file %s, %d bytes\n", module,
                    (int)off, file.c_str(), (int)file_size);
                return false;
            }
            res = fread(&s1,1,2,spanfp);
            if (res != 2) {
                end_span_file();
                if (verb) SPRTF("%s: FAILED 'read' of file %s, request 2, got %d bytes\n", module,
                    file.c_str(), (int)res);
                return false;
            }
            get_ix_short( &s1, &s2 );
            elev += s2;
            cnt++;
        }
    }
#endif // 0
    if (cnt) {
        // this is a simple AVERAGE - could be much more 'complex'
        last_elev = (elev / cnt);
    } else {
        if (verb) SPRTF("%s: WARNING: Returning previous elev %d for x,y %d,%d span %d,%d on %d,%d\n", module,
            last_elev, x, y, span_x, span_y, cols, rows );
    }
    *ps = last_elev;
    return true;
}

// eof - srtm30.cxx
