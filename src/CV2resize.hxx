// CV2resize.hxx
#ifndef _CV2RESIZE_HXX_
#define _CV2RESIZE_HXX_

// like 0.5
extern bool resize_to_scale( std::string file, float scale, std::string out );
// like 0.25 min 0.75 max
extern bool resize_file( std::string file, float min, float max, std::string out );

#endif // #ifndef _CV2RESIZE_HXX_
// eof

