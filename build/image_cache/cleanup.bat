@setlocal
@if EXIST *.png (
@echo Delete PNG
@del *.png >nul
)
@if EXIST *.bmp (
@echo Delete BMP
@del *.bmp >nul
)
@if EXIST *.ppm (
@echo Delete PPM
@del *.ppm >nul
)

@REM eof

