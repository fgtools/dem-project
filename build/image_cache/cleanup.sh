#!/bin/sh
BN=`basename $0`

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "$BN: Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "$BN: Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "$BN: Aborting ... no input!"
        else
            echo "$BN: Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
	wait_for_input "$BN: *** CONTINUE? ***"
}

echo ""
echo "$BN: Moment... counting files..."

TMPFIL1=`ls *.png` 2>/dev/null
TMPFIL2=`ls *.bmp` 2>/dev/null
TMPFIL3=`ls *.ppm` 2>/dev/null
TMPCNT=0
TMPLIST=""
for fil in $TMPFIL1; do
	TMPCNT=`expr $TMPCNT + 1`
	TMPLIST="$TMPLIST $fil"
done
for fil in $TMPFIL2; do
	TMPCNT=`expr $TMPCNT + 1`
	TMPLIST="$TMPLIST $fil"
done
for fil in $TMPFIL3; do
	TMPCNT=`expr $TMPCNT + 1`
	TMPLIST="$TMPLIST $fil"
done
if [ ! -z "$TMPLIST" ]; then
	echo ""
	echo "$TMPLIST"
fi
if [ "$TMPCNT" = "0" ]; then
	echo ""
	echo "$BN: Found nothing to delete!"
	echo ""
	exit 0
fi

echo ""
echo "$BN: Will delete the above $TMPCNT files"

ask
echo "$BN: Doing delete of $TMPCNT files..."

for fil in $TMPFIL1; do
	echo "Delete PNG $fil"
	rm -f $fil
done
for fil in $TMPFIL2; do
	echo "Delete BMP $fil"
	rm -f $fil
done
for fil in $TMPFIL3; do
	echo "Delete PPM $fil"
	rm -f $fil
done

#@REM eof

