#!/bin/sh
#< setup and run http_tests
bn=`basename $0`
TMPEXE="json_server"

if [ ! -x "$TMPEXE" ]; then
	echo ""
	echo "$BN: Unable to locate $TMPEXE!"
	echo "$BN: Has it been built? Else *** FIX ME ***"
	echo ""
	exit 1
fi

TMPJSON="../data/all.json"

if [ ! -f "$TMPJSON" ]; then
	echo "$BN: Can NOT locate $TMPJSON! Has the all-json.zip been unzipped? *** FIX ME ***"
	exit 1
fi

TMPCMD="-j $TMPJSON"

for arg in $@; do
	TMPCMD="$TMPCMD $arg"
done

echo "$BN: Doing './$TMPEXE $TMPCMD'"
./$TMPEXE $TMPCMD
if [ ! "$?" = "0" ]; then
	echo "$BN: Running $TMPEXE FAILED"
	exit 1
fi

# EOF


