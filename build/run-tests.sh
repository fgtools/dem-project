#!/bin/sh
#< setup and run http_tests
BN=`basename $0`
TMPEXE="dem_tests"

if [ ! -x "$TMPEXE" ]; then
	echo "$BN: Unable to locate $TMPEXE"
	exit 1
fi

TMPFIL1="../data/sample-data.zip"
TMPDIR1="../data/srtm1"
TMPDIR2="../data/ferr3"
TMPDIR3="../data/ferrb30"
# TMPJSON="../data/all.json"

if [ ! -d "$TMPDIR1" ]; then
	echo "$BN: Can NOT locate $TMPDIR1! *** FIX ME ***"
	if [ -f "$TMPFIL1" ]; then
		echo "$BN: Found $TMPFIL1... Has it been unzipped?"
	else
		echo "$BN: Also did not find $TMPFIL1???"
		echo "$BN: Has it been removed? Unzipping it should create this directory..."
	fi
	exit 1
fi

# if [ ! -f "$TMPJSON" ]; then
#	echo "$BN: Can NOT locate $TMPJSON! Has the all-json.zip been unzipped? *** FIX ME ***"
#	exit 1
#fi

TMPCMD="-d srtm1:$TMPDIR1 -d ferr3:$TMPDIR2 -d ferrb30:$TMPDIR3"
for arg in $@; do
	TMPCMD="$TMPCMD $arg"
done

echo "$BN: Doing './$TMPEXE $TMPCMD'"
./$TMPEXE $TMPCMD
if [ ! "$?" = "0" ]; then
	echo "$BN: Running $TMPEXE exited with $?"
	exit 1
fi

# EOF

