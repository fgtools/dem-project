@setlocal
@REM file to install
@set TMPFIL=dem_track.exe

@set TMPEXE=Release\%TMPFIL%
@if NOT EXIST %TMPEXE% goto NOEXE

@set TMPDIR=C:\MDOS
@if NOT EXIST %TMPDIR%\nul goto NODST
@set TMPDST=%TMPDIR%\%TMPFIL%
@if NOT EXIST %TMPDST% goto DOCOPY1

@fc4 -? >nul
@if ERRORLEVEL 4 goto NOFC4
@if ERRORLEVEL 3 goto FNDFC4
@echo Got ERRORLEVEL %ERRORLEVEL%! Expected 3
@goto NOFC4
:FNDFC4
@fc4 -Q -B -V0 %TMPEXE% %TMPDST% >nul
@if ERRORLEVEL 1 goto DOCOPY2
@echo.
@echo Appears files %TMPEXE% and %TMPDST% are EXACTLY the SAME.
@echo *** No update required... nothing done...
@echo.
@goto END

:DOCOPY1
@call dirmin %TMPEXE%
@echo.
@echo No destination file %TMPDST% exists...
@echo Continue and copy %TMPEXE%? Only Ctrl+C aborts... All others continue...
@echo.
@pause
@goto DOCOPY

:DOCOPY2
@call dirmin %TMPEXE%
@call dirmin %TMPDST%
@echo.
@echo Files are DIFFERENT!
@echo Continue and copy %TMPEXE%? Only Ctrl+C aborts... All others continue...
@echo.
@pause
@goto DOCOPY


:DOCOPY
@echo Copying updated file...
copy %TMPEXE% %TMPDST%
@goto END

:NOEXE
@echo.
@echo Error: Can NOT locate %TMPEXE%! Has it been built? *** FIX ME ***
@echo.
@goto END

:NOFC4
@echo.
@echo Seem unable to run 'fc4' utility! Is it in your PATH
@echo Else modify this batch file to use some other binary file compare utility
@echo.
@goto END

:NODST
@echo.
@echo Can NOT locate the install location %TMPDIR%!
@echo This is MY EXE install location, and it is in my PATH so the EXE %TMPFIL% can be run from anywhere
@echo Either create or adjust this batch file to suit your environment...
@echo.
@goto END

:END

@REM eof
