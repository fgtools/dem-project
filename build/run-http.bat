@setlocal
@set TMPEXE=Release\http_tests.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD

%TMPEXE% %TMPCMD%

@goto END

:ERR1
@echo Error: Can NOT locate %TMPEXE%! Has it been built? Use build-me.bat...
@goto END

:END

@REM eof
