#!/bin/sh
#< setup and run http_tests
bn=`basename $0`
TMPEXE="dem_server"

if [ ! -x "$TMPEXE" ]; then
	echo ""
	echo "$BN: Unable to locate $TMPEXE! Has it been built?"
	echo ""
	exit 1
fi

TMPDIR1="../data/srtm1"
TMPDIR2="../data/ferr3"
TMPDIR3="../data/ferrb30"
TMPST30="../../data/topo30"

if [ ! -d "$TMPDIR1" ]; then
	echo "$BN: Can NOT locate $TMPDIR1! Has the sample-data.zip been unzipped? *** FIX ME ***"
	exit 1
fi

if [ ! -f "$TMPST30" ]; then
	echo "$BN: Can NOT locate $TMPST30! Has it been downloaded? single 2GB file?"
	exit 1
fi

TMPCMD="-d srtm1:$TMPDIR1 -d ferr3:$TMPDIR2 -d ferrb30:$TMPDIR3 -d stopo30:$TMPST30 -d usgs3:none -d netcdf30:none -d globe30:none"

for arg in $@; do
	TMPCMD="$TMPCMD $arg"
done

echo "$BN: Doing './$TMPEXE $TMPCMD'"
./$TMPEXE $TMPCMD
if [ ! "$?" = "0" ]; then
	echo "$BN: Running $TMPEXE FAILED"
	exit 1
fi

# EOF

