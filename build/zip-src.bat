@setlocal
@set TMPZIP=z:\dem3\build\tempdem3.zip

@if EXIST %TMPZIP% @del %TMPZIP%

cd ..

call zip8 -a -r -P -o -xbuild\* %TMPZIP% *

@pause

call copy2tmp %TMPZIP%

@REM eof
