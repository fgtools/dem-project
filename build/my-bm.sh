#!/bin/sh
#< my-bm.sh - 20140429 - in Dell02
BN=`basename $0`
# THIS IS A PERSONALISED VERSION OF BUILD-ME
# For SURE this needs to be FIX for each personal environment

# THIS IS MY ROOT LOCATION OF THE SIMGEAR HEADERS AND LIBRARIES
# This line needs to be FIXED to suit YOUR install of SimGear
#############################################################
export SG_ROOT=/media/Disk2/FG/fg21/install/simgear
#############################################################
TMPARCH="x86_64-linux-gnu"

# to test that EXPORT - should NOT need to change anything below here
#####################################################################
BAD=0
TMPSGL="$SG_ROOT/lib/libSimGearCore.a"
if [ -z "$SG_ROOT" ]; then
    echo "$BN: Found NO exported of SG_ROOT!"
    echo "$BN: How will Simgear core library be found?"
    BAD=1
else
    if [ -d "$SG_ROOT" ]; then
        echo "$BN: Found exported of SG_ROOT=$SG_ROOT"
        if [ ! -f "$TMPSGL" ]; then
            TMPSGL="$SG_ROOT/lib/$TMPARCH/libSimGearCore.a"
            if [ -f "$TMPSGL" ]; then
                echo "$BN: Found $TMPSGL - GOOD. Hope cmake can do the same"
            else
                echo "$BN: WARNING: $SG_ROOT/lib/libSimGearCore.a NOT found in that ROOT!"
                echo "$BN: Nor $SG_ROOT/lib/$TMPARCH/libSimGearCore.a"
                BAD=1
            fi
        else
            echo "$BN: $TMPSGL found in that ROOT! GOOD"
        fi
        if [ ! -f "$SG_ROOT/include/simgear/math/SGMath.hxx" ]; then
    	    echo "$BN: WARNING: $SG_ROOT/include/simgear/math/SGMath.hxx NOT found in that ROOT!"
            BAD=1
        else
    	    echo "$BN: $SG_ROOT/include/simgear/math/SGMath.hxx found in that ROOT! GOOD"
        fi
    else
        echo "$BN: Found exported of SG_ROOT=$SG_ROOT, but NOT a VALID directory!"
        echo "$BN: Remove or FIX this exported SG_ROOT directory. Aborting..."
        BAD=1
    fi
fi
if [ ! -x "build-me.sh" ]; then
    echo "$BN: Executable build-me.sh file NOT found!"
    BAD=1
else
    echo "$BN: Found executable build-me.sh file. GOOD"
fi

if [ "$BAD" = "1" ]; then
    echo "$BN: Look at the above problem(s), and FIX THEM!"
    exit 1
else
    echo "$BN: Looks GOOD to continue with the build..."
fi
########################################################################
# seem ready to rock and roll ;=))
./build-me.sh $@
########################################################################

# eof

