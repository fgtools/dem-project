#!/bin/sh
#< build-me.sh - dem-project - 20140424
BN=`basename $0`
# 20140519: SimGear no longer required. Uses built in mongoose instead
# To find SimGear and build the HTTP and JSON apps need
# something like -
# export SG_ROOT=/media/Disk2/FG/fg21/install/simgear
# export SG_ROOT=/home/geoff/fg/01/install/simgear
# This should point to where lib/SimGearCore.a is installed
# and include/simgear/math/SGMath.h can be found
TMPARCH="x86_64-linux-gnu"
TMPSGL1="$SG_ROOT/lib/libSimGearCore.a"
TMPSGL2="$SG_ROOT/lib/$TMPARCH/libSimGearCore.a"
TMPLOG="bldlog-1.txt"
TMPOPTS=""
# Possible options to help with problems
# TMPOPTS="$TMPOPTS -DSG2_DEBUG=ON"
# TMPOPTS="$TMPOPTS -DCMAKE_VERBOSE_MAKEFILE=ON"

echo "Begin dem-project build" > $TMPLOG
if [ ! -z "$SG_ROOT" ]; then
    if [ -d "$SG_ROOT" ]; then
        echo "$BN: Found exported of SG_ROOT=$SG_ROOT" >> $TMPLOG
        if [ ! -f "$TMPSGL1" ]; then
    	    echo "Found exported of SG_ROOT=$SG_ROOT"
    	    if [ -f "$TMPSGL2" ]; then
    	        echo "$BN: $TMPSGL2 found in that ROOT! good" >>$TMPLOG
    	    else
    	        echo "$BN: WARNING: $TMPSGL1 not $TMPSGL2 NOT found in that ROOT!"
    	        echo "$BN: WARNING: $TMPSGL1 not $TMPSGL2 NOT found in that ROOT!" >> $TMPLOG
    	    fi
        else
	        echo "$BN: $TMPSGL1 found in that ROOT! good" >>$TMPLOG
        fi
        if [ ! -f "$SG_ROOT/include/simgear/math/SGMath.hxx" ]; then
	    echo "Found exported of SG_ROOT=$SG_ROOT"
	    echo "$BN: WARNING: $SG_ROOT/include/simgear/math/SGMath.hxx NOT found in that ROOT!"
	    echo "$BN: WARNING: $SG_ROOT/include/simgear/math/SGMath.hxx NOT found in that ROOT!" >>$TMPLOG
        else
	    echo "$BN: $SG_ROOT/include/simgear/math/SGMath.hxx found in that ROOT! good" >>$TMPLOG
        fi
    else
        echo "$BN: Found exported of SG_ROOT=$SG_ROOT, but NOT a VALID directory!"
        echo "$BN: Remove or FIX this exported SG_ROOT directory. Aborting..."
        exit 1
    fi
fi
for arg in "$@"; do
    TMPOPTS="$TMPOPTS $arg"
done

echo "$BN: Doing 'cmake .. $TMPOPTS'"
echo "$BN: Doing 'cmake .. $TMPOPTS'" >> $TMPLOG
cmake .. $TMPOPTS >> $TMPLOG 2>&1
if [ ! "$?" =  "0" ]; then
	echo "$BN: cmake config, gen error $?"
	echo "$BN: See $TMPLOG for details..."
	exit 1
fi

echo "$BN: Doing 'make'"
echo "$BN: Doing 'make'" >>$TMPLOG
make >>$TMPLOG 2>&1
if [ ! "$?" =  "0" ]; then
	echo ""
	echo "$BN: make error exit"
	echo "$BN: See $TMPLOG for details..."
	echo ""
	exit 1
fi

grep warning $TMPLOG
if [ ! "$?" =  "0" ]; then
	echo "$BN: No warnings found in $TMPLOG"
	echo "$BN: Appears a successful build..."
else
	echo "$BN: Appears a successful build..."
	echo "$BN: but see above 'warnings' in $TMPLOG"
fi

# eof

