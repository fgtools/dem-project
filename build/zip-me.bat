@setlocal
@set TMPVER=02
@REM set TMPVER=01 - 20130331 - lots of progress

@if EXIST CMakeCache.txt goto DOCLEAN
@if EXIST CMakeFiles\nul goto DOCLEAN
@if EXIST dem3.sdf goto DOCLEAN

@set TMPSRC=Z:\dem3
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@set TMPDST=Z:\zips
@if NOT EXIST %TMPDST%\nul goto NODST
@set TMPFIL=dem3-%TMPVER%.zip
@set TMPZIP=%TMPDST%\%TMPFIL%
@set TMPOPT=-a
@if NOT EXIST %TMPZIP% goto GOTOPT
@set TMPOPT=-u
@echo This is an UPDATE
:GOTOPT
@set TMPOPT=%TMPOPT% -r -P -o
@echo Will do: 'call zip8 %TMPOPT% %TMPZIP% %TMPSRC%\*'
@echo *** CONTINUE? *** Only Ctl+C aborts...
@pause

call zip8 %TMPOPT% %TMPZIP% %TMPSRC%\*
@echo Copy to 'tmp'? Only Ctrl+C will abort...
@pause
cd %TMPDST%
call copy2tmp %TMPFIL%

@goto END

:NOSRC
@echo Can NOT find source %TMPSRC%! *** FIX ME ***
@goto END

:NODST
@echo Can NOT find destination %TMPDST%! *** FIX ME ***
@goto END

:DOCLEAN
@echo ERROR: Found some cmake build products!
@echo Do a FULL cmake-clean before zipping
@goto END

:END
