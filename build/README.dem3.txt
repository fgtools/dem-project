README.dem3.txt - 2010409

Notes made during development of the reading of various DEM file formats

===========================================================================
20140411:
=========

A scan of the 16 GLOBE topo files - http://www.ngdc.noaa.gov/mgg/topo/gltiles.html -
note, these are in little-endian format, unlike many other DEM files,
revealed a global elevation range min -407 to max 8752, 
with 623,551,288 of 933,120,000 pts, 66.8%, as no-data (-500).

But in trying to get the position of the highest, assuming it is Mt Everest,
I pesently computer the quite close lat,lon -
gtopo30: scanned file F:\data\dem3\SRTM30\all10\g10g lat,lon min 0,0 max 50,90
gtopo30: max x,y 10431,3358, lat,lon 22.008333,86.925, elevation 8752
Actual Mount Everest - 8,848 meters  27.988,   86.9253 = N27E086
Is there anything wrong here? Or is that the 'best' I can expect?

Then using the get_elevation(lat,lon,&elev) get the right value
gtopo30: for lon,lat 86.925,22.008333, got x,y 10431,2641 on 10800,6000, offset 57066462 on 129600000
dem3: Elevation at lat,lon 22.008333,86.925 is reported as 8752, expected 8752 (x,y 10431,3358)
BUT the ROW number is very different! HOW COME?
In the scan have row 2641 on 6000, while the get_elevation uses 3358, which looks
suspiciously like the inverse????

Add a show of the OFFSET into the file. For sure the scan is the definative offset,
since it is just a linear scan of the file, short by short, and a 'computed' y...

YOWEE, the offset is the SAME, well +- 2 - Phew
gtopo30: for lon,lat 86.925,22.008333, got x,y 10431,2641 on 10800,6000, offset 57066462 on 129600000
dem3: Elevation at lat,lon 22.008333,86.925 is reported as 8752, expected 8752 (x,y 10431,3358 off 57066464)

Soooo, just 'fix' the y-value shown in the scan...
gtopo30: for lon,lat 86.925,22.008333, got x,y 10431,2641 on 10800,6000, offset 57066462 on 129600000
dem3: Elevation at lat,lon 22.008333,86.925 is reported as 8752, expected 8752 (x,y 10431,2641 off 57066464)

And the same for getting the lowest point, except the +2... which will leave as is for now... a later TODO
gtopo30: for lon,lat 35.358333,19.016667, got x,y 4243,2282 on 10800,6000, offset 49299686 on 129600000
dem3: Elevation at lat,lon 19.016667,35.358333 is reported as -407, expected -407 (x,y 4243,2282 off 49299688)

Perfect, or close enough... FIXED

===========================================================================
20140409:
=========

WHAT IS GOING ON HERE?

For lat,lon 11.317,142.25, Marianas Trench, got elev -10509, expected -10911 

srtm30: Set file lat,lon 40,140, tile e100n90.
srtm30: That set the limits lat,lon to min 40,100 max 90,140
Lowest -10910 found at x,y 291,3435, lon,lat 42.425,68.625

WRONG TILE! WRONG LON!

Ok, for the tile that was a MIN/MAX thing
Originally HAD
        if ((lat <= (double)pt->lat_max) &&
            (lat >= (double)pt->lat_min) &&
            (lon <= (double)pt->lon_max) &&
            (lon >= (double)pt->lon_min))
            return pt->tile;

Changed TO
        if ((lat <= (double)pt->lat_max) &&
            (lat >  (double)pt->lat_min) &&
            (lon <  (double)pt->lon_max) &&
            (lon >= (double)pt->lon_min))
            return pt->tile;

Now
srtm30: Set file lat,lon 40,140, tile e140n40.
srtm30: That set the limits lat,lon to min -10,140 max 40,180
Lowest -10910 found at x,y 291,3435, lon,lat 42.425,68.625 *** BUT THIS IS WRONG
For the lon, fixed that little 'bug'
Lowest -10910 found at x,y 291,3435, lon,lat 142.425,68.625

--------------------------------------------------------------------------

For lat,lon 11.317,142.25, Marianas Trench, got elev -10509, expected -10911 
On de Ferranti Bathymetric .nc (NetCDF) files, like -
netcdf: Converted lon,lat 142.25,11.317, to x,y 270,2558, off 24557340, file 24644384 on 57687044, elev -10509

Then on de Ferranti Bathymetric SRTM files, like -
srtm30: Given lon,lat 11.317,142.25, is tile e140n40, min -10,140, max 40,180
srtm30: Set F:\data\srtm30_plus\e140n40.Bathymetry.srtm as input file, 57600000 bytes.
srtm30: Calculated offset 24557340 on 57600000, col 270 on 4800, row 2558 on 6000
de Ferranti Bathymetric F:\data\srtm30_plus\e140n40.Bathymetry.srtm returned -3977

Doing a FULL scan of the file revealed
Lowest -10910 found at x,y 291,3435, lon,lat 142.425,68.625

Another small 'fix' - was adding lat increments INSTEAD of sutracting so now get
Lowest -10910 found at x,y 291,3435, lon,lat 142.425,11.375

So something wrong with the offset calculation -
Calculated ... x,y 270,2558 on 4800,6000- BAD
Empirical .... x,y 291,3435 - as found

Fixed: As usual was messing up the LAT calculation! Now get -

... For the e140n40.nc, get -
For lat,lon 11.317,142.25, Marianas Trench, got elev -10509, expected -10911 
... For the e140n40.Bathymetry.srtm, get -
srtm30: Calculated x,y 270,3441 on 4800,6000, offset 33034140 on 57600000
srtm30: delta lon 2.25 on 40 of 4800, delta lat 28.683 on 50 of 6000
de Ferranti Bathymetric F:\data\srtm30_plus\e140n40.Bathymetry.srtm returned -10509
srtm30: Set file lat,lon 40,140, tile e140n40.
srtm30: That set the limits lat,lon to min -10,140 max 40,180
Lowest -10910 found at x,y 291,3435, lon,lat 142.425,11.375

As EXPECTED the e140n40.Bathymetry.srtm returns the SAME as the e140n40.nc
As I understand it one is derived from the other so SHOULD be the SAME!

Now to remove the 'debug' noise - and see ALL tests are now the SAME

For lat,lon 11.317,142.25, Marianas Trench, got elev -10509, expected -10911 (F30:-10509)
For lat,lon 27.988,86.9253, Mount Everest, got elev 8569, expected 8848 (F30:8569)
For lat,lon -32.655556,-70.015833, Cerro Aconcagua, got elev 6722, expected 6962 (F30:6722)
For lat,lon -29.202,29.357, Mafadi, SA, got elev 3394, expected 3450 (F30:3394)
For lat,lon -33.398,21.368, Seweweekspoortpiek, got elev 2072, expected 2325 (F30:2072)
For lat,lon -3.075833,37.353333, Kilimanjaro, Tanzania, got elev 5739, expected 5895 (F30:5739)
For lat,lon -36.455981,148.263333, Mount Kosciuszko, got elev 2129, expected 2228 (F30:2129)
For lat,lon 19.820556,-155.468056, Mauna Kea,Hawaii, got elev 4120, expected 4205 (F30:4120)
For lat,lon -37.094444,-12.288611, Tristan da Cunha, got elev 1346, expected 2062 (F30:1346)
For lat,lon 63.069,-151.0063, Mt McKinley, Alaska, got elev 6035, expected 6168 (F30:6035)
For lat,lon -78.75,-85, Ellsworth, Antartic, got elev 1222, expected 5140 (F30:1222)
For lat,lon -78.167,-85.5, Sentinel, Antartic, got elev 1894, expected 5140 (F30:1894)

===========================================================================
