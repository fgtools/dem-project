#!/bin/sh
#< zip-me.sh - 20140330

ask()
{
	pause
	if [ ! "$?" = "0" ]; then
		exit 1
	fi

}

if [ -f "CMakeCache.txt" ] || [ -d "CMakeFiles" ]; then
	# :DOCLEAN
	echo "ERROR: Found some cmake build products!"
	echo "Do a FULL cmake-clean before zipping"
	exit 1
fi

TMPVER="01"
TMPSRC="$HOME/projects/dem3"
if [ ! -d "$TMPSRC" ]; then
	echo "Can NOT find source $TMPSRC! *** FIX ME ***"
	exit 1
fi

TMPDST="$HOME/projects/zips"
if [ ! -d "$TMPDST" ]; then
	echo "Can NOT find destination $TMPDST! *** FIX ME ***"
	exit 1
fi

TMPFIL="dem3-$TMPVER.zip"
TMPZIP="$TMPDST/$TMPFIL"
TMPOPT=""
if [ -f "$TMPZIP" ]; then
	TMPOPT="-u"
	echo "This is an UPDATE"
fi
TMPOPT="$TMPOPT -r -o"
echo "Will do: 'zip $TMPOPT $TMPZIP dem3/*'"
echo "*** CONTINUE? ***"
ask

cd ../..

zip $TMPOPT $TMPZIP dem3/*

echo "Copy to 'tmp'?"
ask
cd $TMPDST
copy2tmp $TMPFIL

# eof

