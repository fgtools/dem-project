@setlocal
@set TMPEXE=Release\dem_tests.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD
@if "%TMPCMD%x" == "x" (
%TMPEXE% -s
%TMPEXE% -?
@echo No command, so just showing help...
@goto END
)

%TMPEXE% %TMPCMD%

@goto END

:ERR1
@echo Error: Can NOT locate %TMPEXE%! Has it been built? Use build-me.bat...
@goto END

:END

@REM eof
