README.txt - 20141112 - 20140427

General Description:
===================

Started out just loading, scanning various DEM files from a number of sources...

Then the idea to build a http server to -
(a) return the elevation of any point on the globe
(b) generate a slippy map for a location and zoom

Still very much a WORK IN PROGRESS!!!

There are 6 classes to handle 6 different types of DEM files, for 
1, 3 and 30 arc-sec files. In general each class supports a -
 bool get_elevation( double lat, double lon, short *pelev );

These handlers are compiled into a single DemFile library.

The class will determine which file that elevation will reside, and if given a folder where the set of data 
files reside, will open, seek to location and extract the elevation.

// 1 arc-sec
class hgt1File; // de Ferranti & USGS - only a sub-set of world, many voids. needs init set_hgt_dir(srtm1) to find files

// 3 arc-sec
class hgt3File; // de Ferranti & USGS - fairly full set of world, some voids. needs init set_hgt_dir(hgt_dir) to find files

// 30 arc-sec
class srtm30; // Scripps Institute of Oceanography, so contain bathymetrics - 33 files - set Ferranti Bathymetric .srtm dir set_file_dir(ferr_dir);
class netcdf; // same 33 files, in CDF format - like "D:\\SRTM\\scripps\\srtm30\\grd\\w060s60.nc"
class stopo30; // or single large file - D:\SRTM\scripps\topo30\topo30

#ifdef INCLUDE_GLOBE_DATA
// seems so BAD, maybe NOT WORTH THE EFFORT!!!
// gtopo30: for file NO DATA cnt 46,155,637, on 64,800,000 points.
class gtopo30; // original GLOBE - set_globe_dir(gtopo_dir);
#endif

******************************************************************************************************************

Building and Dependencies:
==========================

*: cmake
The project is built using cmake, so cmake MUST be installed.

*: SimGear
Has an OPTIONAL dependance on SimGearCore library and headers. The build will use embedded mongoose HTTP server
if this SimGear library and headers are NOT found. Add cmake option -DUSE_SIMGEAR_LIB:BOOL=TRUE to force the 
use of SimGear.

To help finding SimGear, setting CMAKE_PREFIX_PATH, like 
-DCMAKE_PREFIX_PATH:PATH=X:/install/msvc100/simgear can help find it. Or the SG_ROOT
can be set in the environment, like set|export SG_ROOT=/media/Disk2/FG/fg21/install/simgear,
or wherever installed. Additionally adding -DSG2_DEBUG:BOOL=ON will show what is and is not 
found.

*: libpng and zlib
Also libpng, which in turn uses zlib, has to be found to create PNG image files. It may compile even if 
libpng/zlib are NOT found, and will fall back to writing windows BITMAP image files - big and uncompressed.

******************************************************************************************************************
dem_track executable:

This uses the DemFile library to get the elevations along a 'track' entered as [bgn_lat bgn_lon end_lat end_lon]
or through reading in a track xg file. See the project https://gitorious.org/fgtools/osm2xg for more details 
on this sub-set of xgraph formatted file, and a polyView2D viewer of such xg files.

Once the 'track' is extracted the Dem3 data will be used to get a set of elevations along the track, and an 
elevation profile xg file will be generated, as well as a 'track' of the sample elevation points.

Naturally dem_track MUST have access to the necessary DEM elevation files.

******************************************************************************************************************
Testing:

If -DADD_TEST_MODULES:BOOL=ON is added during the cmake configuration step, some 30+ test modules 
will be compiled into the dem_test application. Each test is a 'bit', and a --show (or -s) will show 
all the test names, and the value to be used. Tests can be combined.

But most of these tests depend on the essential raw DEM/SRTM data being available.

In the main this is usually a test to get the elevation of a 'known' location where the expected
elevation is known. This set of tests is from this structure and table -

typedef struct tagELEVTESTS {
    const char *name;
    double lat,lon;
    short expected;
}ELEVTESTS, *PELEVTESTS;

ELEVTESTS elevtests[] = {
    // Later found 'deepest' in SRTM file is - 68.625,142.425 - TRY THAT
    // { "Marianas Trench", 68.625, 142.425, -10911 }, - NO WRONG
    { "Marianas Trench", 11.317, 142.25, -10911 },
    { "Mount Everest",  27.988, 86.9253, 8848   },
    { "Mont Blanc", 45.832704, 6.864797, 4810 }, // N45E006.hgt // usgs30: MAX 4536, at lat,lon 45.833333,6.858333, offset y,x 5300,3223
    { "Honzon Deep", -23.266667, -174.75, -10800 },
    { "Cerro Aconcagua", -32.6555555555556, -70.0158333333333, 6962 },
    { "Mt McKinley, Alaska", 63.0690, -151.0063, 6168 },
    { "Dead Sea, Jordan", 31.758333, 35.508333, -427 }, // gs30_tests: MIN -415, y,x 989,1861 USGS30 E020N40.DEM
    { "Mafadi, SA", -29.202, 29.357, 3450 },
    { "Seweweekspoortpiek", -33.398, 21.368, 2325 },
    { "Kilimanjaro, Tanzania", -3.0758333333, 37.353333333, 5895 },
    { "Mount Kosciuszko", -36.4559806, 148.2633333,  2228 },
    { "Uluru / Ayers Rock", -25.345, 131.0361, 863 },
    { "Mauna Kea,Hawaii", 19.820555556,-155.46805555556, 4205 },
    { "Tristan da Cunha", -37.09444444,-12.288611111, 2062 },
    { "Mount Lamlam, Guam", 13.33861,144.662778, 406 },
    { "Ellsworth, Antartic", -78.75, -85 , 5140 },
    { "Sentinel, Antartic", -78.167, -85.5, 5140 }, 

    // end of table
    { 0, 0, 0, 0 }
};


******************************************************************************************************************

Data:
=====

Of course to be useful the application has to be given path to the unzipped HGT/SRTM files. And below are 
the sources I used, and the storeage paths. While you certainly do NOT have to in any way follow my 
storage paths, they are given since this is the defaults hard coded into the dem_utils.cxx file.

So where ever you decide to put them, you can locate my default folder in the source, and change it 
to your default paths...

At this time it seems by FAR the 'best' DEM data source is -
 http://www.viewfinderpanoramas.org/
maintained by Jonathan de Ferranti BA, Lochmill Farm, Newburgh, Fife, KY14 6EX, United Kingdom.

He has taken the original USGS SRTM data, and removed most of the void, AND has added 
ocean bathymetric depth information.

So some suitable sources of the data are as follows -

=========
1 arc-sec
-------------------------------------------------------------------------------------------------------

1: http://www.viewfinderpanoramas.org/dem3.html 
-----------------------------------------------

From here a small selection of 1" files for Europe can be downloaded.
 http://www.viewfinderpanoramas.org/Coverage%20map%20viewfinderpanoramas_org1.htm - graphical
 http://www.viewfinderpanoramas.org/dem1/ - ftp/wget

A total of about 41 zip files, in the form n47e006.zip ... n40w004.zip, that need to be downloaded and unzipped.

2: http://dds.cr.usgs.gov/srtm/version2_1/SRTM1/
-----------------------------------------------

From here a selection of 1" files for USA can be downloaded.

This USGS SRTM1 site splits the USA into 7 regions. See - http://dds.cr.usgs.gov/srtm/version2_1/SRTM1/Region_definition.jpg

This adds some 1114 zip files, in the form N00W177.hgt.zip ... S15W171.hgt.zip, that need to be downloaded and unzipped

In my case the files from 1 and 2 are stored in
 F:\data\dem1\zips - 1,158 files - 8 GB
and when unzipped
 F:\data\dem1\srtm1 - Some 1,121 have been unzipped at this time...
This represent 1121 lat,lon pairs, on a maximum of 64800 pairs, missing 63679 (63679) 98.2 percent
See the missed.txt for an ASCII chart generated by cnthgt.pl of those found, and as can be seen it 
is JUST United States and its 'territories'.

=========
3 arc-sec
-------------------------------------------------------------------------------------------------------

1: http://www.viewfinderpanoramas.org/dem3.html 
-----------------------------------------------

From here a full selection of 3" files for most of the world can be downloaded.

 http://www.viewfinderpanoramas.org/Coverage%20map%20viewfinderpanoramas_org3.htm - graphical
 http://www.viewfinderpanoramas.org/dem3 - wget

This adds some 1131 zip files, of the form A01.zip ... U47.zip. Each zip will contain one or more [N|S]12[E|W]123/hgt. When all
are unzipped this expands to some 26,177 (or 26,151?) files.

In my case these files are stored in -
 F:\data\dem3\hgt - 26,151, files - 75 GB
This represents 26,151 lat,lon pairs, on a maximum of 64800 pairs, missing 38649 (38649) 59.6 percent
See the missed.txt for an ASCII chart generated by cnthgt.pl, of those found, and as can be seen it 
repesents most of the earths landmass.

2: http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/
-----------------------------------------------

This USGS SRTM3 site splits the world into 6 blocks - Africa/ Australia/ Eurasia/ Islands/ North_America/ 
South_America/

This 6 blocks will add some 12,863 files, of the form N00E006.hgt.zip ... S56W072.hgt.zip, each 
containing a single .hgt file.

In my case the relevant ZIP folders are -
 D:\SRTM\version2_1\SRTM3\<block>\
and the unzipped single folded is
 F:\data\dem3\usgs  - total 13,367 .hgt files - 38 GB
This represents 13,367 lat,lon pairs, on a maximum of 64800 pairs, missing 51433 (51,433) 79.3 percent
See the missed.txt for an ASCII chart generated by cnthgt.pl, of those found, and as can be seen it 
repesents most of the earths landmass, BUT take care, there are LOTS OF VOIDS. The above 
'viewfinderpanoramas' is a better, more extensive, 'cleaned' source.

==========
15 arc-sec
-------------------------------------------------------------------------------------------------------

1: http://www.viewfinderpanoramas.org/Coverage%20map%20viewfinderpanoramas_org15.htm

Not donwloaded yet, and no class to load them.

==========
30 arc-sec
-------------------------------------------------------------------------------------------------------

1: http://topex.ucsd.edu/WWW_html/srtm30_plus.html
--------------------------------------------------

This data is divided into 33 clickable regions, which will expand and shows an image of the 
data. As the site states, their contribution was to add estimated ocean depths (bathymetric) 
to the NASA/USGS SRTM land data.

The data can be downloaded by ftp/wget -
 ftp://topex.ucsd.edu/pub/srtm30_plus 

As can be seen it is presented in several forms

(a) ftp://topex.ucsd.edu/pub/srtm30_plus/srtm30/data/

Here are 33 files in the form e020n40.Bathymetry.srtm ...  w180s60.Bathymetry.srtm

In my case these 33 files are stored in -
F:\data\srtm30_plus - 33 files - 2 GB

(b) ftp://topex.ucsd.edu/pub/srtm30_plus/srtm30/grd/

In my case these 33 files in the form e020n40.nc ... w180s60.nc. These seem equivalent
to the above, except are in NetCDF format. This is essentially the above .srtm data 
with a CDF header at the top.

In my case these 33 files are stored in -
D:\SRTM\scripps\srtm30\grd - 33 files - 2 GB

(c) ftp://topex.ucsd.edu/pub/srtm30_plus/topo30/

This is a single large file topo30 

In my case this single file is stored in -
D:\SRTM\scripps\topo30 - 1 file - 2 GB

This is by far the 'best' world-wide source, but of course it is only 30-arcsecs.
That is each 0.008333333333333 degrees or approximately 1 kilometer,
resulting in a DEM having dimensions of 21,600 rows and 43,200 columns.

2: http://dds.cr.usgs.gov/srtm/version2_1/SRTM30/
-------------------------------------------------

The site has the data arranged in 27 subdirectories, e020n40 ... w180s10

Each subdirectory has a set of files like -
Index of /srtm/version2_1/SRTM30/e020n90
e020n90.dem.zip e020n90.dif.zip e020n90.dmw.zip e020n90.gif.zip e020n90.hdr.zip e020n90.jpg.zip
e020n90.num.zip e020n90.prj.zip e020n90.sch.zip e020n90.src.zip e020n90.std.zip e020n90.stx.zip

Since all this data is 'better' represented in the above srtm30_plus, which ALSO includes bathymetric
ocean dpeths, there seems little need to download these, but I did just for curiosity.

In my case stored in
F:\data\SRTM30\zips - 324 files - 650 MB
Unzipped to
F:\data\SRTM30\data - 324 files - 6.2 GB

Note these files have a 'void', no data, using -9999, and use the top,left as the base location.

The set of 27 GIF files do form a nice greyscale mosaic of the world, and the JPG in color.

They may be useful as some form of comparison...

But at present no class has been derived to load, and view them.

3: http://www.ngdc.noaa.gov/mgg/topo/gltiles.html
-------------------------------------------------

The site has the data arranged in 16 files, in the form a10g ... p10g

In my case stored in
F:\data\dem3\SRTM30\all10g.zip - 1 file - 320 MB
Unzipped
F:\data\dem3\SRTM30\all10 - 17 files - 1.9 GB

******************************************************************************************************************

# eof
